<?php

// NewScheduleControler 
// - omogoča osnovne akcije za kreiranje in urejanje urnika
class VadbaController extends MainController {
	
	private $vadba = null;
	
	function __construct() {
		$this->vadba = new Vadba;
	}
	
	function create($ime, $kratkoIme, $opis, $link, $kalorij, $logo) {
		return $this->vadba->create($ime, $kratkoIme, $opis, $link, $kalorij, $logo);
	}
	
	function update($id, $ime, $kratkoIme, $opis, $link, $kalorij, $logo) {
		return $this->vadba->updateOne($id, $ime, $kratkoIme, $opis, $link, $kalorij, $logo);
	}
	
	function removeClassLogo($id) {
		return $this->vadba->removeClassLogo($id);
	}
	
	function remove($id) {
		return $this->vadba->remove($id);
	}
	
	function getList($p=null) {
		$filter = null;
		if (isset($p["filter"])) {
			$filter = $p["filter"];
		}
		return $this->vadba->getList($filter);	
	}
			
	function newEntryAction($params) {
		
		$entry = $this->vadba->createNew(
			$params["naziv"], 
			/*date("Y-m-d", strtotime(*/$params["veljaOD"]/*))*/, 
			/*date("Y-m-d", strtotime(*/$params["veljaDO"]/*))*/, 
			$params["veljaven"]
		);
		
		return $entry;	
	}	
	function updateEntryAction($changes) {
		$params["bulk"] = false;
		$params["changes"] = $changes;
		try {
			$this->vadba->update($params);
		} catch (Exception $ex) {
			return $ex->getTraceAsString();
		} 
		return "ok";
	}
	function updateBulkAction($changes) {
		$params["bulk"] = true;
		$params["data"] = $changes["data"];
		try {
			$this->vadba->update($params);
		} catch (Exception $ex) {
			return $ex->getTraceAsString();
		} 
		return "ok";
	}
	
		
}