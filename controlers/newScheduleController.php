<?php

// NewScheduleControler 
// - omogoča osnovne akcije za kreiranje in urejanje urnika
class NewScheduleController extends MainController {
	
	private $urnik = null;
	private $ure = null;
	
	function __construct() {
		$this->urnik = new Urnik;
		$this->ure   = new Ura;
	}
	
	function findByID($id) {
		// naložimo podatke o urniku
		//if ($this->urnik==null)
		$res = array();
		$res["urnik"] 	= $this->urnik->findByID($id);
		$res["ure"]		= $this->ure->getListByUrnikID($id);
		// naložimo vse ure in podatke o urah
		return $res;
	}
	
	function cloneSchedule($id) {
		return $this->urnik->cloneSchedule($id);
	}
	
	function newEntryAction($params) {
		
		$entry = $this->urnik->createNew(
			$params["naziv"], 
			/*date("Y-m-d", strtotime(*/$params["veljaOD"]/*))*/, 
			/*date("Y-m-d", strtotime(*/$params["veljaDO"]/*))*/, 
			$params["veljaven"],
			$params["prostor_id"]
		);
		
		return $entry;	
	}	
	function updateEntryAction($changes) {
		$params["bulk"] = false;
		$params["changes"] = $changes;
		try {
			$this->urnik->update($params);
		} catch (Exception $ex) {
			return $ex->getTraceAsString();
		} 
		return "ok";
	}
	function updateBulkAction($changes) {
		$params["bulk"] = true;
		$params["data"] = $changes["data"];
		try {
			$this->urnik->update($params);
		} catch (Exception $ex) {
			return $ex->getTraceAsString();
		} 
		return "ok";
	}
	
	function updateDetails($p) {
		// del za spremembe meta podatkov
		
		// del za spremembe ur
		$this->ure->updateRecords($p["ure"], $p["lastnosti"]["id"]);
		
		$objekt["data"]["podatki"] = array();
		if (isset($p["lastnosti"]["spremembe"])) {
			$spr = $p["lastnosti"]["spremembe"];
			
			if (isset($spr["naziv"]) && $spr["naziv"]!='null') {
				$objekt["data"]["podatki"]["naziv"] = $spr["naziv"];
			}
			if (isset($spr["veljaOD"]) && $spr["veljaOD"]!='null') {
				$objekt["data"]["podatki"]["veljaOD"] = $spr["veljaOD"];
			}
			if (isset($spr["veljaDO"]) && $spr["veljaDO"]!='null') {
				$objekt["data"]["podatki"]["veljaDO"] = $spr["veljaDO"];
				//throw new Exception("spremenjene");
			}
			if (isset($spr["veljaven"]) && $spr["veljaven"]!='null') {
				$objekt["data"]["podatki"]["veljaven"] = $spr["veljaven"];
			}
			$objekt["data"]["pogoj"] = array("id" => $p["lastnosti"]["id"]);
			return $this->urnik->update($objekt, true);
			
		}
	}
	
		
}