<?php

class SifrantController extends MainController {

	private $urnik = null;
	
	function __construct() {
		parent::__construct();
	}
	
	function reportBug($info, $priority, $text) {
		$this->obvestilo = new Obvestilo;
		return $this->obvestilo->reportBug($info, $priority, $text);
	}

	function alterBugReport($id, $status, $response) {
		if (!isset($_SESSION["userid"])) throw new Exception("unauthorized");
		$this->obvestilo = new Obvestilo;
		return $this->obvestilo->alterBugReport($id, $status, $response);
	}
	
	function getTranslation($q, $lang="sl") {
		$db = new DbConfig;
		$pList = array("tag"=>$q, "q"=>$q, "lang"=>$lang);
		
		// poglej ali je v memcachu kaj
		try {
			if (PageSettings::use_memcached)
				$get_result = $this->memcache->get("trans_" . $q);
			else 
				$get_result = false;
			
			//print $q;
			
			if (!$get_result) {
				$get_result = $db->executeSelect($this->insertParams($pList, Queries::getTranslation));
				if (PageSettings::use_memcached)
				 	$this->memcache->set("trans_" . $q, $get_result, true, 5);
			}
		} catch (Exception $ex) {
			$get_result = $db->executeSelect($this->insertParams($pList, Queries::getTranslation));
		}
		
		return $get_result;//$get_result;
	}

	function saveTranslation ($tag, $translation, $lang) {
		global $mainController;
		
		if (!isset($_SESSION["userid"])) throw new Exception("unauthorized");
		
		$translation = str_replace("'", "\'", $translation);
		
		$db = new DbConfig;
		try {
			$db->updateNoCommit("prevodi", array("translation"=>$translation), array("tag"=>$tag, "lang"=>$lang));
			
			if (PageSettings::use_memcached) {
				$mainController->memcache->set("tag_" . $tag, $translation, false, 60);
			}
			
			$db->commitTransaction();
			return "ok";
		} catch (Exception $ex) {
			$db->rollbackTransaction();
			throw $ex;
		}
	}
	
	function update($p) {
		if (!isset($_SESSION["userid"])) throw new Exception("unauthorized");
		$sifObj = $p["sifObjekt"];
		switch ($sifObj) {
			case "instruktor":
				$instruktor = new Instruktor;
				if (isset($p["novi"]) && count($p["novi"])>0) {
					$instruktor->createBulk($p["novi"]);
				}
				if (isset($p["spremembe"]) && count($p["spremembe"])>0) {
					$updParams["bulk"] = true;
					$updParams["data"] = $p["spremembe"];
					$instruktor->update($updParams);
				}
				if (isset($p["brisanje"]) && count($p["brisanje"])>0) {
					$instruktor->removeAll($p["brisanje"]);
				}
				break;
			case "vadba":
				$vadba = new Vadba;
				if (isset($p["novi"]) && count($p["novi"])>0) {
					$vadba->createBulk($p["novi"]);
				}
				if (isset($p["spremembe"]) && count($p["spremembe"])>0) {
					$updParams["bulk"] = true;
					$updParams["data"] = $p["spremembe"];
					$vadba->update($updParams);
				}
				if (isset($p["brisanje"]) && count($p["brisanje"])>0) {
					$vadba->removeAll($p["brisanje"]);
				}
				break;
		}

		// ažuriranje spremenjenih vrstic
		// ažuriranje novih vrstic
	}

}