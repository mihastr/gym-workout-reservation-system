<?php

// NewScheduleControler 
// - omogoča osnovne akcije za kreiranje in urejanje urnika
class InstrukorController extends MainController {
	
	private $instruktor = null;
	
	function __construct() {
		$this->instruktor = new instruktor;
	}
	
	function create($dolgoIme, $kratkoIme, $opis) {
		return $this->instruktor->create($dolgoIme, $kratkoIme, $opis);
	}
	
	function update($id, $dolgoIme, $kratkoIme, $opis) {
		return $this->instruktor->updateOne($id, $dolgoIme, $kratkoIme, $opis);
	}
	
	function remove($id) {
		return $this->instruktor->remove($id);
	}
	
	function getList($params=null) {
		return $this->instruktor->getList($params);	
	}
	
	function getRemoved() {
		$p = "veljaDO < now()";
		return $this->instruktor->findByFilter($p);
	}
			
	function newEntryAction($params) {
		
		$entry = $this->instruktor->createNew(
			$params["naziv"], 
			/*date("Y-m-d", strtotime(*/$params["veljaOD"]/*))*/, 
			/*date("Y-m-d", strtotime(*/$params["veljaDO"]/*))*/, 
			$params["veljaven"]
		);
		
		return $entry;	
	}	
	function updateEntryAction($changes) {
		$params["bulk"] = false;
		$params["changes"] = $changes;
		try {
			$this->instruktor->update($params);
		} catch (Exception $ex) {
			return $ex->getTraceAsString();
		} 
		return "ok";
	}
	function updateBulkAction($changes) {
		$params["bulk"] = true;
		$params["data"] = $changes["data"];
		try {
			$this->instruktor->update($params);
		} catch (Exception $ex) {
			return $ex->getTraceAsString();
		} 
		return "ok";
	}
	
		
}