<?php
//include_once '../config/dbCongig.php';

class MainController {
		
	//const memcacheServer = "localhost";
	
	public $memcache = null;

	function __construct() {
		if (PageSettings::use_memcached) {
			$this->memcache = new Memcache;
			$this->memcache->connect(PageSettings::memcached_url, PageSettings::memcached_port) or die ("Could not connect");
		}
	}

	
	function insertParams($list, $query) {
		foreach ($list as $f=>$v) {
			$query = str_replace("{" . $f . "}", $v, $query);
		}
		return $query;
	}

}
// za upravljanje z urniki
include_once 'newScheduleController.php';

// za upravljanje z inštruktorji
include_once 'instrukorController.php';	
include_once 'vadbaController.php';
include_once 'fcController.php';
include_once 'prostorController.php';
include_once 'obvestiloController.php';

include_once 'urnik7DController.php';
include_once 'siftantController.php';

include_once 'uporabnikController.php';

include_once 'vremeController.php';

