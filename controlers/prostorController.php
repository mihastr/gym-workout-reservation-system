<?php

class ProstorController extends MainController {
	
	private $prostor = null;
	
	function __construct() {
		$this->prostor = new Prostor;
	}
	
	function getListForFC($params=null) {
		return $this->prostor->getListForFK($params);	
	}
	
	function getList() {
		return $this->prostor->getListVSI();
	}
			
	function newEntryAction($params) {
		/*
		$entry = $this->prostor->createNew(
			$params["naziv"], 
			$params["veljaOD"], 
			$params["veljaDO"], 
			$params["veljaven"]
		);
		
		return $entry;
		*/	
	}	
	function updateEntryAction($changes) {
		/*
		$params["bulk"] = false;
		$params["changes"] = $changes;
		try {
			$this->prostor->update($params);
		} catch (Exception $ex) {
			return $ex->getTraceAsString();
		} 
		return "ok";
		*/
	}
	function updateBulkAction($changes) {
		/*
		$params["bulk"] = true;
		$params["data"] = $changes["data"];
		try {
			$this->prostor->update($params);
		} catch (Exception $ex) {
			return $ex->getTraceAsString();
		} 
		return "ok";
		*/
	}
	
	function getListForUser($uid=null) {
		if ($uid==null)
			$userID = $_SESSION["userid"];
		
		return $this->prostor->getListForUser($uid);
	}
	
	
	function getListWithClassesCapacity($vadba) {
		return $this->prostor->getListWithClassesCapacity($vadba);
	}
				/*
			 * spremembe = {
						prostori: kapacitete,
						vadbe: {
							id: vid,
							spremembe: omejitve
						}
					};
			 * */
	function saveListWithClassesCapacity($prostori, $vadbe) {
		$db = new DbConfig;
		$vadba = new Vadba;
		try {
			$db->beginTransaction();
			$this->prostor->updateCapacityData($prostori, $db);
			$vadba->updateUraOmejitve($vadbe["id"], $vadbe["spremembe"], $db);
			$db->commitTransaction();
		} catch (Exception $ex) {
			$db->rollbackTransaction();
			throw $ex;
		}
	}
	function removePlace($id) {
		return $this->prostor->remove($id);
	}
		
}