<?php

class ObvestiloController extends MainController {
	
	private $obvestilo = null;
	
	function __construct() {
		$this->obvestilo = new Obvestilo;
	}

	function getList() {
		return $this->obvestilo->getListForUser();
	}
	
	function getBugList($showResolved=false) {
		if (!isset($_SESSION["userid"])) throw new Exception("unauthorized");
		return $this->obvestilo->listBugs($showResolved);
	}
	
	function deleteMSG($id) {
		if (checkPermission("canDeleteMSG")) {
			$this->obvestilo->delete($id);
		} else {
			throw new Exception("unauthorized");
		}
	}
	
	function getList4Admin($drafts=0, $deleted=0, $q=null) {
		return $this->obvestilo->getMsgs4Admin($drafts, $deleted, $q);
	}
		
	function getMsgRecepients($id) {
		return $this->obvestilo->getMsgRecepients($id);
	}
	
	function changeVisibility($id, $visible) {
		return $this->obvestilo->changeVisibility($id, $visible);
	}
}