<?php

class FitnesCenterController extends MainController {
	
	private $fitnesCenter = null;
	
	function __construct() {
		$this->fitnesCenter = new FitnesCenter;
	}
	
	function create($naziv, $kratkoIme, $naslov) {
		return $this->fitnesCenter->create($naziv, $kratkoIme, $naslov);
	}
	
	function update($id, $naziv, $kratkoIme, $naslov) {
		$this->fitnesCenter->update($id, $naziv, $kratkoIme, $naslov);
	}
	
	function getList($params=null) {
		return $this->fitnesCenter->getList();	
	}
			
	function newEntryAction($params) {
		/*
		$entry = $this->fitnesCenter->createNew(
			$params["naziv"], 
			$params["veljaOD"], 
			$params["veljaDO"], 
			$params["veljaven"]
		);
		
		return $entry;
		*/	
	}	
	function updateEntryAction($changes) {
		/*
		$params["bulk"] = false;
		$params["changes"] = $changes;
		try {
			$this->fitnesCenter->update($params);
		} catch (Exception $ex) {
			return $ex->getTraceAsString();
		} 
		return "ok";
		*/
	}
	
	function changePlaceCapacity($id, $capacity, $name, $fc) {
		$db = new DbConfig;
		return $db->updateOrInsertNoCommit("prostor", array("kapaciteta"=>$capacity, "naziv"=>$name, "fitnescenter_id"=>$fc), array("id"=>$id));
	}
	
	function listPlacesForUser($uid=null) {
		if ($uid==null) $uid = $_SESSION["userid"];
		
		return $this->fitnesCenter->getListForUser($uid);
	}
	
	function remove($id) {
		return $this->fitnesCenter->remove($id);
	}
	
	function updateBulkAction($changes) {
		/*
		$params["bulk"] = true;
		$params["data"] = $changes["data"];
		try {
			$this->fitnesCenter->update($params);
		} catch (Exception $ex) {
			return $ex->getTraceAsString();
		} 
		return "ok";
		*/
	}
	
		
}