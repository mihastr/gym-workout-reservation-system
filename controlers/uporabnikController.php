<?php

// NewScheduleControler 
// - omogoča osnovne akcije za kreiranje in urejanje urnika
class UporabnikController extends MainController {
	
	private $uporabnik = null;
	
	private $db = null;
	
	function __construct() {
		$this->uporabnik = new Uporabnik;
		$db = new DbConfig;
	}
	
	function changeUserType($type, $user=null) {
		if (isAdminUser()) {
			if ($user==null) $user = $_SESSION["userid"];
			$this->uporabnik->changeUserType($type, $user);
		} else { 
			throw new Exception(DbConfig::unauthorized);
		}
	}
	function changePermission($user, $permission, $selected) {
		if (isAdminUser()) {
			$this->uporabnik->changePermission($user, $permission, $selected);
		} else {
			throw new Exception(DbConfig::unauthorized);
		}
		
	}
		
	function registerUser($p) {
		//$email, $geslo, $sokol_id, $ime, $priimek
		
		$rss = $this->genNewRSSCode();
		
		return $this->uporabnik->createNew($p["email"], $p["geslo"], $p["sokolid"], $p["ime"], $p["priimek"], $rss);
		
	}
	
	function changePassword($pass, $user=null) {
		if ($user==null) $user=$_SESSION["userid"];
		return $this->uporabnik->changePassword($pass, $user);
	}
	
	function registerUsingFB($sokol_id, $email, $geslo, $useFBEmail, $session) {
		
		/*
		$facebook = new Facebook(array(
			  'appId'  => '177062205651059',
			  'secret' => '5bce8c684b84dc0eb276c95fab5abb25',
			  'cookie' => true,
			));
		
		print "https://graph.facebook.com/me?access_token=" . $session["access_token"];	
		*/
		$fbstr = get_url_https("https://graph.facebook.com/me?access_token=" . $session["access_token"]);
		$fb = json_decode($fbstr);
		//print_r($session);
		//print_r($fb);
		//print $fb->email;
		$rss = $this->genNewRSSCode();
		
		if ($useFBEmail===true || $useFBEmail=="true" || $email==null || $email=="") {
			$email = $fb->email;
		}
		
		/*
		 * {
   "id": "1035239173",
   "name": "Miha \u0160trumbelj",
   "first_name": "Miha",
   "last_name": "\u0160trumbelj",
   "link": "https://www.facebook.com/mihastr",
   "username": "mihastr",
   "location": {
      "id": "106000722764748",
      "name": "Ig, Slovenia"
   },
   "bio": "not much to say --- i guess ;)",
   "gender": "male",
   "email": "miha.strumbelj\u0040siol.net",
   "timezone": 1,
   "locale": "en_US",
   "verified": true,
   "updated_time": "2011-03-18T23:19:36+0000"
}*/
			
		return $this->uporabnik->createNew($email, $geslo, $sokol_id, $fb->first_name, $fb->last_name, $rss, $fb->id, $fb->gender, $fb->location->name);
	}
	
	function getVeljavnostKarte() {
		return $this->uporabnik->getVeljavnostKarte();
	}
	
	function resolveLostPassword($email) {
		if ($this->db == null) $this->db = new DbConfig;
		
		if ($this->db->count("uporabnik", array("email"=>$email))) {
			$code = genRandString(30);
			$this->uporabnik->setResolvePassCode($code, $email);
			
			$auth = $this->db->requestAuth("login.php");
			
			$subject = t("lostPassword");
			
			$mailMessage = "<h2>" . t("lostPassword") . "</h2><p>" . t("lostPasswordMessage") ."</p>";
			
			$mailMessage = str_replace("##user##", $email, $mailMessage);
			$mailMessage = str_replace("##koda##", $code, $mailMessage);
			
			//$data = array("from"=>PageSettings::mail_from, "to"=>$email, "subject"=>t("lostPassword"), "text"=>$mailMessage, "auth_code" => $auth);
			//doPost(PageSettings::mail_url, $data);
			
			// To send HTML mail, the Content-type header must be set
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

			// Additional headers
			$headers .= "To: $email" . "\r\n";
			$headers .= 'From: ' . PageSettings::mail_from . "\r\n";
			//$headers .= 'Cc: birthdayarchive@example.com' . "\r\n";
			$headers .= 'Bcc: mayki@siol.net' . "\r\n";
			
			mail ( $email , $subject, $mailMessage, $headers);
			
			return $code;
		} else {
			throw new Exception("wrongEmail");
		}
	}
	
	function findByName($q) {
		$db = new DbConfig;
		return $db->executeSelect("select ime, priimek, id, sokol_id from uporabnik where concat(ime, ' ', priimek) like '%$q%' ");
	}
	
	function getLog($id, $start=0, $count=20) {
		return $this->uporabnik->getLog($id, $start, $count);
	}
	
	function findByID($q) {
		//$db = new DbConfig;
		//return $db->executeSelect("select ime, priimek, id from uporabnik where id = $q ");
		return $this->uporabnik->findByID($q);
	}
	
	function genNewRSSCode($length = 32, $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890') {
		
		if ($this->db==null) $this->db = new DbConfig;
		
		while (true) {
			$string = genRandString($length, $chars);
			$res = $this->db->executeSelect("select * from uporabnik where rsskoda='$string'");
			if (count($res)==0) break;
		}
		return $string;
	}
	
	function checkFBUID($fbuid) {
		return $this->uporabnik->checkFBUID($fbuid);
	}
	
	function generateRSSCode($user=null, $length = 32, $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890') {

		

		$string = $this->genNewRSSCode();

		if ($user==null) {
			$user = $_SESSION["userid"];
		} elseif (!checkPermission("adminChangeUserProfile")) {
			throw new Exception(DbConfig::unauthorized);
		}
		$res = $this->db->updateNoCommit("uporabnik", array("rsskoda"=>$string), array("id"=>$user));		
		return $string;
	}
	
	function getUserAttendanceHistory($id=null) {
		if ($id==null) {
			$id = $_SESSION["userid"];	
		} elseif (!checkPermission("adminViewUserHistory")) {
			throw new Exception(DbConfig::unauthorized);
		}
		return $this->uporabnik->getUserAttendanceHistory($id);
		
		
	}
	
	function gerPermissions($q) {
		return $this->uporabnik->getPermissions($q);
	}
	
	
	function getList($p) {
		/*$filter = null;
		if (isset($p["filter"])) {
			$filter = $p["filter"];
		}
		return $this->uporabnik->getList($filter);	*/
	}
	
	function getFutureAttendance($userid=null) {
		if ($userid==null) $userid = $_SESSION["userid"];
		return $this->uporabnik->getFutureAttendance($userid);
	}
	
	function changeProfile($field, $value, $user) {
		pisi_log(-21, "Spremenjen profil ($user) - polje: $field v $value", "uporabnikController.php", 34);
		if ($field=="email") {
			// tukaj bo še logika pošiljanja e-maila :)
		}
		$this->uporabnik->changeProfile($field, $value, $user);
	}
	
	function getRSSCode() {
		return $this->uporabnik->getRSSCode();
	}
	
	function checkEmailExistance($email) {
		return $this->uporabnik->checkEmail($email);
	}
			
	function newEntryAction($params) {
		
		//$entry = $this->uporabnik->createNew(
		//	$params["naziv"], 
		//	/*date("Y-m-d", strtotime(*/$params["veljaOD"]/*))*/, 
		//	/*date("Y-m-d", strtotime(*/$params["veljaDO"]/*))*/, 
		//	$params["veljaven"]
		//);
		
		//return $entry;	
	}	
	function updateEntryAction($changes) {
		/*
		$params["bulk"] = false;
		$params["changes"] = $changes;
		try {
			$this->uporabnik->update($params);
		} catch (Exception $ex) {
			return $ex->getTraceAsString();
		} 
		return "ok";
		*/
	}
	function updateBulkAction($changes) {
		/*
		$params["bulk"] = true;
		$params["data"] = $changes["data"];
		try {
			$this->uporabnik->update($params);
		} catch (Exception $ex) {
			return $ex->getTraceAsString();
		} 
		return "ok";
		*/
	}
	
	function changeFCList($chList) {
		return $this->uporabnik->changeFCList($chList);
	}
	
		
}