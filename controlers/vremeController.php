<?php
//date_default_timezone_set('Europe/Ljubljana');
//print ini_get('date.timezone');

class VremeController extends MainController {
	private $vreme = null; 
	private $napoved = null;
	function __construct() {
		$this->vreme = new Vreme;
		$this->napoved = new VremeNapoved;
	}
	function getTrenutnoVreme() {
		//print date("r", time()) . " ";
		//print date("Y-m-d", time()) . " " . date("H:00", time());
		//$res = $this->vreme->getVreme(date("Y-m-d", time()), date("H:00", time()));
		$res = $this->vreme->getVremeTrenutno();
		return (count($res)>0 ? $res[0] : null);
	}
	function getNapovedi3Dni() {
		return $this->napoved->getNapovedi(date("Y-m-d", time()), VremeNapoved::napoved3dni);
	}
	
	function getIcon($str) {
		//print "this is me";
		$time = date("H", time());
		
		$str = trim($str);
		
		switch ($str) {
	
			case "sončno": 
				echo "<img align='top' src='images/weather/sunny.gif' />";
				break;
			case "soncno": 
				echo "<img align='top' src='images/weather/sunny.gif' />";
				break;
				
			case "oblačno": 
				echo "<img align='top' src='images/weather/cloudy.gif' />";
				break;
			case "oblacno": 
				echo "<img align='top' src='images/weather/cloudy.gif' />";
				break;
				
				
			case "pretežno jasno":
				echo "<img align='top' src='images/weather/mostly_sunny.gif' />"; 
				break;
			case "pretezno jasno":
				echo "<img align='top' src='images/weather/mostly_sunny.gif' />"; 
				break;

				
			case "delno oblačno":
				echo "<img align='top' src='images/weather/mostly_sunny.gif' />"; 
				break;
			case "delno oblacno":
				echo "<img align='top' src='images/weather/mostly_sunny.gif' />"; 
				break;
				
				
			case "pretežno oblačno":
				echo "<img align='top' src='images/weather/mostly_sunny.gif' />"; 
				break;
			case "pretezno oblacno":
				echo "<img align='top' src='images/weather/mostly_sunny.gif' />"; 
				break;
				

			case "jasno": 
				if ($time>18)
					echo "<img align='top' src='images/weather/clear_moon.gif' />";
				else 
					echo "<img align='top' src='images/weather/sunny.gif' />";
				break;
			case "pretežno oblačno": 
				echo "<img align='top' src='images/weather/mostly_cloudy.gif' />";
				break;
			case "pretezno oblazno": 
				echo "<img align='top' src='images/weather/mostly_cloudy.gif' />";
				break;
				
				
			case "zmerno oblačno": 
				echo "<img align='top' src='images/weather/fairly_floudy.gif' />";
				break;
			case "zmerno oblacno": 
				echo "<img align='top' src='images/weather/fairly_floudy.gif' />";
				break;
				
			case "delno oblačno": 
				echo "<img align='top' src='images/weather/partly_cloudy.gif' />";
				break;
			case "delno oblacno": 
				echo "<img align='top' src='images/weather/partly_cloudy.gif' />";
				break;
				
			case "ploha": 
				echo "<img align='top' src='images/weather/lite_rain.gif' />";
				break;				
			case "dežuje": 
				echo "<img align='top' src='images/weather/rain.gif' />";
				break;
			case "dezuje": 
				echo "<img align='top' src='images/weather/rain.gif' />";
				break;
								
			default:
				echo "<img align='top' src='images/weather/sunny.gif' />";
				echo " ni slike {" . $str . "}";
				/*
				$mail = new MailSystem;
				$mail->from = "mayki@siol.net";
				$mail->to = "mayki@siol.net";
				$mail->message = "nimamo ikone";
				$mail->send();
				*/
				break;		
		}
	}

}