﻿<?php
ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_NOTICE);
define('FACEBOOK_APP_ID', '177062205651059');
define('FACEBOOK_SECRET', '5bce8c684b84dc0eb276c95fab5abb25');

function get_fb_url($url) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.2; en-GB; rv:1.8.1.7) Gecko/20070914 Firefox/2.0.0.7');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	$user_data = curl_exec($ch);
	curl_close($ch);
	return $user_data;
}

function get_facebook_cookie($app_id, $application_secret) {
  $args = array();
  parse_str(trim($_COOKIE['fbs_' . $app_id], '\\"'), $args);
  ksort($args);
  $payload = '';
  foreach ($args as $key => $value) {
    if ($key != 'sig') {
      $payload .= $key . '=' . $value;
    }
  }
  if (md5($payload . $application_secret) != $args['sig']) {
    return null;
  }
  return $args;
}

$cookie = get_facebook_cookie(FACEBOOK_APP_ID, FACEBOOK_SECRET);

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:fb="http://www.facebook.com/2008/fbml">
  <body>
    <?php if ($cookie) { ?>
	  <img src='http://graph.facebook.com/<?php print $cookie['uid']; ?>/picture' align='left' />
      Your user ID is <?= $cookie['uid'] ?>
	  <?php
	  print "<pre>";
	  //$user_data = file_get_contents("https://graph.facebook.com/me?access_token=" . $cookie['access_token']);
	  
	  $url = "https://graph.facebook.com/me?access_token=" . $cookie['access_token'];
	  
	  //print $cookie['access_token'];
	  
	  $user_text = get_fb_url($url);
	   
      $user_data = json_decode($user_text);
	  
	  $fp = fopen("user_data.txt", "a");
	  
	  fwrite($fp, $user_text . "\n\n\n");
	  

	
	  print "\n id :      " . $user_data->id;
	  print "\n ime:      " . $user_data->first_name;
	  print "\n priimek:  " . $user_data->last_name;
	  print "\n spol:     " . $user_data->gender;
	  print "\n email:    " . $user_data->email;
	  print "\n rojstni d:" . $user_data->birthday;
	  
	  print "\n\n";
	  $url = 'https://graph.facebook.com/me/friends?access_token=' . $cookie['access_token'];
	  
	  $uft = get_fb_url($url);
	  
	  $user_friends = json_decode($uft);

	  fwrite($fp, $uft . "\n\n\n");	  
	  
	  fclose($fp);
	   /*
	  $ch = curl_init("http://www.example.com/");
  	  $fp = fopen("example_homepage.txt", "w");

	  curl_setopt($ch, CURLOPT_FILE, $fp);
	  curl_setopt($ch, CURLOPT_HEADER, 0);
	
	  curl_exec($ch);
	  curl_close($ch);
	  fclose($fp);
		*/
	   
	  //print_r($user_data);
	  //$user = json_decode();
	  //print_r($cookie);

	  print_r($user_friends);
	  
	  print "</pre>";
	  
	  ?>
    <?php } else { ?>
      <fb:login-button perms="email,user_birthday"></fb:login-button>
    <?php } ?>

    <div id="fb-root"></div>
    <script src="http://connect.facebook.net/en_US/all.js"></script>
    <script>
      FB.init({appId: '<?= FACEBOOK_APP_ID ?>', status: true,
               cookie: true, xfbml: true});
      FB.Event.subscribe('auth.login', function(response) {
        window.location.reload();
      });
    </script>
  </body>
</html>
