<?php
include_once("../config/mod.php");
include_once $proDir . '/config/html_templates/header.php';
try {
	$dbConf = new DbTestControler;
	print '<table>';
	foreach ($dbConf->retArray as $row) {
		print '<tr>';
		foreach ($row as $field) {
			print "<td>$field</td>";
		}
		print '</tr>';
	}
	print '</table>';
} catch (Exception $ex) {
	print $ex->getMessage();
}
include_once $proDir . '/config/html_templates/footer.php';