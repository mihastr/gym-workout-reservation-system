setInterval ( "osveziUrnik()", 5000 );

function osveziUrnik ( )
{
	//alert("lala");
	var urnik = $("#urnikStranke");
	if (waitIndicator != undefined) waitIndicator.show();  
	populateUrnik(urnik, waitIndicator);
}

function updateKalorije(noveKalorije) {
	var kalorij = parseInt(nvl($("#stevecKalorij").text(), 0));
	kalorij += noveKalorije;
	$("#stevecKalorij").text(kalorij);
}

function addKalorijeDan(dan, kalorij) {
	var x = parseInt(nvl($("#urnikStranke thead tr.calRow th[dan='" + dan + "']").text(), 0));
	x += kalorij;
	$("#urnikStranke thead tr.calRow th[dan='" + dan + "']").text(x);
}

function populateUrnik(urnikDiv, waitIndicator) {
	// sproži zahtevek po urniku na server
	// populiraj element urnikDiv
	
	//alert(prostor);
	//clearSchedule();
	
	
	jQuery.post( "api.php?params=action=get7DUrnik", {"prostor": prostor}, function(data, textStatus, XMLHttpRequest) {
		//alert("shranjeno");
		//alert("shranjeno " + textStatus);
		//alert("shranjeno " + JSON.stringify(data));
		if (textStatus=="success")
			if (data.error=="no") {
				$("#urnikStranke thead tr.calRow th").text("0");
				//var kalorij = parseInt($("#stevecKalorij").text());
				var kalorij = 0;
				
				var tmpKalorij = null;
				
				
				
				$(data.message.body.text).each(function(i, val) {
					//alert(JSON.stringify(val));
					var polje = $(urnikDiv).find("tr[ura='" + val.ura + "'] td[dan='" + val.dan + "']");
					
					polje.removeClass();
					polje.addClass("ura");
										
					polje.attr("objid", val.id);
					
					polje.find("div.forPrijava").hide();
					polje.find("div.forOdjava").hide();
					
					if (val.odpovedana=="1" && polje.attr("datum")==val.datum_odpovedi) {
						polje.addClass("odpovedana");
					} else					
					if (val.status=="prijavljen") {
						if (polje.attr("datum")==val.datum) {
							//alert("kalorij: " + val.kalorij);
							
							tmpKalorij = parseInt(nvl(val.kalorij, 0));
							addKalorijeDan(val.dan, tmpKalorij);
							kalorij += tmpKalorij;
							
							polje.addClass("prijavljen");	
							polje.find("div.forOdjava").show();
						} else {
							if (val.mozna_prijava=="0") {
								polje.addClass("zaseden");
							} else if (val.odpovedana=="1") {
								polje.addClass("odpovedana");
							} else {
								polje.find("div.forPrijava").show();
							}	
						}	
					} else {
						if (val.mozna_prijava=="0") {
							polje.addClass("zaseden");
						} else if (val.odpovedana=="1") {
							polje.addClass("odpovedana");
						} else {
							polje.find("div.forPrijava").show();
							polje.addClass("prijavaMozna");
						}
					}
					
					var vadba = polje.find("div.forVadba");
					vadba.attr("kalorij", val.kalorij);
					vadba.attr("objid", val.vid);
					vadba.find("a").attr("title", val.opis);
					
					if (val.logo==null || val.logo=="" || val.logo=="null")
						vadba.find("a").text(val.naziv);
					else
						vadba.find("a").html("<img src='upload/image_" + val.logo + ".jpg' />" + val.naziv);
					
					var instruktor = polje.find("div.forInstruktor"); 
					instruktor.attr("objid", val.iid);
					instruktor.find("a").attr("title", val.oInstruktorju);
					instruktor.find("a").text(val.trener);

				});
				$("#stevecKalorij").text(kalorij);
				//alert(kalorij);
				

			}
			waitIndicator.hide();
			pocistiPrazne();
			onemogociDanesDoUre();
	}, "json" ); 

}

function onemogociDanesDoUre() {
	var danes = new Date();
	var cas = danes.format("H:i");
	var danVTednu = danes.format("w");

	$("#urnikStranke tbody tr td[dan='" + danVTednu + "']").each(function(i, obj) {
		var x = $(obj);
		if (x.parent().attr("ura") <= cas) {
			x.find("div.forPrijava").hide();
			x.find("div.forOdjava").hide();
		};
	});
	
}

function pocistiPrazne() {
	var x = 0;
	$("#urnikStranke tbody tr").each(function(i, obj) {
		var tr = $(this);
		if (tr.find("td[objid]").size()==0) {
			if (x>0) {
				tr.hide();
			}
			x++;
		} else {
			x=0;
		}
	});
}


function odjavi(ura_id, datum) {

		
		$("#urnikStranke tbody td[objid='" + ura_id + "'] div.forOdjava").hide();
		$("#urnikStranke tbody td[objid='" + ura_id + "'] div.forLoading").show();
		
		
		waitIndicator.show();
		jQuery.post( "api.php?params=action=removeAttendance", {"ura_id": ura_id, "datum": datum}, function(data, textStatus, XMLHttpRequest) {
			//alert("shranjeno");
			//alert("shranjeno " + textStatus);
			//alert("shranjeno " + JSON.stringify(data));
			if (textStatus=="success")
				if (data.error=="no") {
					var td = $("#urnikStranke tbody td[objid='" + ura_id + "']");
					td.removeClass("prijavljen");
					td.addClass("prijavaMozna");
					td.find("div.forOdjava").hide();
					td.find("div.forPrijava").show();
					
					var novihKalorij = parseInt(nvl(td.find("div.forVadba").attr("kalorij"), 0));
					updateKalorije(-novihKalorij);
					addKalorijeDan(td.attr("dan"), -novihKalorij);
					
					$("#urnikStranke tbody td[objid='" + ura_id + "'] div.forLoading").hide();
				}
				waitIndicator.hide();
		}, "json" );
}

function prijavi(ura_id, datum) {
		
	$("#urnikStranke tbody td[objid='" + ura_id + "'] div.forPrijava").hide();
	$("#urnikStranke tbody td[objid='" + ura_id + "'] div.forLoading").show(); 
	
	waitIndicator.show();
	jQuery.post( "api.php?params=action=addAttendance", {"ura_id": ura_id, "datum": datum}, function(data, textStatus, XMLHttpRequest) {
		//alert("shranjeno");
		//alert("shranjeno " + textStatus);
		//alert("shranjeno " + JSON.stringify(data));
		if (textStatus=="success")
			var td = $("#urnikStranke tbody td[objid='" + ura_id + "']");
			if (data.error=="no") {
				if (data.message.body.text=="ok") {
					
					td.addClass("prijavljen");
					td.find("div.forPrijava").hide();
					td.find("div.forOdjava").show();
					
					var novihKalorij = parseInt(nvl(td.find("div.forVadba").attr("kalorij"), 0));
					updateKalorije(novihKalorij);
					
					addKalorijeDan(td.attr("dan"), novihKalorij);
				}
			} else {
				if (data.message.body.text == "unauthorized") {
					//alert("za nadaljevanje se morate prijaviti");
					//$( "#dialog-modal" ).html("Za prijavo na uro vodene vadbe je potrebna: <a href='login'>prijava v sistem</a>.");
					//$( "#dialog-modal" ).dialog("open");
					$( "#loginDialog" ).dialog("open");
				} else {
					alert("neznana napaka");
				}
				td.find("div.forPrijava").show();
				console.log(data.message.body.text);
			}
			$("#urnikStranke tbody td[objid='" + ura_id + "'] div.forLoading").hide();
			waitIndicator.hide();
	}, "json" );
}

$(document).ready(function () {
	var urnik = $("#urnikStranke");
	if (waitIndicator != undefined) waitIndicator.show();  
	populateUrnik(urnik, waitIndicator);
	
	urnik.find("tbody td").click(function(cEv) {
		var x = $(this);
		
		var ura_id = x.attr("objid");
		var datum = x.attr("datum");
		
		if (ura_id!=null && x.hasClass("prijavljen")) {
			odjavi(ura_id, datum);			
		} else if (ura_id!=null && x.hasClass("prijavaMozna")) {
			prijavi(ura_id, datum);
		}
		cEv.preventDefault();
	});

	
	$("#loginDialog").dialog({
		autoOpen: false,
		height: 300,
		width: 500,
		modal: true,
		closeOnEscape: true,
		buttons: {
				"SHRANI": function() {
					$( this ).dialog( "close" );
				},
				"Prekliči": function() {
					
					$( this ).dialog( "close" );
				}
		},
		close: function() {
			$("#weekTableSchedukeEdit tbody tr").each(function (i, row) {
				$(this).find("td").each(function (j, primerek) {
					$(this).removeAttr("objid").removeAttr("modified");
					$(this).find("div div").remove();
				});
			});
			waitIndicator.hide();
		}
	});
	
});
