var prostor = null;

// Simulates PHP's date function
Date.prototype.format=function(format){var returnStr='';var replace=Date.replaceChars;for(var i=0;i<format.length;i++){var curChar=format.charAt(i);if(i-1>=0&&format.charAt(i-1)=="\\"){returnStr+=curChar;}else if(replace[curChar]){returnStr+=replace[curChar].call(this);}else if(curChar!="\\"){returnStr+=curChar;}}return returnStr;};Date.replaceChars={shortMonths:['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],longMonths:['January','February','March','April','May','June','July','August','September','October','November','December'],shortDays:['Sun','Mon','Tue','Wed','Thu','Fri','Sat'],longDays:['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'],d:function(){return(this.getDate()<10?'0':'')+this.getDate();},D:function(){return Date.replaceChars.shortDays[this.getDay()];},j:function(){return this.getDate();},l:function(){return Date.replaceChars.longDays[this.getDay()];},N:function(){return this.getDay()+1;},S:function(){return(this.getDate()%10==1&&this.getDate()!=11?'st':(this.getDate()%10==2&&this.getDate()!=12?'nd':(this.getDate()%10==3&&this.getDate()!=13?'rd':'th')));},w:function(){return this.getDay();},z:function(){var d=new Date(this.getFullYear(),0,1);return Math.ceil((this-d)/86400000);},W:function(){var d=new Date(this.getFullYear(),0,1);return Math.ceil((((this-d)/86400000)+d.getDay()+1)/7);},F:function(){return Date.replaceChars.longMonths[this.getMonth()];},m:function(){return(this.getMonth()<9?'0':'')+(this.getMonth()+1);},M:function(){return Date.replaceChars.shortMonths[this.getMonth()];},n:function(){return this.getMonth()+1;},t:function(){var d=new Date();return new Date(d.getFullYear(),d.getMonth(),0).getDate()},L:function(){var year=this.getFullYear();return(year%400==0||(year%100!=0&&year%4==0));},o:function(){var d=new Date(this.valueOf());d.setDate(d.getDate()-((this.getDay()+6)%7)+3);return d.getFullYear();},Y:function(){return this.getFullYear();},y:function(){return(''+this.getFullYear()).substr(2);},a:function(){return this.getHours()<12?'am':'pm';},A:function(){return this.getHours()<12?'AM':'PM';},B:function(){return Math.floor((((this.getUTCHours()+1)%24)+this.getUTCMinutes()/60+this.getUTCSeconds()/3600)*1000/24);},g:function(){return this.getHours()%12||12;},G:function(){return this.getHours();},h:function(){return((this.getHours()%12||12)<10?'0':'')+(this.getHours()%12||12);},H:function(){return(this.getHours()<10?'0':'')+this.getHours();},i:function(){return(this.getMinutes()<10?'0':'')+this.getMinutes();},s:function(){return(this.getSeconds()<10?'0':'')+this.getSeconds();},u:function(){var m=this.getMilliseconds();return(m<10?'00':(m<100?'0':''))+m;},e:function(){return"Not Yet Supported";},I:function(){return"Not Yet Supported";},O:function(){return(-this.getTimezoneOffset()<0?'-':'+')+(Math.abs(this.getTimezoneOffset()/60)<10?'0':'')+(Math.abs(this.getTimezoneOffset()/60))+'00';},P:function(){return(-this.getTimezoneOffset()<0?'-':'+')+(Math.abs(this.getTimezoneOffset()/60)<10?'0':'')+(Math.abs(this.getTimezoneOffset()/60))+':00';},T:function(){var m=this.getMonth();this.setMonth(0);var result=this.toTimeString().replace(/^.+ \(?([^\)]+)\)?$/,'$1');this.setMonth(m);return result;},Z:function(){return-this.getTimezoneOffset()*60;},c:function(){return this.format("Y-m-d\\TH:i:sP");},r:function(){return this.toString();},U:function(){return this.getTime()/1000;}};

$.fn.enable = function() {
	$(this).removeAttr("disabled");
};
$.fn.disable = function() {
	$(this).attr("disabled", "disabled");
};

(function($) {
	  var cache = [];
	  // Arguments are image paths relative to the current page.
	  $.preLoadImages = function() {
	    var args_len = arguments.length;
	    for (var i = args_len; i--;) {
	      var cacheImage = document.createElement('img');
	      cacheImage.src = arguments[i];
	      cache.push(cacheImage);
	    }
	  }
	})(jQuery);


function mysqlTimeStampToDate(timestamp) {
    //function parses mysql datetime string and returns javascript Date object
    //input has to be in this format: 2007-06-05 15:26:02
    var regex=/^([0-9]{2,4})-([0-1][0-9])-([0-3][0-9]) (?:([0-2][0-9]):([0-5][0-9]):([0-5][0-9]))?$/;
    var parts=timestamp.replace(regex,"$1 $2 $3 $4 $5 $6").split(' ');
    return new Date(parts[0],parts[1]-1,parts[2],parts[3],parts[4],parts[5]);
}

function prevediDan(dan) {
	return imenaDnevov[dan-1];	
}

function checkEmail(email) {
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)){
		return true;
	}
	//alert("Invalid E-mail Address! Please re-enter.");
	return false;
}

function checkNumber(number) {
	if (/^\d{5}$/.test(number)){
		return true;
	}
	//alert("Invalid E-mail Address! Please re-enter.");
	return false;
}

function mysqlDateToDate(timestamp) {
    //function parses mysql datetime string and returns javascript Date object
    //input has to be in this format: 2007-06-05 15:26:02
    var regex=/^([0-9]{2,4})-([0-1][0-9])-([0-3][0-9])/;
    var parts=timestamp.replace(regex,"$1 $2 $3").split(' ');
    return new Date(parts[0],parts[1]-1,parts[2],0,0,0);
}

function napolniElement(element, vsebina) {
	if (element!=null)
		$(element).html(vsebina);
}

function clearSchedule() {
	$("#stevecKalorij").text("0");
	$("#urnikStranke tbody tr").show();
	$("#urnikStranke tbody td").removeClass();
	$("#urnikStranke tbody td").removeAttr("objid");
	$("#urnikStranke tbody td div.forVadba").removeAttr("objid");
	$("#urnikStranke tbody td div.forVadba").removeAttr("kalorij");
	$("#urnikStranke tbody td div.forInstruktor").removeAttr("objid");
	$("#urnikStranke tbody td div.forVadba a").text("");
	$("#urnikStranke tbody td div.forInstruktor a").text("");
	$("#urnikStranke tbody td div.forPrijava").hide();
	$("#urnikStranke tbody td div.forOdjava").hide();
	$("#urnikStranke tbody td div.forInfo").hide();
}

function naloziSeznamObvestil() {
	jQuery.post( "api.php?params=action=seznamObvestil", {}, function(data, textStatus, XMLHttpRequest) {
		//alert("shranjeno");
		//alert("shranjeno " + textStatus);
		//alert("shranjeno " + JSON.stringify(data));
		if (textStatus=="success")
			if (data.error=="no") {
				//$("#noticeField").html("shranjeno");
				//$("#noticeField").show().fadeOut(5000);
				//$("#urnikColTable tbody").prepend(data.message.body.text).hide().fadeIn();
				waitIndicator.hide();
				
				var seznamObvestil = "";
				$(data.message.body.text).each(function (i, d) {
					//alert(d.id);
					seznamObvestil += "\n\t<li class='messageItem' objid='" + d.id + "'><b>" + d.naslov + "</b> (" + d.posiljatelj +"): " + d.tekst + "</li>";
				});
				//alert(JSON.stringify(seznamInstruktorjev));
				//alert(JSON.stringify(element));
				napolniElement("#seznamObvestil", seznamObvestil);
				//$("#shraniGumb").attr("disabled","disabled");
			}
		}, "json" );
}

function loadSeznamInstruktorjev(element) {
	//alert("polnim");
	waitIndicator.show();
	seznamInstruktorjev = "";
	jQuery.post( "api.php?params=action=showInstruktorList", {}, function(data, textStatus, XMLHttpRequest) {
		//alert("shranjeno");
		//alert("shranjeno " + textStatus);
		//alert("shranjeno " + JSON.stringify(data));
		if (textStatus=="success")
			if (data.error=="no") {
				//$("#noticeField").html("shranjeno");
				//$("#noticeField").show().fadeOut(5000);
				//$("#urnikColTable tbody").prepend(data.message.body.text).hide().fadeIn();
				waitIndicator.hide();
				
				$(data.message.body.text).each(function (i, d) {
					//alert(d.id);
					seznamInstruktorjev += "<li name='I" + i + "' t='I' rowid='" + d.id + "'>" + d.kratkoIme + "</li>";
				});
				//alert(JSON.stringify(seznamInstruktorjev));
				//alert(JSON.stringify(element));
				napolniElement(element, seznamInstruktorjev);
				//$("#shraniGumb").attr("disabled","disabled");
			}
		}, "json" );
}
function nvl(val, oth) {
	if (val==null) return oth;
	else return val;
}

function loadSeznamVadb(element) {
	//alert("lol");
	waitIndicator.show();

	seznamVadb = "";
	jQuery.post( "api.php?params=action=showVadbaList", {"filter": {"veljaven": "1"}}, function(data, textStatus, XMLHttpRequest) {
		//alert("shranjeno");
		//alert("shranjeno " + textStatus);
		//alert("shranjeno " + JSON.stringify(data));
		if (textStatus=="success")
			if (data.error=="no") {
				//$("#noticeField").html("shranjeno");
				//$("#noticeField").show().fadeOut(5000);
				//$("#urnikColTable tbody").prepend(data.message.body.text).hide().fadeIn();
				waitIndicator.hide();


				$(data.message.body.text).each(function (i, d) {
					//alert(d.id);
					seznamVadb += "<li name='" + d.kratkoIme + "' t='V' rowid='" + d.id + "'>" + d.naziv + "</li>";
				});
				//alert(JSON.stringify(seznamInstruktorjev));
				//alert(JSON.stringify(element));
				napolniElement(element, seznamVadb);
				//$("#shraniGumb").attr("disabled","disabled");
			}


	}, "json" );
}



$(document).ready(function(readyEvent) {
	// tips
	
	
	// izbira prostora za prikaz urnika
	$("ul#placeSelectionMenu li").click(function(cEv) {
		
		$("ul#placeSelectionMenu li").removeClass("selected");
		$(this).addClass("selected");

		
		var link = $(this).find("a").attr("href");
		prostor = $(this).attr("objid");
		//alert(prostor);
		window.location.hash = "#prostor=" + prostor;
		clearSchedule();		
		populateUrnik($("#urnikStranke"), waitIndicator);
		cEv.preventDefault();
	});
	
	$("a.notlink").click(function (ev) {
		//$(this).parent().find("input:first").focus();
		ev.preventDefault();
	});
	
	$("a[title]").qtip({ style: { 
	      width: 400,
	      padding: 5,
	      background: '#A2D959',
	      color: 'black',
	      textAlign: 'center',
	      border: {
	         width: 7,
	         radius: 5,
	         color: '#A2D959'
	      },
		  target: 'bottomRight',
	      tip: 'topLeft',
	      name: 'dark' // Inherit the rest of the attributes from the preset dark style
	   } 
	});
	
	$("#adminUrnikSifrant").draggable({
		handle: "p.ui-widget-header"
	});
	
	$("#calDetailMeta input[ftype='urnik']").change(function (ev) {
		$(this).attr("dirty", "1");
	});
	$("#calDetailMeta input[ftype='veljaOD']").change(function (ev) {
		$(this).attr("dirty", "1");
	});
	$("#calDetailMeta input[ftype='veljaDO']").change(function (ev) {
		$(this).attr("dirty", "1");
	});
	
	$("#reportBugButton").click(function(cEv) {
		$("#reportBugDialog").dialog("open");
	});
	
	$("#reportBugDialog").dialog({
		autoOpen: false,
		height: 400,
		width: 540,
		modal: true,
		closeOnEscape: true,
		buttons: {
				"POŠLJI": function() {
						$("#waitIndicator").show();
						
						var sendObj = {
							info: $("#reportBugFiledInfo").val(),
							priority: $("#reportBugFiledPriority").val(),
							text: $("#reportBugTextBox").val()
						}
						jQuery.post( "api?params=action=reportBug", sendObj, function(data, textStatus, XMLHttpRequest) {
							//alert("shranjeno");
							//alert("shranjeno " + textStatus);
							//alert("shranjeno " + JSON.stringify(data));
							if (textStatus=="success") {
								if (data.error=="no") {
									/*$(data.message.body.text).each(function (i, d) {
									
									});
									*/
									alert("Hvala lepa!");
								}
							} else {
								alert(data.message.body.text);
								alert(JSON.stringify(sendObj));
							}
						}, "json");
						$("#reportBugFiledInfo").val("info");
						$("#reportBugFiledPriority").val("nizka");
						$("#reportBugTextBox").val("");
						
						$( this ).dialog( "close" );
				},
				"Prekliči": function() {
					
					$( this ).dialog( "close" );
				}
		},
		close: function() {
			waitIndicator.hide();
		}
	});
	
	$("#urnikPodrobnostiDIV").dialog({
		autoOpen: false,
		height: windowP.height-50,
		width: windowP.width-50,
		modal: true,
		closeOnEscape: true,
		buttons: {
				"SHRANI": function() {
						$("#waitIndicator").show();
						//alert("shranjeno");
						// naberemo podatke iz celic
						var k = 0;
						
						var spremembe = [];
						var urnikID = $("#calDetailMeta input[ftype='urnik_id']").val();
						//alert(urnikID);
						$("#weekTableSchedukeEdit tbody tr").each(function (i, row) {
							var ura = $(this).attr("ura");
							
							$(this).find("td").each(function (j, primerek) {
								// posamična celica
								
								
								var vadba = $(primerek).find("div.forVadba div").attr("objid");
								var instruktor = $(primerek).find("div.forInstruktor div").attr("objid");
								var dan = $(primerek).attr("dan");
								var objID = $(primerek).attr("objid");
								if (objID==undefined) { objID = -1; };
								
								var sprememba = { "vadba": -1, "instruktor": -1, "ura": ura, "dan": dan, "objid": objID };
								if (instruktor!=undefined) {
									sprememba.instruktor = instruktor;
								};
								if (vadba!=undefined) {
									sprememba.vadba = vadba;
								};
								if (sprememba.vadba!=-1 || sprememba.instruktor!=-1) {
								  spremembe.push(sprememba);
								} else {
									// sprememba kjer so brisani primerki
									if ($(this).attr("modified")=='1') {
										spremembe.push(sprememba);
									}
								}
								$(this).find("div div").remove();
								
							});
						});
						//alert(JSON.stringify(spremembe));
						
						var urnikDetails = {
							"id": urnikID,
							"spremembe": {
								"naziv": null,
								"veljaOD": null,
								"veljaDO": null,
								"veljaven": $("#calDetailMeta input[name='calDetailVeljaven']:checked").val()
							}
						}; // to do - track
						//alert($("#calDetailMeta input[name='calDetailVeljaven']:checked").val());
						
						var naziv = $("#calDetailMeta input[ftype='urnik']");
						if ($(naziv).attr("dirty")=="1")
							urnikDetails.spremembe.naziv = naziv.val();
						
						var veljaOD = $("#calDetailMeta input[ftype='veljaOD']");
						if ($(veljaOD).attr("dirty")=="1")
							urnikDetails.spremembe.veljaOD = formatDate($(veljaOD).datepicker("getDate"), "yyyy-MM-dd"); 
						
						var veljaDO = $("#calDetailMeta input[ftype='veljaDO']");
						if ($(veljaDO).attr("dirty")=="1")
							urnikDetails.spremembe.veljaDO = formatDate($(veljaDO).datepicker("getDate"), "yyyy-MM-dd");
												
						jQuery.post( "api?params=action=saveUrnikDetails", {"lastnosti": urnikDetails, "ure": spremembe }, function(data, textStatus, XMLHttpRequest) {
							//alert("shranjeno");
							//alert("shranjeno " + textStatus);
							//alert("shranjeno " + JSON.stringify(data));
							if (textStatus=="success")
								if (data.error=="no") {
									/*$(data.message.body.text).each(function (i, d) {
									
									});
									*/
									if (data.message.body.text.length=1) {
										var spremenjenaVrstica = data.message.body.text[0];
										var tr = $("#urnikColTable tbody tr[rowid='" + spremenjenaVrstica.id + "']");
										tr.find("td[ftype='urnik']").html(spremenjenaVrstica.naziv);
										tr.find("td[ftype='veljaOD']").html(formatDate(mysqlDateToDate(spremenjenaVrstica.veljaOD), "dd.MM.yyyy"));
										tr.find("td[ftype='veljaDO']").html(formatDate(mysqlDateToDate(spremenjenaVrstica.veljaDO), "dd.MM.yyyy"));
										tr.find("td[ftype='veljaven']").html((spremenjenaVrstica.veljaven=="1" ? "DA" : "NE"));
										tr.find("td[ftype='posodobljen']").html(formatDate(mysqlTimeStampToDate(spremenjenaVrstica.posodobljen), "dd.MM.yyyy HH:mm"));
										//alert(JSON.stringify(spremenjenaVrstica));
									} else {
										alert("nepričakovana napaka, rezultat bi moral vrniti eno vrstico!");
										alert(JSON.stringify(data));
									}
							
									
								}
							waitIndicator.hide();
								
								
						}, "json" );
						
						$( this ).dialog( "close" );
				},
				"Prekliči": function() {
					
					$( this ).dialog( "close" );
				}
		},
		close: function() {
			$("#weekTableSchedukeEdit tbody tr").each(function (i, row) {
				$(this).find("td").each(function (j, primerek) {
					$(this).removeAttr("objid").removeAttr("modified");
					$(this).find("div div").remove();
				});
			});
			waitIndicator.hide();
		}
	});
	$("select#fcID").change(function(ev) {
		//api?params=action=showProstoriZaFC
		waitIndicator.show();
		var fcID = $(this).val();
		
		//alert(fcID);
		var novUrnikDvoranaIDDIV = $("#novUrnikDvoranaIDDIV");
		
		jQuery.post( "api?params=action=showProstoriZaFC", {"fcID": fcID}, function(data, textStatus, XMLHttpRequest) {
			//alert("shranjeno");
			//alert("shranjeno " + textStatus);
			var seznamProstorov = "<select id='prostorID'>";
			//alert("shranjeno " + JSON.stringify(data));
			if (textStatus=="success")
				if (data.error=="no") {
					$(data.message.body.text).each(function (i, d) {
						seznamProstorov += "<option value='" + d.id + "'>" + d.naziv + "</option>";
					});
					waitIndicator.hide();
				} else {
					//alert();
				}
			
				
			seznamProstorov += "</select>";
			novUrnikDvoranaIDDIV.html(seznamProstorov);
				
		}, "json" );

	});
	
	$("#tabelaSifrant a").live("click", function(clkEv) {
		var action = $(this).attr("action");
		
		var vrednosti = $("#tabelaSifrant table thead"); 
		
		var tabela = $("#tabelaSifrant table tbody");
		
		switch (action) {
		  case "addInstruktor":
			  var nErr = 0;
			  if (vrednosti.find("input[ftype='dolgoIme']").val().length==0) {
				  vrednosti.find("input[ftype='dolgoIme']").addClass("error");
				  nErr++;
			  }
			  if (vrednosti.find("input[ftype='kratkoIme']").val().length==0) {
				  vrednosti.find("input[ftype='kratkoIme']").addClass("error");
				  nErr++;
			  }
			  if (nErr>0) break;
			  tabela.prepend("<tr toAdd='1' class='newItem'><td>&nbsp;</td>" +
			  		"<td ftype='dolgoIme'>" + vrednosti.find("input[ftype='dolgoIme']").val() + "</td>" +
			  		"<td ftype='kratkoIme'>" + vrednosti.find("input[ftype='kratkoIme']").val() + "</td>" +
			  		"<td ftype='veljaven'>" + (vrednosti.find("input[ftype='veljaven']:checked").val() == 1 ? "da" : "ne" ) + "</td>" +
			  		"<td colspan='2'>&nbsp;</td>" +
			  		"<td><a href='#' action='removeInstruktor'><img src='images/minus.gif'></a></td>" + 
			  	"</tr>").hide().fadeIn();	
			  vrednosti.find("input[ftype='dolgoIme']").val("");
			  vrednosti.find("input[ftype='kratkoIme']").val("");
			  break;
		  case "removeInstruktor":
			  var row = $(this).parent().parent();
			  if (row.attr("toadd")=="1") {
				  row.fadeOut("slow", function () {
					  $(this).remove()
				  });
			  } else {
				  row.fadeOut("slow");
				  row.attr("toRemove", "1");  
			  }
			  
			  break;
		  case "addVadba":
			  var nErr = 0;
			  if (vrednosti.find("input[ftype='naziv']").val().length==0) {
				  vrednosti.find("input[ftype='naziv']").addClass("error");
				  nErr++;
			  }
			  if (vrednosti.find("input[ftype='kratkoIme']").val().length==0) {
				  vrednosti.find("input[ftype='kratkoIme']").addClass("error");
				  nErr++;
			  }
			  if (nErr>0) break;
			  tabela.prepend("<tr toAdd='1' class='newItem'><td>&nbsp;</td>" +
			  		"<td ftype='naziv'>" + vrednosti.find("input[ftype='naziv']").val() + "</td>" +
			  		"<td ftype='kratkoIme'>" + vrednosti.find("input[ftype='kratkoIme']").val() + "</td>" +
			  		"<td ftype='veljaven'>" + (vrednosti.find("input[ftype='veljaven']:checked").val() == 1 ? "da" : "ne" ) + "</td>" +
			  		"<td colspan='2'>&nbsp;</td>" +
			  		"<td><a href='#' action='removeInstruktor'><img src='images/minus.gif'></a></td>" + 
			  	"</tr>").hide().fadeIn();	
			  vrednosti.find("input[ftype='naziv']").val("");
			  vrednosti.find("input[ftype='kratkoIme']").val("");
			  break;
		  case "removeVadba":
			  var row = $(this).parent().parent();
			  if (row.attr("toadd")=="1") {
				  row.fadeOut("slow", function () {
					  $(this).remove();
				  });
			  } else {
				  row.fadeOut("slow");
				  row.attr("toRemove", "1");  
			  }
			  
			  break;			  
		}
		
		clkEv.preventDefault();
	});
	
	$("#tabelaSifrant input").live("click", function(clkEv) {
		$(this).removeClass("error");
	});
	
	
	$("#urediSeznamInstruktorjevLink").click(function(ev) {
		waitIndicator.show();
		$("#sifObjekt").val("instruktor");
		var sifDialog = $("#sifrantiDialog");
		sifDialog.find("h2[ftype='naslov']").html("Seznam inštruktorjev");
		sifDialog.find("p[ftype='navodilo']").html("urejanje seznama");
		var tabela = sifDialog.find("#tabelaSifrant table");
		var thead = tabela.find("thead");
		var tbody = tabela.find("tbody");
		
		tbody.html("");
		
		thead.html("<tr>" +
		 		"<td>id</td>" +
		 		"<td>naziv</td>" +
		 		"<td>kratko</td>" +
		 		"<td>aktiven</td>" +
		 		"<td>ustvarjen</td>" +
		 		"<td>posodobljen</td>" +
		 		"<td rowspan='2'><a href='#' action='addInstruktor'><img src='images/plus.gif' /></a></td></tr>" +
		 		"<tr>" +
		 		"<td>&nbsp;</td>" +
		 		"<td><input type='text' ftype='dolgoIme' /></td>" +
		 		"<td><input type='text' ftype='kratkoIme' /></td>" +
		 		"<td><label for='veljavnost'><input type='radio' ftype='veljaven' name='veljavnost' id='veljavenDA' value='1' checked='checked' />da</label> <label for='veljavnost'><input type='radio' ftype='veljaven' name='veljavnost' id='veljavenNE' value='0' />ne</label></td>" +
		 		"<td>&nbsp;</td>" +
		 		"<td>&nbsp;</td>" +
		 		"</tr>");
		jQuery.post( "api.php?params=action=showInstruktorList", { "showAll": 1 }, function(data, textStatus, XMLHttpRequest) {
			//alert("shranjeno");
			//alert("shranjeno " + textStatus);
			//alert("shranjeno " + JSON.stringify(data));
			if (textStatus=="success")
				if (data.error=="no") {
					//$("#noticeField").html("shranjeno");
					//$("#noticeField").show().fadeOut(5000);
					//$("#urnikColTable tbody").prepend(data.message.body.text).hide().fadeIn();
					waitIndicator.hide();

					$(data.message.body.text).each(function (i, d) {
						//alert(d.id);
						 tbody.append("<tr rowid='" + d.id + "'>" +
						 		"<td>" + d.id + "</td>" +
						 		"<td ftype='editable' fcol='dolgoIme'>" + d.dolgoIme + "</td>" +
						 		"<td ftype='editable' fcol='kratkoIme'>" + d.kratkoIme + "</td>" +
						 		"<td ftype='editable' fcol='veljaven'>" + (d.veljaven=="1" ? "da" : "ne") + "</td>" +
						 		"<td>" + d.ustvarjen + "</td>" +
						 		"<td>" + (d.posodobljen==null ? "&nbsp;" : d.posodobljen) + "</td>" + "" +
						 				"<td>&nbsp;</td></tr>");
						 var element = tbody.find("tr[rowid='" + d.id + "']");
						 /*
						 element.click(function(clkEvent) {
							 alert("hihi");
						 });
						 */
						 element.find("td[ftype='editable']").click(function (edclkEv) {
							 var ftype = $(this).attr("ftype");
							 if (ftype=="modified") return;
							 $(this).attr("ftype", "modified");
							 $(this).parent().attr("toupdate", "1");
							 var oldVal = $(this).html();
							 var rid = $(this).parent().attr("rowid");
							 
							 if ($(this).attr("fcol")=="veljaven") {
								 var vsDa = "";
								 var vsNe = "";
								 
								 if (oldVal=="da") {
									 oldVal = "1";
									 vsDa = "checked='checked'";
								 } else {
									 oldVal = "0";
									 vsNe = "checked='checked'";
								 }

								 $(this).html("<input type='radio' name='veljavenRB" + rid +"' value='1' " + vsDa + "/> da <input type='radio' name='veljavenRB" + rid +"' value='0' " + vsNe + "/> ne");
							 } else {
								 $(this).html("<input type='text' value='" + oldVal + "' />");
							 }
							 
							 $(this).attr("oldVal", oldVal);
						 });
						 
					});
					
					
					//alert(JSON.stringify(seznamInstruktorjev));
					//alert(JSON.stringify(element));
					//$("#shraniGumb").attr("disabled","disabled");
				}
			
				
		}, "json" );
		
		sifDialog.dialog("open");
		ev.preventDefault();
	});
	
	$("#urediSeznamVadbLink").click(function(ev) {
		waitIndicator.show();
		$("#sifObjekt").val("vadba");
		var sifDialog = $("#sifrantiDialog");
		sifDialog.find("h2[ftype='naslov']").html("Seznam vadb");
		sifDialog.find("p[ftype='navodilo']").html("urejanje seznama");
		var tabela = sifDialog.find("#tabelaSifrant table");
		var thead = tabela.find("thead");
		var tbody = tabela.find("tbody");
		
		tbody.html("");
		
		thead.html("<tr>" +
		 		"<td>id</td>" +
		 		"<td>naziv</td>" +
		 		"<td>kratko</td>" +
		 		"<td>veljaven</td>" +
		 		"<td>ustvarjen</td>" +
		 		"<td>posodobljen</td>" +
		 		"<td rowspan='2'><a href='#' action='addVadba'><img src='images/plus.gif' /></a></td></tr>" +
		 		"<tr>" +
		 		"<td>&nbsp;</td>" +
		 		"<td><input type='text' ftype='naziv' /></td>" +
		 		"<td><input type='text' ftype='kratkoIme' /></td>" +
		 		"<td><label for='veljavnost'><input type='radio' ftype='veljaven' name='veljavnost' id='veljavenDA' value='1' checked='checked'  />da</label> <label for='veljavnost'><input type='radio' ftype='veljaven' name='veljavnost' id='veljavenNE' value='0' />ne</label></td>" +
		 		"<td>&nbsp;</td>" +
		 		"<td>&nbsp;</td></tr>");
		jQuery.post( "api.php?params=action=showVadbaList", {}, function(data, textStatus, XMLHttpRequest) {
			//alert("shranjeno");
			//alert("shranjeno " + textStatus);
			//alert("shranjeno " + JSON.stringify(data));
			if (textStatus=="success")
				if (data.error=="no") {
					//$("#noticeField").html("shranjeno");
					//$("#noticeField").show().fadeOut(5000);
					//$("#urnikColTable tbody").prepend(data.message.body.text).hide().fadeIn();
					waitIndicator.hide();

					$(data.message.body.text).each(function (i, d) {
						//alert(d.id);
						 tbody.append("<tr rowid='" + d.id + "'>" +
						 		"<td>" + d.id + "</td>" +
						 		"<td ftype='editable' fcol='naziv'>" + d.naziv + "</td>" +
						 		"<td ftype='editable' fcol='kratkoIme'>" + d.kratkoIme + "</td>" +
						 		"<td ftype='editable' fcol='veljaven'>" + (d.veljaven=="1" ? "da" : "ne") + "</td>" +
						 		"<td>" + d.ustvarjen + "</td>" +
						 		"<td>" + (d.posodobljen==null ? "&nbsp;" : d.posodobljen) + "</td>" + "<td>&nbsp;</td></tr>");
						 var element = tbody.find("tr[rowid='" + d.id + "']");
						 /*
						 element.click(function(clkEvent) {
							 alert("hihi");
						 });
						 */
						 element.find("td[ftype='editable']").click(function (edclkEv) {
							 var ftype = $(this).attr("ftype");
							 if (ftype=="modified") return;
							 $(this).attr("ftype", "modified");
							 $(this).parent().attr("toupdate", "1");
							 
							 var oldVal = $(this).html();
							 var rid = $(this).parent().attr("rowid");
							 //alert(rid);
							 
							 if ($(this).attr("fcol")=="veljaven") {
								 var vsDa = "";
								 var vsNe = "";
								 
								 if (oldVal=="da") {
									 oldVal = "1";
									 vsDa = "checked='checked'";
								 } else {
									 oldVal = "0";
									 vsNe = "checked='checked'";
								 }

								 $(this).html("<input type='radio' name='veljavenRB" + rid +"' value='1' " + vsDa + "/> da <input type='radio' name='veljavenRB" + rid +"' value='0' " + vsNe + "/> ne");
							 } else {
								 $(this).html("<input type='text' value='" + oldVal + "' />");
							 }
							 
							 $(this).attr("oldVal", oldVal);
						 });
						 
					});
					
					
					//alert(JSON.stringify(seznamInstruktorjev));
					//alert(JSON.stringify(element));
					//$("#shraniGumb").attr("disabled","disabled");
				}
			
				
		}, "json" );
		sifDialog.dialog("open");
		ev.preventDefault();
	});
	
	$("#sifrantiDialog").dialog({
		autoOpen: false,
		height: windowP.height-50,
		width: windowP.width-50,
		modal: true,
		closeOnEscape: true,
		buttons: {
			"SHRANI": function() {
				var novi = []; 
				$(this).find("tr[toadd='1']").each(function (i, row) {
					var novaVrstica = {};
					$(row).find("td[ftype]").each(function (j, field) {
						var vrednost = $(field).html();
						if ($(field).attr("ftype")=="veljaven") {
							vrednost = (vrednost=="da" ? 1 : 0);
						}
						novaVrstica[$(field).attr("ftype")] = vrednost;
					});
					novi.push(novaVrstica);
				});
				//alert(JSON.stringify(novi));
				// spremenjeni
				var spremenjeni = [];
				$(this).find("tr[toupdate='1']").each(function(i, row) {
					var spremenjenaVrstica = {
						"podatki": {},
						"pogoj": {}
					};
					spremenjenaVrstica.pogoj.id = $(row).attr("rowid");

					var stSprememb = 0;
					$(row).find("td[ftype='modified']").each(function(j, field) {
						var sprememba = "";
						if ($(field).find("input").attr("type")=="text") {
							sprememba = $(field).find("input").val();	
						} else if ($(field).find("input").attr("type")=="radio") {
							sprememba = $(field).find("input:checked").val();
						}
						if ($(field).attr("oldval")!=sprememba) {
							spremenjenaVrstica.podatki[$(field).attr("fcol")] = sprememba;
							stSprememb++;
						} 
					});
					if (stSprememb>0)
						spremenjeni.push(spremenjenaVrstica);
				});
				var zaBrisanje = [];
				$(this).find("tr[toremove='1']").each(function(i, row) {
					var brisi = {
							"id": $(row).attr("rowid")
					};
					zaBrisanje.push(brisi);
				});
				//alert(JSON.stringify(spremenjeni));
				//alert("shranjujem");
				
				$( this ).dialog( "close" );
				
				var sprSifranta = {
					"sifObjekt" : $(this).find("#sifObjekt").val(),
					"novi" 		: novi,
					"spremembe" : spremenjeni,
					"brisanje"	: zaBrisanje
				};				
				//alert(JSON.stringify(sprSifranta));
				
				
				waitIndicator.show();
				
				
				
				jQuery.post( "api.php?params=action=updateSifranta", sprSifranta, function(data, textStatus, XMLHttpRequest) {
					//alert("shranjeno");
					//alert("shranjeno " + textStatus);
					//alert("shranjeno " + JSON.stringify(data));
					if (textStatus=="success")
						if (data.error=="no") {
							//$("#noticeField").html("shranjeno");
							//$("#noticeField").show().fadeOut(5000);
							//$("#urnikColTable tbody").prepend(data.message.body.text).hide().fadeIn();
							
							//alert(JSON.stringify(seznamInstruktorjev));
							//alert(JSON.stringify(element));
							//$("#shraniGumb").attr("disabled","disabled");
							loadSeznamInstruktorjev($("#seznamInstruktorjev"));
							loadSeznamVadb($("#seznamVadb"));
							//alert("napolnjeno");

						}
					waitIndicator.hide();
				}, "json" );				
			},
			"ZAPRI": function() {
					
				$( this ).dialog( "close" );
			}
		},
		close: function() {			
			$(this).find("table thead").html("");
			$(this).find("table tbody").html("");
			waitIndicator.hide();
		}
	});
	
	$('textarea.autoResize').autoResize({
	    // On resize:
	    onResize : function() {
	        $(this).css({opacity:0.8});
	    },
	    // After resize:
	    animateCallback : function() {
	        $(this).css({opacity:1});
	    },
	    // Quite slow animation:
	    animateDuration : 300,
	    // More extra space:
	    extraSpace : 10
	});
	
	$(".styledTable tbody tr").live("click", function(cEv) {
		$(".styledTable tbody tr").removeClass("selectedTR");
		$(this).toggleClass("selectedTR");
	});
	$(".styledTableV2 tbody tr").live("click", function(cEv) {
		$(".styledTable tbody tr").removeClass("selectedTR");
		$(this).toggleClass("selectedTR");
	});
	
	$("a.cloneSchedule").click(function(cEv) {
		var urnikId = $(this).parent().parent().find("td[ftype='id']").text();
		if (!confirm("Zares želite klonirati urnik?")) return;
		

		jQuery.post( "api.php?params=action=cloneSchedule", {"id": urnikId}, function(data, textStatus, XMLHttpRequest) {
			//alert("shranjeno");
			//alert("shranjeno " + textStatus);
			//alert("shranjeno " + JSON.stringify(data));
			if (textStatus=="success")
				if (data.error=="no") {
					//$("#noticeField").html("shranjeno");
					//$("#noticeField").show().fadeOut(5000);
					//$("#urnikColTable tbody").prepend(data.message.body.text).hide().fadeIn();
					
					//alert(JSON.stringify(seznamInstruktorjev));
					//alert(JSON.stringify(element));
					//$("#shraniGumb").attr("disabled","disabled");

					//alert("napolnjeno");
					//alert("klonirano");
					var tab = $("#urnikColTable");
				
					var oldRow = tab.find("tbody tr[rowid='" + urnikId + "']").clone();
					
					oldRow.attr("rowid", data.message.body.text);
					oldRow.find("td[ftype='id']").text(data.message.body.text);
					
					oldRow.addClass("selectedTR");
						
					tab.find("tbody").append(oldRow);
				}
			waitIndicator.hide();
		}, "json" );				

	})
	
});