setInterval ( "osveziUrnik()", 5000 );

var uraUpdateTimerId = null;

function osveziUrnik ( )
{

	var urnik = $("#urnikStranke");
	if (waitIndicator != undefined) waitIndicator.show();  
	populateUrnik(urnik, waitIndicator);

}


function updateUraInfoDialogAtt(att) {
	var prijav = parseInt($("#podrobnostiPrijav").text()) + att;
	var omejitev = parseInt($("#podrobnostiMesta").text());
	if (prijav>omejitev) {
		$("#podrobnostiMesta").addClass("error");
	} else {
		$("#podrobnostiMesta").removeClass("error");
	}
	$("#podrobnostiPrijav").text(prijav);
}

function populateUrnik(urnikDiv, waitIndicator) {
	//clearSchedule();
	// sproži zahtevek po urniku na server
	// populiraj element urnikDiv
	//alert("admin");
	
	//alert(pid);
	
	jQuery.post( "api.php?params=action=get7DUrnik", {"prostor": prostor}, function(data, textStatus, XMLHttpRequest) {
		//alert("shranjeno");
		//alert("shranjeno " + textStatus);
		//alert("shranjeno " + JSON.stringify(data));
		if (textStatus=="success")
			if (data.error=="no") {
				$(data.message.body.text).each(function(i, val) {
					//alert(JSON.stringify(val));
					 				
					var polje = $(urnikDiv).find("tr[ura='" + val.ura + "'] td[dan='" + val.dan + "']");
					polje.attr("objid", val.id);
					polje.find("div.forInfo").html("<a href='#'>" + val.prijav + "<img src='images/edit.png' /></a>").show();
					
					if (val.odpovedana=="1" && polje.attr("datum")==val.datum_odpovedi) {
						polje.addClass("odpovedana");
					}
					
					var vadba = polje.find("div.forVadba"); 
					vadba.attr("objid", val.vid);
					vadba.find("a").attr("title", val.opis);
					vadba.find("a").text(val.naziv);
					
					var instruktor = polje.find("div.forInstruktor"); 
					instruktor.attr("objid", val.iid);
					instruktor.find("a").attr("title", val.oInstruktorju);
					instruktor.find("a").text(val.trener);
				});

			}
			waitIndicator.hide();
	}, "json" ); 
}
function getSeznamVadecih(datum, ura_id) {

	//alert("poklican " + datum + " " + ura_id);
	$("#uraDetailUra").val(ura_id);
	$("#uraDetailDatum").val(datum);

	jQuery.post( "api.php?params=action=getAtendees", {"ura_id": ura_id, "datum": datum}, function(data, textStatus, XMLHttpRequest) {
		//alert("shranjeno");
		//alert("shranjeno " + textStatus);
		//alert("shranjeno " + JSON.stringify(data));
		if (textStatus=="success")
			if (data.error=="no") {
				//alert(JSON.stringify(data));
				//"classInfo":[{"id":"28","datum":"2011-03-03","dan":"4","ura":"18:00:00","instruktor":"Tina Gro\u0161elj","vadba":"Body step","omejitev":"25","prijav":"2"}]
				             
				var classInfo = data.message.body.text.classInfo[0];
				
				$("#podrobnostiVadba").text(classInfo.vadba);
				$("#podrobnostiInstruktor").text(classInfo.instruktor);
				$("#podrobnostiPrijav").text(classInfo.prijav);
				$("#podrobnostiMesta").text(classInfo.omejitev);
				
				if (classInfo.prijav>classInfo.omejitev) {
					$("#podrobnostiMesta").addClass("error");	
				} else {
					$("#podrobnostiMesta").removeClass("error");	
				}
				
				
				$("#seznamVadecih li.vadeci").remove();
				if (data.message.body.text.participants.length>0) {
					$("#seznamVadecih li.nodata").hide();
					$(data.message.body.text.participants).each(function (i, obj) {
						$("<li class='vadeci' objid='" + obj.id + "'>[<a action='removeAttendance' href='#'>x</a>] " + obj.ime + "</li>").prependTo("#seznamVadecih");
					});
				} else {
					$("#seznamVadecih li.nodata").show();
				}
			} else {
				alert(JSON.stringify(data));
			}
			waitIndicator.hide();
	}, "json" );
	
}
$(document).ready(function (rEvent) {
	
	$("#seznamVadecih li a[action='removeAttendance']").live("click", function (cEvent) { 
		jQuery.post( "api.php?params=action=removeAttendance", {"ura_id": $("#uraDetailUra").val(), "datum": $("#uraDetailDatum").val(), "uporabnik": $(this).parent().attr("objid")}, function(data, textStatus, XMLHttpRequest) {
			//alert("shranjeno");
			//alert("shranjeno " + textStatus);
			//alert("shranjeno " + JSON.stringify(data));
			if (textStatus=="success")
				if (data.error=="no") {
					$("#seznamVadecih li[objid='" + data.message.body.text[0].uporabnik_id + "']").remove();
					updateUraInfoDialogAtt(-1);
				}
				waitIndicator.hide();
		}, "json" );
	});
	
	var urnik = $("#urnikStranke");
	if (waitIndicator != undefined) waitIndicator.show();  
	populateUrnik(urnik, waitIndicator);

	urnik.find("div.forOdjava").click(function(ev){
		// obdelaj odjavo od ure
		var ura_id = $(this).parent().attr("objid");
		var datum = $(this).parent().attr("datum");
		
		waitIndicator.show();
		jQuery.post( "api.php?params=action=removeAttendance", {"ura_id": ura_id, "datum": datum}, function(data, textStatus, XMLHttpRequest) {
			//alert("shranjeno");
			//alert("shranjeno " + textStatus);
			//alert("shranjeno " + JSON.stringify(data));
			if (textStatus=="success")
				if (data.error=="no") {
					var td = $("#urnikStranke tbody td[objid='" + ura_id + "']");
					td.removeClass("prijavljen");
					td.find("div.forOdjava").hide();
					td.find("div.forPrijava").show();
				}
				waitIndicator.hide();
		}, "json" );
		ev.preventDefault();
	});
	
	urnik.find("div.forPrijava").click(function(ev){
		// obdelaj odjavo od ure
		var ura_id = $(this).parent().attr("objid");
		var datum = $(this).parent().attr("datum");
		
		waitIndicator.show();
		jQuery.post( "api.php?params=action=addAttendance", {"ura_id": ura_id, "datum": datum}, function(data, textStatus, XMLHttpRequest) {
			//alert("shranjeno");
			//alert("shranjeno " + textStatus);
			//alert("shranjeno " + JSON.stringify(data));
			if (textStatus=="success")
				if (data.error=="no") {
					if (data.message.body.text=="ok") {
						var td = $("#urnikStranke tbody td[objid='" + ura_id + "']");
						td.addClass("prijavljen");
						td.find("div.forPrijava").hide();
						td.find("div.forOdjava").show();
						
					}
				}
				waitIndicator.hide();
		}, "json" );
		ev.preventDefault();
	});
	
	//$("div.forInfo a").live("click", function (cEvent) {
	urnik.find("tbody td").live("click", function (cEvent) {

		//var ura = $(this).parent().parent();
		var ura = $(this);
		
		var datum = ura.attr("datum");
		var objid = ura.attr("objid");
		
		if (!(objid==null || objid==undefined)) {
			uraUpdateTimerId = setInterval("getSeznamVadecih('" + datum + "', " + objid + ")", 3000);
			getSeznamVadecih(ura.attr("datum"), ura.attr("objid"));
			$("#uraInfoDialog").dialog("open");
		}
		
		cEvent.preventDefault();
	});
	
	
	$( "#newAtendee" ).autocomplete({
		source: function( request, response ) {
			$.ajax({
				url: "api.php?params=action=searchUserByName",
				type: "POST",
				dataType: "json",
				data: {
					q: request.term
				},
				success: function( data ) {
					//alert("lala");
					response( $.map( data.message.body.text, function( item ) {
						//alert(JSON.stringify(item));
						return {
							label: item.ime + ' ' + item.priimek + ' - ' + item.sokol_id,
							value: item.ime + ' ' + item.priimek,
							id: item.id
						};
					}));
				}
			});
		},
		minLength: 2,
		select: function( event, ui ) {
			waitIndicator.show();
			jQuery.post( "api.php?params=action=addAttendance", {"ura_id": $("#uraDetailUra").val(), "datum": $("#uraDetailDatum").val(), "uporabnik": ui.item.id }, function(data, textStatus, XMLHttpRequest) {
				//alert("shranjeno");
				//alert("shranjeno " + textStatus);
				//alert("shranjeno " + JSON.stringify(data));
				if (textStatus=="success")
					if (data.error=="no") {
						if (data.message.body.text=="ok") {
							$("#seznamVadecih .nodata").hide();
							$('<li class="vadeci" objid=' + ui.item.id + '>[<a action="removeAttendance" href="#">x</a>] ' + ui.item.label + '</li>').prependTo("#seznamVadecih").hide().fadeIn("slow");
							updateUraInfoDialogAtt(1);
						}
					}
					waitIndicator.hide();
			}, "json" );
			
			$(this).val("");
			event.preventDefault();
		},
		open: function() {
			$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
		},
		close: function() {
			$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
		}
	});
	
	$("#odpovedUreDialog").dialog({
		autoOpen: false,
		height: 200,
		width: 370,
		modal: true,
		buttons: { 
			"Vredu": function() {
				var cancelationReason = $("#cancelationReason").val();
				var cbxNotifyAtt = $("#cbxNotifyAtt").is(":checked") == true ? "da" : "ne";
				var classToCancel = $("#uraDetailUra").val();
				var dateOfClass = $("#uraDetailDatum").val();
				//$p["uraid"], $p["datum"], $p["obvesti"], $p["razlog"]
				jQuery.post( "api.php?params=action=cancelClass", {"uraid": classToCancel, "datum": dateOfClass, "obvesti": cbxNotifyAtt, "razlog": cancelationReason }, function(data, textStatus, XMLHttpRequest) {
					//alert("shranjeno");
					//alert("shranjeno " + textStatus);
					alert("shranjeno " + JSON.stringify(data));
					if (textStatus=="success")
						if (data.error=="no") {
							$("#odpovedUreDialog").dialog("close");
							$("#uraInfoDialog").dialog("close");
							alert("ok");
						} else {
							
							alert("Neznana napaka, kontaktirajte administratorja.");
						}
						waitIndicator.hide();
				}, "json" );
				 
			}, 
			"Prekliči dejanje": function() { 
				$(this).dialog("close"); 
			}
		}
	});
	
	$("#uraInfoDialog").dialog({
		autoOpen: false,
		height: 600,
		width: 350,
		modal: true,
		buttons: {
			"odpoved ure": function() {
				// klic funkcije za odpovedanje ure
				$("#odpovedUreDialog").dialog("open");
			},
			"X": function() {
				$(this).dialog("close");
			} 
		},
		close: function(closeE) {
			//alert("sio");
			$("#seznamVadecih li.vadeci").remove();
			$("#seznamVadecih li.nodata").show();
			var urnik = $("#urnikStranke");
			if (waitIndicator != undefined) waitIndicator.show();  
			populateUrnik(urnik, waitIndicator);
			clearInterval(uraUpdateTimerId);
		}
	});
});
