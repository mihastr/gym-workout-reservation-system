<?php session_start();
include_once '../config/dbconfig.php';
pisi_log(-21, "obisk registracija.php", "registracija.php", 3);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Strict//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sokol group d.d.</title>
<link rel="Shortcut Icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="Stylesheet" type="text/css" href="css/stil.css" />
<style>
#loginContainer {
	padding-top: 30px;
	padding-bottom: 30px;
	background-color: white;
	margin-left: auto;
	margin-right: auto;
	width: 100%;
	overflow: auto;
	background-image: url("images/sara.gif");
	background-repeat: no-repeat;
	background-position: bottom right;
}

#loginContainer table {
	border: none;
	background-color: transparent;
}

#loginContainer table th {
	border: none;	
	text-align: right;
	padding-right: 10px;
}
#loginContainer table td {
	border: none;	
}


input[type="text"],input[type="password"] {
	border: thin solid gray;
	width: 300px;
	padding: 7px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	font-weight: bold;
}

input[type="submit"] {
	border: thin solid black;
	background-color: gray;
	padding: 7px;
	margin-top: 20px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	font-weight: bold;
}

.helpDiv {
	display: none;
}

#waitIndicator {
	display: none;
	margin-top: 30px;
	margin-right: 20px;
}

.err {
	display: none;
}

#loginContainer table th.glavaTabele {
	text-align: center;
	padding-top: 15px;
	padding-bottom: 5px;
}

a.notlink {
	cursor: default;
}

a.notlink:hover {
	cursor: default;
	text-decoration: none;
}
</style>
</head>
<!--script type="text/javascript" src="js/jquery-1.4.3.js"></script-->
<script type="text/javascript" src="js/jquery-1.4.4.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.6.custom.min.js"></script>
<script type="text/javascript" src="js/jquery.qtip-1.0.0-rc3.js"></script>
<link type="text/css" href="css/custom-theme/jquery-ui-1.8.6.custom.css" rel="stylesheet" />

<script type="text/javascript">
var waitIndicator = null;
var emailFault = false;

function checkNumber(number) {
	if (/^\d{5}$/.test(number)){
		return true;
	}
	//alert("Invalid E-mail Address! Please re-enter.");
	return false;
}

function checkEmail(email) {
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)){
		return true;
	}
	//alert("Invalid E-mail Address! Please re-enter.");
	return false;
}

$(document).ready(function(documentReadyEvent) {
	waitIndicator = $("#waitIndicator");
	var err = {
			"email": 0,
			"geslo": 0
	};
	$("#emailFiled").change(function (chEv) {
		$(this).parent().parent().find(".err").hide();
		if (!checkEmail($(this).val())) {
			$(this).parent().parent().find(".errEmail").show();
		} else {
			waitIndicator.show();
			jQuery.post( "api.php?params=action=checkEmailExistance", {"email": $(this).val() }, function(data, textStatus, XMLHttpRequest) {
				if (textStatus=="success")
					if (data.error=="no") {
						if (data.message.body.text == 0) {
							$("#emailFiled").parent().parent().find(".errOK").show();
							emailFault = false;
							err.email = 0;
						} else {
							$("#emailFiled").parent().parent().find(".errEmailExists").show();
							emailFault = true;
							err.email = 1;
						}
						waitIndicator.hide();
					}
			}, "json");
			
		}
	});
	$("#geslo").change(function (chEv) {
		$(this).parent().parent().find(".err").hide();
		if ($(this).val().length < 6) {
			$(this).parent().parent().find(".err2Short").show();
			err.geslo = 1;
		} else {
			$(this).parent().parent().find(".errOK").show();
			err.geslo = 2;
		}
	});
	$("#gesloPonovno").change(function (chEv) {
		$(this).parent().parent().find(".err").hide();
		//alert($(this).val() + " " + $("#geslo").val());
		if ($(this).val()!= $("#geslo").val()) {
			$(this).parent().parent().find(".errNotSame").show();
			err.geslo = 1;
		} else {
			$(this).parent().parent().find(".errOK").show();
			if (err.geslo==2) {
				err.geslo = 0;
			}
		}
	});



	function validateForm(form) {
		var email = $("#emailFiled").val();
		var pass1 = $("#geslo").val();
		var pass2 = $("#gesloPonovno").val();
		var sokolID = $("#sokolID").val();
		var ime		= $("#ime").val();
		var priimek	= $("#priimek").val();

		var errList = [];

		if (email.length==0) {
			errList.push("<?php print t("email"); ?> - <?php print t("mandatory"); ?>");
		}
		if (emailFault) {
			errList.push("<?php print t("emailExistanceError"); ?>");
		}
		if (email.length>0 && !checkEmail(email)) {
			errList.push("<?php print t("emailFormatErr"); ?>");
		}
		if (pass1.length<6) {
			errList.push("<?php print t("passShortErr"); ?>");
		}
		if (pass1!=pass2) {
			errList.push("<?php print t("passMatchErr"); ?>");
		}
		if (sokolID.length == 0 || checkNumber(sokolID)) {
			errList.push("<?php print t("echeckFormatError"); ?>");
		}
		if (ime.length==0) {
			errList.push("<?php print t("name"); ?> - <?php print t("mandatory"); ?>");
		}
		if (priimek.length==0) {
			errList.push("<?php print t("surname"); ?> - <?php print t("mandatory"); ?>");
		}
		
		var seznamNapakStr = "<ul class='seznamNapak'>";
		$(errList).each(function (i, obj) {
			seznamNapakStr += "<li>" + obj + "</li>";
		});
		seznamNapakStr += "</ul>";
		if (errList.length>0)
			$("#seznamNapak").html(seznamNapakStr);
		//alert(errList.length);
		return errList.length==0;
	}

	$("#registracija").submit(function(sEvent) {
		sEvent.preventDefault();
		var form = $(this);
		var isValid = validateForm(form);
		if (isValid) {
			$("#registracijaSubmit").attr("disabled", "disabled");
			jQuery.post( "api.php?params=action=register", {
				"email": $("#emailFiled").val(),
				"geslo":  $("#geslo").val(),
				"sokolid": $("#sokolID").val(),
				"ime": $("#ime").val(),
				"priimek": $("#priimek").val()
				}, function(data, textStatus, XMLHttpRequest) {
				if (textStatus=="success")
					if (data.error=="no") {

						if (data.message.body.text.error=="no") {
							// registracija uspešna
							//alert("all ok id " + data.message.body.text.uporabnik_id);
							$( "#okMessage" ).dialog({
								modal: true,
								buttons: {
									"OK": function()  {
								 	$(this).dialog('close');
								 }
								},
								close: function () {
									window.location = 'login';
								}
							});
						} else {
							alert("napake");							
						}
						waitIndicator.hide();
						$("#seznamNapak").hide();
					} else {
						$("#seznamNapak").html("Napake, prosimo popravite polja - ter poskusite ponovno. Če mislite da je sporočilo napaka nam pišite na pomoc@sokolgroup.com");
						$("#registracijaSubmit").removeAttr("disabled");
					}
			}, "json");
		}
	});

	$("a.notlink").click(function (ev) {
		//$(this).parent().find("input:first").focus();
		ev.preventDefault();
	});

	$("a[title]").qtip({ style: { name: 'blue', tip: true } });
	$("p[title]").qtip({ style: { name: 'blue', tip: true } });
});
</script>
<body>
<div id="headerDiv">&nbsp;
<div id="header" style="float: left;"><a href="index"><img src="images/header.jpg" /></a></div>
<div id="dialog-modal" style="display: none;"><?php print t("error"); ?></div>
<div id="waitIndicator"><img src="images/ajax-loader3.gif" /></div>
<!-- 
	<h1><a style="color: white" href="index">MOJ SOKOL</a></h1>
	<H3>PRIJAVA NA URE VODENE VADBE</H3>
	 --></div>
<div id="okMessage" style="display:none">
<?php print t("regOkMessage"); ?></p>
</div>
<div id="loginContainer">
<h1></h1>
<div style="float: left; margin-left: 15px;">
<form id="registracija">
<table>
	<thead>
		<tr>
			<td colspan="3" class="glavaTabele"><div id="seznamNapak"></div></td>
		</tr>
		<tr>
			<td colspan="3" class="glavaTabele"><h3><?php print t("mandatorySection"); ?></h3></td>
		</tr>
		<tr>
			<th><p href="#" class="notlink" title="<?php print t("regEmailHelp"); ?>"><?php print t("email"); ?></p></th>
			<td><input type="text" id="emailFiled" /></td>
			<td width="300">
				<div class="err errObvezen"><?php print t("mandatory"); ?></div>
				<div class="err errEmail"><?php print t("emailFormErr"); ?></div>				
				<div class="err errEmailExists"><?php print t("emailUserErr"); ?></div>
				<div class="err errOK"><img src="images/ok.jpg" /></div>
			</td>
		</tr>
		
		<tr>
			<th><p href="#" class="notlink" title="<?php print t("regEmailHelp"); ?>"><?php print t("emailAgain"); ?></p></th>
			<td><input type="text" id="emailFiled2" /></td>
			<td width="300">
				<div class="err errObvezen"><?php print t("mandatory"); ?></div>
				<div class="err errEmail"><?php print t("emailFormErr"); ?></div>				
				<div class="err errEmailExists"><?php print t("emailUserErr"); ?></div>
				<div class="err errOK"><img src="images/ok.jpg" /></div>
			</td>
		</tr>
		
		<tr>
			<th><p href="#" class="notlink" title="<?php print t("regPassHelp"); ?>"><?php print t("password"); ?></p></th>
			<td><input type="password" id="geslo" /></td>
			<td>
				<div class="err errObvezen"><?php print t("mandatory"); ?></div>
				<div class="err err2Short"><?php print t("passShortErr"); ?></div>				
				<div class="err errOK"><img src="images/ok.jpg" /></div>
			</td>			
		</tr>
		<tr>
			<th><p href="#" class="notlink" title="<?php print t("regPassHelp"); ?> - <?php print t("repeat"); ?>"><?php print t("regPassAgain"); ?></p></th>
			<td><input type="password" id="gesloPonovno" /></td>
			<td>
				<div class="err errObvezen"><?php print t("mandatory"); ?></div>
				<div class="err errNotSame"><?php print t("passRepeatHelp"); ?></div>				
				<div class="err errOK"><img src="images/ok.jpg" /></div>
			</td>
		</tr>
	</thead>
	<toptional>
	<tr>
		<th colspan="3" class="glavaTabele">&nbsp;</th>
	</tr>
	<tr>
		<th><p href="#" class="notlink" title="<?php print t("regECheckHelp"); ?>"><?php print t("regEcheckNo"); ?></p></th>
		<td><input type="text" id="sokolID" /></td>
	</tr>
	<tr>
		<th><p href="#" class="notlink" title="<?php print t("regSurnameHelp"); ?>)"><?php print t("name"); ?></p></th>
		<td><input type="text" id="ime" /></td>
	</tr>
	<tr>
		<th><p href="#" class="notlink" title="<?php print t("regSurnameHelp"); ?>"><?php print t("surname"); ?></p></th>
		<td><input type="text" id="priimek" /></td>
	</tr>
	</toptional>
	<tsubmit>
	<tr>
		<td>&nbsp;</td>
		<td><input id="registracijaSubmit" type="submit" value="<?php print strtoupper(t("register")); ?>" /></td>
	</tr>
	</tsubmit>

</table>
</form>
</div>

</div>
</body>
</html>
