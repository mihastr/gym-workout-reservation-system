<?php 
// RSS provider
header("Content-type: text/xml");
echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";

if (!isset($_GET["get"])) exit(1);

$modul = $_GET["get"];

$rssKoda = (isset($_GET["rsskoda"]) ? $_GET["rsskoda"] : "");

include_once '../config/dbconfig.php';

$db = new DbConfig;

$items = array();
switch ($modul) {
	case "mojUrnik": {
		$res = $db->executeSelect("SELECT\n".
			/*"up.ime,\n".*/
			"concat(ud.datum, \" \", ura.ura) cas,\n".
			"v.naziv nazivVadbe,\n".
			"v.opis\n".
			"FROM udelezba ud\n".
			"INNER JOIN uporabnik up ON ud.uporabnik_id = up.id\n".
			"INNER JOIN ura ON ura.id=ud.ura_id\n".
			"INNER JOIN vadba v ON v.id=ura.VADBA_id\n".
			"WHERE ud.`status`='prijavljen' \n".
			"AND up.rsskoda='$rssKoda' \n".
			"AND ud.datum>=now()");
		//print_r($res);
		foreach ($res as $row) {
			array_push($items, array("title"=>$row["nazivVadbe"], "time"=>strtotime($row["cas"]), "url"=>"opisVadbe.php", "description"=>$row["opis"]));
		}
	}
	break;
}

?>

<rss version="2.0">
<channel>
  <title>Uporabnikov urnik vadb</title>
  <link>http://mayki.dnsalias.com/dipl/project/web/</link>
  <description>Tekoči - veljavni urnik vadb, na katere ste prijavljeni</description>
  <pubDate>Tue, 15 Apr 2008 18:00:00 +0000</pubDate>


<?php foreach ($items as $item) { ?>
  <item>
    <title><?php print $item['title']?></title>
    <pubDate><?php print date("r",$item['time'])?></pubDate>
    <link><?php print $item['url']?></link>
    <description><?php print $item['description']?></description>
  </item>
<?php } ?>
</channel>
</rss>

