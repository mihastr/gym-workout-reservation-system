<?php 
// iCal provider
//header("Content-type: text/xml");
header('Content-type: text/calendar; charset=utf-8');
header('Content-Disposition: inline; filename=mojSokol.ics');

if (!isset($_GET["get"])) exit(1);
$modul = $_GET["get"];

$rssKoda = (isset($_GET["rsskoda"]) ? $_GET["rsskoda"] : "");

include_once '../config/mod.php';

$db = new DbConfig;

$items = array();
switch ($modul) {
	case "mojUrnik": {
		$res = $db->executeSelect("SELECT\n".
			/*"up.ime,\n".*/
			"concat(ud.datum, \" \", ura.ura) cas,\n".
			"v.naziv nazivVadbe,\n".
			"v.opis\n".
			"FROM udelezba ud\n".
			"INNER JOIN uporabnik up ON ud.uporabnik_id = up.id\n".
			"INNER JOIN ura ON ura.id=ud.ura_id\n".
			"INNER JOIN vadba v ON v.id=ura.VADBA_id\n".
			"WHERE ud.`status`='prijavljen' \n".
			"AND up.rsskoda='$rssKoda' \n".
			"AND ud.datum>=now()");
		//print_r($res);
		foreach ($res as $row) {
			array_push($items, array("title"=>$row["nazivVadbe"], "time"=>strtotime($row["cas"]), "url"=>"opisVadbe.php", "description"=>$row["opis"]));
		}
	}
	break;
	case "urnik": {
		$controller = new Urnik7DController;
		$res = $controller->getList(0, 11);
		//print_r($res);
		foreach ($res as $row) {
			array_push($items, array("title"=>$row["naziv"], "time"=>strtotime($row["cas"]), "url"=>"opisVadbe.php", "description"=>$row["opis"]));
		}
	}
	break;
}
?>
BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//hacksw/handcal//NONSGML v1.0//EN
<?php foreach ($items as $item) { ?>
BEGIN:VEVENT
UID:example@example.com
DTSTAMP:<?php echo date("Ymd\THis", time()); ?>Z
ORGANIZER;CN=Sokol group:MAILTO:pomoc@sokolgroup.com
DTSTART:<?php print date("Ymd\THis\Z",$item["time"]) . "\r\n"; ?>
DTEND:<?php print date("Ymd\THis\Z",($item["time"]+60*60)) . "\r\n"; ?>
SUMMARY:<?php print $item["title"] . "\r\n"; ?>
DESCRIPTION:<?php print $item["description"] . "\r\n"; ?>
LOCATION:Sokol vič
END:VEVENT
<?php } ?>
END:VCALENDAR
