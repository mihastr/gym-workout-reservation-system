<?php
include_once '../config/dbconfig.php';
$koda = $_REQUEST["koda"];

if (strlen($koda)>1) {
	$db = new DbConfig;
	$db->beginTransaction();
	if ($db->count("uporabnik", array("auth_code"=>$koda))==1) {
		$db->updateNoCommit("uporabnik", array("veljaven"=>1, "auth_code"=>null), array("auth_code" => $koda));	
	}
	$db->commitTransaction();
	
}


?>
<html>
<head>
	<title>Pozabljeno geslo</title>
	<style type="text/css">
	#headerDiv {
	  background-color: #ABABAB;
	  background-image: url('images/bg_top.jpg'); 
	  height: 103px;
	  border-bottom: 2px solid #ABABAB;
	}
	input[type='password'] {
		width: 200px;
		border: 1px solid #AAA;
	}
	input[type='submit'] {
		width: 200px;
		background-color: #EEE;
		border: 1px solid #AAA;
	}
	.error {
		background-color: #E66;
		text-align: center;
	}
	</style>
	<link rel="Shortcut Icon" href="images/favicon.ico" type="image/x-icon" />
</head>
<body style="margin: 0px; background-color: #ABABAB; font-family: Verdana,Geneva,Kalimati,sans-serif;">
<div id="headerDiv">&nbsp;</div>
<form action="geslo.php?koda=<?php echo $koda; ?>" method="POST">
<input type="hidden" id="koda" name="koda" value="<?php echo $koda; ?>" /> 
<div style="background-color: white; ">
	<div style="margin-left: auto; margin-right: auto; width: 350px; padding: 20px;">
	
	<img src="images/sokol_logo.jpg" />

	<h2>Hvala! Registracija je bila uspešna!</h2>
	<p><a href="index">Če ne boste v 3 sekundah avtomatsko preusmerjeni kliknite sem.</a></p>
	<script language="javascript">
	setTimeout("doChangeLocation()", 3000);
	function doChangeLocation() {
		window.location.href="index";
	}
	</script>
	</div>
</div>
</form>
</body>
</html>
