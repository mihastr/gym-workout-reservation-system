<?php
include_once '../config/dbconfig.php';
$koda = $_REQUEST["koda"];
//print $koda;
// najdi kodo med uporabniki
// če je geslo izpolnjeno potem spremeni geslo
$napaka = null;
if (strlen($koda)>0) {
	if (isset($_POST["pass1"]) && isset($_POST["pass2"])) {
		if ($_POST["pass1"]==$_POST["pass2"]) {
			// preveri kodo, če je vse ok, zamenjaj geslo
			$geslo = $_POST["pass1"];
			if (strlen($geslo)>=4) {
				// preveri kodo
				$db = new DbConfig;	
				
				if ($db->count("uporabnik", array("auth_code" => $koda)) == 1) {
					try {
						$db->beginTransaction();
						$db->updateNoCommit("uporabnik", array("geslo"=>md5($geslo)), array("auth_code"=>$koda));
						$db->updateNoCommit("uporabnik", array("auth_code"=>null), array("auth_code"=>$koda));
						$db->commitTransaction();
						echo "<script language='javascript'> window.location.href='index' </script>";
					} catch (Exception $ex) {
						$db->rollbackTransaction();
						$napaka = "neznana napaka, kontaktirajte Sokol pomoč";
					}
					
				} else {
					$napaka = "napaka pri uporabi aplikacije, preverite e-sporočilo še enkrat ali poiščite pomoč";
				}
			} else {
				$napaka = "dolžina gesla mora biti večja ali enaka od 4 zankov";
			}
		} else {
			$napaka = "gesli se morata ujemati";
		}
	} else {
		// ni napaka - gesli nista nastavljeni
	}
} else {
	$napaka = "napaka pri uporabi aplikacije";
}


?>
<html>
<head>
	<title>Pozabljeno geslo</title>
	<style type="text/css">
	#headerDiv {
	  background-color: #ABABAB;
	  background-image: url('images/bg_top.jpg'); 
	  height: 103px;
	  border-bottom: 2px solid #ABABAB;
	}
	input[type='password'] {
		width: 200px;
		border: 1px solid #AAA;
	}
	input[type='submit'] {
		width: 200px;
		background-color: #EEE;
		border: 1px solid #AAA;
	}
	.error {
		background-color: #E66;
		text-align: center;
	}
	</style>
	<link rel="Shortcut Icon" href="images/favicon.ico" type="image/x-icon" />
</head>
<body style="margin: 0px; background-color: #ABABAB; font-family: Verdana,Geneva,Kalimati,sans-serif;">
<div id="headerDiv">&nbsp;</div>
<form action="geslo.php?koda=<?php echo $koda; ?>" method="POST">
<input type="hidden" id="koda" name="koda" value="<?php echo $koda; ?>" /> 
<div style="background-color: white; ">
	<div style="margin-left: auto; margin-right: auto; width: 350px; padding: 20px;">
	
	<table>
		<tr height="80">
			<td colspan="2" valign="top" align="center"><img src="images/sokol_logo.jpg" /></id>
		
		</tr>
		<?php if (isset($napaka) && $napaka!=null) {?>
		<tr height="40">
			<td colspan="2" class="error"><?php print $napaka; ?></td>
		</tr>
		<?php } ?>
		<tr>
			<td colspan="2" align="center">
			<h2>Ponastavitev gesla</h2>
			</id>
		
		</tr>
	
		<tr>
			<td align="right">Novo geslo:</td>
			<td><input name="pass1" type="password" /></td>
		</tr>
		<tr>
			<td align="right">Ponovite novo geslo:</td>
			<td><input name="pass2" type="password" /></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><input type="submit" value="Potrdi" /></td>
		</tr>
	</table>
	</div>
</div>
</form>
</body>
</html>
