<?php
//include_once '../config/do_login.php';


$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$start = $time;

include_once '../config/mod.php';

session_start();

$p = $_POST;
$s = $_SESSION;
$message = array("error"=> "no");
$params = array();


function error_handler($errno, $errstr, $errfile, $errline) {
	global $message;
	$message["error"] = "yes";
	$mpos["no"] = $errno;
	$mpos["text"] = $errstr;
	$mpos["erf"] = $errfile;
	$mpos["erl"] = $errline;
	
	$message["message"]["body"]["errlist"][] = $mpos;
	
} 

set_error_handler("error_handler", E_ALL);
// v spremenljivki post se nahajajo vsi podatki, ki se nanašajo na določen objekt (iskanje, urejanje, vstavljanje) 
if (isset($_GET["params"])) {
	// get program parameters
	$paramsCombined = explode(";", $_GET["params"]);
	$message["message"]["body"]["no"] = 1;
	$message["message"]["body"]["text"] = "no of params " . count($paramsCombined);
	foreach ($paramsCombined as $param) {
		$x = explode("=", $param);
		$params[$x[0]] = $x[1];
	}
	$message["message"]["body"]["params"] = $params;
	
	if (isset($params["action"])) {
		try {
			$actionResult = executeAction($params["action"], $p);
			$message["message"]["body"]["no"] = 0;
			$message["message"]["body"]["text"] = $actionResult;	
		} catch (Exception $ex) {
			$message["error"] = "yes";
			$message["message"]["body"]["no"] = $ex->getCode();
			$message["message"]["body"]["text"] = $ex->getMessage();	
		}
	} else {
		$message["error"] = "yes";
		$message["message"]["body"]["no"] = 2;
		$message["message"]["body"]["text"] = "action not specified";
	}
		
	
} else {
	$message["error"] = "yes";
	$message["message"]["body"]["no"] = 0;
	$message["message"]["body"]["text"] = "no params";
}

function executeAction($action, $p) {
	$actionResult = null;
	$controller = null;
	switch ($action) {
		// poročanje o hroščih
		case "reportBug": {
			$controller = new SifrantController;
			$actionResult = $controller->reportBug($p["info"], $p["priority"], $p["text"]);
		} break;
		case "alterBugReport": {
			$controller = new SifrantController;
			$actionResult = $controller->alterBugReport($p["id"], $p["status"], $p["response"]);
		} break;
		// akcija za ustvarjanje novega urnika
		case "createNewSchedule": {
			$controller = new NewScheduleController;
			$actionResult = convertToTR($controller->newEntryAction($p));
		} break;
		case "cloneSchedule": {
			$controller = new NewScheduleController;
			$actionResult = $controller->cloneSchedule($p["id"]);
		} break;
		// akcija za spreminjanje vrstnega reda urnika
		case "scheduleChangeOrder": {
			$controller = new NewScheduleController;
			$params["data"] = $p["data"]; 
			$actionResult = $controller->updateBulkAction($params);
		} break;
		// akcija za prikaz inštruktorjev
		case "showInstruktorList": {
			$controller = new InstrukorController;
			$actionResult = $controller->getList($p);
		} break;
		case "showRemovedInstruktorList": {
			$controller = new InstrukorController;
			$actionResult = $controller->getRemoved();
		} break;
		
		case "addInstruktor": {
			$controller = new InstrukorController;
			$actionResult = $controller->create($p["dolgoIme"], $p["kratkoIme"], $p["opis"]);
		} break;
		case "removeInstruktor": {
			$controller = new InstrukorController;
			$actionResult = $controller->remove($p["id"]);
		} break;
		case "editInstruktor": {
			$controller = new InstrukorController;
			$actionResult = $controller->update($p["id"], $p["dolgoIme"], $p["kratkoIme"], $p["opis"]);
		} break;
		
		// akcija za prikaz seznama vadb
		case "showVadbaList": {
			$controller = new VadbaController;
			$actionResult = $controller->getList($p);
		} break;
		case "addVadba": {
			$controller = new VadbaController;
			$actionResult = $controller->create($p["naziv"], $p["kratkoIme"], $p["opis"], $p["link"], $p["kalorije"], $p["image"]);
		} break;
		case "editVadba": {
			$controller = new VadbaController;
			if (isset($p["image"])) {
				$image = $p["image"];
			} else {
				$image = null;
			}
			$actionResult = $controller->update($p["id"], $p["naziv"], $p["kratkoIme"], $p["opis"], $p["link"], $p["kalorije"], $image);
		} break;
		case "removeVadba": {
			$controller = new VadbaController;
			$actionResult = $controller->remove($p["id"]);
		} break;
		case "removeVadbaLogo": {
			$controller = new VadbaController;
			$actionResult = $controller->removeClassLogo($p["id"]);
		} break;
		case "showProstoriZaFC": {
			$controller = new ProstorController;
			$actionResult = $controller->getListForFC($p["fcID"]);
		} break;
		case "showProsoriKapacitete": {
			$controller = new ProstorController;
			$actionResult = $controller->getListWithClassesCapacity($p["vadba_id"]);
		} break;
		case "savePlacesCapacityChanges": {
			$controller = new ProstorController;
			$actionResult = $controller->saveListWithClassesCapacity($p["prostori"], $p["vadbe"]);
		} break;
		case "removePlace": {
			$controller = new ProstorController;
			$actionResult = $controller->removePlace($p["id"]);
		} break;
		case "removeFC": {
			$controller = new FitnesCenterController;
			$actionResult = $controller->remove($p["id"]);
		} break;
		case "addFC": {
			$controller = new FitnesCenterController;
			$actionResult = $controller->create($p["naziv"], $p["kratkoIme"], $p["naslov"]);
		} break;
		case "editFC": {
			$controller = new FitnesCenterController;
			$actionResult = $controller->update($p["id"], $p["naziv"], $p["kratkoIme"], $p["naslov"]);
		} break;
		case "showUrnikDetails": {
			$controller = new NewScheduleController;
			$actionResult = $controller->findByID($p["id"]);
		} break;
		case "checkFBUID": {
			$controller = new UporabnikController;
			$actionResult = $controller->checkFBUID($p["fbuid"]);
		} break;
		case "saveUrnikDetails": {
			$controller = new NewScheduleController;
			pisi_log(-21, "debug: " . json_encode($p), "api.php", 94);
			$actionResult = $controller->updateDetails($p);
		} break;
		case "get7DUrnik": {
			$controller = new Urnik7DController;
			if (isset($_SESSION["userid"]))
				$user = $_SESSION["userid"];
			else 
				$user = null;
			$actionResult = $controller->getList($user, $p["prostor"]);
		} break;
		case "removeAttendance": {
			$controller = new Urnik7DController;
			if (isset($p["uporabnik"])) {
				$uporabnik = $p["uporabnik"];
			} else {
				$uporabnik = $_SESSION["userid"];
			}
			$actionResult = $controller->removeAttendance($p["ura_id"], $uporabnik, $p["datum"]);
		} break;
		case "addAttendance": {
			$controller = new Urnik7DController;
			if (isset($p["uporabnik"])) {
				$uporabnik = $p["uporabnik"];
			} else {
				$uporabnik = $_SESSION["userid"];
			}
			$actionResult = $controller->addAttendance($p["ura_id"], $uporabnik, $p["datum"]);
			pisi_log(-21, json_encode(array($p["ura_id"], $_SESSION["userid"], $p["datum"])), "api.php", 113);
		} break;
		case "updateSifranta": {
			$controller = new SifrantController;
			$actionResult = $controller->update($p);
		} break;
		case "checkEmailExistance": {
			$controller = new UporabnikController;
			$actionResult = $controller->checkEmailExistance($p["email"]);
		} break;
		case "register": {
			$controller = new UporabnikController;
			$actionResult = $controller->registerUser($p);
		} break;
		case "registerUsingFB": {
			$controller = new UporabnikController;
			$actionResult = $controller->registerUsingFB($p["sokol_id"], $p["email"], $p["geslo1"], $p["useFBEmail"], $p["fbsession"]);
		} break;
		case "lostPassword": {
			$controller = new UporabnikController;
			$actionResult = $controller->resolveLostPassword($p["email"]);
		} break;
		case "insertObvestilo": {
			include_once '../models/Obvestilo.php';
			$model = new Obvestilo;
			$actionResult = $model->createNew($p["naslov"], $p["tekst"], $_SESSION["userid"], $p["prikazan"], $p["veljaod"], $p["veljado"], $p["prejemniki"]);
		} break;
		case "getMsgRecepients": {
			include_once '../models/Obvestilo.php';
			$model = new Obvestilo;
			$actionResult = $model->getMsgRecepients($p["id"]);
		} break;		
		case "seznamObvestil": {
			include_once '../models/Obvestilo.php';
			$model = new Obvestilo;
			$actionResult = $model->getListForUser();
		} break;
		case "searchUserByName": {
			$controller = new UporabnikController;
			$actionResult = $controller->findByName($p["q"]);
		} break;
		case "findTranslation": {
			$controller = new SifrantController;
			$actionResult = $controller->getTranslation($p["q"], $p["lang"]);
		} break;
		case "saveTranslation": {
			$controller = new SifrantController;
			$actionResult = $controller->saveTranslation($p["tag"], $p["translation"], "sl");
		} break;
		case "changeProfile": {
			$controller = new UporabnikController;
			if (isset($p["user"])) $user = $p["user"]; else $user = null;
			$actionResult = $controller->changeProfile($p["field"], $p["value"], $user);
		} break;
		case "changePassword": {
			$controller = new UporabnikController;
			$actionResult = $controller->changePassword($p["pass"]);
		} break;
		case "changeUserType": {
			$controller = new UporabnikController;
			$actionResult = $controller->changeUserType($p["userType"], $p["user"]);
		} break;
		case "changePermission": {
			$controller = new UporabnikController;
			$actionResult = $controller->changePermission($p["user"], $p["permission"], $p["selected"]);
		} break;
		
		
		case "getAtendees": {
			$controller = new Urnik7DController;
			$actionResult = $controller->getAtendees($p["datum"], $p["ura_id"]);
		} break;		
		case "removeMSG": {
			$controller = new ObvestiloController;
			$actionResult = $controller->deleteMSG($p["msgid"]);
		} break;
		case "msgChangeVisibility": {
			$controller = new ObvestiloController;
			$actionResult = $controller->changeVisibility($p["id"], $p["visible"]);
		} break;
		case "showMessages4Admin": {
			$controller = new ObvestiloController;
			if (isset($p["q"])) $q=$p["q"]; else $q=null;
			$actionResult = $controller->getList4Admin($p["invisible"], $p["deleted"], $q);
		} break;
		case "changePlaceParams": {
			$controller = new FitnesCenterController;
			$actionResult = $controller->changePlaceCapacity($p["placeID"], $p["value"], $p["placename"], $p["fc"]);
		} break;
		case "cancelClass": {
			$controller = new Urnik7DController;
			//$actionResult = "ok";
			$actionResult = $controller->cancelClass($p["uraid"], $p["datum"], $p["obvesti"], $p["razlog"]);
		} break;
		case "getBasicStats": {
			$controller = new Urnik7DController;
			$actionResult = $controller->getOsnovnaStatistika($p["od"], $p["do"], $p["prostor"]);
		} break;
		case "generateRSSCode": {
			$controller = new UporabnikController;
			if (isset($p["user"]))
				$actionResult = $controller->generateRSSCode($p["user"]);
			else
				$actionResult = $controller->generateRSSCode();
		} break;
		case "profileChangeFcLis4User": {
			$controller = new UporabnikController;
			if (isset($p["data"]))
				$actionResult = $controller->changeFCList($p["data"]);
			else throw new Exception("invalid request");
		} break;				
		default: 
			throw new Exception("invalid request");
		
	}
	return $actionResult;
} 

$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$finish = $time;
$total_time = round(($finish - $start), 4);

$message["genTime"] = $total_time;

print json_encode($message);
?>