<?php
//ini_set("error_reporting", E_ALL);

function getUrl($url) {
	$content = null;
	// make sure curl is installed
	if (function_exists('curl_init')) {
	   // initialize a new curl resource
	   $ch = curl_init(); 
	
	   // set the url to fetch
	   curl_setopt($ch, CURLOPT_URL, $url); 
	   
	   // don't give me the headers just the content
	   curl_setopt($ch, CURLOPT_HEADER, 0); 
	
	   // return the value instead of printing the response to browser
	   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	
	   // use a user agent to mimic a browser
	   curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.5) Gecko/20041107 Firefox/1.0'); 

	   curl_setopt($ch, CURLOPT_HTTPHEADER, array (
        "Content-Type: text/xml; charset=UTF-8",
        "Expect: 100-continue"
    	));
	   
	   
	   $content = curl_exec($ch); 
	
	   // remember to always close the session and free all resources 
	   curl_close($ch); 
	} else {
	   // curl library is not installed so we better use something else
	   throw new Exception("curl not installed");
	}
	return $content;
}
//Initialize the XML parser
$parser=xml_parser_create();
$parserAms=xml_parser_create();

/*
$sql = "show COLUMNS from vreme_napoved";
mysql_connect("localhost", "api", "geslo123") or die(mysql_error());
mysql_select_db("mojsokol");
$q = mysql_query($sql) or die(mysql_error());
print "<pre>";
print "\n array (";
while ($row = mysql_fetch_array($q, MYSQL_ASSOC))  {
	print "\n\t" . $row["Field"] . ", " ;
}
print "\n}";
print "</pre>";
mysql_close();
*/

//$url = "http://darvinov.net/sokol_project/web/test.xml";
//$url = "http://meteo.arso.gov.si/uploads/probase/www/observ/surface/text/sl/observation_LJUBL-ANA_BEZIGRAD_latest.xml";
//xmlVremeNapoved5Dni=http://meteo.arso.gov.si/uploads/probase/www/fproduct/text/sl/fcast_SLOVENIA_latest.xml
$url = "http://meteo.arso.gov.si/uploads/probase/www/fproduct/text/sl/fcast_SLOVENIA_MIDDLE_latest.xml"; // 3 dni


//Open XML file
$data = getUrl($url);
//print str_replace('ž, 'z', $data);
$find = array('č', 'ž', 'š');
$replace = array('c','z','s');
$data = str_replace($find, $replace, $data);

//print $data;

//print $data;
xml_parse_into_struct($parser, $data, $vals, $index);

/*
print_r($vals);
print_r($index);
*/
	
function forecastForDay($day=0) {
	global $index, $vals;
	$vreme = array (
		"id" => null, 
		"n_dni" => 3, 
	
		"datum" => date("Y-m-d"), 
		"ura" => date("H:00"), 
		
		"kraj" => $vals[$index["DOMAIN_METEOSIID"][$day]]["value"], 
		"podrocje" => $vals[$index["DOMAIN_SHORTTITLE"][$day]]["value"], 
		"za_dan" => date("Y-m-d H:i", strtotime($vals[$index["VALID_UTC"][$day]]["value"])), 
		"oblacnost_icon" => $vals[$index["NN_ICON"][$day]]["value"], 
		"oblacnost_tekst" => str_replace("ž", "z", str_replace("č", "c", $vals[$index["NN_SHORTTEXT"][$day]]["value"])), 
		"vreme_icon" => $vals[$index["WWSYN_ICON"][$day]]["value"], 
		"vreme_tekst" => $vals[$index["WWSYN_LONGTEXT"][$day]]["value"], 
		"min_temp" => $vals[$index["TN"][$day]]["value"], 
		"max_temp" => $vals[$index["TX"][$day]]["value"], 
		"temp" => $vals[$index["T"][$day]]["value"], 
		"vlaga" => $vals[$index["RH"][$day]]["value"], 
		"temp_rosisca" => $vals[$index["TD"][$day]]["value"], 
		"smer_vetra" => $vals[$index["DD_ICON"][$day]]["value"], 
		"hitrost_vetra" => $vals[$index["FF_VALUE"][$day]]["value"], 
		"sunki_vetra" => $vals[$index["FFMAX_VAL"][$day]]["value"], 
		"tlak" => $vals[$index["MSL_MB"][$day]]["value"], 
		"veljavna" => 1
	);
	print $vreme["za_dan"];
	return $vreme;
}

function insertNapoved($day) {
	$sql = "INSERT INTO vreme_napoved VALUES (";
	
	$vreme = forecastForDay($day);
	
	foreach($vreme as $f=>$v) {
		if ($v!=null && strlen($v)>0) {
			if (is_numeric($v))
				$sql .= $v . ", ";
			else 
				$sql .= "'" . $v . "', ";
		}
		else 
			$sql .= "null, ";
	}
	$sql = substr($sql, 0, strlen($sql)-2);
	$sql .= ")";
	
	print "<br />" . $sql;
	
	
	mysql_query($sql) or die(mysql_error());
	
	
}


mysql_connect("localhost", "api", "api123") or die(mysql_error());
mysql_select_db("mojsokol") or die(mysql_error());

mysql_query("update vreme_napoved set veljavna = 0 where datum = '" . date("Y-m-d") . "'");

insertNapoved(0);
insertNapoved(1);
insertNapoved(2);
	
mysql_close();

//Free the XML parser
xml_parser_free($parser);
xml_parser_free($parserAms);
?>
ok
