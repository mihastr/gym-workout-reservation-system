<?php
//ini_set("error_reporting", E_ALL);

function getUrl($url) {
	$content = null;
	// make sure curl is installed
	if (function_exists('curl_init')) {
	   // initialize a new curl resource
	   $ch = curl_init(); 
	
	   // set the url to fetch
	   curl_setopt($ch, CURLOPT_URL, $url); 
	
	   // don't give me the headers just the content
	   curl_setopt($ch, CURLOPT_HEADER, 0); 
	
	   // return the value instead of printing the response to browser
	   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	
	   // use a user agent to mimic a browser
	   curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.5) Gecko/20041107 Firefox/1.0'); 
	
	   $content = curl_exec($ch); 
	
	   // remember to always close the session and free all resources 
	   curl_close($ch); 
	} else {
	   // curl library is not installed so we better use something else
	   throw new Exception("curl not installed");
	}
	return $content;
}
//Initialize the XML parser
$parser=xml_parser_create();
$parserAms=xml_parser_create();

/*
$sql = "show COLUMNS from vreme";
mysql_connect("localhost", "api", "geslo123") or die(mysql_error());
mysql_select_db("mojsokol");
$q = mysql_query($sql) or die(mysql_error());
print "<pre>";
print "\n array (";
while ($row = mysql_fetch_array($q, MYSQL_ASSOC))  {
	/*
	 * (
    [Field] => datum
    [Type] => date
    [Null] => NO
    [Key] => PRI
    [Default] => 0000-00-00
    [Extra] => 
)
	/ * 
	print "\n\t" . $row["Field"] . ", " ;
}
print "\n}";
print "</pre>";
mysql_close();
*/

//$url = "http://darvinov.net/sokol_project/web/test.xml";
$url = "http://meteo.arso.gov.si/uploads/probase/www/observ/surface/text/sl/observation_LJUBL-ANA_BEZIGRAD_latest.xml";
$urlAms = "http://meteo.arso.gov.si/uploads/probase/www/observ/surface/text/sl/observationAms_LJUBL-ANA_BEZIGRAD_latest.xml";
//Open XML file
$find = array('�^�', 'ž', 'š');
$replace = array('c','z','s');
$data = str_replace($find, $replace, $data);

$data = str_replace($find, $replace, getUrl($url));
$dataAms = str_replace($find, $replace, getUrl($urlAms));
//print $data;


xml_parse_into_struct($parser, $data, $vals, $index);
xml_parse_into_struct($parserAms, $dataAms, $valsAms, $indexAms);


$vreme =  array (
	datum => date("Y-m-d"), 
	ura => date("H:00", time()+16*60), 
	kraj => "ljubljana",
	vreme => str_replace("ž", "z", str_replace("č", "c", $vals[$index["NN_SHORTTEXT"][0]]["value"])), 
	kolicina_oblacnosti => $vals[$index["NN_DECODETEXT"][0]]["value"], 
	temperatura =>  $vals[$index["T_DEGREESC"][0]]["value"],
	temp_ams =>  $valsAms[$indexAms["T"][0]]["value"],
	vlaga =>  $vals[$index["RH"][0]]["value"],
	temp_rosisca =>  $vals[$index["TD"][0]]["value"],
	hitrost_vetra =>  $vals[$index["FF_VAL_KMH"][0]]["value"],
	smer_vetra =>  $vals[$index["DD_ICON"][0]]["value"],
	sunki_vetra =>  $vals[$index["FFMAX_VAL_KMH"][0]]["value"],
	tlak =>  $vals[$index["MSL_MB"][0]]["value"],
	tlak_smer =>  $vals[$index["PA_SHORTTEXT"][0]]["value"],
	padavine_mm =>  $vals[$index["RR24H_VAL"][0]]["value"],
	sneg_cm =>  $vals[$index["SNOW"][0]]["value"],
	sneg_cm_nov =>  $vals[$index["SNOWNEW_VAL"][0]]["value"],
	
	temp_ams_min =>  $valsAms[$indexAms["TN"][0]]["value"],
	temp_ams_avg =>  $valsAms[$indexAms["TAVG"][0]]["value"],
	temp_ams_max =>  $valsAms[$indexAms["TX"][0]]["value"],
	vlaga_ams =>  $valsAms[$indexAms["RHAVG"][0]]["value"],
	hitrost_vetra_ams =>  $valsAms[$indexAms["FF_VAL"][0]]["value"],
	
	hitrost_vetra_avg_ams =>  $valsAms[$indexAms["FFAVG_VAL_KMH"][0]]["value"],
	hitrost_vetra_max_ams =>  $valsAms[$indexAms["FFMAX_VAL_KMH"][0]]["value"],
	tlak_avg_ams => $valsAms[$indexAms["MSLAVG"][0]]["value"],
	padavine_acc_stanje => $valsAms[$indexAms["RR_STATE"][0]]["value"],
	padavine_interval_mm_ams => $valsAms[$indexAms["RR_VAL"][0]]["value"],
	soncno_obsevanje_ams => $valsAms[$indexAms["GSUNRAD"][0]]["value"],
	soncno_obsevanje_avg_ams => $valsAms[$indexAms["GSUNRADAVG"][0]]["value"],
	temp_tal_avg_30cm_ams => $valsAms[$indexAms["TGAVG_30_CM"][0]]["value"],
	temp_tal_avg_10cm_ams => $valsAms[$indexAms["TGAVG_10_CM"][0]]["value"]
	);



$sql = "INSERT INTO vreme VALUES (";
foreach($vreme as $f=>$v) {
	if ($v!=null && strlen($v)>0) {
		if (is_numeric($v))
			$sql .= $v . ", ";
		else 
			$sql .= "'" . $v . "', ";
	}
	else 
		$sql .= "null, ";
}
$sql = substr($sql, 0, strlen($sql)-2);
$sql .= ")";

print $sql;
  /*
  }
  */
mysql_connect("localhost", "api", "api123") or die(mysql_error());
mysql_select_db("mojsokol") or die(mysql_error());
mysql_query($sql) or die(mysql_error());
mysql_close();

//Free the XML parser
xml_parser_free($parser);
xml_parser_free($parserAms);
?>
ok
