<?php session_start();
include_once '../config/dbconfig.php';
//pisi_log(-21, "obisk login.php", "login.php", 3);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Strict//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sokol group d.d.</title>
<link rel="Shortcut Icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="Stylesheet" type="text/css" href="css/stil.css" />
<link rel="stylesheet" type="text/css" href="css/custom-theme/jquery-ui-1.8.6.custom.css"  />
<style>
#loginContainer {
	padding-top: 30px;
	padding-bottom: 30px;
	background-color: white;
	margin-left: auto;
	margin-right: auto;
	width: 100%;
	overflow: auto;
}

#loginContainer table {
	border: none;
}

#loginContainer table th {
	border: none;
}

#loginContainer table thead th {
	text-align: center;
	padding-bottom: 30px;
}

#loginContainer table tbody th {
	text-align: right;
	padding-right: 10px;
	color: #650000;
}

#loginContainer table tbody td {
	padding: 5px;
}

input[type="text"],input[type="password"] {
	border: thin solid gray;
	width: 300px;
	padding: 7px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	font-weight: bold;
}

input[type="submit"] {
	border: thin solid black;
	background-color: gray;
	padding: 7px;
	margin-top: 20px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	font-weight: bold;
}

.helpDiv {
	display: none;
	padding: 10px;
	border: thin solid black;
	background-color: #DDD;
	padding: 7px;
	margin-left: 20px;
	-moz-border-radius: 5px;
	border-radius: 5px;
}

#waitIndicator {
	display: none;
	margin-top: 30px;
	margin-right: 20px;
}

a.notlink {
	cursor: default;
}

a.notlink:hover {
	cursor: default;
	text-decoration: none;
}
</style>

<!-- 
<script type="text/javascript" src="js/jquery-1.4.3.js"></script>
<script type="text/javascript" src="js/jquery.qtip-1.0.0-rc3.js"></script>
<script src="http://connect.facebook.net/en_US/all.js"></script>
 -->

<!-- script src="https://connect.facebook.net/en_US/all.js"></script-->

<script type="text/javascript" src="<?php print PageSettings::rootUrl ?>/min/?b=sokol_project/web/js&amp;f=jquery-1.5.min.js,json2.min.js,jquery.qtip-1.0.0-rc3.min.js,jquery-ui-1.8.6.custom.min.js"></script>



<script type="text/javascript">
var nacin = "";

function checkEmail(email) {
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)){
		return true;
	}
	//alert("Invalid E-mail Address! Please re-enter.");
	return false;
}

function registerUsingFB(sokol_id, email, geslo, useFBEmail) {
	var session = FB.getSession();
	var rObj = { "sokol_id": sokol_id, "email": email, "geslo1": geslo, "useFBEmail": useFBEmail, "fbsession": session };
	
	jQuery.post( "api?params=action=registerUsingFB", rObj, function(data, textStatus, XMLHttpRequest) {
		//alert("shranjeno");
		//alert("shranjeno " + textStatus);
		alert("shranjeno " + JSON.stringify(data));
		if (textStatus=="success")
			if (data.error=="no") {
				alert(JSON.stringify(data));
			} else {
				if (data.message.body.text=="wrongEmail") {
					alert("<?php print t("wrongEmail"); ?>");
				}
				alert("error");
			}
	}, "json" );
}

function openFBLogin() {
	FB.login(function(response) {
		  if (response.session) {
		    // user successfully logged in
			  doFBLogin();
		  } else {
		    // user cancelled login
			  alert("uporabnik je preklical");
		  }
	}, {perms:'email,publish_stream'});
}


function resolveLostPassword() {
	var email = $("#lostPassEmail").val();
	jQuery.post( "api?params=action=lostPassword", {"email": email }, function(data, textStatus, XMLHttpRequest) {
		//alert("shranjeno");
		//alert("shranjeno " + textStatus);
		//alert("shranjeno " + JSON.stringify(data));
		$("#lostPassErrorDIV").show();
		if (textStatus=="success")
			if (data.error=="no") {
				$("#lostPassErrorDIV").html("<?php print t("passResetMailSent"); ?>");
				//alert(JSON.stringify(data));
			} else {
				if (data.message.body.text=="wrongEmail") {
					$("#lostPassErrorDIV").html("<?php print t("wrongEmail"); ?>");
					//alert("<?php print t("wrongEmail"); ?>");
				} else {
					$("#lostPassErrorDIV").html("<?php print t("error"); ?>");
					//alert("<?php print t("error"); ?>");
				}
				
			}
	}, "json" );
}
function showGetPasswordDialog() {
	//alert("vprašaj za varnostno geslo");
	$("#fbLoginErrorDIV").hide();
	$("#fbLoginDialog").dialog("open");
	
	//1. geslo je pravo - opravimo login
	//2. geslo je napačno - obvestilo, javimo uporabniku
}

function fbLoginSubmit() {
	var pass = $("#fbLoginPassword").val();
	loginAndConnectFB("##fblogin##", pass, "fblogin");
}

function loginAndConnectFB(email, pass) {
	var session = FB.getSession();
	
	jQuery.post("checkLogin.php", {"userName": email, "userPassword": pass, "fbid": session.uid }, function(data) {
		//alert(JSON.stringify(data));
		if (data.message == "OK") {
			//alert("login ok");
			window.location = "index.php";
		} else {
			$("#fbLoginErrorDIV").show();
		}
	}, "json");
}

function showConnectWithFBDialog() {
	// poklicano če v bazi nismo našli računa s katerim bi lahko povezali fbuid
	$("#fbLoginDialogConnectWithFB").dialog("open");
	$("#fbNewAccount div.error").hide();
	// 1. uporabnik se je že registriral, in bo sedaj povezal račun s FB
		// prikaži dilaog za vnos:
		//	sokol_id številke ali e-naslova
		//  geslo
	// 2. uporabnik ni registriran, registriral se bo preko te aplikacije  želi uporabiti podate iz FB
		// prikaži dialog za registracijo (poenostavljen)
		//	sokol_id
		//  e-naslov
		//  e-naslov ponovno
		// 	geslo
		//  ponovno geslo
}
function doFBLogin() {
	var session = FB.getSession();
	if (session!=null)
	jQuery.post( "api?params=action=checkFBUID", {"fbuid": session.uid }, function(data, textStatus, XMLHttpRequest) {
		//alert("shranjeno");
		//alert("shranjeno " + textStatus);
		//alert("shranjeno " + JSON.stringify(data));
		if (textStatus=="success")
			if (data.error=="no") {
				if (data.message.body.text==1) {
					//alert("prijavljen");
					showGetPasswordDialog();
				} else if (data.message.body.text==0) { 
					//alert("noben račun ni povezan"); 
					showConnectWithFBDialog();
				} else {
					alert("napačen status");
				}
			} else {
				if (data.message.body.text=="paramFail") {
					alert("<?php print t("paramFail"); ?>");
				} else
				alert("error");
			}
	}, "json" );
}


$(document).ready(function(documentReadyEvent) {
	$.fn.enable = function() {
		$(this).removeAttr("disabled");
	};
	$.fn.disable = function() {
		$(this).attr("disabled", "disabled");
	};

	/*
	FB.getLoginStatus(function(response) {
		if (response.session) {
			doFBLogin();			
		}
	});

	FB.Event.subscribe('auth.login', function(response) {
		//alert("event prijevaljen s FB - se sproži ko se uporabnik priavi");

		// preveri ali je račun že povezan z sokolom in naredi določene akcije
		//alert(JSON.stringify(response));
		doFBLogin();
		
	})
	*/
	
	$(".helpLink").mouseenter(function(event) {
		var q = $(this).attr("no");
		//$(".helpDiv").hide();
		$("#" + q).fadeIn();
		
		event.preventDefault();
	});
	$(".helpLink").mouseleave(function(event) {
		var q = $(this).attr("no");
		$(".helpDiv").hide();
		//$("#" + q).fadeIn();
		
		event.preventDefault();
	});
	
	$("#loginForm").submit(function(event) {
		//alert("ne še no");
		$("#loginErrorDIV").hide();
		var user = $("#loginForm input[name='userName']").val();
		var pass = $("#loginForm input[name='userPassword']").val();
		jQuery.post("checkLogin.php", {"userName": user, "userPassword": pass}, function(data) {
			//alert(JSON.stringify(data));
			if (data.message == "OK") {
				//alert("login ok");
				window.location = "index.php";
			} else {
				$("#loginErrorDIV").show();
			}
		}, "json");
		event.preventDefault();
		
	});
	$("a.notlink").click(function (ev) {
		//$(this).parent().find("input:first").focus();
		ev.preventDefault();
	});

	$("a[title]").qtip({ style: { name: 'blue', tip: true } });
	$("p[title]").qtip({ style: { name: 'blue', tip: true } });

	$("#lostPassButton").click(function(cEv) {
		cEv.preventDefault();
		$("#lostPassDialog").dialog("open");	
	});

	$("#lostPassDialog").dialog({
		autoOpen: false,
		height: 300,
		width: 400,
		modal: true,
		closeOnEscape: true,
		buttons: {
				"<?php print t("send"); ?>": function() {
					resolveLostPassword();
				},
				"<?php print t("close"); ?>": function() {
					$(this).dialog("close");
				}
		}
	});



	
	// se odpre če je uporabnik uporabni FB storitve in če je logiran tam ter ima tukaj račun že povezan s FB 
	$("#fbLoginDialog").dialog({
		autoOpen: false,
		height: 290,
		width: 400,
		modal: true,
		closeOnEscape: true,
		buttons: {
				"<?php print t("login"); ?>": function() {
					fbLoginSubmit();
				},
				"<?php print t("close"); ?>": function() {
					$(this).dialog("close");
				}
		}
	});

	// se odpre v primeru da smo logirani v FB, uporabnik pa je dovolil uporabo storitve		
	$("#fbLoginDialogConnectWithFB").dialog({
		autoOpen: false,
		height: 400,
		width: 520,
		modal: true,
		closeOnEscape: true,
		buttons: {
				"<?php print t("login"); ?>": function() {
					if (nacin==null || nacin=="") nacin = "#fbNewAccount";
					if (nacin == "#fbNewAccount") {

						// izvedi registracijo
						var form = $("#fbNewAccountForm");
						var sokol_id = form.find("input[name='sokolID']").val();
						var email  = form.find("input[name='email']").val();
						var geslo1 = form.find("input[name='pass1']").val();
						var geslo2 = form.find("input[name='pass2']").val();
						var useFBEmail = $("#useFBEmailCBX").is(":checked");

						var errStr = "";
						if (sokol_id==null || sokol_id.length<4) {
							errStr += "<li>Številka kartice je obvezen podatek</li>";
						}
						if ((email==null || !checkEmail(email)) && ! $("#useFBEmailCBX").is(":checked")) {
							errStr += "<li>E-naslov je obvezen in v obliki ime@domena.si</li>";
						}
						if (geslo1==null || geslo1.length<4) {
							errStr += "<li>Geslo je obvezno in mora biti dolgo 4 ali več znakov</li>";
						} else if (geslo1!=geslo2) {
							errStr += "<li>Gesli se morata ujemati</li>";
						}
						if (errStr.length>0) {
							$("#fbNewAccount div.error").html("<ul>" + errStr + "</ul>");
							$("#fbNewAccount div.error").show();
						} else {
							// vse ok
							registerUsingFB(sokol_id, email, geslo1, useFBEmail);
						}
					} else if(nacin=="#fbOldAccount") {
						var email = $("#loginEmail").val();
						var pass = $("#loginPassword").val();
						loginAndConnectFB(email, pass);
					}
				},
				"<?php print t("close"); ?>": function() {
					$(this).dialog("close");
				}
		}
	});

	$("ul.menu li").click(function(cEv) {
		$("ul.menu li").removeClass("selected");
		$(this).addClass("selected");
		var link = $(this).attr("divid");

		// prenesemo v globalno spremenljivko
		nacin = link;
		
		var height = $(this).attr("divheight");
		
		$("#fbLoginDialogConnectWithFB div.ctab").hide();

		$("#fbLoginDialogConnectWithFB").dialog("option", "height", height);
		//alert(link);
		$(link).fadeIn();
	});


	$("#useFBEmailCBX").click(function() {
		if ($(this).is(":checked"))
			$("#accFBEmailField").disable();
		else 
			$("#accFBEmailField").enable();
	});

/*
	$("#fbLoginButton").click(function(cEV) {
		//cEV.preventDefault();
		openFBLogin();

	});
	*/
});
</script>
</head>
<body>
<div id="fb-root" style="display: none; visibility: hidden"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '177062205651059', // App ID
      channelUrl : '//zgodi.se/sokol/web/channel.html', // Channel File
      status     : true, // check login status
      cookie     : true, // enable cookies to allow the server to access the session
      xfbml      : true  // parse XFBML
    });

    // Additional initialization code here
  };

  // Load the SDK Asynchronously
  (function(d){
     var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement('script'); js.id = id; js.async = true;
     js.src = "//connect.facebook.net/en_US/all.js";
     ref.parentNode.insertBefore(js, ref);
   }(document));
</script>
<div id="fbLoginDialog" style="display:none" title="<?php print t("loginWithFB"); ?>">
<h3><?php print t("connectFBDialogTitle"); ?></h3>
<div id="fbLoginErrorDIV"
	style="padding: 10px; display: none; background-color: #F66"><?php print t("errorUserPass"); ?></div>
<p>Vnesite vaše geslo za sokol portal:</p>
<form action="#" onsubmit="fbLoginSubmit(); return false;">
<p align="center">
<input type="password" id="fbLoginPassword" /></p>
</form>
</div>
<div id="fbLoginDialogConnectWithFB" style="display: none;" title="<?php print t("connectFBDialogTitle"); ?>">
	<ul class="menu">
	<li class="selected" divid="#fbNewAccount" divheight="400"><a href="#fbNewAccount"><?php echo t("newAccount"); ?></a></li>
	<li 				 divid="#fbOldAccount" divheight="300"><a href="#fbOldAccount"><?php echo t("oldAccount"); ?></a></li>
	</ul>
	<div id="fbNewAccount" class="ctab">
		<div class="error"></div>
		<form id="fbNewAccountForm">
		<table>
		<tr>
			<td align="right"><?php print t("regEcheckNo"); ?>:</td><td align="left"><input name="sokolID" type="text" /></td>
		</tr>
		<tr height="30">
			<td>&nbsp;</td><td valign="bottom" align="left"><input type="checkbox" checked="checked" id="useFBEmailCBX" /> uporabi e-naslov iz FB</td>
		</tr>
		<tr>
			<td align="right"><?php print t("email"); ?>:</td><td align="left"><input disabled="disabled" name="email" id="accFBEmailField" type="text" /></td>
		</tr>
		<tr height="50">
			<td valign="bottom" align="right"><?php print t("password"); ?>:</td><td valign="bottom" align="left"><input name="pass1" type="password" /></td>
		</tr>
		<tr>
			<td align="right"><?php print t("regPassAgain"); ?>:</td><td align="left"><input name="pass2" type="password" /></td>
		</tr>
		</table>
		</form>
	</div>
	<div id="fbOldAccount" style="display:none"  class="ctab">
		<table>
		<tr>
			<td valign="middle" align="right"><?php print t("loginUsername"); ?>:</td><td align="left"><input id="loginEmail" type="text" /></td>
		</tr>
		<tr height="50">
			<td valign="middle" align="right"><?php print t("password"); ?>:</td><td valign="bottom" align="left"><input id="loginPassword" type="password" /></td>
		</tr>
		</table>
		
	</div>	
</div>
<div id="headerDiv">&nbsp;
<a href="index">
	<div id="header" style="float: left;"><img src="images/header.jpg" /></div>
</a>
<div id="dialog-modal" style="display: none;">napaka</div>
<div id="waitIndicator"><img src="images/ajax-loader3.gif" /></div>
<!-- 
	<h1><a style="color: white" href="index">MOJ SOKOL</a></h1>
	<H3>PRIJAVA NA URE VODENE VADBE</H3>
	 --></div>
<div id="loginContainer">
<div style="float: left; margin-right: 50px;"><img src="images/bp2.jpg" />
</div>
<div style="float: left; width: 300px; padding-left: 40px;">
<h3><?php print t("loginAboutServiceHeading"); ?></h3>
<p><?php 

$arr = explode("-", t("loginAboutServiceAbout"));

print $arr[0];
?>

<ul>
<?php for ($i = 1; $i < count($arr); $i++) {
	print "<li>" . $arr[$i] . "</li>";
}?>
</ul>
</p>
<!-- 
<p><a href="<?php print PageSettings::secureUrl; ?>">
	<img src="images/openssl.jpg" align="left" /> 
	Uporabite varno povezavo. 
	Opozorilo: certifikat je samopodpisan zato bo vas bo brskalnik na to opozoril, kljub temu je bolje da so podatki med prenosom zaščiteni. Uporabljeno je 256-bitno kodiranje. 
	Uporaba varne povezave je priporočljiva.
	</a>
</p>
-->
<p style="padding-top: 30px;" id="fbLoginButton">
<a href="#" id="fbLoginButton" onclick="javascript:openFBLogin()">
<img src="images/f.png" align="left" style="padding: 10px;" /> 
<p>Si uporabnik Facebooka - za prijavo lahko uporabiš svoj račun!</p>
</a>
</p>
</div>
<div style="float: left;">
<h3><?php print t("loginTeaser"); ?></h3> 
<form id="loginForm" action="index.php">
<div id="loginErrorDIV"
	style="padding: 10px; display: none; background-color: #F66"><?php print t("errorUserPass"); ?></div>
<table border="0">
	<tbody>

		<tr>
			<th>
			<p class="notlink" href="#"
				title="<?php print t("loginUsernameHelp"); ?>"><?php print t("loginUsername"); ?>
			</p>
			</th>
			<td><input type="text" name="userName" /></td>
		</tr>
		<tr>
			<th>
			<p class="notlink" href="#"
				title="<?php print t("loginPasswordHelp"); ?>"><?php print t("loginPassword"); ?>
			</p>
			</th>
			<td><input type="password" name="userPassword" /></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><input type="submit" value="<?php print t("login"); ?>" /> <?php print t("or"); ?>
			<a href="registracija"
				title="Če še nisi uporabnik se brezplačno registriraj in takoj prični uporabljati storitev">
				<?php print t("loginJoin"); ?> </a> <?php print t("or"); ?>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><a href="#" id="lostPassButton"><?php print t("lostPassword"); ?></a></td>
		</tr>

		<tr>
			<td colspan="2">
			<div style="float: left;"><?php print t("faq"); ?>
			<ul>
				<li><a class="helpLink" no="faq1" href="#">kje najdem sokol id</a></li>
				<li><a class="helpLink" no="faq2" href="#">pozabil sem geslo</a></li>
				<li><a class="helpLink" no="faq3" href="#">sem član vendar nimam
				gesla</a></li>
			</ul>
			</div>
			<div style="float: left; width: 300px; margin-top: 15px;">
			<div class="helpDiv" id="faq1">na kartici</div>
			<div class="helpDiv" id="faq2">klikni sem</div>
			<div class="helpDiv" id="faq3">dobiš na recepciji ali pa se
			registriraj tukaj, če si podal geslo dobiš na recepciji ali pa se
			registriraj tukaj, če si podal geslo dobiš na recepciji ali pa se
			registriraj tukaj, če si podal geslo dobiš na recepciji ali pa se
			registriraj tukaj, če si podal geslo dobiš na recepciji ali pa se
			registriraj tukaj, če si podal geslo dobiš na recepciji ali pa se
			registriraj tukaj, če si podal geslo</div>
			</div>
			</td>
		</tr>
	</tbody>
</table>
</form>
</div>
<div style="display: none" id="lostPassDialog">
<h3>Izgubljeno geslo:</h3>
<div id="lostPassErrorDIV" class="error" style="display: none;"></div>
<p>Vpišite svoj e-naslov, ki ste ga uporabili pri prijavi. Poslano vam bo elektronsko sporočilo z navodili:</p>
<p align="center"><input id="lostPassEmail" type="text" /></p>
</div>

</div>
<fb:like send="true" width="450" show_faces="true" />
</body>
</html>
