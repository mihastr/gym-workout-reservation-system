<?php
session_start();

include_once '../config/pageSettings.php';

$pageSetting = new PageSettings;

if (isset($_GET["page"])) {
	$page = "?page=" . $_GET["page"];
} else {
	$page = "";
}

$_SESSION["userRoleChanegP"] = true;
if (isset($_GET["role"]) && $_SESSION["userRoleChanegP"]) {
	if (key_exists($_GET["role"], $pageSetting->roleOptions)) {
		$_SESSION["newRole"] = $_GET["role"];
		header('Location: index'. $page);
		print 'vloga spremenjena';
	} else {
		die("vloga ne obstoji");
	}
} else {
	die("napaka pri uporabi programa");
}

