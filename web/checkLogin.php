<?php
session_start();
include_once '../config/dbconfig.php';
include_once '../models/mainModel.php';
//pisi_log(-21, "poskus prijave: " . json_encode($_POST), "checkLogin.php", 5);

if (!isset($_POST["userName"]) || !isset($_POST["userPassword"])) {
	print '{"message": "notOK", "params": ' . json_encode($_POST) .'}';
	pisi_log(-21, "napačen poskus prijave: " . json_encode($_POST), "checkLogin.php", 9);
	exit();
}
//$_SESSION["userName"] = $_POST["userName"];

$db = new DbConfig;

// query za logiranje uporabnika
//$res = $db->executeSelect("select * from uporabnik where (email='" . $_POST['userName'] . "' OR sokol_id='" . $_POST['userName'] . "') AND geslo=MD5('". $_POST["userPassword"] ."')");

// select * from 

//if (true || ($_SESSION["userName"]=="miha" || $_SESSION["userName"]=="admin") && $_POST["userPassword"]=="admin") {
$uporabnik = new Uporabnik;


// klasična prijava
if ($uporabnik->checkCredidentials($_POST['userName'], $_POST["userPassword"])) {
//if (count($res)>0) {
	//$_SESSION["role"] = "admin";
	//$_SESSION["userid"] = "1";
	//$_SESSION["can"]["ChangeRole"] = true;
	//$_SESSION["can"]["enterNewSchedule"] = true;
	
	// če je ob prijavi sporočil še id FB računa
	if (isset($_POST["fbid"]) && $_POST["fbid"]!="") {
		// login uspešen, račun lahko povežemo s FB
		$uporabnik->setAccountFBID($_POST["fbid"]);
	}
	
	print '{"message": "OK", "user": "' . $_SESSION["userName"] . '", "login": ' . json_encode($res) . '}';
	
// prijava z obstoječim FB računom
} else if (isset($_POST["fbid"]) && $_POST["fbid"]!="" && 
	isset($_POST["userName"]) && $_POST["userName"]=="##fblogin##" && 
	$uporabnik->checkCredidentialsWithFBID($_POST["fbid"], $_POST["userPassword"])) {
		
	print '{"message": "OK", "user": "' . $_SESSION["userName"] . '", "login": ' . json_encode($res) . '}';
	
// nobena od prijavnih poti ni uspela
} else {
	print '{"message": "notOK", "msgText": "userPassFailed"}';
}