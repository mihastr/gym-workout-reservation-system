<?php 
require_once '../config/mod.php';

$page = mapPage($_GET["page"]) or die("helper not found");

//print_r($_REQUEST);

$p = $_POST;

if ($page instanceof PageNotFoundHelper) {
	exit("stran ne obstaja");
}

$subPage = $_GET["sub"];


if (isset($_GET["action"]))
	$action = $_GET["action"];
else 
	$action = "";
	
/**

print " page " . $subPage;	
print " ";
print "action " . $action;
*/
	
switch ($subPage) {
	case "people":
		$page->uporabnik = new UporabnikController;
		switch ($action) {
			case "permissions": {
				include_once '../viewHelpers/views/adminView/people/permissions.php';
			} break;
			case "profile": {
				include_once '../viewHelpers/views/adminView/people/profile.php';
			} break;
			case "stats": {
				include_once '../viewHelpers/views/adminView/people/stats.php';
			} break;
			case "log": {
				include_once '../viewHelpers/views/adminView/people/log.php';
			} break;
			case "attendance": {
				include_once '../viewHelpers/views/adminView/people/attendance.php';
			} break;
			default: $page->getPeopleAdminView(); 
		}
		
	break;
	case "messaging":
		switch ($action) {
			case "getMsgRecepients":
				$page->getMsgRecepientsView(); 
				break;
			default:
				$page->getMessagingView();
		}
	break;
	case "attStats":
		$page->getAttStatsView();
	break;
	case "scheduleStats":
		$page->getScheduleStatsView();
	break;
	case "content":
		$page->getContentView();
	break;
	case "instructors":
		$page->getInstructorsAdminView();
	break;
	case "places":
		$page->getPlacesAdminView();
	break;
	case "settings":
		$page->getSettingsAdminView();
	break;
	case "translations":
		$page->getTranslationsView();
	break;
	case "bugreports":
		$page->getBugReportView();
	break;
	
	default: print "plz select on left";
	
}


?>
