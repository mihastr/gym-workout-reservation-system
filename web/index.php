<?php
include_once '../config/do_login.php';

include_once '../config/mod.php';
include_once '../config/html_templates/header.php';
if (isset($_GET["page"])) {
	$page = $_GET["page"];
} else {
	$page = 'index';
}

$pageObject = mapPage($page);
if (!is_object($pageObject)) include_once '../views/indexView.php';;

?>
<div id="topMenu" style="position:relaviteM">
	<?php $pageObject->renderTopMenu(); ?>
</div>
<div id="mainContainer">
<h2><?php echo $pageObject->title; ?></h2>

<?php $pageObject->renderHTML(); ?>
<?php 
include_once '../config/html_templates/footer.php';
?>
</div>