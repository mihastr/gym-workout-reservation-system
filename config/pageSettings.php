<?php

//print $_SERVER['DOCUMENT_ROOT'];

class PageSettings {
	
	const dateFormat = "d.m.Y";
	
	const dbDateFormat = "Y-m-d";
	
	const mysqlDateFormat = "%Y-%m-%d";

	const secureUrl = "https://192.168.2.59/sokol/web/login";

	//const libMinUrl = "http://localhost/dipl/project/web/js";
	
	//const mail_url = "http://127.0.0.1:8080/Mail/Mail";
	const mail_url = "http://192.168.2.10:8080/Mail/Mail";
	const mail_from = "mayki@siol.net";
	
	public static $dnevi = array("monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday");
	
	//const rootUrl = "http://192.168.2.10:81";
	//const rootUrl = "http://darvinov.net";
	const rootUrl = "https://zgodi.se";

	//const rootUrl = "http://mayki.dnsalias.com";
	const rootUrlSSL = "https://localhost";
	
	//const path = "localhost/dipl/project/web";
	//const path = "darvinov.net/sokol_project/web";
	const path = "zgodi.se/sokol/web";
	//const path = "mayki.dnsalias.com/dipl/project/web";
	
	//tomcat 7
	//const reportExporterURL = "http://127.0.0.1:8080/JasperWS/JasperService";
	const reportExporterURL = "http://192.168.2.10:8080/JasperWS/JasperService";
	
	//tomcat 6
	//const reportExporterURL = "http://localhost:9230/JasperWS/JasperService";
	
	const use_memcached = true;
	const memcached_url = "localhost";
	const memcached_port = 11211;
	
	//const proDir = "D:/wamp/www/dipl/project";
	//const proDir = "F:/Program Files/Apache Software Foundation/Apache2.2/htdocs/project";
	
	//const proDir = "/var/www/vhosts/darvinov.net/httpdocs/sokol_project";
	const proDir = "/var/www/sokol";
	
	private $html = null;
	private $role = "user";
	
	
	public $roleOptions = array(
		"guest" => array("index"=>"home"),
		"user"=> array("index"=>"home", "profil" => "profile", "inbox" => "messages"),
		"manager" => array("index"=>"home", "profil" => "profile", "inbox"=>"messages"),
		"admin" => array("index" => "home", "profil" => "profile", "sifranti" => "administration", "adminurniki" => "scheduleAdmin", "statistike" => "stats")
	);
	
	
	public function setRole($role) {
		if (key_exists($role, $this->roleOptions)) {
			$this->role = $role;
		}
	}

	function renderRoleOptions() {
		//echo "menu";
		//print_r($_SERVER['HTTPS']); 
		//print $_SESSION["can"]["ChangeRole"];
		if (isset($_SESSION["can"]["ChangeRole"]) && in_array($_SESSION["can"]["ChangeRole"], array("r","w", "u"))) {
			// continue
		} else {
			return;
		}
		
		$roles = array_keys($this->roleOptions);
		
		if ($this->html == null) {
			$this->html ="<div id='roleChangeDIV' style='float: right;'>
			zamenjaj vlogo (" . $this->role . "): <select id='roleChangeForm'>";
			foreach($roles as $role) {
				if ($role == $this->role) {
					$this->html .="<option selected>$role</option>";
				} else {
					$this->html .="<option>$role</option>";
				}
			}
			
			$this->html .="</select>";
		}
		print '<script>
		$(document).ready(function(drEvent) {
			$("#roleChangeForm").change(function(event) {
				var role = $(this).val();
				location = "switchRole?role=" + role;
			});
		});
		</script>';
		print $this->html;
		
	}
	public function renderMenuOptions($role) {
		//print "menu";
		if ($role == null) {
			$role = "guest";
		}
		//print $role;
		$curr = $_GET["page"];
		
		if ($curr==null) $curr="index";

		if (isset($this->roleOptions[$role])) {
			foreach ($this->roleOptions[$role] as $page=>$option) {
				if ($page==$curr)
					print "\n\t<li class='active'><a href=\"index?page=$page\">" . t($option) . "</a></li>";
				else
					print "\n\t<li><a href=\"index?page=$page\">" . t($option) . "</a></li>";
			}
		} else {
			print_r($options[$role]);
			throw new Exception("vloga ne obstaja");
		}
		echo "<div style='float:right;  margin-left: 20px; margin-right: 20px;'>";
		if (isset($_SESSION["userName"])) {
			print "<a href='logout'>(" . $_SESSION["userName"] . ") odjavi</a>";
		} else {
			print "<a href='login'>" . t("login") . "</a>";
		}
		echo " [<a href='#' id='reportBugButton'>skratek na strani</a>]";
		echo "</div>";
		
		
	}
	
}
