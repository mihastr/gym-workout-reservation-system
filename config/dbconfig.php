<?php
//session_start();

include_once 'pageSettings.php';

include_once '../libs/facebook-php-sdk-2-1-2/src/facebook.php';



function pisi_log($errno, $errstr, $errfile, $errline) {
	$uporabnik = mysql_escape_string(json_encode($_SESSION));
	if (isset($_SESSION['userid']))
	$userName = $_SESSION['userid'];
	else
	$userName = "";
	$sql = "insert into log values (null, '$errno', '" . mysql_escape_string($errstr) . "', now(), '" . $_SERVER['SCRIPT_FILENAME'] . "', '" . mysql_escape_string($errfile) . "', '$errline', '" . json_encode($_GET) . "', '" . json_encode($_POST) . "', '$uporabnik', '" . $userName . "', '" . $_SERVER["REMOTE_ADDR"] . "')";
	$conn = mysql_connect(DbConfig::host, DbConfig::user, DbConfig::pass);
	mysql_select_db("mojsokol") or die(mysql_error());
	mysql_query($sql) or die(mysql_error());
}



function main_error_handler($errno, $errstr, $errfile, $errline) {
	global $message;
	/*$sql = "insert into log values (null, '$errno', '" . mysql_escape_string($errstr) . "', now(), '" . $_SERVER['SCRIPT_FILENAME'] . "', '" . mysql_escape_string($errfile) . "', '$errline', '" . json_encode($_GET) . "', '" . json_encode($_POST) . "')";
	 $conn = mysql_connect(DbConfig::host, DbConfig::user, DbConfig::pass);
	 mysql_select_db("mojsokol") or die(mysql_error());
	 mysql_query($sql) or die(mysql_error());*/
	if ($errno!=8)
	pisi_log($errno, $errstr, $errfile, $errline);
}

set_error_handler("main_error_handler", E_ALL);

session_start();

include_once '../controlers/mainController.php';

$mainController = new MainController;
$db = new DbConfig;

function isAdminUser() {
	//print $_SESSION["userType"];
	if (isset($_SESSION["userType"]) && $_SESSION["userType"]=="admin") return true;
	else return false;
}

function getFromCache($tag) {
	global $mainController;

	if (PageSettings::use_memcached==false) return null;

	$t = $mainController->memcache->get("cache_" . $tag);

	if ($t===FALSE) return null;

	return $t;
}
function setToCache($tag, $value, $timeout=180) {
	global $mainController;

	if (PageSettings::use_memcached==false) return false;

	$mainController->memcache->set("cache_" . $tag, $value, false, $timeout);

	return true;

}

function t($tag) {
	global $mainController, $db;

	$t = false;
	if (PageSettings::use_memcached)
	$t = $mainController->memcache->get("tag_" . $tag);

	if ($t===FALSE) {
		//print " (ni keširano) ";

		$res = $db->executeSelect("select translation from prevodi where tag = '$tag'");
		if (count($res)==1) {
			$t = $res[0]["translation"];
		} else {
			return " not_found ($tag) ";
		}
		if (PageSettings::use_memcached)
		$mainController->memcache->set("tag_" . $tag, $t, false, 60);
	} else {
		//print " (JE keširano) ";
	}

	return $t;

}

function genRandString($length = 32, $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890') {

	$chars_length = (strlen($chars) - 1);
	$string = $chars{rand(0, $chars_length)};
	for ($i = 1; $i < $length; $i = strlen($string))
	{
		$r = $chars{rand(0, $chars_length)};
		if ($r != $string{$i - 1}) $string .=  $r;
	}
	return $string;
}

function nvl($obj, $def) {
	//print "nvl=[" + $obj + "]";
	if ($obj===null) return $def; else return $obj;
}

function isLoggedIn() {
	if (isset($_SESSION["userid"]) && is_numeric($_SESSION["userid"]) && $_SESSION["userid"]>0) {
		return true;
	} else {
		return false;
	}
}

function zapisi_prenos($fn) {
	$db = new DbConfig;
	pisi_log(-21, "debug: userid " . $_SESSION["userid"], "dbconfig.php", 112);
	return $db->insertNoCommit("prenos", array("original_fn" => $fn, "uporabnik_id" => $db->getUserID(), "ip" => $_SERVER['REMOTE_ADDR']));
}

function resize_image($target_path, $dest_path, $target_width, $target_height ) {
	pisi_log(-22, "image upload: " . $target_path, "dbconfig.php", 116);

	$extension = pathinfo($target_path);
	$allowed_ext = "jpg, gif, png, JPG, GIF, PNG, jpeg, JPEG";
	$extension = trim($extension[extension]);
	$allowed_paths = explode(", ", $allowed_ext);
	$ok = 0;

	for($i = 0; $i < count($allowed_paths); $i++) {
		if ($allowed_paths[$i] == "$extension") {
			$ok = "1";
		}
	}

	if ($ok == "1") {

		if($extension == "jpg" || $extension == "jpeg" || $extension == "JPG"){
			$tmp_image=imagecreatefromjpeg($target_path);
		}

		if($extension == "png" || $extension == "PNG") {
			$tmp_image=imagecreatefrompng($target_path);
		}

		if($extension == "gif" || $extension == "GIF") {
			$tmp_image=imagecreatefromgif($target_path);
		}

		$width = imagesx($tmp_image);
		$height = imagesy($tmp_image);

		//calculate the image ratio
		$imgratio = ($width / $height);

		if ($imgratio>1) {
			$new_width = $target_width;
			$new_height = ($target_width / $imgratio);
		} else {
			$new_height = $target_height;
			$new_width = ($target_height * $imgratio);
		}

		$new_image = imagecreatetruecolor($new_width,$new_height);
		imagecopyresized($new_image, $tmp_image,0,0,0,0, $new_width, $new_height, $width, $height);
		//Grab new image
		imagejpeg($new_image, $dest_path);
		$image_buffer = ob_get_contents();
		imagedestroy($new_image);
		imagedestroy($tmp_image);
	}
}

class MailSystem {
	public $from;
	public $to;
	public $subject;
	public $message;

	private $headers;
	private $params;

	function send() {
		mail($this->to, $this->subject, $this->message, $this->headers, $this->params);
	}
}

class DbConfig {
	const host = "localhost:3306";
	//const host = "192.168.2.6:3307";
	//const host = "mayki.dnsalias.com:3306";
	//const host = "192.168.2.59:3306";
	const user = "api";
	const pass = "api123";
	const database = "mojsokol";
	const dbType = "myslql";

	const fkFail = "fkFail";
	const unauthorized = "unauthorized";
	const paramFail = "paramFail";

	private $dbConnection = null;

	private $fString = "";

	function __construct() {
		$this->initDBConnection();
	}

	private function initDBConnection() {
		if (!isset($this->dbConnection)) {
			$this->dbConnection = mysql_connect(DbConfig::host, DbConfig::user, DbConfig::pass);
			mysql_set_charset('utf8',$this->dbConnection);
			//$this->dbConnection = mysql_connect(DbConfig::host, DbConfig::user, DbConfig::pass);

			if (!$this->dbConnection) {
				// če je napaka recimo "mysql_server has gone away" - potem poskusimo še enkrat
				$this->dbConnection = mysql_connect(DbConfig::host, DbConfig::user, DbConfig::pass);
				if ( !$this->dbConnection ) {
					pisi_log(mysql_errno(), "še vedno napaka: " . mysql_error(), "dbconfig.php", 48);
				} else {
					pisi_log(0, "ni bilo več napake", "dbconfig.php", 48);
				}

			}
			//pisi_log(mysql_errno(), mysql_error(), "dbconfig.php", 43);
			if ($this->dbConnection)
			{
				mysql_select_db(DbConfig::database) or die(mysql_error());
				//print "selecting db" . DbConfig::database;
				if (!mysql_set_charset('utf8', $this->dbConnection)) throw new Exception("unable to set charset to UTF-8");
			}
		}
		if (!$this->dbConnection) {
			throw new Exception("napaka: " . mysql_error(), mysql_errno());
		}
		return $this->dbConnection;
	}

	function addAND($f, $v) {
		$this->fString .= " AND $f='" . mysql_escape_string($v) . "'";
	}

	function addOR($f, $v) {
		$this->fString .= " OR $f='" . mysql_escape_string($v) . "'";
	}

	function getFilterString() {
		return $this->fString;
	}


	function getConnection() {
		return $this->dbConnection;
	}

	function requestAuth($redirLink) {
		$auth = genRandString(20);
		$this->insertNoCommit("avtorizacija", array("uporabnik_id" => $this->getUserID(), "auth_string" => $auth, "ip" => $_SERVER['REMOTE_ADDR'], "link" => $redirLink));
		return $auth;
	}

	function getUserID() {
		if (isset($_SESSION["userid"])) return $_SESSION["userid"];
		else return -1;
	}

	function executeSelect($sql) {
		$conn = $this->initDBConnection();
		//print_r($conn);
		//print $sql;
		$res = mysql_query($sql, $conn);
		if (!$res) {
			throw new Exception("napaka: " . mysql_error());
		}
		$resultList = array();

		while (($row=mysql_fetch_assoc($res))!=null) {
			array_push($resultList, $row);
		}
		//if (count($resultList)==0) return $sql;
		return $resultList;
	}

	function executeUpdate($table, $objekt, $isTransaction=false, $conn=null, $returnRow=false) {
		/*
		 $object = {
		 "pogoj": {
		 "id": "48"
		 },
		 "podatki": {
		 "prioriteta": "0"
		 }
		 }*/
		$sql = "";
		if (!$isTransaction)
		$conn = $this->initDBConnection();

		if (!$isTransaction)
		$this->beginTransaction();
		try {
			// pripravimo objekt
			$sets = "";
			foreach ($objekt["podatki"] as $f=>$v) {
				if ($v!="now()")
				$sets .= "$f='$v', ";
				else
				$sets .= "$f=now(), ";
			}
			//$sets = substr($sets, 0, strlen($sets)-2);
			//pripravimo pogoje
			$params = "";
			foreach ($objekt["pogoj"] as $f=>$v) {
				$params .= "$f='$v' AND ";
			}
			$params = substr($params, 0, strlen($params)-4);
			$sql = "update $table set $sets posodobljen=now() where $params";
			//pisi_log(-21, "test: " . $sql, "dbconfig.php", 133);
			//throw new Exception($sql);
			$res = mysql_query($sql, $conn);
			//print $sql;
			if (!$isTransaction)
			$this->commitTransaction();

			if (!$res) throw new Exception("ne morem posodobiti: " . $sql . "; " . mysql_error());

			if ($returnRow) {
				$sql = "select * from $table where $params";
				$res = $this->executeSelect($sql);
				if (!$res) throw new Exception("ne morem dobiti rezulata: " . mysql_error());
				return $res;
			}
		} catch (Exception $ex) {
			if (!$isTransaction)
			$this->rollbackTransaction();
			throw new Exception("napaka pri posodabljanju: " . $ex->getMessage() . " " . $ex->getTraceAsString() . "  " . mysql_error());
		}
	}

	function executeBulkUpdate($table, $objektList) {
		$this->beginTransaction();
		$conn = $this->initDBConnection();
		if (is_array($objektList) && count($objektList)>0)
		foreach ($objektList as $objekt) {
			$this->executeUpdate($table, $objekt, true, $conn);
		}
		$this->commitTransaction();
	}

	function beginTransaction() {
		mysql_query("begin", $this->dbConnection);
	}
	function commitTransaction() {
		mysql_query("commit", $this->dbConnection);
	}
	function rollbackTransaction() {
		mysql_query("rollback", $this->dbConnection);
	}

	function insertNoCommit($table, $record, $conn=null) {
		if ($conn==null) {
			$conn = $this->getConnection();
		}

		foreach ($record as $f=>$v) {
			if ($v!=-1)
			$record[$f] = "'" . mysql_escape_string($v) . "'";
			else
			$record[$f] = "null";
		}
		$record["ustvarjen"] = "now()";
			
		$f = implode(", ", array_keys($record));
		$v = implode(", ", array_values($record));

		$v = str_replace("'now()'", "now()", $v);

		$sql = "INSERT INTO $table ($f) VALUES ($v)";

		$res = mysql_query($sql, $conn);
		if (!$res) {
			throw new Exception("napaka: " . $sql . " " . mysql_error());
		}
		$id = mysql_insert_id($conn);

		return $id;
	}

	function updateOrInsertNoCommit($table, $record, $condition, $conn=null) {
		if ($conn==null) {
			$conn = $this->getConnection();
		}

		/*
		 * INSERT INTO table (a,b,c) VALUES (1,2,3)
		 ON DUPLICATE KEY UPDATE c=c+1;

		 */

		$polja = implode(",", array_keys($record)) . ", " . implode(",", array_keys($condition));
		//$vrednosti = implode(",", array_values($record)) . ", " . implode(",", array_values($condition));

		$vrednosti = "";
		foreach ($record as $f=>$v) {
			if ($v!=null && $v!='null') {
				$vrednosti .= "'$v', ";
			} else {
				$vrednosti .= "null, ";
			}
		}
		foreach ($condition as $f=>$v) {
			if ($v!=null && $v!='null') {
				$vrednosti .= "'$v', ";
			} else {
				$vrednosti .= "null, ";
			}
		}
		$vrednosti = substr($vrednosti, 0, strlen($vrednosti)-2);

		$zaUpdate = "";
		foreach ($record as $f=>$v) {
			if ($v!=null && $v!='null') {
				$zaUpdate .= " $f='$v', ";
			} else {
				$zaUpdate .= " $f=null, ";
			}
		}
		$zaUpdate = substr($zaUpdate, 0, strlen($zaUpdate)-2);

		$sql = "insert into $table ($polja) values ($vrednosti)
			ON DUPLICATE KEY UPDATE $zaUpdate";

		//print $sql;

		//throw new Exception("napaka: " . $sql . " " . mysql_error());

		//pisi_log(-20, "update sql", $sql, 191);

		$res = mysql_query($sql, $conn);
		if (!$res) {
			throw new Exception("napaka: " . $sql . " " . mysql_error());
		}
		return mysql_insert_id($conn);

	}


	function count($table, $params) {

		$condition="";
		foreach ($params as $f=>$v) {
			if ($v!="null")
			$condition .= " $f = '$v' AND ";
			else
			$condition .= " $f is null AND ";
		}
		$condition = substr($condition, 0, strlen($condition)-4);

		$sql = "select count(*) st from $table where $condition";

		$res = $this->executeSelect($sql);

		return $res[0]['st'];
	}


	function updateNoCommit($table, $record, $condition, $conn=null) {
		if ($conn==null) {
			$conn = $this->getConnection();
		}
		$sqlSet = "";
		$sqlCondition = "";
		foreach ($record as $f=>$v) {
			if ($v!=null) {
				if ($v!="now()")
				$sqlSet .= " $f = '$v', ";
				else
				$sqlSet .= " $f = now(), ";
			} else {
				$sqlSet .= " $f=null, ";
			}
		}
		//throw new Exception("sql set" . $sqlSet);
		$sqlSet = substr($sqlSet, 0, strlen($sqlSet)-2);



		foreach ($condition as $f=>$v) {
			$sqlCondition .= " $f='$v' AND ";
		}
		$sqlCondition = substr($sqlCondition, 0, strlen($sqlCondition)-4);

		$sql = "update $table set $sqlSet where $sqlCondition";

		//throw new Exception("napaka: " . $sql . " " . mysql_error());

		//pisi_log(-20, "update sql", $sql, 191);

		$res = mysql_query($sql, $conn);
		if (!$res) {
			throw new Exception("napaka: " . $sql . " " . mysql_error());
		}

	}

	function insertObject($table, $object, $isTransaction=true) {
		if (! $object instanceof MainModel) throw new Exception("ne morem zgraditi objekta");

		$fields = $object->getFields();

		$f = implode(", ", array_keys($fields));
		$v = implode(", ", array_values($fields));

		$v = str_replace("''", "null", $v);
		$v = str_replace("'now()'", "now()", $v);


		$sql = "INSERT INTO $table ($f) VALUES ($v)";

		//print $sql;

		$conn = $this->initDBConnection();

		if ($isTransaction)
		mysql_query("begin", $conn);

		$res = mysql_query($sql, $conn);
		if (!$res) {
			throw new Exception("napaka: " . $sql . " " . mysql_error());
			mysql_query("rollback", $conn);
		}
		$id = mysql_insert_id($conn);

		$sql = "select * from $table where id='$id'";

		$res = $this->executeSelect($sql);
		if (!$res) {
			mysql_query("rollback", $conn);
			throw new Exception("napaka: " . $sql . " " . mysql_error());
		}
		if ($isTransaction)
		mysql_query("commit", $conn);

		return $res;
	}
	function executeSql($sql) {
		$conn = $this->initDBConnection();
		return mysql_query($sql, $conn) or die(mysql_error());
	}

}

function getCombobox($list, $id) {
	$res = "<select id='$id'><option value='-1' disabled='disabled' selected='selected'>----</option>";
	foreach ($list as $item) {
		$res .= "<option value='" . $item["id"] . "'>" . (isset($item["kratkoIme"])? $item["kratkoIme"] : $item["naziv"]) . "</option>";
	}
	return $res . "</select>";
}

function convertToTR($array) {
	//print_r($array);
	$rows = "";
	if (count($array)>0) {
		foreach($array as $row) {
			$rows .= "\n\t<tr class='normal' rowid='" . $row["id"] . "'>";
			foreach($row as $ftype=>$field) {
				$rows .= "<td ftype='$ftype'>";
				switch ($ftype) {
					case "veljaven":
						$rows .= ($field==1) ? "da" : "ne";
						break;
					case "veljaOD":
						if ($field!=null)
						$rows .= date( 'd.m.Y', strtotime($field) );
						break;
					case "veljaDO":
						if ($field!=null)
						$rows .= date( 'd.m.Y', strtotime($field) );
						break;
					case "ustvarjen":
						if ($field!=null)
						$rows .= date( 'd.m.Y H:i', strtotime($field) );
						break;
					case "posodobljen":
						if ($field!=null)
						$rows .= date( 'd.m.Y H:i', strtotime($field) );
						break;
					default: {
						$rows .= $field;
					}
				}
				$rows .= "</td>";
			}
			$rows .= '<td><a href="#" class="cloneSchedule">kloniraj</a></td>';
			$rows .= '</tr>';
		}
	}
	return $rows;

}

function doPost($url, $data) {

	$Curl_Session = curl_init($url);
	curl_setopt ($Curl_Session, CURLOPT_POST, 1);
	
	//"Name=$Name&Email=$Email&Message=$Message"
	$post_string = "";
	foreach ($data as $f=>$v) {
		$post_string .= "$f=$v&";
	}
	
	curl_setopt ($Curl_Session, CURLOPT_POSTFIELDS, $post_string);
	curl_setopt ($Curl_Session, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt ($Curl_Session, CURLOPT_RETURNTRANSFER,1);
	$res = curl_exec ($Curl_Session);
	curl_close ($Curl_Session);
	
	return $res;
}

function get_url_https($url,$refer = "",$usecookie = false) {

	if ($usecookie) {
		if (file_exists($usecookie)) {
			if (!is_writable($usecookie)) {
				return "Can't write to $usecookie cookie file, change file permission to 777 or remove read only for windows.";
			}
		} else {
			$usecookie = "cookie.txt";
			if (!is_writable($usecookie)) {
				return "Can't write to $usecookie cookie file, change file permission to 777 or remove read only for windows.";
			}
		}
	}

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");

	if ($usecookie) {
		curl_setopt($ch, CURLOPT_COOKIEJAR, $usecookie);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $usecookie);
	}

	if ($refer != "") {
		curl_setopt($ch, CURLOPT_REFERER, $refer );
	}

	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	$result =curl_exec ($ch);

	curl_close ($ch);
	return $result;
}
function get_url($url) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.2; en-GB; rv:1.8.1.7) Gecko/20070914 Firefox/2.0.0.7');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$user_data = curl_exec($ch);
	curl_close($ch);
	print $user_data;
	return $user_data;
}

function get_facebook_cookie($app_id, $application_secret) {
	$args = array();
	parse_str(trim($_COOKIE['fbs_' . $app_id], '\\"'), $args);
	ksort($args);
	$payload = '';
	foreach ($args as $key => $value) {
		if ($key != 'sig') {
			$payload .= $key . '=' . $value;
		}
	}
	if (md5($payload . $application_secret) != $args['sig']) {
		return null;
	}
	return $args;
}

function check_email_address($email) {
	if (!preg_match("[/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i]", $email)) {
		return true;
	}
	return false;

}

function datePrintFormated($mysqlDate, $format=null) {
	$date = strtotime($mysqlDate);
	if ($date!=null)
	if ($format==null)
		return date(PageSettings::dateFormat, $date);
	else 
		return date($format, $date);
	else return "";
}
function getDayName($mysqlDate) {
	$date = strtotime($mysqlDate);
	if ($date!=null)
	return t(date("l", $date));
	else return "";
}
?>
