<?php 
ob_start();
//require_once '../../pageSettings.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Strict//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="Shortcut Icon" href="images/favicon.ico" type="image/x-icon" />
<title>Sokol group d.d.</title>

<link rel="stylesheet" type="text/css" href="js/dist/jquery.jqplot.css" />
<link rel="stylesheet" type="text/css" href="css/stil.css" />
<link rel="stylesheet" type="text/css" href="css/custom-theme/jquery-ui-1.8.6.custom.css"  />
<link rel="stylesheet" type="text/css" href="js/jquery.uploadify-v2.1.4/uploadify.css" />

<!-- 
<script type="text/javascript" src="js/jquery-1.4.3.js"></script>
 -->
<!--  

<script type="text/javascript" src="js/jquery-ui-1.8.6.custom.min.js"></script>
<script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>


<script type="text/javascript" src="js/jquery-1.5.min.js"></script>
<script type="text/javascript" src="js/json2.min.js"></script>
<script type="text/javascript" src="js/date.js"></script>
<script type="text/javascript" src="js/autoresize.jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.qtip-1.0.0-rc3.min.js"></script>
<script type="text/javascript" src="js/skripts.js"></script>
<script language="javascript" type="text/javascript" src="js/dist/jquery.jqplot.min.js"></script>
jquery.qtip-1.0.0-rc3.min.js
-->

<!--
<script type="text/javascript" src="<?php echo PageSettings::rootUrl ?>/min/?b=sokol_project/web/js&amp;f=jquery-1.5.min.js,skripts.js,jquery.tablesorter.min.js,jquery.tablesorter.pager.js,json2.min.js,date.js,autoresize.jquery.min.js,jquery.qtip-1.0.0-rc3.min.js,dist/jquery.jqplot.min.js,jquery-ui-1.8.6.custom.min.js&debug=true"></script>
-->
<script type="text/javascript" src="<?php echo PageSettings::rootUrl ?>/min/?b=sokol_project/web/js&amp;f=jquery-1.6.2.min.js,skripts.js,jquery.tablesorter.min.js,jquery.tablesorter.pager.js,json2.min.js,date.js,autoresize.jquery.min.js,jquery.qtip-1.0.0-rc3.min.js,dist/jquery.jqplot.min.js,jquery-ui-1.8.6.custom.min.js&debug=true"></script>
<script type="text/javascript" src="<?php echo PageSettings::rootUrl ?>/min/?b=sokol_project/web/js/jquery.uploadify-v2.1.4&amp;f=jquery.uploadify.v2.1.4.min.js,swfobject.js&debug=true"></script>


<script type="text/javascript" language="javascript">
//var shramba = $.webStorage.session();
var imenaDnevov = ['ponedeljek', 'torek', 'sreda', 'četrtek', 'petek', 'sobota', 'nedelja'];
var imenaDnevovKratka = ['ne', 'po', 'to', 'sr', 'če', 'pe', 'so' ];
var formatDatuma = "dd.mm.yy";

var imenaMesecov = ['Januar', 'Februar', 'Marec', 'April', 'Maj', 'Junij', 'Julij', 'Avgust', 'September', 'Oktober', 'November', 'December'];
var imenaMesecovKratka = ['Jan', 'Feb', 'Mar', 'Apr', 'Maj', 'Jun', 'Jul', 'Avg', 'Sep', 'Okt', 'Nov', 'Dec'];

var waitIndicator = null;
var obvestiloDialog = null;



var gumbi = {
		ok: { "zapri": function() { $(this).dialog("close"); } }
};

var dialog = {
		naslov: "obvestilo",
		gumbi: gumbi.ok
};

var calConfig = {
		changeMonth: true,
		currentText: 'Now',
		changeYear: true,
		dateFormat: formatDatuma, 
		altFormat: 'yyyy-mm-dd',
		dayNamesMin: imenaDnevovKratka,
		monthNames: imenaMesecov,
		monthNamesShort: imenaMesecovKratka,
		firstDay: 1
	};
var windowP = {
		height: $(window).height(),
		width: $(window).width()
	};

$(document).ready(function(documentReadyEvent) {

	$.jqplot.config.enablePlugins = true; // on the page before plot creation.
	
	windowP = {
			height: $(window).height(),
			width: $(window).width()
		};
	waitIndicator = $("#waitIndicator");
	obvestiloDialog = $("#dialog-modal");

	$.datepicker.setDefaults( $.datepicker.regional[ "sl" ] );
	$(".helpLink").click(function(event) {
		var q = $(this).attr("no");
		$(".helpDiv").hide();
		$("#" + q).show();
		event.preventDefault();
	});

	$( "#dialog-modal" ).dialog({
		height: 'auto',
		modal: true,
		autoOpen: false,
		title: dialog.naslov,
		buttons: dialog.gumbi,
		resizable: true
	});
	
	$("#loginForm").submit(function(event) {
		//alert("ne še no");
		$("#loginErrorDIV").hide();
		var user = $("#loginForm input[name='userName']").val();
		var pass = $("#loginForm input[name='userPassword']").val();
		$.post("checkLogin.php", {"userName": user, "userPassword": pass}, function(data) {
			//alert(JSON.stringify(data));
			if (data.message == "OK") {
				//alert("login ok");
				window.location = "index.php";
			} else {
				$("#loginErrorDIV").show();
			}
		}, "json");
		event.preventDefault();
		
	});
});
</script>


</head>
<body>
<div id="fb-root" style="display: none; visibility: hidden"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '177062205651059', // App ID
      channelUrl : '//zgodi.se/sokol/web/channel.html', // Channel File
      status     : true, // check login status
      cookie     : true, // enable cookies to allow the server to access the session
      xfbml      : true  // parse XFBML
    });

    // Additional initialization code here
  };

  // Load the SDK Asynchronously
  (function(d){
     var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement('script'); js.id = id; js.async = true;
     js.src = "//connect.facebook.net/en_US/all.js";
     ref.parentNode.insertBefore(js, ref);
   }(document));
</script>
<div id="headerDiv">&nbsp;
	<div id="header" style="float: left;"><a href="index"><img src="images/header.jpg" /></a></div>
	<div id="dialog-modal" style="display:none;"> napaka </div>
	<div id="waitIndicator"><img src="images/ajax-loader3.gif" /></div>
	<!-- 
	<h1><a style="color: white" href="index">MOJ SOKOL</a></h1>
	<H3>PRIJAVA NA URE VODENE VADBE</H3>
	 -->
</div>
<div id="loginDialog">
Prijavi se, lahko je:
<form>
<table>
<tr>
<td><a href="login">PRIJAVI SE</a></td>
</tr><tr>
<td><a href="#">ne hvala</a></td>
</tr>
</table>
</form>
</div>
<div id="reportBugDialog" style="display:none" title="Prijavi nepravilnost na portalu">
	<table>
		<tr><td align="right">nivo: </td><td align="left"><select id="reportBugFiledInfo"><option>info</option><option>vsebina</option><option>tehnicna</option></select></td></tr>
		<tr><td align="right">prioriteta: </td><td align="left"><select id="reportBugFiledPriority"><option>nizka</option><option>srednja</option><option>visoka</option></select></td></tr>
		<tr><td align="right">Opis napake: </td></tr><tr><td colspan="2" align="left"><textarea id="reportBugTextBox" name="reportBugText" cols="10" rows="5"></textarea></td></tr>
	</table>
</div>
<div id="sifrantiDialog">
<h2 ftype="naslov">To je naslov šifranta</h2>
<p ftype="navodilo">Navodilo</p>
<input type="hidden" id="sifObjekt" />
<div id="tabelaSifrant">
<table width="100%">
<thead>
<tr ftype="stolpci"></tr>
<tr ftype="vnosnaPolja"></tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div>

