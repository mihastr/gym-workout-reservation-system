<?php
include_once 'UrnikQueries.php';
class Queries {
    // dobimo seznam vseh fitnes centrov
    const date_format = "%d.%c.%Y";
	const getUrniki = "select u.id, u.naziv urnik, u.veljaOD, u.veljaDO, u.prioriteta, u.veljaven, u.ustvarjen, u.posodobljen, f.kratkoIme fc, p.naziv prostor
	    from urnik u 
	    join prostor p on u.prostor_id=p.id
	    join fitnescenter f on p.fitnescenter_id=f.id
	    order by u.prioriteta, u.ustvarjen desc";
	const getUrnikiByFC = "select u.id, u.naziv urnik, u.veljaOD, u.veljaDO, u.prioriteta, u.veljaven, u.ustvarjen, u.posodobljen, f.kratkoIme fc, p.naziv prostor
	    from urnik u 
	    join prostor p on u.prostor_id=p.id
	    join fitnescenter f on p.fitnescenter_id=f.id
	    where f.id='{p1}'
	    order by u.prioriteta, u.posodobljen asc, u.ustvarjen asc";
	const getUrnikiByProstor = "select u.id, u.naziv urnik, date_format(u.veljaOD, '%d.%c.%Y') veljaOD, date_format(u.veljaDO, '%d.%c.%Y') veljaDO, u.prioriteta, u.veljaven, u.ustvarjen, u.posodobljen, f.kratkoIme fc, p.naziv prostor
	    from urnik u 
	    join prostor p on u.prostor_id=p.id
	    join fitnescenter f on p.fitnescenter_id=f.id
	    where p.id='{p1}'
	    order by u.prioriteta, u.posodobljen asc, u.ustvarjen asc";
	
	const getUrnikiByID = "select u.id, u.naziv urnik, date_format(u.veljaOD, '%d.%c.%Y') veljaOD, date_format(u.veljaDO, '%d.%c.%Y') veljaDO, u.prioriteta, u.veljaven, u.ustvarjen, u.posodobljen, f.kratkoIme fc, p.naziv prostor
	    from urnik u 
	    join prostor p on u.prostor_id=p.id
	    join fitnescenter f on p.fitnescenter_id=f.id
	    where u.id='{p1}'
	    order by u.prioriteta, u.posodobljen asc, u.ustvarjen asc";
	
	const getUreByUrnikID = "SELECT u.id, v.naziv, v.id vid, i.kratkoIme instruktor, i.id iid, u.dan, time_format(u.ura, '%H:%i') ura
    FROM ura u 
        left outer join instruktor i on i.id=u.instruktor_id
        left outer join vadba v on v.id = u.vadba_id
        where urnik_id='{p1}' AND (u.veljaven_do is null OR u.veljaven_do > now())";
	
	const getInstruktorji = "select * from instruktor where veljaven={veljaven} and (veljaDO>now() or veljaDO is null) order by dolgoIme";
	const getInstruktorjiAdmin = "select * from instruktor";
	const getInstruktorjiFiltered = "select * from instruktor where 1=1 {filter}";
	const getVadbe = "select * from vadba where (veljaDO>now() or veljaDO is null) {filter} order by naziv";
	const getFitnesCentri = "select * from fitnescenter where veljaven=1 and (veljaDO>now() or veljaDO is null)";
	const getFCProstori = "select * from prostor where fitnescenter_id='{p1}' and veljaven=1 and (veljaDO>now() or veljaDO is null)";
	const getProstori = "select * from prostor where veljaven=1 and (veljaDO<now() or veljaDO is null)";
	
	const getTranslation = "select * from prevodi where lang='{lang}' and (tag like '{tag}%' or translation like '{q}%') limit 20";
	
	const get7DUrnik = "select 
			ura.id, ura.dan, time_format(ura.ura, '%H:%i') ura, v.naziv, v.opis, i.opis oInstruktorju, i.kratkoIme trener, i.id iid, v.id vid, 		
			case when v.kalorij is null then 0 else v.kalorij end 'kalorij', 
			case when ud.`status`='prijavljen' or ud.`status`='potrjen' then 'prijavljen' else ud.`status` end 'status',
			CASE
				when uo.omejitev is not null then if(count(ud2.uporabnik_id)>=uo.omejitev, 0, 1) 
				else if(count(ud2.uporabnik_id)>=pr.kapaciteta, 0, 1) 
			END
			mozna_prijava, u.veljaOD, u.veljaDO, ud.datum, if(ou.ura_id is not null, 1, 0) odpovedana, ou.datum datum_odpovedi, ou.razlog, v.logo    
		from urnik u 
			join ura on (ura.urnik_id=u.id)
			left outer join vadba v on (v.id=ura.vadba_id) 
			left outer join instruktor i on (ura.INSTRUKTOR_id=i.id)
			left outer join ura_omejitev uo on (uo.vadba_id=ura.VADBA_id and uo.prostor_id=u.prostor_id)
			left outer join udelezba ud on (ud.uporabnik_id={uporabnik} and ura.id=ud.ura_id and (ud.`status` = 'prijavljen' or ud.`status`='potrjen') AND (ud.datum is null or ud.datum>=date_sub(now(), INTERVAL 1 DAY)))  
			left outer join udelezba ud2 on (ura.id=ud2.ura_id and (ud2.`status` = 'prijavljen' or ud2.`status`='potrjen') AND (ud2.datum is null or ud2.datum>=date_sub(now(), INTERVAL 1 DAY)))  
			left outer join odpovedana_ura ou on (ou.ura_id =ura.id AND ou.datum>=date_sub(now(), INTERVAL 1 DAY))
			left outer join urnik on urnik.id = ura.URNIK_id
			left outer join prostor pr on pr.id = urnik.prostor_id
		where u.prostor_id = {prostor} AND u.veljaven= 1
		  AND u.veljaDO>=CURDATE() AND NOT u.veljaOD>CURDATE()+7
		  AND (ura.veljaven_do is null OR ura.veljaven_do > now())
		  AND (ura.INSTRUKTOR_id is not null or ura.vadba_id is not null)
		group by ura.id, ud.datum
		order by dan, ura, urnik.prioriteta, u.veljaOD, u.ustvarjen, ud.datum desc";
	
	const get7DUrnikBasic = "select 
			ura.id, ura.dan, time_format(ura.ura, '%H:%i') ura, v.naziv, v.opis, i.opis oInstruktorju, i.kratkoIme trener, i.id iid, v.id vid, 		
			case when v.kalorij is null then 0 else v.kalorij end 'kalorij', 
			/*case when ud.`status`='prijavljen' or ud.`status`='potrjen' then 'prijavljen' else ud.`status` end 'status', */
			CASE
				when uo.omejitev is not null then if(count(ud2.uporabnik_id)>=uo.omejitev, 0, 1) 
				else if(count(ud2.uporabnik_id)>=pr.kapaciteta, 0, 1) 
			END
			'mozna_prijava', u.veljaOD, u.veljaDO, ud2.datum, if(ou.ura_id is not null, 1, 0) odpovedana, ou.datum datum_odpovedi, ou.razlog, v.logo    
		from urnik u 
			join ura on (ura.urnik_id=u.id)
			left outer join vadba v on (v.id=ura.vadba_id) 
			left outer join instruktor i on (ura.INSTRUKTOR_id=i.id)
			left outer join ura_omejitev uo on (uo.vadba_id=ura.VADBA_id and uo.prostor_id=u.prostor_id)
			/*left outer join udelezba ud on (ud.uporabnik_id={uporabnik} and ura.id=ud.ura_id and (ud.`status` = 'prijavljen' or ud.`status`='potrjen') AND (ud.datum is null or ud.datum>=date_sub(now(), INTERVAL 1 DAY)))  */
			left outer join udelezba ud2 on (ura.id=ud2.ura_id and (ud2.`status` = 'prijavljen' or ud2.`status`='potrjen') AND (ud2.datum is null or ud2.datum>=date_sub(now(), INTERVAL 1 DAY)))  
			left outer join odpovedana_ura ou on (ou.ura_id =ura.id AND ou.datum>=date_sub(now(), INTERVAL 1 DAY))
			left outer join urnik on urnik.id = ura.URNIK_id
			left outer join prostor pr on pr.id = urnik.prostor_id
		where u.prostor_id = {prostor} AND u.veljaven= 1
		  AND u.veljaDO>=CURDATE() AND NOT u.veljaOD>CURDATE()+7
		  AND (ura.veljaven_do is null OR ura.veljaven_do > now())
		  AND (ura.INSTRUKTOR_id is not null or ura.vadba_id is not null)
		group by ura.id , ud2.datum
		order by dan, ura, urnik.prioriteta, u.veljaOD, u.ustvarjen /*, ud.datum desc*/";
	
	const get7DUrnikDetail = "select 
		ura.id, ura.dan, time_format(ura.ura, '%H:%i') ura, v.naziv, v.opis, i.opis oInstruktorju, i.kratkoIme trener, i.id iid, v.id vid, count(ud.uporabnik_id) prijav, 
		case when uo.omejitev is not null then uo.omejitev
		     when uo.omejitev is null then pr.kapaciteta
		end
		'omejitev', u.veljaOD, u.veljaDO, ud.datum, if(ou.ura_id is not null, 1, 0) odpovedana, ou.datum datum_odpovedi, ou.razlog
		from urnik u 
			join ura on (ura.urnik_id=u.id)
			left outer join vadba v on (v.id=ura.vadba_id) 
			left outer join instruktor i on (ura.INSTRUKTOR_id=i.id)
			left outer join ura_omejitev uo on (uo.vadba_id=ura.VADBA_id and uo.prostor_id=u.prostor_id)  
			left outer join udelezba ud on (ura.id=ud.ura_id and ud.`status` in ('prijavljen', 'potrjen') AND (ud.datum is null or ud.datum>=date_sub(now(), INTERVAL 1 DAY)))  
			left outer join odpovedana_ura ou on (ou.ura_id =ura.id AND ou.datum>=date_sub(now(), INTERVAL 1 DAY))
			left outer join urnik on urnik.id = ura.URNIK_id
			left outer join prostor pr on pr.id = urnik.prostor_id
		where u.prostor_id = {prostor} AND u.veljaven='1'
		  AND u.veljaDO>=CURDATE() AND NOT u.veljaOD>CURDATE()+7
		  AND (ura.veljaven_do is null OR ura.veljaven_do > now())
		  AND (ura.INSTRUKTOR_id is not null or ura.vadba_id is not null)
		group by ura.id, ud.datum
		order by dan, ura, urnik.prioriteta, u.veljaOD, u.ustvarjen, ud.datum desc";
	
	const checkEmailExistance = "select 1 from uporabnik u where u.email='{email}';";
	
	const getVsiProstorList = "SELECT
		fitnescenter.kratkoIme naziv_fc,
		prostor.naziv naziv_prostora, 
		prostor.id pid
		FROM
		prostor
		INNER JOIN fitnescenter ON prostor.fitnescenter_id = fitnescenter.id
		WHERE fitnescenter.veljaven=1 AND prostor.veljaven=1 and (prostor.veljaDO is null or prostor.veljaDO>now())
		ORDER BY fitnescenter.id";
	
	const getVsiProstorListFiltered = "SELECT
		fitnescenter.kratkoIme naziv_fc,
		prostor.naziv naziv_prostora, 
		prostor.id pid
		FROM
		prostor
		INNER JOIN fitnescenter ON prostor.fitnescenter_id = fitnescenter.id
		WHERE fitnescenter.veljaven=1 AND prostor.veljaven=1 and (prostor.veljaDO is null or prostor.veljaDO>now())
		and (prostor.id in (
			SELECT prostor_id from uporabnik_prostor up where up.uporabnik_id = {uporabnik} and up.prikazi=1
			) or prostor.id not in (
				SELECT up2.prostor_id from uporabnik_prostor up2 where up2.uporabnik_id = {uporabnik})
		)
		ORDER BY fitnescenter.id;";
	
	const getObvestila = "SELECT o.*, concat(u.ime, ' ', u.priimek) posiljatelj, brisan 
		FROM obvestilo o
	  join uporabnik u on (o.od=u.id)
		WHERE (brisan is null or not brisan='1') and  (now() BETWEEN o.veljaOD AND o.veljaDO OR o.veljaDO is NULL)  AND prikazan=\"da\" AND (o.od={uporabnik} or o.za=\"vsi\" or o.id IN 
		(SELECT obvestilo_id 
		 FROM obvestilo_uporabnik ou 
		 WHERE ou.uporabnik_id={uporabnik}))
		 ORDER BY o.id DESC";
	
	const getMsgRecepients = "SELECT u.id, u.ime, u.priimek, case when (u.naziv is not null) then CONCAT(' - (', u.naziv,')') else '' end vzdevek
		from obvestilo_uporabnik ou 
		  join uporabnik u on (u.id=ou.uporabnik_id)
		where ou.obvestilo_id={id}";
	
	const getProstoriVadbeOmejitve = "select p.id prid, f.naziv fitnescenter, p.naziv prostor, u.omejitev, p.kapaciteta, u.vadba_id vid
		from prostor p
			left outer join ura_omejitev u on p.id=u.prostor_id and u.vadba_id={vadba}
			join fitnescenter f on f.id=p.fitnescenter_id
		order by f.id, p.id
		 
			";
	
	const getUporabnikDovoljenja = "select * 
		from dovoljenje d JOIN uporabnik_dovoljenje ud on (ud.dovoljenje_id=d.id)
		where ud.uporabnik_id={uporabnik}";
	
	const getAtendees = "SELECT up.id, concat(up.ime, ' ', up.priimek) ime, up.sokol_id, gsm, email 
		FROM `udelezba` ud join uporabnik up on (ud.uporabnik_id=up.id) 
		WHERE ud.datum='{datum}' AND ud.ura_id={ura} AND `status` in ('prijavljen', 'potrjen')";
	
	const getClassInfo = "SELECT ura.id, ud.datum, ura.dan, ura.ura, i.dolgoIme 'instruktor', v.naziv 'vadba',
			CASE
			  WHEN uo.omejitev is not null then uo.omejitev
				WHEN uo.omejitev is null and p.kapaciteta is not null then p.kapaciteta
				ELSE 0
			END 'omejitev',
			COUNT(ud.uporabnik_id) 'prijav'
		FROM ura
			JOIN vadba v on v.id=ura.VADBA_id
			JOIN instruktor i on i.id = ura.INSTRUKTOR_id
			JOIN urnik u on u.id = ura.URNIK_id
			JOIN prostor p on p.id = u.prostor_id
			LEFT OUTER JOIN ura_omejitev uo on uo.prostor_id = p.id and uo.vadba_id=v.id
			LEFT OUTER JOIN udelezba ud on ud.ura_id=ura.id and ud.datum='{datum}' and ud.`status` = 'prijavljen'
		WHERE ura.id = {ura}
		GROUP BY ura.id";
	
	const getFCsForUser = "select fc.id 'fid', p.id 'pid', fc.naziv 'center', p.naziv 'prostor', case when up.prikazi is null then 1 else up.prikazi end 'prikazi'
		from fitnescenter fc 
		join prostor p on (p.fitnescenter_id=fc.id) and p.veljaven=1 and (p.veljaDO is null or p.veljaDO>now())
		left outer join uporabnik_prostor up on (p.id = up.prostor_id and up.uporabnik_id={uporabnik})";
	
	const getVeljavnostKarte = "select veljavnost_karte from uporabnik where id={uporabnik}";
	
	const getMinMaxDatumUdelezbe = "SELECT min(datum) 'min_datum', max(datum) 'max_datum' from udelezba";
	
	const getOsnovnaStatistikaOdDo = "select ura.id, v.id vid, i.id iid, ud.datum, ura.ura, v.naziv, i.dolgoIme, i.kratkoIme, COUNT(ud.uporabnik_id) 'udelezba'
		,vr.vreme, vr.temp_ams, vr.kolicina_oblacnosti /*, vn.vreme 'vreme_napoved', vn.temp 'temp_napoved' */
		FROM  ura 
			join urnik ur on ura.URNIK_id=ur.id
			left outer join vadba v on (v.id=ura.VADBA_id)
			left outer join instruktor i on (i.id=ura.INSTRUKTOR_id)
			left outer join udelezba ud on ( ura.id = ud.ura_id  )
			left outer join vreme vr on vr.datum=ud.datum and hour(vr.ura)=hour(ura.ura)
			/*left outer join vreme vr on vr.datum=ud.datum and hour(ura.ura)=hour(vr.ura)
			left outer join vreme_napoved vn on (vn.za_dan=ud.datum and vn.n_dni=5 and vn.veljavna=1)*/
		WHERE 
			ud.datum BETWEEN '{od}' AND '{do}'
			AND ur.prostor_id = {prostor}
			AND ud.`status` in ('prijavljen', 'potrjen')
		GROUP BY ud.datum, ura.ura
		ORDER BY ud.datum, ura.ura";
	
	const getUporabnikZgodovinaObiska = "SELECT ud.datum, ura.ura, fc.naziv 'center', prostor.naziv 'prostor', v.naziv 'vadba', i.dolgoIme 'instruktor', ud.`status`
		FROM udelezba ud
			JOIN ura on ura.id = ud.ura_id
			JOIN vadba v on v.id = ura.VADBA_id
			JOIN instruktor i on i.id = ura.INSTRUKTOR_id
			JOIN urnik on urnik.id = ura.URNIK_id
			JOIN prostor on urnik.prostor_id = prostor.id
			JOIN fitnescenter fc on fc.id = prostor.fitnescenter_id
		WHERE ud.uporabnik_id={uporabnik}";
	
	const countUserFBUID = "select count(*) st from uporabnik where fbid = {fbuid}";
	
	const getVremeForTime = "select * from vreme where datum='{datum}' and ura='{ura}'";
	
	const getVreme = "select * from vreme order by datum desc, ura desc limit 1";
	
	const getNapovediForTime = "SELECT * 
		from vreme_napoved 
		where n_dni = {n_dni} and datum = '{datum}' and veljavna=1";
	
	const getUporabnikBodociObiski = "select ura.id, u.datum, ura.ura, v.naziv, i.kratkoIme, f.kratkoIme 'fc', p.naziv 'prostor'
		from udelezba u 
		join ura on u.ura_id=ura.id
		join vadba v on v.id=ura.VADBA_id
		join instruktor i on i.id=ura.INSTRUKTOR_id
		join urnik ur on ura.URNIK_id=ur.id
		join prostor p on ur.prostor_id=p.id
		join fitnescenter f on f.id=p.fitnescenter_id
		where u.uporabnik_id={id}
		and u.`status`='prijavljen'
		and u.datum>date(now())
		order by u.datum desc, ura.ura desc";
	
	const getProstoriVSI = "select p.id pid, f.kratkoIme center, p.naziv prostor,
		case when f.veljaven=0 or p.veljaven=0 
			  or p.veljaDO<now() or f.veljaDO<now() or f.veljaOD>now() or p.veljaOD>now()
			then 0
			else 1
		end as 'veljaven'
		from fitnescenter f 
		join prostor p on f.id=p.fitnescenter_id";
	
	const userGetPermissions = "SELECT 
		vsa.id, case when dodeljena.uporabnik_id is not null and dodeljena.rwu is not null then 1 else 0 end 'izbran'
				, vsa.naziv, vsa.modul, dodeljena.rwu
		 from (
			SELECT * FROM uporabnik_dovoljenje ud where ud.uporabnik_id={id}
		) dodeljena right OUTER join (
			SELECT * FROM dovoljenje d
		) vsa
		on vsa.id = dodeljena.dovoljenje_id or dodeljena.dovoljenje_id is null
		ORDER BY vsa.modul, vsa.naziv";
}
?>