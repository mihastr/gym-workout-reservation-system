<?php
//session_start();
//$proDir = "D:/wamp/www/dipl/project";
date_default_timezone_set("Europe/Berlin");
include_once 'pageSettings.php';

$to_include = array(
 "/config/dbconfig.php",
 "/config/queries/queries.php",
 "/controlers/dbtestControler.php",

 "/controlers/mainController.php",
 
 "/viewHelpers/indexHelper.php",
 "/viewHelpers/adminUrnikHelper.php",
 "/viewHelpers/adminHelper.php",
 "/viewHelpers/statsHelper.php",
 "/viewHelpers/profileHelper.php",
 "/viewHelpers/inboxHelper.php",
 
 "/viewHelpers/pageNotFoundHelper.php"
);

foreach ($to_include as $file) {
	include_once PageSettings::proDir. '/' . $file;
}

// zemljevid strani -> razredov
$map = array(
	"index" => "IndexHelper",
	"adminurniki" => "AdminUrnikHelper",
	"sifranti" => "AdminHelper", 
	"statistike" => "StatsHelper",
	"profil" => "ProfileHelper",
	"inbox" =>  "InboxHelper"
);


function mapPage($page) {
	global $map;
	$cls = null;
	if (isset($map[$page]))
		eval("\$cls = new {$map[$page]};");
	else
		eval("\$cls = new PageNotFoundHelper;");
	return $cls;
}

function checkPermission($permission) {
	if (isset($_SESSION["can"][$permission]) && $_SESSION["can"][$permission]=true)
		return true;
	else
		return false;
}
