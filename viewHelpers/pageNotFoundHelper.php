<?php
include_once('main.php');
class PageNotFoundHelper extends MainHelper {
	function __construct() {
		$this->title = "SPLETNA STRAN NE OBSTAJA";
		$this->html = "vsebina prve strani";
	}
	function renderHTML() {
		print "<script>
		$(document).ready(function() { waitIndicator.hide(); });		
		</script>";
		include_once 'views/404view.php';
	}
}