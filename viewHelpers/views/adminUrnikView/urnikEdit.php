	<script language="javascript">
		$(document).ready(function(event) {
			$("#calDetailMeta input[ftype='veljaOD']").datepicker(calConfig);
			$("#calDetailMeta input[ftype='veljaDO']").datepicker(calConfig);
			$("#calDetailMeta input.calDetailInput").click(function(ev) {
				//alert("klik");
			});
		});
	</script>
	<h2 id="calNameField">...</h2>
	<table id="calDetailMeta">
		<tr><th>naziv</th>
			<td>
				<input ftype="urnik" dirty="0" class="calDetailInput" type="text" />
				<input ftype="urnik_id" type="hidden" />	
			</td>
		</tr>
		<tr><th>velja od</th>
			<td>
				<input ftype="veljaOD" dirty="0" class="calDetailInput" type="text" />
			</td>		
		</tr>
		<tr><th>velja do</th>
			<td>
				<input ftype="veljaDO" dirty="0" class="calDetailInput" type="text" />
			</td>
		</tr>
		<tr><th>aktiven</th>
			<td>
			  <div id="calDetailVeljaven">
			    <input type="radio" value="1" name="calDetailVeljaven" id="vDA" /><label for="vDA">DA</label>
			    <input type="radio" value="0" name="calDetailVeljaven" id="vNE" /><label for="vNE">NE</label>
			  </div>
			</td>
		</tr>
		<tr><th>ustvarjen</th>
			<td>
				<input ftype="ustvarjen" dirty="0" class="calDetailInput" type="text" disabled="disabled" />
			</td>
		</tr>
		<tr><th>zadnja sprememba</th>
			<td>
				<input ftype="posodobljen" dirty="0" class="calDetailInput" type="text" disabled="disabled" />
			</td>
		</tr>
	</table>
	<p>Pred vami je orazec za urejanje urnika. Prosim da iz seznama na desni  strani za vsakjo uro, ki jo želite vnesti izberete inštruktorja ter vadbo.</p>
	<p>Po zaključenem urejanju ne pozabite pritiniti gumb SHRANI (desno spodaj)</p>
	<div style="float:left">
	<table id="weekTableSchedukeEdit" border="1">
	<thead>
	  <tr>
	  	<th>ura</th>
	  	<th>ponedeljek</th>
	  	<th>torek</th>
	  	<th>sreda</th>
	  	<th>četrtek</th>
	  	<th>petek</th>
	  	<th>sobota</th>
	  	<th>nedelja</th>
	  </tr>
	</thead>
	<tbody>
	<?php
	$startTime = 0;
	for ($i=(6*60); $i<(24*60); $i+=30) {
		$ura = sprintf("%2s:%2s", str_pad(floor($i/60), 2, '0', STR_PAD_LEFT), str_pad(($i%60), 2, '0', STR_PAD_LEFT));
		print "<tr class='normal' ura='" . $ura . "'>";
		print "
		<th>" . $ura . "</th>
		<td dan='1'><div class='forVadba'></div> <div class='forInstruktor'></div></td>
	  	<td dan='2'><div class='forVadba'></div> <div class='forInstruktor'></div></td>
	  	<td dan='3'><div class='forVadba'></div> <div class='forInstruktor'></div></td>
	  	<td dan='4'><div class='forVadba'></div> <div class='forInstruktor'></div></td>
	  	<td dan='5'><div class='forVadba'></div> <div class='forInstruktor'></div></td>
	  	<td dan='6'><div class='forVadba'></div> <div class='forInstruktor'></div></td>
	  	<td dan='7'><div class='forVadba'></div> <div class='forInstruktor'></div></td>";
		print "</tr>";
	}
	?>
	</tbody>  
	</table>
	</div>
	<div id="adminUrnikSifrant">
	<p class="ui-widget-header">tukaj primi in premakni</p>
	<h3>Na izbiro imate sledeče inštruktorje ter vadbe</h3>
	
	<div>
	<ul id="seznamVadb">
	<li t="V" name="BP" rowid="1">Body pump</li>
	<li t="V" name="BP" rowid="2">Body atack</li>
	<li t="V" name="BP" rowid="3">Body vive</li>
	</ul>
	<a href="#" id="urediSeznamVadbLink">hitro urejanje vadb</a>
	</div>
	<div>
	<ul id="seznamInstruktorjev" class="seznamInstruktorjev">
	<li t="I" name="I1" rowid="1">Inštruktor 1</li>
	<li t="I" name="I2" rowid="2">Inštruktor 2</li>
	<li t="I" name="I3" rowid="3">Inštruktor 3</li>
	</ul>
	<a href="#" id="urediSeznamInstruktorjevLink">hitro urejanje inštruktorjev</a>
	</div>
	</div>