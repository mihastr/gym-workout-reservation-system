<script>

function saveChanges() {
	var chList = [];
	$("table#fcUserlistTable tbody tr").each(function (i, obj) {
		var pid = $(obj).attr("pid");
		var old = ($(obj).attr("prikazi")==1 ? 1 : 0);
		var nVal = $(obj).find("input:checked").val();
		if (nVal != old) {
			chList.push({ "pid": pid, "value": nVal });
		}
	});
	if (chList.length>0) {
		waitIndicator.show();
		jQuery.post( "api.php?params=action=profileChangeFcLis4User", {"data": chList }, function(data, textStatus, XMLHttpRequest) {
			//alert("shranjeno");
			//alert("shranjeno " + textStatus);
			//alert("shranjeno " + JSON.stringify(data));
			if (textStatus=="success")
				if (data.error=="no") {
					//alert(prostor);
					//$("#noticeField").html("shranjeno");
					//$("#noticeField").show().fadeOut(5000);
					//$("#urnikColTable tbody").prepend(data.message.body.text).hide().fadeIn();
					waitIndicator.hide();
				}
			}, "json" );
	}
};

$(document).ready(function(rEvent) {
	$("#userAttendanceTable") 
    .tablesorter({sortList: [[0,1], [1,1]], widthFixed: true, widgets: ['zebra']}) 
    .tablesorterPager({container: $("#pager")});
	
	waitIndicator.hide();
	$("#userProfileData a.profileFieldEdit").click(function (cEvent) {
		cEvent.preventDefault();
		var row = $(this).parent().parent();
		var editable = row.find(".editable");
		var oldValue = editable.text();
				
		editable.html("<input type='text' value='" + oldValue + "' /> <a href='#' action='ok'><img src='images/ok.png' /></a> <a href='#' action='cancel'><img src='images/cancel.png' /></a>");
	});

	function changeProfile(field, value) {
		jQuery.post( "api.php?params=action=changeProfile", {"field": field, "value": value}, function(data, textStatus, XMLHttpRequest) {
			//alert("shranjeno");
			//alert("shranjeno " + textStatus);
			//alert("shranjeno " + JSON.stringify(data));
			if (textStatus=="success")
				if (data.error=="no") {
					//$("#noticeField").html("shranjeno");
					//$("#noticeField").show().fadeOut(5000);
					//$("#urnikColTable tbody").prepend(data.message.body.text).hide().fadeIn();
					waitIndicator.hide();
					
					//alert("all ok");
				} else {
					alert("napaka, prosimo kontaktirajte Sokol - pomoč");
				}
			}, "json" );
	}

	$("#genNewRSSCode").click(function(cEv) {
		cEv.preventDefault();
		jQuery.post( "api.php?params=action=generateRSSCode", {}, function(data, textStatus, XMLHttpRequest) {
			//alert("shranjeno");
			//alert("shranjeno " + textStatus);
			//alert("shranjeno " + JSON.stringify(data));
			if (textStatus=="success")
				if (data.error=="no") {
					//$("#noticeField").html("shranjeno");
					//$("#noticeField").show().fadeOut(5000);
					//$("#urnikColTable tbody").prepend(data.message.body.text).hide().fadeIn();
					waitIndicator.hide();
					//alert(data.message.body.text);
					$("#rssCodeSpan").text(data.message.body.text);		
					//alert("all ok");
				} else {
					alert("napaka, prosimo kontaktirajte Sokol - pomoč");
				}
			}, "json" );
	})
	
	$("#userProfileData td.editable a").live("click", function (cEvent) {
		cEvent.preventDefault();
		var action = $(this).attr("action");
		var row = $(this).parent().parent();
		var oldValue = row.attr("old");
		var td = row.find(".editable");
		var ftype = row.attr("ftype");
		var colname = row.attr("colname");
		switch (action) {
		case "ok":
			var value = td.find("input").val();
			if (ftype == "email") {
				
				if (!checkEmail(value)) {
					alert("E-naslov mora biti v pravilni obliki primer: primer@naslov.com");
					return;
				}
				td.text(value);
			} else {
				td.text(value);
			}
			changeProfile(colname, value);
			
			break;
		case "cancel":
			td.text(oldValue);
			break;			
		default:
			break;
		}
	});

	$("input.fcRadio").click(function() {
		$("input#fcSaveChangesButton").enable();
	});

	$("input#fcSaveChangesButton").click(function() {
		saveChanges();
		$("input#fcSaveChangesButton").disable();
	});

	$("#passChangeDialog").dialog({
		autoOpen: false,
		height: 280,
		width: 400,
		modal: true,
		closeOnEscape: true,
		buttons: {
			"<?php echo t('change');?>": function() {
				var pass1 = $("#passChangeDialog input[name='pass1']").val();
				var pass2 = $("#passChangeDialog input[name='pass2']").val();
				if (pass1!=pass2) {
					$("#passChangeDialog p.error").text("obe polji z geslom morata biti enali");
					$("#passChangeDialog p.error").show();
				} else if (pass1==pass2 && pass1.length<4) {
					$("#passChangeDialog p.error").text("geslo mora biti dolgo vsaj 4 znake");
					$("#passChangeDialog p.error").show();
				} else {
					// spremeni geslo
					$("#passChangeDialog p.error").text("prosimo počakajte");
					$("#passChangeDialog p.error").show();
					jQuery.post( "api.php?params=action=changePassword", { "pass": pass1 }, function(data, textStatus, XMLHttpRequest) {
						//alert("shranjeno");
						//alert("shranjeno " + textStatus);
						//alert("shranjeno " + JSON.stringify(data));
						if (textStatus=="success")
							if (data.error=="no") {
								$("#passChangeDialog").dialog("close");
							} else {
								alert("Geslo ni bilo spremenjeno, kontaktirajte pomoč.");
							}
					}, "json");
				}
			}, 
			"<?php echo t('close');?>": function() {
				$(this).dialog("close");
			}, 
			
		},
		close: function() {
			//alert("ne se no");
			}
	});
	
	$("#passChangeButton").click(function(cEv) {
		$("#passChangeDialog").dialog("open");
	});
});

</script>

<div style="display: none;" id="passChangeDialog" title="<?php echo t("passChangeDialog"); ?>">
<h3><?php echo t("passChangeTitle"); ?></h3>
<p><?php echo t("passChangeInstruction"); ?></p>
<p class="error" style="display: none;"></p>
<form id="passChangeForm">
<input type="password" name="pass1" />
<input type="password" name="pass2" />
</form>
</div>

<div id="profileDIV">
<h2><?php print $this->naziv; ?></h2>

<div class="roundedTableCorner" id="userProfileData">
<table>
	<tr ftype="text" colname="naziv" old="<?php print $this->profil["naziv"]; ?>">
		<th><?php print t("nickname"); ?></th>
		<td class="editable"><?php print $this->profil["naziv"]; ?></td>
		<td class="tableActions"><a href="#" class="profileFieldEdit"><?php echo t("edit"); ?></a></td>
	</tr>
	<tr ftype="text" colname="naslov" old="<?php print $this->profil["naslov"]; ?>">
		<th><?php print t("address"); ?></th>
		<td class="editable"><?php print $this->profil["naslov"]; ?></td>
		<td class="tableActions"><a href="#" class="profileFieldEdit"><?php echo t("edit"); ?></a></td>
	</tr>
	<tr ftype="email" colname="email" old="<?php print $this->profil["email"]; ?>">
		<th><?php print t("email"); ?></th>
		<td class="editable"><?php print $this->profil["email"]; ?></td>
		<td class="tableActions"><a href="#" class="profileFieldEdit"><?php echo t("edit"); ?></a></td>
	</tr>
	<tr ftype="text" colname="gsm" old="<?php print $this->profil["gsm"]; ?>">
		<th><?php print t("gsm"); ?></th>
		<td class="editable"><?php print $this->profil["gsm"]; ?></td>
		<td class="tableActions"><a href="#" class="profileFieldEdit"><?php echo t("edit"); ?></a></td>
	</tr>
	<tr ftype="text" colname="kontakt_preko" old="<?php print $this->profil["kontakt_preko"]; ?>">
		<th><?php print t("defContact"); ?></th>
		<td class="editable"><?php print $this->profil["kontakt_preko"]; ?></td>
		<td class="tableActions"><a href="#" class="profileFieldEdit"><?php echo t("edit"); ?></a></td>
	</tr>
	<tr>
		<th>zasebna koda (za RSS ter iCal)</th>
		<td><span id="rssCodeSpan"><?php print $this->profil["rsskoda"]; ?></span> </td>
		<td><a href="#" id="genNewRSSCode"><?php echo t("generateCode"); ?></a></td>
	</tr>
	<tr>
		<th><?php echo t("changePassword"); ?></th>
		<td>************</td>
		<td><a href='#spremeniGeslo' id="passChangeButton"><?php echo t("change"); ?></a></td>
	</tr>

</table>
</div>

<h3><?php print t("fcUserConn"); ?></h3>
<div class="roundedTableCorner" id="userProfileData">
<table id="fcUserlistTable">
<thead>
<tr>
	<th><?php print t("fc") ?></th><th><?php print t("place") ?></th><th><?php print t("show") ?></th>
</tr>
</thead>
<tbody>

<?php 
$fcList = $this->getFcListForUser();
foreach ($fcList as $item) {
	//print_r($item);
	print "<tr fid='" . $item["fid"] . "' pid='" . $item["pid"] . "' prikazi='" . $item["prikazi"] . "'>";
	print "<td>" . $item["center"] . "</td><td>" . $item["prostor"] . "</td>";
	print "<td>
		<label for='place" . $item[pid] . "'>" . t("yes") . "<input class='fcRadio' value='1' name='place" . $item["pid"] . "'  " . (($item["prikazi"]== 1 ) ? "checked='checked'" : "") . " type='radio' /></label> 
		<label for='place" . $item[pid] . "'>" . t("no") . " <input class='fcRadio' value='0' name='place" . $item["pid"] . "'  " . (($item["prikazi"]== 0 ) ? "checked='checked'" : "") . " type='radio' /></label>
		</tr>";
}
?>
</tbody>
</table>
<input type="button" id="fcSaveChangesButton" value="<?php print t("save"); ?>" disabled="disabled" /> 

</div>

<h3><?php print t("userAttendanceHistory"); ?></h3>
	<div style="min-height: 250px;">
		<div class="roundedTableCorner" id="userAttendanceHistory"> 
		
		<div id="pager" class="pager" style="display: inline;">
					<form>
						<img src="images/first.png" class="first"/>
						<img src="images/prev.png" class="prev"/>
						<input type="text" disabled="disabled" class="pagedisplay"/>
						<img src="images/next.png" class="next"/>
						<img src="images/last.png" class="last"/>
						<select class="pagesize">
							<option selected="selected"  value="10">10</option>
				
							<option value="20">20</option>
							<option value="30">30</option>
							<option  value="40">40</option>
						</select>
					</form>
				</div>
		
		
			<table id="userAttendanceTable" class="sortableTable styledTable">
			<thead>
			<tr>
				<th><?php print t("date") ?></th>
				<th><?php print t("time") ?></th>
				<th><?php print t("fc") ?></th>
				<th><?php print t("place") ?></th>
				<th><?php print t("class") ?></th>
				<th><?php print t("instructor") ?></th>
				<th><?php print t("status") ?></th>
			</tr>
			</thead>
			<tbody>
			<?php 
			$list = $this->getUserAttendanceHistory();
			if (is_array($list)) {
				foreach ($list as $item) {
					print "\n<tr>";
					foreach($item as $f) {
						print "<td>$f</td>";
					}
					print "</tr>";
				}
			} else {
				print $list;
			}
			?>
			</tbody>
			</table>
		</div>
	</div>
	
</div>
