<!-- 
<script language="javascript" type="text/javascript" src="js/dist/plugins/jqplot.canvasTextRenderer.min.js"></script>
<script language="javascript" type="text/javascript" src="js/dist/plugins/jqplot.canvasAxisTickRenderer.min.js"></script>  
<script language="javascript" type="text/javascript" src="js/dist/plugins/jqplot.categoryAxisRenderer.min.js"></script>
<script language="javascript" type="text/javascript" src="js/dist/plugins/jqplot.barRenderer.min.js"></script> 
<script language="javascript" type="text/javascript" src="js/dist/plugins/jqplot.highlighter.js"></script> 
<script language="javascript" type="text/javascript" src="js/dist/plugins/jqplot.cursor.min.js"></script>
<script language="javascript" type="text/javascript" src="js/dist/plugins/jqplot.dateAxisRenderer.min.js"></script>
<script language="javascript" type="text/javascript" src="js/dist/plugins/jqplot.pieRenderer.min.js"></script>
 -->

<script type="text/javascript" src="/min/b=sokol_project/web/js/dist/plugins&amp;f=jqplot.canvasTextRenderer.min.js,jqplot.canvasAxisTickRenderer.min.js,jqplot.categoryAxisRenderer.min.js,jqplot.barRenderer.min.js,jqplot.highlighter.js,jqplot.cursor.min.js,jqplot.dateAxisRenderer.min.js,jqplot.pieRenderer.min.js"></script>

<script language="javascript">
var action = window.location.hash.split("#")[1];

if (action ==undefined || action.length==0) {
	action = "attStats";
}

function doAction() {
	waitIndicator.show();
	window.location.hash = action;

	$("ul.menuVertical li.selected").removeClass("selected");
	$("ul.menuVertical li a[href='#" + action +"']").parent().addClass("selected");
	$("#adminContent").load("load.php?page=statistike&sub=" + action, {}, function(a, b, c) {
		//alert("done");
		waitIndicator.hide();
	});
	
}


$(document).ready(function (rEvent) {
	
	doAction();
		
	$("ul.menuVertical li").click(function(cEvent) {
		action = $(this).find("a").attr("href").replace("#", "");
		doAction();
	});
});
</script>
<p><?php print ucfirst(t("statsBody")); ?></p>
<div class="roundedTableCorner">
<table>
<tr>
<td valign="top">
	<p><?php print ucfirst(t("options")); ?>:</p>
	<ul class="menuVertical">
	<!-- <li><a href="#messaging"><?php print ucfirst(t("messaging")); ?></a></li>  -->
	<li><a href="#attStats"><?php print ucfirst(t("attStats")); ?></a></li>
	<li><a href="#scheduleStats"><?php print ucfirst(t("general")); ?></a></li>
	<li><a href="#content"><?php print ucfirst(t("programme")); ?></a></li>
	<li><a href="#instructors"><?php print ucfirst(t("instructors")); ?></a></li>
	<li><a href="#places"><?php print ucfirst(t("fitnesCenters")); ?></a></li>
	<li><a href="#settings"><?php print ucfirst(t("settings")); ?></a></li>
	</ul>
</td>
<td valign="top">
	<div id="adminContent"></div>
</td>
</tr>
</table>
</div>