<?php
if (isset($_SESSION["userName"])) {
	$up = new UporabnikController;
	$rssCode = $up->getRSSCode();
	
	$veljavnost = $up->getVeljavnostKarte();
} else {
	$rssCode = "no rss code";
	
	$veljavnost = "not logged in";
}

?>

<div style="width: 780px; float: left; margin-left: 10px;">
	<p>
	<?php print t("next7DaySch"); ?>
	</p>

	<?php 
	include_once 'indexView/showSchedule.php';
	?>
</div>

<?php
	if (isset($_SESSION["userId"])) {
?>
	<div style="width: 300px; float: left ">
	<?php 
	include_once 'indexView/caloriesView.php';
	?>
	</div>
<?php
}
?>




<div style="min-height: 300px; float: left; margin-left: 15px;" id="seznamObvestilDIV">
<script type="text/javascript">

setInterval ( "osveziNovice()", 5000 );
function osveziNovice ( ) {
	if (waitIndicator != undefined) waitIndicator.show();
	naloziSeznamObvestil();
}

$(document).ready(function () {
	naloziSeznamObvestil();
});
</script>
<div class="roundedTableCorner" >

<?php 
if (checkPermission("enterNewPost")) {
	?>
	
<a href="#addNew" id="dodajObvestiloGumb">+ Dodaj novo obvestilo</a>
<script type="text/javascript">


function removeFromList(list, obj) {
	var objid = obj.href.split("#")[1];
	$(list).find("li[objid=" + objid + "]").fadeOut(function(hEvent) { 
		$(this).remove();	
		var n = $(list).attr("n")-1;
		if (n==0) {
			$(list).find(".niPodatkov").fadeIn();
		} 
		$(list).attr("n", n);	
	});
	
	
	
}

$(document).ready(function(event) {

	$("#dodajObvestiloGumb").click(function() {
		$("#novoObvestiloDIV").fadeIn();
	});
	$("#insertObvestilo").click(function(e) {
		var nPrejemnikov = $("#seznamPrejemnikov").attr("n");

		var veljaOD = $("#obvestilDatumOD").datepicker("getDate");
		var veljaDO = $("#obvestilDatumDO").datepicker("getDate");

		if (veljaOD!=null)
			veljaOD = formatDate(veljaOD, "yyyy-MM-dd");
		if (veljaDO!=null)
			veljaDO = formatDate(veljaDO, "yyyy-MM-dd");
		
		var obj = {
				"naslov": $("#naslov").val(),
				"tekst": $("#vsebinaObvestila").val(),
				"prikazan": $("input[name='obvestiloPrikazano']").val(),
				"veljaod": veljaOD,
				"veljado": veljaDO,
				"prejemniki": null
			};
		
		obj.prejemniki = jQuery.map($("#seznamPrejemnikov li.item"), function(prejemnik, i) {
				return $(prejemnik).attr("objid");
		});
		
		//alert(JSON.stringify(obj));
		//return;
		jQuery.post( "api.php?params=action=insertObvestilo", obj, function(data, textStatus, XMLHttpRequest) {
			//alert("shranjeno");
			//alert("shranjeno " + textStatus);
			//alert("shranjeno " + JSON.stringify(data));
			if (textStatus=="success")
				if (data.error=="no") {
					//$("#noticeField").html("shranjeno");
					waitIndicator.hide();
					
					$("<li objid='" + data.message.body.text[0].id + "'><b>" + data.message.body.text[0].naslov + "</b>: "+ data.message.body.text[0].tekst +"</li>").prependTo("#seznamObvestil").hide().fadeIn();
				}
			}, "json" );

		e.preventDefault();
	});

	
	
	$( "#za" ).autocomplete({
		source: function( request, response ) {
			$.ajax({
				url: "api.php?params=action=searchUserByName",
				type: "POST",
				dataType: "json",
				data: {
					q: request.term
				},
				success: function( data ) {
					//alert("lala");
					response( $.map( data.message.body.text, function( item ) {
						//alert(JSON.stringify(item));
						return {
							label: item.ime + ' ' + item.priimek + ' - ' + item.sokol_id,
							value: item.ime + ' ' + item.priimek,
							id: item.id
						};
					}));
				}
			});
		},
		minLength: 2,
		select: function( event, ui ) {
			//alert(JSON.stringify(ui.item));
			/*alert( ui.item ?
				"Selected: " + ui.item.label :
				"Nothing selected, input was " + this.value);*/
			$("#seznamPrejemnikov .niPodatkov").hide();
			$("#seznamPrejemnikov").attr("n", parseInt(($("#seznamPrejemnikov").attr("n"))+1));
			$('<li class="item" objid=' + ui.item.id + '><a onClick="removeFromList(\'#seznamPrejemnikov\', this); return false;" href="#'+ ui.item.id +'">x</a> ' + ui.item.label + '</li>').prependTo("#seznamPrejemnikov").hide().fadeIn("slow");
			$(this).val("");
			event.preventDefault();
		},
		open: function() {
			//$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
		},
		close: function() {
			//$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
		}
	});

	
	$("#obvestilDatumOD").datepicker( calConfig );
	$("#obvestilDatumOD").val((new Date()).format("d.m.Y"));
	$("#obvestilDatumDO").datepicker( calConfig );
	
});
</script>
<div id="novoObvestiloDIV" style="display: none;">
<p>naslov: <br /><input id="naslov" type='text' /></p> 
<p>objavi: 
	<label for="obvPrikazanoDa">DA</label> <input type="radio" name="obvestiloPrikazano" id="obvPrikazanoDa" value="da" checked="checked" /> |
	<label for="obvPrikazanoNe">NE</label> <input type="radio" name="obvestiloPrikazano" value="ne" id="obvPrikazanoNe" />
</p>
<p>prikazano od: <input id="obvestilDatumOD" type="text" class="dateField" /> do: <input  id="obvestilDatumDO" type="text" class="dateField" /></p>
<p>obvestilo: <br /><textarea class="autoResize" id="vsebinaObvestila"></textarea></p>  
<p>uporabniku: <br /><input id="za" type='text' /></p> 
<p>Seznam prejemnikov</p>
<ul id="seznamPrejemnikov" n=0>
<li class="niPodatkov">
<?php print t("allUsers"); ?>
</li>
</ul>
<input id='insertObvestilo' type='submit' value='OBJAVI' />
<hr />
</div>	
	<?php 
}
?>
<p><?php print t("msgList"); ?></p>
<ul id="seznamObvestil">

</ul>
</div>
<div class="roundedTableCorner" style="width: 300px:">
<?php 
include_once 'indexView/weatherView.php';
?>
</div>
</div>