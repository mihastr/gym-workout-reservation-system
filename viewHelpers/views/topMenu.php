<?php 
//$role = $_SESSION["role"];
//$role = "user";
include_once '../config/pageSettings.php';


if (isset($_SESSION["newRole"])) {
	$role = $_SESSION["newRole"];
} else {
	$role = $_SESSION["role"];
} 
$pageSettings = new PageSettings;
$pageSettings->setRole($role);

?>
<style>
#mainMenu {
 	height: 30px;
	width: 100%;
	background-color: #FF6622;
	padding-top: 2px;
	padding-bottom: 5px;
	border-top: 1px solid black;
	border-bottom: 1px solid black;
}
#menuItems {
	padding-left: 0px;
	margin-left: 0px;

}
#menuItems li {
	float: left;
	padding-left: 15px;
	padding-right: 15px;
	border: none;
	list-style: none;
}
#menuItems a {
	color: #000;
}


</style>
<script type="text/javascript">
$(document).ready(function() {
		$("#menuItems li").click(function(event) {
			//alert($(this).html());
		});
});
</script>
<div id="mainMenu">
	<ul id="menuItems">
		<?php
		$pageSettings->renderMenuOptions($role); 
		?>
	</ul>
	<?php $pageSettings->renderRoleOptions(); ?>
</div>