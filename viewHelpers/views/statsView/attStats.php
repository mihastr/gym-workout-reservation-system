<?php 
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$start = $time;
?>

<script language="javascript">

var timer = null;
var year =  null;
var month = null;
var fc = null;

function clearFirstWeek(tabela) {
	tabela.find("tr td div").html(" ");
}

function populateScheduleMonth(year, month, data) {
	

	var tabela = $("#urnikStats tbody");
	clearFirstWeek(tabela);
	
	if (year==null) year = 1970;
	if (month==null) month = 0;
	var d = new Date(year, month-1, 1, 0, 0, 0, 0);
	var firstDayIdx = d.getDay();
	var lastDayIdx = daysInMonth(month,year)+firstDayIdx;

	//alert(firstDayIdx + "  " + lastDayIdx);

	

	var row = 1;
	var col = 1;
	for (var i = firstDayIdx; i<lastDayIdx; i++) {
		col = i%7;
		if (col==0) {
			tabela.find("tr[no='" + row +"'] td[dan='7'] div[class='dan']").text(i-firstDayIdx+1);
			tabela.find("tr[no='" + row +"'] td[dan='7']").attr("dIdx", i-firstDayIdx+1);
			row++;
		} else {
			tabela.find("tr[no='" + row +"'] td[dan='" + col + "'] div[class='dan']").text(i-firstDayIdx+1);
			tabela.find("tr[no='" + row +"'] td[dan='" + col + "']").attr("dIdx", i-firstDayIdx+1);
		}
		
	}

	var curr = 0;
	$(data).each(function(i, obj) {
		//alert(curr);
		curr = parseInt(obj.datum.substring(8,10));
		tabela.find("td[dIdx='" + curr + "'] div[class='ure']").append("<div datum='" + obj.datum + "' ura_id='" + obj.id + "'>" + obj.ura + "<br /> <span class='vadba'>" + obj.naziv + "</span> <br />(" + obj.kratkoIme + "): <b style='float: right;'>" + obj.udelezba + "</b></div>");
	});
	//$(".sortableTable").tablesorter( );
	//$.trigger("appendCache");
}

function daysInMonth(month,year) {
    return new Date(year, month, 0).getDate();
}

function refreshHash(month, year, fc) {
	window.location.hash = "#attStats#" + year + "#" + month + "#" + fc; 
}

function refreshView(year, month, fc) {
	

	refreshHash(month, year, fc);

	//populateScheduleMonth(year, month);
	
	$("#filterYear").val(year);
	$("#filterMonth").val(month);

	$("#attStatFCMenu li").removeClass("selected");
	$("#attStatFCMenu li[objid='" + fc + "']").addClass("selected");

	var startDate = year + "-" + month + "-1";
	var endDate = year + "-" + month + "-" + daysInMonth(month, year);

	waitIndicator.show();

	jQuery.post( "api?params=action=getBasicStats", { "od": startDate, "do": endDate, "prostor": fc }, function(data, textStatus, XMLHttpRequest) {
		//alert("shranjeno");
		//alert("shranjeno " + textStatus);
		//alert("shranjeno " + JSON.stringify(data));
		
		if (textStatus=="success")
			if (data.error=="no") {
				timer.text(data.genTime);
				var tabela = $("#t1 tbody");
				tabela.find("tr").remove();
				var sumUdelezbe = 0; 
 
				$(data.message.body.text).each(function (i, obj){
					tabela.append("<tr>" +
							"<td>" + obj.vid + "</td>" +
							"<td>" + obj.iid + "</td>" +
							"<td>" + obj.datum + "</td>" +
							"<td>" + obj.ura + "</td>" +
							"<td>" + obj.naziv + "</td>" +
							"<td>" + obj.dolgoIme + "</td>" +
							"<td>" + obj.udelezba + "</td>" +
							"<td>" + nvl(obj.vreme, "") + "</td>" +
							"<td>" + nvl(obj.temp_ams, "") + "</td>" +
							"<td>" + nvl(obj.kolicina_oblacnosti, "") + "</td>" +
							//"<td>" + nvl(obj.vreme_napoved, "") + "</td>" +
							//"<td>" + nvl(obj.temp_napoved, "") + "</td>" +
							"</tr>");
					sumUdelezbe += parseInt(obj.udelezba);
				});
				$("#tabsum").text(sumUdelezbe);
				$("#tabavg").text((sumUdelezbe/data.message.body.text.length).toFixed(2));
				populateScheduleMonth(year, month, data.message.body.text);


				$("#t1").trigger("update");
				
				waitIndicator.hide();
				
				
			} else {
				//alert();
			}
	}, "json" );
	
}
function setYearMonth() {
	var params = (window.location.hash.split("#"));
	
	if (params.length==5) {
		year =  params[2];
		month = params[3];
		fc	= params[4];
	} else {
		year = $("#filterYear").val();
		month = (new Date()).getMonth()+1;
		fc = $("#attStatFCMenu li:first").attr("objid");
		//alert(fc);
	}

	refreshView(year, month, fc);
	
}

$(document).ready(function(rEv) {
	$("#t1").tablesorter( );
	
	timer = $("#timeCounter");

	setYearMonth();

	//populateScheduleMonth(year, month);
	
	
	$("#filterYear").change(function() {
		year = $("#filterYear").val();
		month = $("#filterMonth").val();
			
		refreshView(year, month, fc);
	});	
	$("#filterMonth").change(function() {
		year = $("#filterYear").val();
		month = $("#filterMonth").val();
			
		refreshView(year, month, fc);
	});	

	$("a.changeMonth").click(function(cEV) {
		cEV.preventDefault();
		var value = parseInt($(this).attr("value"));

		if (value>0) {
			if (month==12) {
				month = 1;
				if (year<$("#filterYear option:first").val())
					year++;
			} else {
				month++;
			}
		} else {
			if (month==1) {
				month = 12;
				if (year>$("#filterYear option:last").val())
					year--;
			} else {
				month--;
			}
		}
		refreshView(year, month, fc);
		
	});

	$("#attStatFCMenu li").click(function(cEv) {
		var objid = $(this).attr("objid");
		fc = objid;
		refreshView(year, month, fc);
	});
	
});
</script>

<h3><?php print t("scheduleStatsPerHour"); ?></h3>

<?php
$time = time(); 

$currYear = date("Y", $time);
$currMonth = date("n", $time);

$urnik = new Urnik7DController;

$fc = new ProstorController;
$fcList = $fc->getList();

$minMaxDatumUdelezbe = $urnik->getMinMaxDatumUdelezbe();

$minLeto = (int)substr($minMaxDatumUdelezbe["min_datum"], 0, 4);
$maxLeto = (int)substr($minMaxDatumUdelezbe["max_datum"], 0, 4);


?>

<p>
<h4><?php print ucfirst(t("filter")); ?></h4>
<p><?php echo ucfirst(t("fc")); ?>
<ul class="menu" id="attStatFCMenu">
<?php 
foreach($fcList as $center) {
	echo "<li class='veljaven" . $center["veljaven"] . "' objid='" . $center["pid"] . "'>" . $center["center"] . " (" . $center["prostor"]  . ") " . "</li>";
}
?>
</ul>
</p>
<?php
echo t("year") . "<select id='filterYear'>";
$j = 0;
for ($i=$maxLeto; $i>=$minLeto; $i--) {
	print "<option>$i</option>";
	if ($j++>100) break;
}
print "</select>";
print t("month"); 
print "<select id='filterMonth'>";
$j = 0;
for ($i=1; $i<=12; $i++) {
	if ($i!=$currMonth)
		print "<option>$i</option>";
	else
		print "<option seleced>$i</option>";	
	if ($j++>100) break;
}
print "</select>";

?>
<p><?php print t("or"); ?></p>
<p>
<a href="#" class="changeMonth" value="-1"><?php print t("previous"); ?></a> | <a href="#" class="changeMonth" value="1"><?php print t("next"); ?></a> <?php print t("month"); ?>
</p>
</p>
<p><?php print t("export"); ?>: 

<script language='javascript'>
	$(document).ready(function(rEvent) {
		$("a.repGenLink").click(function(cEv) {
			var link = $(this).attr("href") + fc;
			window.location.href = link;
			cEv.preventDefault();
		});
	});
</script>

<a id="pdfReportLink" class="repGenLink" href="avtoriziraj.php?link=<?php print PageSettings::reportExporterURL ?>?file=polletnoPorocilo,type=pdf,prostor="><img src="images/pdf_icon.gif" /></a> 
<?php print t("or"); ?> 
<a id="xlsReportLink" class="repGenLink" href="avtoriziraj.php?link=<?php print PageSettings::reportExporterURL ?>?file=polletnoPorocilo,type=xls,prostor="><img src="images/xls_icon.gif" /></a>
</p>
<table id="urnikStats" width="800" class="styledTable" cellspacing="0" border="1">
<thead>
<tr>
<th width="114"><?php print t("monday"); ?></th>
<th width="114"><?php print t("tuesday"); ?></th>
<th width="114"><?php print t("wednesday"); ?></th>
<th width="114"><?php print t("thursday"); ?></th>
<th width="114"><?php print t("friday"); ?></th>
<th width="114"><?php print t("saturday"); ?></th>
<th width="115"><?php print t("sunday"); ?></th>
</tr>
</thead>
<tbody>
<?php 
for ($y=1; $y<=6; $y++) {
	print "\n\t<tr no='$y'>";
	for ($x=1; $x<=7; $x++) {
		if ($x<6)
			print "\n\t\t<td valign='top' dan='$x'><div class='dan'>&nbsp;</div><div class='ure'> </div></td>";
		else
			print "\n\t\t<td valign='top' class='weekend' dan='$x'><div class='dan'>&nbsp;</div><div class='ure'> </div></td>";
	}
	print "</tr>";
}
?>
</tbody>
</table>
<br /><br />

<table id="t1" width="800" class="styledTable sortableTable" cellspacing="0">
<thead>
	<tr>
	<th align="left">vid</th>
	<th align="left">iid</th>
	<th align="left"><?php print t("date"); ?></th>
	<th align="left"><?php print t("time"); ?></th>
	<th align="left"><?php print t("class"); ?></th>
	<th align="left"><?php print t("instructor"); ?></th>
	<th align="left"><?php print t("attendance"); ?></th>
	<th align="left"><?php print t("weather"); ?></th>
	<th align="left"><?php print t("avgTemp"); ?></th>
	<th align="left"><?php print t("cloudCoverage"); ?></th>
	</tr>
</thead>
<tbody>

</tbody>
<tfoot>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td><b>skupaj</b></td>
<td><b><div id="tabsum"></div></b></td>
</tr>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td><b>povprečje</b></td>
<td><b><div id="tabavg"></div></b></td>
</tr>
</tfoot>

</table>

<?php 
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$finish = $time;
	$total_time = round(($finish - $start), 4);
	echo '<p>Page generated in '.$total_time.' seconds.'."</p>";	
?>
<p>Data delivered in <b id="timeCounter"></b> seconds.</p>
