<?php
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$start = $time;
?>


<script language="javascript">


var keys = [];

var plot4 = null;

function toggleSeries(i, val) {
	  var toggle = plot1.series[i].show;
	  plot1.series[i].show = val;
	  //plot1.replot(); 
}

function indexOf(array, obj) {
	for (var i=0; i<array.length; i++) {
		if (array[i]==obj) return i;
	}
	return -1;
}


function prepareDataT4() {
	var data = [];
	var labels = [];
	var dates = [];
	var hours = [];
	$("#t1 tbody tr").each(function(i, obj) {
		
		var x = parseInt( $(obj).find("td[ftype='udelezba']").text() );

		var naziv = $(obj).find("td[ftype='center']").text() + " " + $(obj).find("td[ftype='dvorana']").text();

		data.push([naziv, x]);
	});
	
	//alert(JSON.stringify(data));
	
	return data;
	
}

$(document).ready(function(rEvt) {

	$(".sortableTable").tablesorter( {sortList: [[2,1], [3,1], [1,0]]} );

	var dataT4 = prepareDataT4();


	plot4 = $.jqplot('chartdiv', [dataT4], {
	    title: 'Porazdelitev udeležencev po prostorih',
	    grid: {
            drawBorder: false, 
            drawGridlines: false,
            background: '#ffffff',
            shadow:false
        },
        axesDefaults: {
            
        },
        seriesDefaults:{
            renderer:$.jqplot.PieRenderer,
            rendererOptions: {
                showDataLabels: true
            }
        },
        legend: {
            show: true,
            rendererOptions: {
                numberRows: 4
            },
            location: 'e'
        },
        cursor: {  
	        showVerticalLine: false,
	        showHorizontalLine: false,
	        showCursorLegend: true,
	        showTooltip: false,
	        zoom: true
	      } 
	});

	
	$("#seriesList input[type='checkbox']").live("change", function(cEv) {
		cEv.preventDefault();
		var pos = $(this).attr("pos");
		toggleSeries(pos, $(this).is(":checked"));
		plot1.replot();
	});

	$("#markAll").change(function(cEv) {
		var checked = $(this).is(":checked");
		$("#seriesList input[type='checkbox']").each(function(i, obj) {
			toggleSeries($(obj).attr("pos"), checked);
			 if (checked) {
				 $(obj).attr("checked", "checked");
			 } else {
				 $(obj).removeAttr("checked");
			 }
		});
		plot1.replot();
	});
	
});

</script>

<h3><?php print t("statsPlaces"); ?> - mesečna</h3>


<table id="t2" width="800" class="sortableTable styledTable" cellspacing="0">
<thead>
	<tr>
	<th align="left">id prostora</th>
	<th align="left">fitnes center</th>
	<th align="left">dvorana</th>
	<th align="left">leto</th>
	<th align="left">mesec</th>
	<th align="left">skupaj vadecih</th>
	</tr>
</thead>
<tbody>
<?php 
$db = new DbConfig;
$sql = "select pr.id, f.naziv 'center', pr.naziv 'dvorana', year(u.datum) 'leto', MONTH(u.datum) 'mesec', COUNT(*) 'udelezba'
from  udelezba u 
join ura on u.ura_id=ura.id
join urnik ur on ur.id=ura.URNIK_id
join prostor pr on ur.prostor_id=pr.id
join fitnescenter f on f.id=pr.fitnescenter_id
where u.`status` in ('prijavljen', 'potrjen')
group by f.id, year(u.datum), MONTH(u.datum)";

$res = $db->executeSelect($sql);


foreach ($res as $row) {
	print "<tr>";
	foreach ($row as $f=>$data) {
		print "<td ftype='$f' align='left'>$data</td>";
	}
	print "</tr>";
}
?>
</tbody>
</table>

<h3><?php print t("statsPlaces"); ?> - letna</h3>


<table id="t3" width="800" class="sortableTable styledTable" cellspacing="0">
<thead>
	<tr>
	<th align="left">id prostora</th>
	<th align="left">fitnes center</th>
	<th align="left">dvorana</th>
	<th align="left">leto</th>
	<th align="left">skupaj vadecih</th>
	</tr>
</thead>
<tbody>
<?php 
$db = new DbConfig;
$sql = "select pr.id, f.naziv 'center', pr.naziv 'dvorana', year(u.datum) 'leto', COUNT(*) 'udelezba'
from  udelezba u 
join ura on u.ura_id=ura.id
join urnik ur on ur.id=ura.URNIK_id
join prostor pr on ur.prostor_id=pr.id
join fitnescenter f on f.id=pr.fitnescenter_id
where u.`status`='prijavljen' or u.`status`='potrjen'
group by f.id, year(u.datum)";

$res = $db->executeSelect($sql);


foreach ($res as $row) {
	print "<tr>";
	foreach ($row as $f=>$data) {
		print "<td ftype='$f' align='left'>$data</td>";
	}
	print "</tr>";
}
?>
</tbody>
</table>


<h3><?php print t("statsPlaces"); ?> - splošna</h3>
<p>
<div id="chartdiv" style="height:300px;width:600px; margin-top: 15px; margin-bottom: 30px; margin-left: 60px;"></div> 
</p>

<table id="t1" width="800" class="sortableTable styledTable" cellspacing="0">
<thead>
	<tr>
	<th align="left">id prostora</th>
	<th align="left">fitnes center</th>
	<th align="left">dvorana</th>
	<th align="left">skupaj vadecih</th>
	</tr>
</thead>
<tbody>
<?php 
$db = new DbConfig;
$sql = "select pr.id, f.naziv 'center', pr.naziv 'dvorana', COUNT(*) 'udelezba'
from  udelezba u 
join ura on u.ura_id=ura.id
join urnik ur on ur.id=ura.URNIK_id
join prostor pr on ur.prostor_id=pr.id
join fitnescenter f on f.id=pr.fitnescenter_id
where u.`status` in ('prijavljen', 'potrjen')
group by f.id";

$res = $db->executeSelect($sql);


foreach ($res as $row) {
	print "<tr>";
	foreach ($row as $f=>$data) {
		print "<td ftype='$f' align='left'>$data</td>";
	}
	print "</tr>";
}
?>
</tbody>
</table>

<?php 
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$finish = $time;
	$total_time = round(($finish - $start), 4);
	echo '<p>Page generated in '.$total_time.' seconds.'."</p>";
	?>
