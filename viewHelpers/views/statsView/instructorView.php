<?php
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$start = $time;
?>

<script language="javascript">


var keys = [];

var plot4 = null;

function toggleSeries(i, val) {
	  var toggle = plot1.series[i].show;
	  plot1.series[i].show = val;
	  //plot1.replot(); 
}

function indexOf(array, obj) {
	for (var i=0; i<array.length; i++) {
		if (array[i]==obj) return i;
	}
	return -1;
}


function prepareDataT4() {
	var data = [];
	var labels = [];
	var dates = [];
	var hours = [];
	$("#t3 tbody tr").each(function(i, obj) {
		
		var x = parseInt( $(obj).find("td[ftype='skupaj']").text() );

		var naziv = $(obj).find("td[ftype='dolgoIme']").text();

		data.push([naziv, x]);
	});
	
	//alert(JSON.stringify(data));
	
	return data;
	
}

$(document).ready(function(rEvt) {

	$("#id1").tablesorter( {sortList: [[2,0], [4,1], [5,1]]} );
	$("#id2").tablesorter( {sortList: [[2,0], [4,1]]} );
	$("#id3").tablesorter( {sortList: [[2,0]]} );
	$("#id4").tablesorter( {sortList: [[1,0]]} );

	var dataT4 = prepareDataT4();


	plot4 = $.jqplot('chartdiv', [dataT4], {
	    title: 'Pie Chart with Legend and sliceMargin',
	    grid: {
            drawBorder: false, 
            drawGridlines: false,
            background: '#ffffff',
            shadow:false
        },
        axesDefaults: {
            
        },
        seriesDefaults:{
            renderer:$.jqplot.PieRenderer,
            rendererOptions: {
                showDataLabels: true
            }
        },
        legend: {
            show: true,
            rendererOptions: {
                numberRows: 4
            },
            location: 'e'
        },
        cursor: {  
	        showVerticalLine: false,
	        showHorizontalLine: false,
	        showCursorLegend: true,
	        showTooltip: false,
	        zoom: true
	      } 
	});

	
	$("#seriesList input[type='checkbox']").live("change", function(cEv) {
		cEv.preventDefault();
		var pos = $(this).attr("pos");
		toggleSeries(pos, $(this).is(":checked"));
		plot1.replot();
	});

	$("#markAll").change(function(cEv) {
		var checked = $(this).is(":checked");
		$("#seriesList input[type='checkbox']").each(function(i, obj) {
			toggleSeries($(obj).attr("pos"), checked);
			 if (checked) {
				 $(obj).attr("checked", "checked");
			 } else {
				 $(obj).removeAttr("checked");
			 }
		});
		plot1.replot();
	});
	
});

</script>




<h3>Statistike po inštrukorjih</h3>


<table  id="id1" width="100%" class="sortableTable styledTable" cellspacing="0">
<THEAD>
<tr>
<th align="left">iid</th>
<th align="left">vid</th>
<th align="left"><?php print t("instructor"); ?></th>
<th align="left"><?php print t("class"); ?></th>
<th align="left"><?php print t("year"); ?></th>
<th align="left"><?php print t("month"); ?></th>
<th align="left"><?php print t("hours"); ?></th>
<th align="left"><?php print t("average"); ?></th>
<th align="left"><?php print t("std"); ?></th>
<th align="left"><?php print t("var"); ?></th>
<th align="left"><?php print t("minimum"); ?></th>
<th align="left"><?php print t("maximum"); ?></th>
<th align="left"><?php print t("sum"); ?></th>

</tr>
</THEAD>
<TBODY>
<?php 
$db = new DbConfig;
/*
$sql = "/* odvodenih ur z udelezbo po mesecih */
/*
select T1.id, T1.dolgoIme, T1.naziv, T1.leto, T1.mesec, T1.udelezencev, 
min(T2.udelezencev) 'najmanj', max(T2.udelezencev) 'najvec', STD(T2.udelezencev) 'odklon', VAR_POP(T2.udelezencev) 'varianca'
from 
 (select i.id, v.id vid, i.dolgoIme, v.naziv, year(ud.datum) leto, MONTH(ud.datum) mesec, count(*) 'udelezencev'
		from instruktor i join ura u on (u.INSTRUKTOR_id = i.id) join udelezba ud on (u.id = ud.ura_id and ud.`status`='prijavljen') join vadba v on (v.id = u.VADBA_id)
	GROUP BY i.id, v.id, year(ud.datum), MONTH(ud.datum)) T1,
 (select i.id, v.id vid, i.dolgoIme, v.naziv, year(ud.datum) leto, MONTH(ud.datum) mesec, DAY(ud.datum) dan, v.kratkoIme, count(*) 'udelezencev'
		from instruktor i join ura u on (u.INSTRUKTOR_id = i.id) join udelezba ud on (u.id = ud.ura_id and ud.`status`='prijavljen') join vadba v on (v.id = u.VADBA_id)
	GROUP BY i.id, v.id, ud.datum) T2
where T1.id=T2.id and T1.vid=T2.vid and T1.leto = T2.leto and T1.mesec = T2.mesec
group by T1.id, T1.vid, T2.leto, T1.mesec
";
*/

$sql = "select T.iid, T.vid, T.dolgoIme, T.naziv, T.leto, T.mesec, 
count(*) 'ur', 
AVG( T.udelezencev ) 'povprecno', STD(T.udelezencev) 'odlon', VAR_POP(T.udelezencev) 'varianca',
min(T.udelezencev) 'najmanj', max(T.udelezencev) 'najvec',
sum( T.udelezencev ) 'skupaj'
from
(select i.id iid, v.id vid, i.dolgoIme, v.naziv, year(ud.datum) leto, MONTH(ud.datum) mesec, DAY(ud.datum) dan, v.kratkoIme, count(*) 'udelezencev'
		from instruktor i join ura u on (u.INSTRUKTOR_id = i.id) join udelezba ud on (u.id = ud.ura_id and ud.`status` in ('prijavljen', 'potrjen')) join vadba v on (v.id = u.VADBA_id)
	GROUP BY i.id, v.id, ud.datum) T
group by T.iid, T.vid, T.leto, T.mesec";

$res = $db->executeSelect($sql);
foreach ($res as $row) {
	print "<tr>";
	foreach ($row as $data) {
		print "<td align='left'>$data</td>";
	}
	print "</tr>";
}
?>
</TBODY>
</table>


<h3>Statistike po inštrukorjih - letne</h3>

<table id="id2" width="100%" class="sortableTable styledTable" cellspacing="0">
<THEAD>
<tr>
<th align="left">iid</th>
<th align="left">vid</th>
<th align="left"><?php print t("instructor"); ?></th>
<th align="left"><?php print t("class"); ?></th>
<th align="left"><?php print t("year"); ?></th>
<th align="left"><?php print t("hours"); ?></th>
<th align="left"><?php print t("average"); ?></th>
<th align="left"><?php print t("std"); ?></th>
<th align="left"><?php print t("var"); ?></th>
<th align="left"><?php print t("minimum"); ?></th>
<th align="left"><?php print t("maximum"); ?></th>
<th align="left"><?php print t("sum"); ?></th>

</tr>
</THEAD>
<TBODY>
<?php 
$db = new DbConfig;
/*
$sql = "/* odvodenih ur z udelezbo po mesecih */
/*
select T1.id, T1.dolgoIme, T1.naziv, T1.leto, T1.mesec, T1.udelezencev, 
min(T2.udelezencev) 'najmanj', max(T2.udelezencev) 'najvec', STD(T2.udelezencev) 'odklon', VAR_POP(T2.udelezencev) 'varianca'
from 
 (select i.id, v.id vid, i.dolgoIme, v.naziv, year(ud.datum) leto, MONTH(ud.datum) mesec, count(*) 'udelezencev'
		from instruktor i join ura u on (u.INSTRUKTOR_id = i.id) join udelezba ud on (u.id = ud.ura_id and ud.`status`='prijavljen') join vadba v on (v.id = u.VADBA_id)
	GROUP BY i.id, v.id, year(ud.datum), MONTH(ud.datum)) T1,
 (select i.id, v.id vid, i.dolgoIme, v.naziv, year(ud.datum) leto, MONTH(ud.datum) mesec, DAY(ud.datum) dan, v.kratkoIme, count(*) 'udelezencev'
		from instruktor i join ura u on (u.INSTRUKTOR_id = i.id) join udelezba ud on (u.id = ud.ura_id and ud.`status`='prijavljen') join vadba v on (v.id = u.VADBA_id)
	GROUP BY i.id, v.id, ud.datum) T2
where T1.id=T2.id and T1.vid=T2.vid and T1.leto = T2.leto and T1.mesec = T2.mesec
group by T1.id, T1.vid, T2.leto, T1.mesec
";
*/

$sql = "select T.iid, T.vid, T.dolgoIme, T.naziv, T.leto, 
count(*) 'ur', 
AVG( T.udelezencev ) 'povprecno', STD(T.udelezencev) 'odlon', VAR_POP(T.udelezencev) 'varianca',
min(T.udelezencev) 'najmanj', max(T.udelezencev) 'najvec',
sum( T.udelezencev ) 'skupaj'
from
(select i.id iid, v.id vid, i.dolgoIme, v.naziv, year(ud.datum) leto, MONTH(ud.datum) mesec, DAY(ud.datum) dan, v.kratkoIme, count(*) 'udelezencev'
		from instruktor i join ura u on (u.INSTRUKTOR_id = i.id) join udelezba ud on (u.id = ud.ura_id and ud.`status` in ('prijavljen', 'potrjen')) join vadba v on (v.id = u.VADBA_id)
	GROUP BY i.id, v.id, ud.datum) T
group by T.iid, T.vid, T.leto";

$res = $db->executeSelect($sql);
foreach ($res as $row) {
	print "<tr>";
	foreach ($row as $data) {
		print "<td align='left'>$data</td>";
	}
	print "</tr>";
}
?>
</TBODY>
</table>


<h3>Statistike po inštrukorjih - splošne</h3>

<table id="id3" width="100%" class="sortableTable styledTable" cellspacing="0">
<THEAD>
<tr>
<th align="left">iid</th>
<th align="left">vid</th>
<th align="left"><?php print t("instructor"); ?></th>
<th align="left"><?php print t("class"); ?></th>
<th align="left"><?php print t("hours"); ?></th>
<th align="left"><?php print t("average"); ?></th>
<th align="left"><?php print t("std"); ?></th>
<th align="left"><?php print t("var"); ?></th>
<th align="left"><?php print t("minimum"); ?></th>
<th align="left"><?php print t("maximum"); ?></th>
<th align="left"><?php print t("sum"); ?></th>

</tr>
</THEAD>
<TBODY>
<?php 
$db = new DbConfig;
/*
$sql = "/* odvodenih ur z udelezbo po mesecih */
/*
select T1.id, T1.dolgoIme, T1.naziv, T1.leto, T1.mesec, T1.udelezencev, 
min(T2.udelezencev) 'najmanj', max(T2.udelezencev) 'najvec', STD(T2.udelezencev) 'odklon', VAR_POP(T2.udelezencev) 'varianca'
from 
 (select i.id, v.id vid, i.dolgoIme, v.naziv, year(ud.datum) leto, MONTH(ud.datum) mesec, count(*) 'udelezencev'
		from instruktor i join ura u on (u.INSTRUKTOR_id = i.id) join udelezba ud on (u.id = ud.ura_id and ud.`status`='prijavljen') join vadba v on (v.id = u.VADBA_id)
	GROUP BY i.id, v.id, year(ud.datum), MONTH(ud.datum)) T1,
 (select i.id, v.id vid, i.dolgoIme, v.naziv, year(ud.datum) leto, MONTH(ud.datum) mesec, DAY(ud.datum) dan, v.kratkoIme, count(*) 'udelezencev'
		from instruktor i join ura u on (u.INSTRUKTOR_id = i.id) join udelezba ud on (u.id = ud.ura_id and ud.`status`='prijavljen') join vadba v on (v.id = u.VADBA_id)
	GROUP BY i.id, v.id, ud.datum) T2
where T1.id=T2.id and T1.vid=T2.vid and T1.leto = T2.leto and T1.mesec = T2.mesec
group by T1.id, T1.vid, T2.leto, T1.mesec
";
*/

$sql = "select T.iid, T.vid, T.dolgoIme, T.naziv,
count(*) 'ur', 
AVG( T.udelezencev ) 'povprecno', STD(T.udelezencev) 'odlon', VAR_POP(T.udelezencev) 'varianca',
min(T.udelezencev) 'najmanj', max(T.udelezencev) 'najvec',
sum( T.udelezencev ) 'skupaj'
from
(select i.id iid, v.id vid, i.dolgoIme, v.naziv, year(ud.datum) leto, MONTH(ud.datum) mesec, DAY(ud.datum) dan, v.kratkoIme, count(*) 'udelezencev'
		from instruktor i join ura u on (u.INSTRUKTOR_id = i.id) join udelezba ud on (u.id = ud.ura_id and ud.`status` in ('prijavljen', 'potrjen')) join vadba v on (v.id = u.VADBA_id)
	GROUP BY i.id, v.id, ud.datum) T
group by T.iid, T.vid";

$res = $db->executeSelect($sql);
foreach ($res as $row) {
	print "<tr>";
	foreach ($row as $f=>$data) {
		print "<td ftype='$f' align='left'>$data</td>";
	}
	print "</tr>";
}
?>
</TBODY>
</table>



<h3>Statistike po inštrukorjih - splošne 2</h3>

<table id="id4" width="100%" class="sortableTable styledTable" cellspacing="0">
<THEAD>
<tr>
<th align="left">iid</th>
<th align="left"><?php print t("instructor"); ?></th>
<th align="left"><?php print t("hours"); ?></th>
<th align="left"><?php print t("average"); ?></th>
<th align="left"><?php print t("std"); ?></th>
<th align="left"><?php print t("var"); ?></th>
<th align="left"><?php print t("minimum"); ?></th>
<th align="left"><?php print t("maximum"); ?></th>
<th align="left"><?php print t("sum"); ?></th>

</tr>
</THEAD>
<TBODY>
<?php 
$db = new DbConfig;
/*
$sql = "/* odvodenih ur z udelezbo po mesecih */
/*
select T1.id, T1.dolgoIme, T1.naziv, T1.leto, T1.mesec, T1.udelezencev, 
min(T2.udelezencev) 'najmanj', max(T2.udelezencev) 'najvec', STD(T2.udelezencev) 'odklon', VAR_POP(T2.udelezencev) 'varianca'
from 
 (select i.id, v.id vid, i.dolgoIme, v.naziv, year(ud.datum) leto, MONTH(ud.datum) mesec, count(*) 'udelezencev'
		from instruktor i join ura u on (u.INSTRUKTOR_id = i.id) join udelezba ud on (u.id = ud.ura_id and ud.`status`='prijavljen') join vadba v on (v.id = u.VADBA_id)
	GROUP BY i.id, v.id, year(ud.datum), MONTH(ud.datum)) T1,
 (select i.id, v.id vid, i.dolgoIme, v.naziv, year(ud.datum) leto, MONTH(ud.datum) mesec, DAY(ud.datum) dan, v.kratkoIme, count(*) 'udelezencev'
		from instruktor i join ura u on (u.INSTRUKTOR_id = i.id) join udelezba ud on (u.id = ud.ura_id and ud.`status`='prijavljen') join vadba v on (v.id = u.VADBA_id)
	GROUP BY i.id, v.id, ud.datum) T2
where T1.id=T2.id and T1.vid=T2.vid and T1.leto = T2.leto and T1.mesec = T2.mesec
group by T1.id, T1.vid, T2.leto, T1.mesec
";
*/

$sql = "select T.iid, T.dolgoIme,
count(*) 'ur', 
AVG( T.udelezencev ) 'povprecno', STD(T.udelezencev) 'odlon', VAR_POP(T.udelezencev) 'varianca',
min(T.udelezencev) 'najmanj', max(T.udelezencev) 'najvec',
sum( T.udelezencev ) 'skupaj'
from
(select i.id iid, v.id vid, i.dolgoIme, v.naziv, year(ud.datum) leto, MONTH(ud.datum) mesec, DAY(ud.datum) dan, v.kratkoIme, count(*) 'udelezencev'
		from instruktor i join ura u on (u.INSTRUKTOR_id = i.id) join udelezba ud on (u.id = ud.ura_id and ud.`status` in ('prijavljen', 'potrjen')) join vadba v on (v.id = u.VADBA_id)
	GROUP BY i.id, v.id, ud.datum) T
group by T.iid";

$res = $db->executeSelect($sql);
foreach ($res as $row) {
	print "<tr>";
	foreach ($row as $f=>$data) {
		print "<td ftype='$f' align='left'>$data</td>";
	}
	print "</tr>";
}
?>
</TBODY>
</table>

<?php 
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$finish = $time;
	$total_time = round(($finish - $start), 4);
	echo '<p>Page generated in '.$total_time.' seconds.'."</p>";
?>