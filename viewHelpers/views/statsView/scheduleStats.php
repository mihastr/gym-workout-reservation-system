<?php 
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$start = $time;

?>
<script language="javascript">

var keys = [];
var arrs = [];

var plot1 = null;

function toggleSeries(i, val) {
	  var toggle = plot1.series[i].show;
	  plot1.series[i].show = val;
	  //plot1.replot(); 
}

function indexOf(array, obj) {
	for (var i=0; i<array.length; i++) {
		if (array[i]==obj) return i;
	}
	return -1;
}

function prepareData() {
	var data = [];
	var labels = [];
	var dates = [];
	var hours = [];
	$("#t1 tbody tr").each(function(i, obj) {
		var x = parseInt( $(obj).find("td[ftype='udelezba']").text() );
		var naziv = $(obj).find("td[ftype='naziv']").text();
		var nShort = naziv.replace(" ", "_");
		var datum = $(obj).find("td[ftype='datum']").text();
		var ura =   $(obj).find("td[ftype='ura']").text();

		var pos = indexOf(keys, naziv);
		if (pos==-1) {
			keys.push(naziv);

			pos = keys.length-1;
			
			$("#seriesList").append("<li><input type='checkbox' checked='checked' pos='" + pos + "' />" + naziv + "</li>");
			
			data.push(new Array());
		}
		data[pos].push([datum + " " + ura, x]);
		//alert(pos + " " + naziv);
		//arrs[pos].push([datum + " " + ura, x]);
		
	});
	
	//alert(JSON.stringify(data));
	
	return data;
	
}

$(document).ready(function(rEvt) {

	$("table tbody td[ftype='dan']").each(function(i, obj) {
		$(obj).text(prevediDan($(obj).text()));
	});

	$(".sortableTable1").tablesorter( {sortList: [[1,0], [4,0]]} );
	$(".sortableTable2").tablesorter( {sortList: [[0,0],[6,0], [7,0]]} );

	var data = prepareData();

	var labels = keys.map(function(label) {
		return {"label": label, "markerOptions":{style:'square'}, showLabel: true };
	});

	plot1 = $.jqplot('chartdiv', data, {  
	    title: 'Obisk po urah',
	    stackSeries: false,
	    series: labels,
	    legend: {show: true, location: 'nw', fontSize: 14 },
	    axesDefaults: {
	        tickRenderer: $.jqplot.CanvasAxisTickRenderer,
	        tickOptions: {
	          angle: -60
	    	}
        },
	    axes: {
	      xaxis: {
	        renderer: $.jqplot.DateAxisRenderer, 
	        tickOptions: {
	          labelPosition: 'end',
	          formatString:'%Y-%#m-%#d %H:%M:%S'
	        },
	        autoscale: true
	      },
	      yaxis: {
	        autoscale: true,
	        renderer: $.jqplot.LogAxisRenderer,
	        tickRenderer: $.jqplot.CanvasAxisTickRenderer,
	        tickOptions: {
	          labelPosition: 'middle',
	          fontSize: 20
	        }
	      }
	    },
	    cursor: {  
	        showVerticalLine:true,
	        showHorizontalLine:false,
	        showCursorLegend:true,
	        showTooltip: false,
	        zoom:true
	      } 
	});

	$("#seriesList input[type='checkbox']").live("change", function(cEv) {
		cEv.preventDefault();
		var pos = $(this).attr("pos");
		toggleSeries(pos, $(this).is(":checked"));
		plot1.replot();
	});

	$("#markAll").change(function(cEv) {
		var checked = $(this).is(":checked");
		$("#seriesList input[type='checkbox']").each(function(i, obj) {
			toggleSeries($(obj).attr("pos"), checked);
			 if (checked) {
				 $(obj).attr("checked", "checked");
			 } else {
				 $(obj).removeAttr("checked");
			 }
		});
		plot1.replot();
	});
	
});

</script>

<h3><?php print t("scheduleStatsPerHour"); ?></h3>
<p>
<input type='checkbox' checked='checked' id='markAll' /><?php print t("markAll"); ?>
<ul id="seriesList">
</ul>
<div id="chartdiv" style="height:500px;width:900px; margin-top: 15px; margin-bottom: 30px; margin-left: 60px;"></div> 
</p>
<table id="t1" width="800" class="sortableTable1 styledTable" cellspacing="0">
<thead>
	<tr>
	<th align="left">id</th>
	<th align="left"><?php print t("date"); ?></th>
	<th align="left">did</th>
	<th align="left"><?php print t("day"); ?></th>
	<th align="left"><?php print t("time"); ?></th>
	<th align="left"><?php print t("class"); ?></th>
	<th align="left"><?php print t("attendance"); ?></th>
	</tr>
</thead>
<tbody>
<?php 
$db = new DbConfig;
$sql = "select ura.id, datum, ura.dan 'did', ura.dan, ura.ura, vadba.naziv, count(uporabnik_id) 'udelezba'
from vadba 
join ura on vadba.id=ura.VADBA_id
join udelezba u on ura.id=u.ura_id
where u.`status` in ('prijavljen', 'potrjen')
group by ura_id, datum
order by datum desc, ura.ura desc
";

$res = $db->executeSelect($sql) or die("lalal");

$danes = date("Y-m-d", time());

$sedaj = date("H:i", time());

foreach ($res as $row) {
	if ($row["datum"]<$danes || ($row["datum"]==$danes && $row["ura"]<=$sedaj)) {
		$s = "r1";
	} else {
		$s = "r2";
	}
	print "<tr class='$s'>";
	foreach ($row as $f=>$data) {
		print "<td ftype='$f' align='left'>$data</td>";
	}
	print "</tr>";
}

?>
</tbody>
</table>


<h3><?php print t("scheduleStats"); ?></h3>

<table width="100%" class="sortableTable2 styledTableV2" cellspacing="0" cellpadding="3">
<thead>
	<tr>
	<th align="left">id</th>
	<th align="left"><?php print t("schedule"); ?></th>
	<th align="left"><?php print t("validFrom"); ?></th>
	<th align="left"><?php print t("validTo"); ?></th>
	<th align="left"><?php print t("class"); ?></th>
	<th align="left"><?php print t("day"); ?></th>
	<th align="left">did</th>
	<th align="left"><?php print t("hour"); ?></th>
	<th align="left"><?php print t("repeat"); ?></th>
	<th align="left"><?php print t("average"); ?></th>
	<th align="left"><?php print t("std"); ?></th>
	<th align="left"><?php print t("var"); ?></th>
	<th align="left"><?php print t("minimum"); ?></th>
	<th align="left"><?php print t("maximum"); ?></th>
	<th align="left"><?php print t("sum"); ?></th>
	</tr>
</thead>
<tbody>
<?php 
$db = new DbConfig;
$sql = "select T.id, T.urnik, T.veljaOD, T.veljaDO, naziv, dan, dan 'did', ura, 
count(*) 'ponovitev',
AVG( udelezba ) 'povprecno', STD( udelezba ) 'odlon', VAR_POP( udelezba ) 'varianca',
min( udelezba ) 'najmanj', max( udelezba ) 'najvec',
sum( udelezba ) 'skupaj'
from 
(select 
	ur.id, ur.naziv 'urnik', ur.veljaOD, ur.veljaDO, u.dan,
	v.naziv, datum, u.ura, v.kratkoIme, ura_id, count(*) 'udelezba'
		from vadba v 
		join ura u on (u.VADBA_id = v.id) 
		join urnik ur on (u.URNIK_id=ur.id)
		join udelezba ud on (u.id = ud.ura_id and ud.`status` in('prijavljen', 'potrjen'))
group by ura_id, datum) T
group by ura_id";

$res = $db->executeSelect($sql) or die("lalal");

//print_r($res);

$colors = array("r1", "r2", "r2");

$urnik = 0;
$i=0;

$aSize = count($colors);
foreach ($res as $row) {
	if ($urnik!=$row["id"]) {
		$i++;
		$urnik = $row["id"];
	}
	print "<tr class='" . $colors[$i%$aSize] . "'>";
	foreach ($row as $f=>$data) {
		print "<td ftype='$f' align='left'>$data</td>";
	}
	print "</tr>";
}
?>
</tbody>
</table>

<?php 
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$finish = $time;
	$total_time = round(($finish - $start), 4);
	echo '<p>Page generated in '.$total_time.' seconds.'."</p>";
?>
