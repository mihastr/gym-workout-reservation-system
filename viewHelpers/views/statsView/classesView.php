<?php 
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$start = $time;
?>

<script language="javascript">

var keys = [];

var plot1 = null;
var plot4 = null;

function toggleSeries(i, val) {
	  var toggle = plot1.series[i].show;
	  plot1.series[i].show = val;
	  //plot1.replot(); 
}

function indexOf(array, obj) {
	for (var i=0; i<array.length; i++) {
		if (array[i]==obj) return i;
	}
	return -1;
}

function prepareData() {
	var data = [];
	var labels = [];
	var dates = [];
	var hours = [];
	$("#t2 tbody tr").each(function(i, obj) {
		
		var x = parseInt( $(obj).find("td[ftype='skupaj']").text() );
		var naziv = $(obj).find("td[ftype='naziv']").text();
		var nShort = naziv.replace(" ", "_");
		var leto = $(obj).find("td[ftype='leto']").text();
		var mesec =   $(obj).find("td[ftype='mesec']").text();

		var pos = indexOf(keys, naziv);
		if (pos==-1) {
			keys.push(naziv);

			pos = keys.length-1;
			
			$("#seriesList").append("<li><input type='checkbox' checked='checked' pos='" + pos + "' />" + naziv + "</li>");
			
			data.push(new Array());
		}
		data[pos].push([leto + "-" + mesec + "-01", x]);
		//alert(pos + " " + naziv);
		//arrs[pos].push([datum + " " + ura, x]);
		
	});
	
	//alert(JSON.stringify(data));
	
	return data;
	
}


function prepareDataT4() {
	var data = [];
	var labels = [];
	var dates = [];
	var hours = [];
	$("#t4 tbody tr").each(function(i, obj) {
		
		var x = parseInt( $(obj).find("td[ftype='skupaj']").text() );

		var naziv = $(obj).find("td[ftype='naziv']").text();

		data.push([naziv, x]);
	});
	
	//alert(JSON.stringify(data));
	
	return data;
	
}

$(document).ready(function(rEvt) {

	$("#t2").tablesorter( {sortList: [[2,1], [3,1], [1,0]]} );
	$("#t3").tablesorter( {sortList: [[2,1], [1,0]]} );
	$("#t4").tablesorter(  );

	var data = prepareData();

	var dataT4 = prepareDataT4();

	var labels = keys.map(function(label) {
		return {"label": label, "markerOptions":{style:'square'}, showLabel: true };
	});

	plot1 = $.jqplot('chartdiv', data, {  
	    title: 'Obiskov po vadbah po mesecih',
	    stackSeries: false,
	    series: labels,
	    legend: {show: true, location: 'nw', fontSize: 14 },
	    axesDefaults: {
	        tickRenderer: $.jqplot.CanvasAxisTickRenderer,
	        tickOptions: {
	          angle: -40
	    	}
        },
	    axes: {
	      xaxis: {
	        renderer: $.jqplot.DateAxisRenderer, 
	        tickOptions: {
	          labelPosition: 'end',
	          formatString:'%Y-%#m'
	        },
	        autoscale: true
	      },
	      yaxis: {
	        autoscale: true,
	        renderer: $.jqplot.LogAxisRenderer,
	        tickRenderer: $.jqplot.CanvasAxisTickRenderer,
	        tickOptions: {
	          labelPosition: 'left',
	          fontSize: 20
	        }
	      }
	    },
	    cursor: {  
	        showVerticalLine:true,
	        showHorizontalLine:false,
	        showCursorLegend:true,
	        showTooltip: false,
	        zoom:true
	      } 
	});



	plot4 = $.jqplot('chartdiv4', [dataT4], {
	    title: 'Razporeditev udeležbe po vadbah',
	    grid: {
            drawBorder: false, 
            drawGridlines: false,
            background: '#ffffff',
            shadow:false
        },
        axesDefaults: {
            
        },
        seriesDefaults:{
            renderer:$.jqplot.PieRenderer,
            rendererOptions: {
                showDataLabels: true
            }
        },
        legend: {
            show: true,
            rendererOptions: {
                numberRows: 4
            },
            location: 'e'
        },
        cursor: {  
	        showVerticalLine: false,
	        showHorizontalLine: false,
	        showCursorLegend: true,
	        showTooltip: false,
	        zoom: true
	      } 
	});

	
	$("#seriesList input[type='checkbox']").live("change", function(cEv) {
		cEv.preventDefault();
		var pos = $(this).attr("pos");
		toggleSeries(pos, $(this).is(":checked"));
		plot1.replot();
	});

	$("#markAll").change(function(cEv) {
		var checked = $(this).is(":checked");
		$("#seriesList input[type='checkbox']").each(function(i, obj) {
			toggleSeries($(obj).attr("pos"), checked);
			 if (checked) {
				 $(obj).attr("checked", "checked");
			 } else {
				 $(obj).removeAttr("checked");
			 }
		});
		plot1.replot();
	});
	
});

</script>

<h3><?php print t("statsClasses"); ?></h3>

<p>
<input type='checkbox' checked='checked' id='markAll' /><?php print t("markAll"); ?>
<ul id="seriesList">
</ul>
<div id="chartdiv" style="height:500px;width:900px; margin-top: 15px; margin-bottom: 30px; margin-left: 60px;"></div> 
</p>

<table id="t2" width="100%" class="sortableTable styledTable" cellspacing="0" cellpadding="5">
<thead>
	<tr>
	<th align="left">id</th>
	<th align="left"><?php print t("class"); ?></th>
	<th align="left"><?php print t("year"); ?></th>
	<th align="left"><?php print t("month"); ?></th>
	<th align="left"><?php print t("hours"); ?></th>
	<th align="left"><?php print t("average"); ?></th>
	<th align="left"><?php print t("std"); ?></th>
	<th align="left"><?php print t("var"); ?></th>
	<th align="left"><?php print t("minimum"); ?></th>
	<th align="left"><?php print t("maximum"); ?></th>
	<th align="left"><?php print t("sum"); ?></th>
	</tr>
</thead>
<tbody>
<?php 
$db = new DbConfig;
/*
$sql = "select T1.id, T1.naziv, T1.leto, T1.mesec, T1.skupaj, T2.povprecno, T2.najvec, T2.najmanj from (
	select vadba.id, vadba.naziv,  year(datum) leto, MONTH(datum) mesec, count(*) skupaj from 
		udelezba u join ura on u.ura_id=ura.id join vadba on ura.VADBA_id = vadba.id
		where u.`status`='prijavljen'
		group by ura.VADBA_id, year(datum), MONTH(datum)
) T1,
	(select T3.id, naziv, leto, mesec, avg(obisk) povprecno, min(obisk) 'najmanj', max(obisk) 'najvec' from (
		select vadba.id, vadba.naziv,  year(datum) leto, MONTH(datum) mesec, DAY(datum) dan, count(*) obisk from 
			udelezba u join ura on u.ura_id=ura.id join vadba on ura.VADBA_id = vadba.id
			where u.`status`='prijavljen'
			group by ura.VADBA_id, year(datum), MONTH(datum), DAY(datum)) T3
	group by id, leto, mesec) T2
WHERE T1.id = T2.id and T1.leto = T2.leto and T1.mesec=T2.mesec
";
*/
$sql = "select T.vid, T.naziv, T.leto, T.mesec, 
count(ura_id) 'ur', 
AVG( T.udelezencev ) 'povprecno', STD(T.udelezencev) 'odlon', VAR_POP(T.udelezencev) 'varianca',
min(T.udelezencev) 'najmanj', max(T.udelezencev) 'najvec',
sum( T.udelezencev ) 'skupaj'
from
(select v.id vid, v.naziv, year(ud.datum) leto, MONTH(ud.datum) mesec, DAY(ud.datum) dan, ura_id, v.kratkoIme, count(*) 'udelezencev'
		from vadba v 
		join ura u on (u.VADBA_id = v.id) 
		join udelezba ud on (u.id = ud.ura_id)
		where ud.`status` in ('prijavljen', 'potrjen')
	GROUP BY ura_id, ud.datum) T
group by T.vid, T.leto, T.mesec;
";
$res = $db->executeSelect($sql) or die("lalal");


foreach ($res as $row) {
	print "<tr>";
	foreach ($row as $f=>$data) {
		print "<td ftype='$f' align='left'>$data</td>";
	}
	print "</tr>";
}
?>
</tbody>
</table>


<h3><?php print t("statsClassesYearlyReport"); ?></h3>

<table id="t3" width="100%" class="sortableTable styledTable" cellspacing="0" cellpadding="5">
<thead>
	<tr>
	<th align="left">id</th>
	<th align="left"><?php print t("class"); ?></th>
	<th align="left"><?php print t("year"); ?></th>
	<th align="left"><?php print t("hours"); ?></th>
	<th align="left"><?php print t("average"); ?></th>
	<th align="left"><?php print t("std"); ?></th>
	<th align="left"><?php print t("var"); ?></th>
	<th align="left"><?php print t("minimum"); ?></th>
	<th align="left"><?php print t("maximum"); ?></th>
	<th align="left"><?php print t("sum"); ?></th>
	</tr>
</thead>
<tbody>
<?php 

/*
$sql = "select T1.id, T1.naziv, T1.leto, T1.mesec, T1.skupaj, T2.povprecno, T2.najvec, T2.najmanj from (
	select vadba.id, vadba.naziv,  year(datum) leto, MONTH(datum) mesec, count(*) skupaj from 
		udelezba u join ura on u.ura_id=ura.id join vadba on ura.VADBA_id = vadba.id
		where u.`status`='prijavljen'
		group by ura.VADBA_id, year(datum), MONTH(datum)
) T1,
	(select T3.id, naziv, leto, mesec, avg(obisk) povprecno, min(obisk) 'najmanj', max(obisk) 'najvec' from (
		select vadba.id, vadba.naziv,  year(datum) leto, MONTH(datum) mesec, DAY(datum) dan, count(*) obisk from 
			udelezba u join ura on u.ura_id=ura.id join vadba on ura.VADBA_id = vadba.id
			where u.`status`='prijavljen'
			group by ura.VADBA_id, year(datum), MONTH(datum), DAY(datum)) T3
	group by id, leto, mesec) T2
WHERE T1.id = T2.id and T1.leto = T2.leto and T1.mesec=T2.mesec
";
*/
$sql = "select T.vid, T.naziv, T.leto, 
count(ura_id) 'ur', 
AVG( T.udelezencev ) 'povprecno', STD(T.udelezencev) 'odlon', VAR_POP(T.udelezencev) 'varianca',
min(T.udelezencev) 'najmanj', max(T.udelezencev) 'najvec',
sum( T.udelezencev ) 'skupaj'
from
(select v.id vid, v.naziv, year(ud.datum) leto, MONTH(ud.datum) mesec, DAY(ud.datum) dan, ura_id, v.kratkoIme, count(*) 'udelezencev'
		from vadba v 
		join ura u on (u.VADBA_id = v.id) 
		join udelezba ud on (u.id = ud.ura_id)
		where ud.`status` in ('prijavljen', 'potrjen')
	GROUP BY ura_id, ud.datum) T
group by T.vid, T.leto;
";
$res = $db->executeSelect($sql) or die("lalal");


foreach ($res as $row) {
	print "<tr>";
	foreach ($row as $f=>$data) {
		print "<td ftype='$f' align='left'>$data</td>";
	}
	print "</tr>";
}
?>
</tbody>
</table>


<h3><?php print t("statsClassesGeneralReport"); ?></h3>


<p>
<div id="chartdiv4" style="height:400px;width:600px; margin-top: 15px; margin-bottom: 30px; margin-left: 60px;"></div> 
</p>


<table id="t4" width="100%" class="sortableTable styledTable" cellspacing="0" cellpadding="5">
<thead>
	<tr>
	<th align="left">id</th>
	<th align="left"><?php print t("class"); ?></th>
	<th align="left"><?php print t("hours"); ?></th>
	<th align="left"><?php print t("average"); ?></th>
	<th align="left"><?php print t("std"); ?></th>
	<th align="left"><?php print t("var"); ?></th>
	<th align="left"><?php print t("minimum"); ?></th>
	<th align="left"><?php print t("maximum"); ?></th>
	<th align="left"><?php print t("sum"); ?></th>
	</tr>
</thead>
<tbody>
<?php 

$sql = "select t.vid, t.naziv, count(*) 'ur', 
AVG( udelezba ) 'povprecno', STD( udelezba ) 'odlon', VAR_POP( udelezba ) 'varianca',
min( udelezba ) 'najmanj', max( udelezba ) 'najvec',
sum( udelezba ) 'skupaj'  
from 
(SELECT v.id vid, v.naziv, count(*) 'udelezba'
from vadba v join ura on ura.VADBA_id=v.id
join udelezba u on ura.id=u.ura_id
where u.`status` in ('prijavljen', 'potrjen')
group by ura.id, datum) t
group by t.vid
ORDER BY naziv
";
$res = $db->executeSelect($sql) or die("lalal");


foreach ($res as $row) {
	print "<tr>";
	foreach ($row as $f=>$data) {
		print "<td ftype='$f' align='left'>$data</td>";
	}
	print "</tr>";
}
?>
</tbody>
</table>


<?php 
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$finish = $time;
	$total_time = round(($finish - $start), 4);
	echo '<p>Page generated in '.$total_time.' seconds.'."</p>";
?>
