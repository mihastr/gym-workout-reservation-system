<?php 
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$start = $time;
?>
<table width="100%" id="urnikStranke">
	<thead>
		<tr>
			<th>URA</th>
			<?php
			$dnevi = array("nedelja", "ponedeljek", "torek", "sreda", "četrtek", "petek", "sobota");
			
			$danVTednu =  date("w", time());
			$j=0;
			for ($i=$danVTednu; $i<$danVTednu+7; $i++) {
				print "<th>" . $dnevi[$i%7] . " (" . date("d.m.Y", time()+$j++*24*60*60) . ")</th>";
			}
			$danVTednu =  date("N", time());
			?>
		</tr>
	</thead>
	<tbody>


	<?php
	$startTime = 0;
	//print "dan : $danVTednu ";
	
	for ($i=(6*60); $i<(24*60); $i+=30) {
		$ura = sprintf("%2s:%2s", str_pad(floor($i/60), 2, '0', STR_PAD_LEFT), str_pad(($i%60), 2, '0', STR_PAD_LEFT));
		print "<tr class='normal' ura='" . $ura . "'>";
		print "
		<td>" . $ura . "</td>
		<td datum='" . date("Y-m-d", time()) . "' dan='" . ((($danVTednu-1 + 0) % 7)+1) . "'><div class='forVadba'><a href='#' title=''></a></div> <div class='forInstruktor'><a href='#' title=''></a></div><div class='forOdjava'><a href='#' title='Odjavi'><img src='images/minus.gif' /></a></div><div class='forPrijava'><a href='#' title='Prijava na uro'><img src='images/plus.gif' /></a></div><div class='forInfo'></div></td>
	  	<td datum='" . date("Y-m-d", time()+24*60*60) . "' dan='" . ((($danVTednu-1 + 1) % 7)+1) . "'><div class='forVadba'><a href='#' title=''></a></div> <div class='forInstruktor'><a href='#' title=''></a></div><div class='forOdjava'><a href='#' title='Odjavi'><img src='images/minus.gif' /></a></div><div class='forPrijava'><a href='#' title='Prijava na uro'><img src='images/plus.gif' /></a></div><div class='forInfo'></div></td>
	  	<td datum='" . date("Y-m-d", time()+2*24*60*60) . "' dan='" . ((($danVTednu-1 + 2) % 7)+1) . "'><div class='forVadba'><a href='#' title=''></a></div> <div class='forInstruktor'><a href='#' title=''></a></div><div class='forOdjava'><a href='#' title='Odjavi'><img src='images/minus.gif' /></a></div><div class='forPrijava'><a href='#' title='Prijava na uro'><img src='images/plus.gif' /></a></div><div class='forInfo'></div></td>
	  	<td datum='" . date("Y-m-d", time()+3*24*60*60) . "' dan='" . ((($danVTednu-1 + 3) % 7)+1) . "'><div class='forVadba'><a href='#' title=''></a></div> <div class='forInstruktor'><a href='#' title=''></a></div><div class='forOdjava'><a href='#' title='Odjavi'><img src='images/minus.gif' /></a></div><div class='forPrijava'><a href='#' title='Prijava na uro'><img src='images/plus.gif' /></a></div><div class='forInfo'></div></td>
	  	<td datum='" . date("Y-m-d", time()+4*24*60*60) . "' dan='" . ((($danVTednu-1 + 4) % 7)+1) . "'><div class='forVadba'><a href='#' title=''></a></div> <div class='forInstruktor'><a href='#' title=''></a></div><div class='forOdjava'><a href='#' title='Odjavi'><img src='images/minus.gif' /></a></div><div class='forPrijava'><a href='#' title='Prijava na uro'><img src='images/plus.gif' /></a></div><div class='forInfo'></div></td>
	  	<td datum='" . date("Y-m-d", time()+5*24*60*60) . "' dan='" . ((($danVTednu-1 + 5) % 7)+1) . "'><div class='forVadba'><a href='#' title=''></a></div> <div class='forInstruktor'><a href='#' title=''></a></div><div class='forOdjava'><a href='#' title='Odjavi'><img src='images/minus.gif' /></a></div><div class='forPrijava'><a href='#' title='Prijava na uro'><img src='images/plus.gif' /></a></div><div class='forInfo'></div></td>
	  	<td datum='" . date("Y-m-d", time()+6*24*60*60) . "' dan='" . ((($danVTednu-1 + 6) % 7)+1) . "'><div class='forVadba'><a href='#' title=''></a></div> <div class='forInstruktor'><a href='#' title=''></a></div><div class='forOdjava'><a href='#' title='Odjavi'><img src='images/minus.gif' /></a></div><div class='forPrijava'><a href='#' title='Prijava na uro'><img src='images/plus.gif' /></a></div><div class='forInfo'></div></td>";
		print "</tr>";
	}
	
	

	?>

	</tbody>

</table>


<?php 
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$finish = $time;
	$total_time = round(($finish - $start), 4);
	echo '<p>Page generated in '.$total_time.' seconds.'."</p>";
	?>