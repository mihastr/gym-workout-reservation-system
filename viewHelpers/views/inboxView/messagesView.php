<script>
$(document).ready(function(rEvt) {
	$(".sortableTable").tablesorter( { "sortList": [[2,1]]} );
});
</script>

<?php 
if (checkPermission("enterNewPost")) {
	$dir = 'newMsgForm.php';
	
	?>
	<?php require_once( $dir ); ?>
	<script>
	$(document).ready(function(rEvent) {
		$("#newMSGButton").click(function(cEvt) {
			$("#novoObvestiloDIV").dialog("open");
		});
				
	});
	</script>
	<p>
	<input id="newMSGButton" type="button" value="<?php print t("new"); ?>" />
	</p>
	<?php 
}
?>

<div class="roundedTableCorner" style="width: 800px;">
<table cellpadding="5" id="msgListTable" class="styledTable sortableTable">
<thead>
<tr>
	<th><?php print t("title") ?></th>
	<th><?php print t("content") ?></th>
	<th><?php print t("created") ?></th>
	<th><?php print t("updated") ?></th>
	<th><?php print t("sender") ?></th>
	<th><?php print t("receiver") ?></th>
	<th><?php print t("validFrom") ?></th>
	<th><?php print t("validTo") ?></th>
</tr>
</thead>
<tbody>
<?php 
foreach ($this->list as $item) {
	print "\n<tr objid='" . $item["id"] . " " . ($item["brisan"]=="1" ? " class='brisan'" : "") ."'>";
	print "
	<td ftype='naslov'>" . $item["naslov"] . "</td>
	<td ftype='tekst'>" . $item["tekst"] . "</td>
	<td ftype='ustvarjen'>" . datePrintFormated($item["ustvarjen"]) . "</td>
	<td ftype='posodobljen'>" . datePrintFormated($item["posodobljen"]) . "</td>
	<td ftype='posiljatelj'>" . $item["posiljatelj"] . "</td>

	<td ftype='za'>" .
	($item["za"]=="vsi" ? $item["za"] : "<a href='#' class='msgsShowRecipients'>" . t("msgsShowRecipients") . "</a>" ) 
	 . "</td>
	
	<td ftype='veljaOD'>" . datePrintFormated($item["veljaOD"]) . "</td>
	<td ftype='veljaDO'>" . datePrintFormated($item["veljaDO"]) . "</td>
	";
	print "\n</tr>";
}
?>
</tbody>
</table>
</div>