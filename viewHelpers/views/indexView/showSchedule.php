<?php
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$start = $time;
/*
if (isset($_GET["prostor"])) {
	$prostorUrnika = (int)$_GET["prostor"];
	Uporabnik::changeDefSchedule((int)$_GET["prostor"]);
} else {
*/
	if (isset($_SESSION["nastavitve"]["defSchedule"]["uporabniska"]))
		$prostorUrnika = $_SESSION["nastavitve"]["defSchedule"]["uporabniska"];
	else
		$prostorUrnika = 8;
/*
}
*/

?>
<div id="odpovedUreDialog" style="display:none">Prosimo, navedite razlog:<br />
	<input type='text' id='cancelationReason' /><br />
	<input id='cbxNotifyAtt' type='checkbox' checked='checked' /> pošlji
	obvestilo prijavljenim?</div>
	<div style="display: none" id="uraInfoDialog">Informacije o izbrani uri
	<p><input type="hidden" id="uraDetailDatum" /> <input type="hidden"
		id="uraDetailUra" /> Vadba: <span id="podrobnostiVadba"></span><br />
	<p>Instruktor: <span id="podrobnostiInstruktor"></span></p>
	<p>Prijav: <span id="podrobnostiPrijav"></span></p>
	<p>Kapaciteta: <span id="podrobnostiMesta"></span></p>
	<p>dodaj: <input type="text" id="newAtendee" /></p>
	</p>
	<ul id="seznamVadecih">
		<li class='nodata'>ni prijav</li>
	</ul>
</div>
<script language="javascript">
prostor = <?php print $prostorUrnika; ?>;
//alert(prostor);
</script>
<?php if (((isset($_SESSION["newRole"]) && ($_SESSION["newRole"]=="admin" || $_SESSION["newRole"]=="manager")) ||
(isset($_SESSION["role"]) && ($_SESSION["role"]=="admin" || $_SESSION["role"]=="manager"))) && $_SESSION["newRole"]!="user" && $_SESSION["newRole"]!="guest") { ?>
<script
	language="javascript" src="js/urnikAdmin.js"></script>
<?php } else { ?>
<script
	language="javascript" src="js/urnik.js"></script>
<?php }?>
<?php
// skriptni del
$dnevi = array("nedelja", "ponedeljek", "torek", "sreda", "četrtek", "petek", "sobota");
?>
<p><?php print t("schPlaceSelectionText"); ?>
<div><?php 
if (isset($_SESSION["userid"])) {
	$user = $_SESSION["userid"];
} else {
	$user = 1;
}
$seznamProstorov = Urnik::getSeznamProstorov(array("uporabnik"=>$user));

print "<ul class='menu' id='placeSelectionMenu'>";
foreach($seznamProstorov as $prostor) {
	if ($prostorUrnika==$prostor["pid"]) {
		print "<li class='selected' objid='" . $prostor["pid"] . "'><a href='index.php?prostor=" . $prostor["pid"] . "'>" . $prostor["naziv_fc"] . " " . $prostor["naziv_prostora"] . "</a></li>";
	} else {
		print "<li objid='" . $prostor["pid"] . "'><a href='index.php?prostor=" . $prostor["pid"] . "'>" . $prostor["naziv_fc"] . " " . $prostor["naziv_prostora"] . "</a></li>";
	}
	//print_r($prostor);
}
print "</ul>";
?></div>
</p>
<br />
<div class="roundedTableCorner">
<table width="100%" id="urnikStranke">
	<thead>
		<tr>
			<th>URA</th>
			<?php
			$danVTednu =  date("w", time());
			$j=0;
			for ($i=$danVTednu; $i<$danVTednu+7; $i++) {
				print "<th>" . $dnevi[$i%7] . " (" . date("d.m.Y", time()+$j++*24*60*60) . ")</th>";
			}
			//$danVTednu =  date("N", time());
			?>
		</tr>
		<tr class="calRow">
			<th>kalorije</th>
			<?php 
			for ($i=$danVTednu; $i<$danVTednu+7; $i++) {
				print "<th class='dayCalContainer' dan='". ($i%7) ."'>0</th>";
			}
			$danVTednu =  date("N", time());
			?>
		</tr>
	</thead>
	<tbody>


	<?php
	$startTime = 0;
	//print "dan : $danVTednu ";
	
	function genTable($danVTednu) {
		$str = "";
		for ($i=(6*60); $i<(24*60); $i+=30) {
			$ura = sprintf("%2s:%2s", str_pad(floor($i/60), 2, '0', STR_PAD_LEFT), str_pad(($i%60), 2, '0', STR_PAD_LEFT));
			$str .= "\n<tr class='normal' ura='" . $ura . "'>";
			$str .= "
			<td>" . $ura . "</td>
			<td datum='" . date("Y-m-d", time()) . "' dan='" .            ((($danVTednu-1 + 0) % 7)+1) . "'><div class='forVadba'><a class='notlink' href='#' title=''></a></div> <div class='forInstruktor'><a href='#' title=''></a></div><div class='forLoading'><img src='images/refresh.gif' /></div><div class='forOdjava'><a href='#' title='Odjavi'><img src='images/minus.gif' /></a></div><div class='forPrijava'><a href='#' title='Prijava na uro'><img src='images/plus.gif' /></a></div><div class='forInfo'></div></td>
		  	<td datum='" . date("Y-m-d", time()+24*60*60) . "' dan='" .   ((($danVTednu-1 + 1) % 7)+1) . "'><div class='forVadba'><a href='#' title=''></a></div> <div class='forInstruktor'><a href='#' title=''></a></div><div class='forLoading'><img src='images/refresh.gif' /></div><div class='forOdjava'><a href='#' title='Odjavi'><img src='images/minus.gif' /></a></div><div class='forPrijava'><a href='#' title='Prijava na uro'><img src='images/plus.gif' /></a></div><div class='forInfo'></div></td>
		  	<td datum='" . date("Y-m-d", time()+2*24*60*60) . "' dan='" . ((($danVTednu-1 + 2) % 7)+1) . "'><div class='forVadba'><a href='#' title=''></a></div> <div class='forInstruktor'><a href='#' title=''></a></div><div class='forLoading'><img src='images/refresh.gif' /></div><div class='forOdjava'><a href='#' title='Odjavi'><img src='images/minus.gif' /></a></div><div class='forPrijava'><a href='#' title='Prijava na uro'><img src='images/plus.gif' /></a></div><div class='forInfo'></div></td>
		  	<td datum='" . date("Y-m-d", time()+3*24*60*60) . "' dan='" . ((($danVTednu-1 + 3) % 7)+1) . "'><div class='forVadba'><a href='#' title=''></a></div> <div class='forInstruktor'><a href='#' title=''></a></div><div class='forLoading'><img src='images/refresh.gif' /></div><div class='forOdjava'><a href='#' title='Odjavi'><img src='images/minus.gif' /></a></div><div class='forPrijava'><a href='#' title='Prijava na uro'><img src='images/plus.gif' /></a></div><div class='forInfo'></div></td>
		  	<td datum='" . date("Y-m-d", time()+4*24*60*60) . "' dan='" . ((($danVTednu-1 + 4) % 7)+1) . "'><div class='forVadba'><a href='#' title=''></a></div> <div class='forInstruktor'><a href='#' title=''></a></div><div class='forLoading'><img src='images/refresh.gif' /></div><div class='forOdjava'><a href='#' title='Odjavi'><img src='images/minus.gif' /></a></div><div class='forPrijava'><a href='#' title='Prijava na uro'><img src='images/plus.gif' /></a></div><div class='forInfo'></div></td>
		  	<td datum='" . date("Y-m-d", time()+5*24*60*60) . "' dan='" . ((($danVTednu-1 + 5) % 7)+1) . "'><div class='forVadba'><a href='#' title=''></a></div> <div class='forInstruktor'><a href='#' title=''></a></div><div class='forLoading'><img src='images/refresh.gif' /></div><div class='forOdjava'><a href='#' title='Odjavi'><img src='images/minus.gif' /></a></div><div class='forPrijava'><a href='#' title='Prijava na uro'><img src='images/plus.gif' /></a></div><div class='forInfo'></div></td>
		  	<td datum='" . date("Y-m-d", time()+6*24*60*60) . "' dan='" . ((($danVTednu-1 + 6) % 7)+1) . "'><div class='forVadba'><a href='#' title=''></a></div> <div class='forInstruktor'><a href='#' title=''></a></div><div class='forLoading'><img src='images/refresh.gif' /></div><div class='forOdjava'><a href='#' title='Odjavi'><img src='images/minus.gif' /></a></div><div class='forPrijava'><a href='#' title='Prijava na uro'><img src='images/plus.gif' /></a></div><div class='forInfo'></div></td>";
			$str .= "\n</tr>";
		}
		return $str;
	}
	
	$tabela = "";
	if (PageSettings::use_memcached) {
		$tabela = getFromCache("tedenski_urnik");
		if ($tabela==null) {
			$tabela = genTable($danVTednu);
			setToCache("tedenski_urnik", $tabela);
			//print "tabela sveža";
		} else {
			//print "tabela iz spomina";
		}
	} else {
		$tabela = genTable($danVTednu);
		//print "memcahe onemogočen";
	}

	echo $tabela;
	
	?>



	</tbody>

</table>
<p style="margin-top: 15px;">
<a href='http://<?php print PageSettings::path; ?>/rss?get=mojUrnik&rsskoda=<?php print $rssCode; ?>'><img src="images/rss.png" height="18px" /> RSS vašega urnika</a>
<a href='http://<?php print PageSettings::path; ?>/ical?get=mojUrnik&rsskoda=<?php print $rssCode; ?>'><img src="images/ical.gif" /> iCal vašega urnika</a>
</p>

</div>

<?php 
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$finish = $time;
	$total_time = round(($finish - $start), 4);
	echo '<p id="genit">Page generated in '.$total_time.' seconds.'."</p>";
	?>
