<h3><?php echo t("weatherTitle"); ?></h3>
<div>
<?php 
$v = $this->vreme->getTrenutnoVreme();
echo "<table>
<tr>
  <td>posodobljeno: " . $v["datum"] . "</td><td>" . $v["ura"] . "</td>
</tr>
<tr>";
echo "<td>";
$this->vreme->getIcon($v["vreme"]);
echo "</td>";
echo "<td>";
echo "<b>" . $v["vreme"] . "</b><br />";
echo t("temperature") . ": <b>" . $v["temp_ams"] . "°C</b><br />";
echo t("humidity") . ": <b>" . $v["vlaga_ams"] . "%</b><br />";
echo t("preasure") . ": <b>" . $v["tlak_avg_ams"] . " mbar</b><br />";
echo t("wind") . ": <b>" . $v["hitrost_vetra"] . "km/h (" . nvl($v["smer_vetra"], "-") . ")</b><br />";
echo "</td>";
echo "</tr></table>";

$napoved = $this->vreme->getNapovedi3Dni();

echo "<h3>" . t("forecast") . "</h3>";
echo "<table CELLPADDING='10'><tr>";
foreach ($napoved as $dan) {
	echo "<td valign='top'><b>" . getDayName($dan["za_dan"]) . "<br /> " . datePrintFormated($dan["za_dan"]) . "</b><br />";
	
	$vreme = ($dan["vreme_tekst"]!=null ? $dan["vreme_tekst"] : $dan["oblacnost_tekst"]);
	$smerVetra = ($dan["smer_vetra"]!=null ? "(" . $dan["smer_vetra"] . ")" : "");
	
	$this->vreme->getIcon($vreme);
	echo "<br />";
	echo "<b>" . $dan["oblacnost_tekst"] . ", " . $dan["vreme_tekst"] . "</b><br />";
	echo t("temperature") . ": <br /><b>" . $dan["temp"] . " (min: " . $dan["min_temp"] . ", maks: " . $dan["max_temp"] . ")</b><br />";
	//echo t("humidity") . ": " . $dan["vlaga"] . "<br />";
	//echo t("preasure") . ": <b>" . $dan["tlak"] . "</b><br />";
	//echo t("wind") . ": <b>" . $dan["hitrost_vetra"] . "km/h " . $smerVetra . "</b><br />";
	
	/*
	 * 
	 * 
	 * <td>Array
(
    [id] =&gt; 9
    [n_dni] =&gt; 3
    [datum] =&gt; 2011-03-15
    [ura] =&gt; 12:40:46
    [kraj] =&gt; Ljubljana
    [podrocje] =&gt; Slovenija / osrednja
    [za_dan] =&gt; 2011-03-15
    [oblacnost_icon] =&gt; overcast
    [oblacnost_tekst] =&gt; oblačno
    [vreme_icon] =&gt; SHRA
    [vreme_tekst] =&gt; ploha
    [min_temp] =&gt; 7
    [max_temp] =&gt; 14
    [temp] =&gt; 13
    [vlaga] =&gt; 
    [temp_rosisca] =&gt; 8
    [smer_vetra] =&gt; SW
    [hitrost_vetra] =&gt; 4
    [sunki_vetra] =&gt; 
    [tlak] =&gt; 1019
    [veljavna] =&gt; 1
)
</td>
	 */
	//print_r($dan);
	echo "</td>";
}
echo "</tr></table>";
?>
</div>