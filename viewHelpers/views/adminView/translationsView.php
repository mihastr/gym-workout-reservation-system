<script language="javascript">
$(document).ready(function(rEv) {
	$( "#wordSearch" ).autocomplete({
		source: function( request, response ) {
			$.ajax({
				url: "api.php?params=action=findTranslation",
				type: "POST",
				dataType: "json",
				data: {
					q: request.term,
					lang: "sl"
				},
				success: function( data ) {
					//alert("lala");
					$("#timer").text(data.genTime);
					response( $.map( data.message.body.text, function( item ) {
						//alert(JSON.stringify(item));
						return {
							//{"id":"1","tag":"helo","lang":"sl","translation":"Pozdravljeni"}
							label: item.translation,
							value: item.translation,
							tag: item.tag,
							id: item.id
						};
					}));
				}
			});
		},
		minLength: 2,
		select: function( event, ui ) {
			$("#wordTag").val(ui.item.tag);
			$("#wordTranslation").val(ui.item.value);
		},
		open: function() {
			$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
		},
		close: function() {
			$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
		}
	});

	function clearFields() {
		$("#wordSearch").val("");
		$("#wordTranslation").val("");
		$("#wordTag").val("");
	}

	$("#clearButton").click(function(cEv) {
		clearFields();
	});
	
	$("#saveTranslation").click(function(cEv) {
		var translation = $("#wordTranslation").val();
		var tag = $("#wordTag").val();
		
		jQuery.post( "api.php?params=action=saveTranslation", { "tag": tag, "translation": translation }, function(data, textStatus, XMLHttpRequest) {
			//alert("shranjeno");
			//alert("shranjeno " + textStatus);
			//alert("shranjeno " + JSON.stringify(data));
			if (textStatus=="success")
				if (data.error=="no") {
					if (data.message.body.text=="ok") {
						alert("shranjeno");
						clearFields();
					} else {
						alert("napaka");
					}
					$("#timer").text(data.genTime);
				}
				waitIndicator.hide();
		}, "json" );

	});
});
</script>
<h3><?php print t("translationTitle"); ?></h3>
<?php print t("search"); ?>:<br />
<input type="text" id="wordSearch" /> <img src="images/clear.png" id="clearButton" />
<div>
<p>
<?php print t("tag"); ?>:<br />
<input type="text" disabled="disabled" id="wordTag" />
</p>
<p align="left">
<?php print t("text"); ?>:<br /><br />
<textarea style="width: 450px; height: 250px;" id="wordTranslation"></textarea>
</p>
<input type="button" id="saveTranslation" value="<?php print t("save"); ?>" />
<p>Čas za akcijo: <b id="timer">0</b></p>
</div>