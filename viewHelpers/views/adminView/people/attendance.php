<script language="javascript">
/*
 * 
function loadLogEntries() {
	jQuery.post("load.php?page=sifranti&sub=people&action=log", {"uporabnik": userid}, 
		function(data, textStatus, XMLHttpRequest) {
			$("#userProfileLog tbody").append("\n<tr><td colspan=3>lal</td></tr>");
		}
	);
} */
$(document).ready(function(rEvent) {
	$("#userProfileAttendance a.userRemoveAttendance").click(function(cEv) {
		waitIndicator.show();
		cEv.preventDefault();
		var id = $(this).attr("objid");
		var datum = $(this).attr("datum");
		var uporabnik = <?php echo $p["uporabnik"];  ?>;
 
		jQuery.post( "api.php?params=action=removeAttendance", {"ura_id": id, "datum": datum, "uporabnik": uporabnik }, function(data, textStatus, XMLHttpRequest) {
			//alert("shranjeno");
			//alert("shranjeno " + textStatus);
			//alert("shranjeno " + JSON.stringify(data));
			if (textStatus=="success")
				if (data.error=="no") {
					//alert(data.message.body.text[0].uporabnik_id);
					$("#userProfileAttendance tbody tr[objid='" + data.message.body.text[0].ura_id + "'][datum='" + data.message.body.text[0].datum + "']").fadeOut();
				}
				waitIndicator.hide();
		}, "json" );
		
		//alert("id=" + id);
	});
});
</script>
<?php 
/*
if (isset($_GET["start"]) && is_numeric($_GET["start"])) $start = $_GET["start"]; else $start=0;
if (isset($_GET["count"]) && is_numeric($_GET["count"])) $count = $_GET["count"]; else $count=10;
*/
$dveniki = $page->uporabnik->getFutureAttendance($p["uporabnik"]);
?>
<div class="roundedTableCorner" id="userProfileAttendance">
<table>
<tr>
<th>id ure</th>
<th>datum</th>
<th>ura</th>
<th>vadba</th>
<th>inštruktor</th>
<th>center</th>
<th>prostor</th>
<th>&nbsp;</th>
</tr>
<tbody>
<?php 
if (count($dveniki)==0) {
	print "<tr><td colspan='8'>ni prijav</td></tr>";
} else {
	foreach ($dveniki as $vrstica) {
		print "\n<tr objid='" . $vrstica["id"] . "' datum='" . $vrstica["datum"] . "'>";
		foreach ($vrstica as $polje) {
			print "<td>$polje</td>";
		}
		print "<td><a class='userRemoveAttendance' datum='" . $vrstica["datum"] . "' objid='" . $vrstica["id"] . "' href='#'>odjavi</a></td></tr>";
	}
}
?>
</tbody>
</table>
</div>
