<?php 
$dovoljenja = $page->uporabnik->gerPermissions($p["uporabnik"]);
?>
<script language="javascript">
$(document).ready(function(rEv) {
	$("#userPresmissionsForm input[type='checkbox'][name='dovoljenje']").change(function(ev) {
		var checked = $(this).is(":checked");
		var dovoljenje = $(this).attr("objid");
		
		var user = $("#userid").val(); 
		jQuery.post( "api.php?params=action=changePermission", { "user": user, "permission": dovoljenje, "selected": checked }, function(data, textStatus, XMLHttpRequest) {
			//alert("shranjeno");
			//alert("shranjeno " + textStatus);
			//alert("shranjeno " + JSON.stringify(data));
			if (textStatus=="success")
				if (data.error=="no") {
				} else {
					if (data.message.body.text=="unauthorized") {
						alert("<?php echo t("unauthorized"); ?>");
					} else {
						alert("ni shranjeno " + JSON.stringify(data));
					}
				}
		}, "json" );
	});
});
</script>
<p><b>Seznam dovoljenj za uporabnika</b></p>
<div class="roundedTableCorner" id="userProfilePermissions">

<form id="userPresmissionsForm">
<table cellpadding="7" width="100%">
<tr>
<th>&nbsp;</th>
<th>dovoljenje</th>
<th>modul</th>
<th>pravica</th>
</tr>

<?php
if (count($dovoljenja)==0) {
	print '<tr><td colspan="4">ni posebnih dovoljenj</td></tr>';
} else {
	foreach ($dovoljenja as $dovoljenje) {
		print '<tr><td><input name="dovoljenje" objid="' . $dovoljenje["id"] . '" type="checkbox" '. ($dovoljenje["izbran"]==1 ? "checked='checked'" : "") . ' /></td><td>'. $dovoljenje["naziv"] .'</td><td>'. $dovoljenje["modul"] .'</td><td>'. $dovoljenje["rwu"] .'</td></tr>';
	}
}
?>
</table>
</form>
</div>