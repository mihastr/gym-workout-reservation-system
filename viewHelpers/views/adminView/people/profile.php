<?php 
$page->profil = $page->uporabnik->findByID($_POST["uporabnik"]);
		
$page->naziv = $page->profil["ime"] . " " . $page->profil["priimek"] . ((isset($page->profil["naziv"]) &&  strlen($page->profil["naziv"])>0) ? " (" . $page->profil["naziv"] . ")" : "");

$userid = $_POST["uporabnik"];
?>
<script>
$(document).ready(function(rEvent) {
	waitIndicator.hide();
	$("#userProfileData a.profileFieldEdit").click(function (cEvent) {
		cEvent.preventDefault();
		var row = $(this).parent().parent();
		var editable = row.find(".editable");
		var oldValue = editable.text();
				
		editable.html("<input type='text' value='" + oldValue + "' /> <a href='#' action='ok'><img src='images/ok.png' /></a> <a href='#' action='cancel'><img src='images/cancel.png' /></a>");
	});

	function changeProfile(field, value, user) {
		jQuery.post( "api.php?params=action=changeProfile", {"field": field, "value": value, "user": user}, function(data, textStatus, XMLHttpRequest) {
			//alert("shranjeno");
			//alert("shranjeno " + textStatus);
			//alert("shranjeno " + JSON.stringify(data));
			if (textStatus=="success")
				if (data.error=="no") {
					//$("#noticeField").html("shranjeno");
					//$("#noticeField").show().fadeOut(5000);
					//$("#urnikColTable tbody").prepend(data.message.body.text).hide().fadeIn();
					waitIndicator.hide();
					
					//alert("all ok");
				} else {
					alert("napaka, prosimo kontaktirajte Sokol - pomoč");
				}
			}, "json" );
	}
	
	$("#userProfileData td.editable a").live("click", function (cEvent) {
		cEvent.preventDefault();
		var user = $("#userid").val(); 
		
		var action = $(this).attr("action");
		var row = $(this).parent().parent();
		var oldValue = row.attr("old");
		var td = row.find(".editable");
		var ftype = row.attr("ftype");
		var colname = row.attr("colname");
		switch (action) {
		case "ok":
			var value = td.find("input").val();
			if (ftype == "email") {
				
				if (!checkEmail(value)) {
					alert("E-naslov mora biti v pravilni obliki primer: primer@naslov.com");
					return;
				}
				td.text(value);
			} else {
				td.text(value);
			}
			if (colname!=null && colname.length>0 && value!=null)
				changeProfile(colname, value, user);
			
			break;
		case "cancel":
			td.text(oldValue);
			break;			
		default:
			break;
		}
	});

	$("#genRSSCode").click(function (cEv) {
		cEv.preventDefault();
		var user = $("#userid").val();
		jQuery.post( "api.php?params=action=generateRSSCode", {"user": user}, function(data, textStatus, XMLHttpRequest) {
			//alert("shranjeno");
			//alert("shranjeno " + textStatus);
			//alert("shranjeno " + JSON.stringify(data));
			if (textStatus=="success")
				if (data.error=="no") {
					//$("#noticeField").html("shranjeno");
					//$("#noticeField").show().fadeOut(5000);
					//$("#urnikColTable tbody").prepend(data.message.body.text).hide().fadeIn();
					waitIndicator.hide();
					//alert(data.message.body.text);
					$("#rssCodeSpan").text(data.message.body.text);		
					//alert("all ok");
				} else {
					alert("napaka, prosimo kontaktirajte Sokol - pomoč");
				}
			}, "json" );
	});

	$("#userType input[type='radio'][name='type']").change(function() {
		var choice = $(this).val();
		var user = $("#userid").val(); 
		jQuery.post( "api.php?params=action=changeUserType", {"userType": choice, "user": user }, function(data, textStatus, XMLHttpRequest) {
			//alert("shranjeno");
			//alert("shranjeno " + textStatus);
			//alert("shranjeno " + JSON.stringify(data));
			if (textStatus=="success")
				if (data.error=="no") {
				} else {
					if (data.message.body.text=="unauthorized") {
						alert("<?php echo t("unauthorized"); ?>");
					} else {
						alert("ni shranjeno " + JSON.stringify(data));
					}
				}
		}, "json" );
	});
});
</script>
<div id="profileDIV">
<p><b><?php print $page->naziv; ?></b></p>
<p>ID uporabnika: <input type="text" id="userid" value="<?php print $userid; ?>" disabled="disabled" /></p>
<div class="roundedTableCorner" id="userProfileData">
<table>
	<tr ftype="text" colname="ime" old="<?php print $page->profil["ime"]; ?>">
		<th>ime</th>
		<td class="editable"><?php print $page->profil["ime"]; ?></td>
		<td class="tableActions"><a href="#" class="profileFieldEdit">uredi</a></td>
	</tr>
	<tr ftype="text" colname="priimek" old="<?php print $page->profil["priimek"]; ?>">
		<th>priimek</th>
		<td class="editable"><?php print $page->profil["priimek"]; ?></td>
		<td class="tableActions"><a href="#" class="profileFieldEdit">uredi</a></td>
	</tr>
	<tr ftype="text" colname="naziv" old="<?php print $page->profil["naziv"]; ?>">
		<th>vzdevek</th>
		<td class="editable"><?php print $page->profil["naziv"]; ?></td>
		<td class="tableActions"><a href="#" class="profileFieldEdit">uredi</a></td>
	</tr>
	<tr ftype="text" colname="naslov" old="<?php print $page->profil["naslov"]; ?>">
		<th>Naslov</th>
		<td class="editable"><?php print $page->profil["naslov"]; ?></td>
		<td class="tableActions"><a href="#" class="profileFieldEdit">uredi</a></td>
	</tr>
	<tr ftype="email" colname="email" old="<?php print $page->profil["email"]; ?>">
		<th>email</th>
		<td class="editable"><?php print $page->profil["email"]; ?></td>
		<td class="tableActions"><a href="#" class="profileFieldEdit">uredi</a></td>
	</tr>
	<tr ftype="text" colname="gsm" old="<?php print $page->profil["gsm"]; ?>">
		<th>gsm</th>
		<td class="editable"><?php print $page->profil["gsm"]; ?></td>
		<td class="tableActions"><a href="#" class="profileFieldEdit">uredi</a></td>
	</tr>
	<tr ftype="text" colname="kontakt_preko" old="<?php print $page->profil["kontakt_preko"]; ?>">
		<th>kontakt preko:</th>
		<td class="editable"><?php print $page->profil["kontakt_preko"]; ?></td>
		<td class="tableActions"><a href="#" class="profileFieldEdit">uredi</a></td>
	</tr>
	<tr>
		<th>zasebna koda (za RSS ter iCal)</th>
		<td><span id="rssCodeSpan"><?php print $page->profil["rsskoda"]; ?></span> <a href="#" id="genRSSCode">generiraj kodo</a></td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<th>uporabite FB za prijavo</th>
		<td>fb link</td>
		<td>&nbsp;</td>
	</tr>
	<?php if ($page->profil["id"]!=$_SESSION["userid"]) { ?>
	<tr>
		<th><?php echo t("userType"); ?></th>
		<td>
		<form id="userType">
		<input type="radio" name='type' value="user" <?php echo ($page->profil["vloga"] == "user" ? "checked='checked'" : ""); ?> /> <?php echo t("user"); ?><br />
		<input type="radio" name='type' value="admin" <?php echo ($page->profil["vloga"] == "admin" ? "checked='checked'" : ""); ?>/> <?php echo t("admin"); ?><br />
		<input type="radio" name='type' value="manager" <?php echo ($page->profil["vloga"] == "manager" ? "checked='checked'" : ""); ?>/> <?php echo t("manager"); ?>
		</form></td>
		<td>&nbsp;</td>
	</tr>
	<?php } else {
		?>
		<tr>
		<th><?php echo t("userType"); ?></th>
		<td>
			<?php echo t($page->profil["vloga"]);?>
		</td>
		<td>&nbsp;</td>
	</tr>
		<?php 
	} ?>
	<tr>
	<th><?php echo t("isInstructor"); ?></th>
	<th><?php echo $page->profil["instruktor_id"] == null; ?></th>
	<th></th>
	</tr>
</table>
</div>
</div>
