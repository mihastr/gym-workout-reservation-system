<div>
<h3>Poročilo o napakah</h3>
<p>
<form> 
<input type='checkbox' id="bugReportShowResolved" <?php
if (isset($_REQUEST["showResolved"]) && ($_REQUEST["showResolved"]==1)) {
	echo "checked";
	$showResolved = true;
} else {
	$showResolved = false;
}
?> /> prikaži rešene
</form>
</p>
<script>
	var selectedRow = 0;
	$(".sortableTable").tablesorter( {
		sortList: [[0,1]]
    } ).tablesorterPager({container: $("#pager")});
	
	$("#bugReportShowResolved").click(function(cEV) {
		refreshBugReport();
	});
	
	$("#bugReportAddResponse").dialog({
		autoOpen: false,
		height: 380,
		width: 415,
		modal: true,
		closeOnEscape: true,
		buttons: {
				"ODGOVORI": function() {
						$("#waitIndicator").show();
						var msg = $("#bugReportAddResponse textarea").val();
						if (msg.length==0) return;
						
						
						jQuery.post( "api.php?params=action=alterBugReport", {id: selectedRow, status: status, response: msg }, function(data, textStatus, XMLHttpRequest) {
							//alert("shranjeno");
							//alert("shranjeno " + textStatus);
							//alert("shranjeno " + JSON.stringify(data));
							if (textStatus=="success")
								if (data.error=="no") {
									$("#bugReporTable tr[rowid='" + selectedRow + "'] td[ftype='response'] a").text(msg);
								} else {
									alert(data.message.body.text);
								}
							}, "json" );
						$( this ).dialog( "close" );
				},
				"Prekliči": function() {
					
					$( this ).dialog( "close" );
				}
		},
		close: function() {
			waitIndicator.hide();
		}
	});
	
	$("#bugReporTable a.addReponse").click(function(cEv) {
		var rowid = $(this).parent().parent().attr("rowid");
		selectedRow = rowid;
		$("#bugReportAddResponse").dialog("open");
		cEv.preventDefault();
	});
	
	$("#bugReporTable select.statusSelect").change(function(ev) {
		var rowid = $(this).parent().parent().attr("rowid");
		var status = $(this).val();
		//alert(status);
		jQuery.post( "api.php?params=action=alterBugReport", {id: rowid, status: status, response: null }, function(data, textStatus, XMLHttpRequest) {
			//alert("shranjeno");
			//alert("shranjeno " + textStatus);
			//alert("shranjeno " + JSON.stringify(data));
			if (textStatus=="success")
				if (data.error=="no") {
				} else {
					alert(data.message.body.text);
				}
			}, "json" );
	});
	
	function refreshBugReport() {
		waitIndicator.show();
		window.location.hash = action;

		$("#adminContent").load("load.php?page=sifranti&sub=bugreports", {
				showResolved: ($("#bugReportShowResolved").is(":checked") ? 1: 0) 
				}, function(a, b, c) {
			//alert("done");
			waitIndicator.hide();
		});
	
	}

</script>
<table id="bugReporTable" class="styledTable sortableTable" >
<thead>
<tr>
	<th>uporabnik</th>
	<th>opis napake</th>
	<th>odgovor</th>
	<th>nivo</th>
	<th>prioriteta</th>
	<th>ustvarjen</th>
	<th>modul</th>	
	<th>cas odprave</th>	
	<th>status</th>
</tr>
</thead>
<tbody>
<?php
$controller = new ObvestiloController;



function printStatus($selected) {
	$statusi = array("nova", "prevzeta", "v resevanju", "resena", "ne bo popravljena");
	echo '<td><select class="statusSelect">';
	foreach ($statusi as $option) {
		if ($option==$selected) {
			echo "<option selected>$option</option>";
		} else {
			echo "<option>$option</option>";
		}
	}
	echo '</select></td>';
}

$seznam = $controller->getBugList($showResolved);
foreach ($seznam as $row) {
	echo "<tr rowid='" . $row["id"] . "'>
		<td>" . $row["email"] . "</td>
		<td>" . $row["opis_napake"] . "</td>";
		if ($row["odgovor"]==null || $row["odgovor"]=='null') {
			echo "<td ftype='response'><a class='addReponse' href='#'>dodaj dogovor</a></td>";
		} else {
			echo "<td ftype='response'><a class='addReponse' href='#'>" . $row["odgovor"] . "</a> </td>";
		}
	echo "<td>" . $row["nivo"] . "</td>
		<td>" . $row["prioriteta"] . "</td>
		<td>" . $row["ustvarjen"] . "</td>
		<td>" . $row["modul"] . "</td>
		<td>" . $row["cas_odprave"] . "</td>";
		
	printStatus($row["status"]);
	
	echo "</tr>";
}
if (count($seznam)==0) {
	echo "<tr><td colspan='9'>ni prijav</td></tr>";
}
?>
</tbody>
</table>
<div id="pager" class="pager" style="display: inline; margin-top: 30px;">
					<form>
						<img src="images/first.png" class="first"/>
						<img src="images/prev.png" class="prev"/>
						<input type="text" disabled="disabled" class="pagedisplay"/>
						<img src="images/next.png" class="next"/>
						<img src="images/last.png" class="last"/>
						<select class="pagesize">
							<option selected="selected"  value="10">10</option>
				
							<option value="20">20</option>
							<option value="30">30</option>
							<option  value="40">40</option>
						</select>
					</form>
</div>
</div>
<div id="bugReportAddResponse" style="display:none" title="Odgovori na prijavo">
<textarea class="bugReportReponse"></textarea>
</div>