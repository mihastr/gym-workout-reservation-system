<script language="javascript">
var fileName = null;
var fileID = null;
var nVadb = 0;
function saveFormData(fileID) {
	var id = $("#classEditForm input[name='classID']").val();
	var naziv = $("#classEditForm input[name='className']").val();
	var kratkoIme = $("#classEditForm input[name='classAcronym']").val();
	var opis = $("#classEditForm textarea[name='classDescription']").val();

	var link = $("#classEditForm textarea[name='classLink']").val();
	var kalorije = $("#classEditForm input[name='classCalories']").val();


	if (id=="") {
		jQuery.post( "api.php?params=action=addVadba", { "naziv": naziv, "kratkoIme": kratkoIme, "opis": opis, "link": link, "kalorije": kalorije, "image": fileID}, function(data, textStatus, XMLHttpRequest) {
			//alert("shranjeno");
			//alert("shranjeno " + textStatus);
			//alert("shranjeno " + JSON.stringify(data));
			if (textStatus=="success")
				if (data.error=="no") {
					//$("#noticeField").html("shranjeno");
					//$("#noticeField").show().fadeOut(5000);
					//$("#urnikColTable tbody").prepend(data.message.body.text).hide().fadeIn();
					
					
					$("<tr class='newLine' objid='" + data.message.body.text + "'>" +
							
							"<td ftype='naziv'>" + naziv + "</td>" + 
							"<td ftype='kratkoIme'>" + kratkoIme + "</td>" +
							"<td ftype='opis'>" + opis + "</td>" +
							"<td ftype='link'>" + link + "</td>" +
							"<td ftype='kalorij'>" + kalorije + "</td>" +
							"<td>&nbsp;</td>" +
							"<td>&nbsp;</td>" +
							"<td><a class='showCapacityButton' href='#'>kapacitete</a></td>" +
							"<td><a href='#' class='editButton'><img src='images/edit-icon.png' /></a> " +
							"<a href='#' class='removeButton'><img src='images/delete.png' /></a></td>" +
							"</tr>").appendTo("#adminClassList tbody");

						
					waitIndicator.hide();
					
				} else {
					switch (data.message.body.text) {
					case "unauthorized":
						alert('<?php print t("unauthorized") ?>');
						break;
					case "fkFail":
						//alert('<?php print t("centerPlaceExists") ?>');
						break;
					default:
						alert('<?php print t("error") ?>');
						break;
					}	
					waitIndicator.hide();
				}
				//$("#newClassDialogForm").dialog("close");				
			}, "json" );
	} else {
		jQuery.post( "api.php?params=action=editVadba", { "id": id, "naziv": naziv, "kratkoIme": kratkoIme, "opis": opis, "link": link, "kalorije": kalorije, "image": fileID }, function(data, textStatus, XMLHttpRequest) {
			//alert("shranjeno");
			//alert("shranjeno " + textStatus);
			//alert("shranjeno " + JSON.stringify(data));
			if (textStatus=="success")
				if (data.error=="no") {
					//$("#noticeField").html("shranjeno");
					//$("#noticeField").show().fadeOut(5000);
					//$("#urnikColTable tbody").prepend(data.message.body.text).hide().fadeIn();
					
					$("#adminClassList tbody tr[objid='" + id + "'] td[ftype='naziv']").text(naziv);
					$("#adminClassList tbody tr[objid='" + id + "'] td[ftype='kratkoIme']").text(kratkoIme);
					$("#adminClassList tbody tr[objid='" + id + "'] td[ftype='opis']").text(opis);
					$("#adminClassList tbody tr[objid='" + id + "'] td[ftype='link']").text(link);
					$("#adminClassList tbody tr[objid='" + id + "'] td[ftype='kalorij']").text(kalorije);

					if (fileID!=null && fileID!="") {
						$("#adminClassList tbody tr[objid='" + id + "'] td[ftype='logo']").html("<img src='upload/image_" + fileID + ".jpg' />");
					} else {
						fileID="";
					}
					
					$("#adminClassList tbody tr[objid='" + id + "'] td[ftype='logo']").attr("fval", fileID);

						
					waitIndicator.hide();
					
				} else {
					switch (data.message.body.text) {
					case "unauthorized":
						alert('<?php print t("unauthorized") ?>');
						break;
					case "fkFail":
						//alert('<?php print t("centerPlaceExists") ?>');
						break;
					default:
						alert('<?php print t("error") ?>');
						break;
					}	
					waitIndicator.hide();
				}
				//$("#newClassDialogForm").dialog("close");				
			}, "json" );
	}
	
}

$(document).ready(function() {

	

	$("#newClassDialogForm").dialog({
		autoOpen: false,
		minHeight: 380,
		minWidth: 400,
		modal: true,
		closeOnEscape: true,
		buttons: {
			"<?php print t("save"); ?>": function() {
				if (nVadb==0) {
					saveFormData();
					$("#newClassDialogForm").dialog("close");
				} else {
					$("#classLogo").uploadifyUpload();
				}
				
				
			},
			"<?php print t("cancel"); ?>": function() {
				$("#classLogo").uploadifyClearQueue();
				$("#newClassDialogForm").dialog("close");
			}
		},
		close: function() {
			$("#classEditForm input[name='classID']").val("");
			$("#classEditForm input[name='className']").val("");
			$("#classEditForm input[name='classAcronym']").val("");
			$("#classEditForm textarea[name='classDescription']").val("");
			$("#classEditForm textarea[name='classLink']").val("");
			$("#classEditForm input[name='classCalories']").val("");
		}
	});
	
	$('textarea.autoResize').autoResize({
	    // On resize:
	    onResize : function() {
	        $(this).css({opacity:0.8});
	    },
	    // After resize:
	    animateCallback : function() {
	        $(this).css({opacity:1});
	    },
	    // Quite slow animation:
	    animateDuration : 300,
	    // More extra space:
	    extraSpace : 10
	});

	$("#classLogo").uploadify({
	    'uploader'  : 'js/jquery.uploadify-v2.1.4/uploadify.swf',
	    'script'    : 'uploadify.php',
	    'cancelImg' : 'js/jquery.uploadify-v2.1.4/cancel.png',
	    'scriptData'     : {'PHPSESSID' : '<?php echo session_id(); ?>'},
	    'folder'    : 'upload',
	    'method'      : 'post',
	    'multi'       : false,
	    'buttonText' : '<?php print t("selectFile"); ?>',
	    'fileExt'     : '*.jpg;*.gif;*.png',
		'fileDesc'    : 'Image Files',
		'onCancel' 	  : function(event,ID,fileObj,data) {
	    	nVadb = 0;
		},
		'onSelect'    : function(event,ID,fileObj) {
	    		fileName = fileObj.name;
	    		nVadb = 1;
	    		
		    },
		'onComplete': function(event, ID, fileObj, response, data) {
		    	saveFormData(response);
				//alert(JSON.stringify(response));
				//alert(JSON.stringify(data));
				
		    	//alert('There are ' + data.fileCount + ' files remaining in the queue.');
				//if (data.fileCount==0) 
				nVadb = 0;
		 		$("#newClassDialogForm").dialog("close");
	    },
	    'onError'     : function (event,ID,fileObj,errorObj) {
	        alert(errorObj.type + ' Error: ' + errorObj.info);
	      }

			
	  });

	  $("#newClassDialogForm img.classLogoDeleteButton").click(function(cEv) {
		  	if (!confirm("<?php echo t('areyousure'); ?>")) return;
		  	var id = $("#classEditForm input[name='classID']").val();
			jQuery.post( "api.php?params=action=removeVadbaLogo", { "id": id}, function(data, textStatus, XMLHttpRequest) {
				//alert("shranjeno");
				alert("shranjeno " + textStatus);
				alert("shranjeno " + JSON.stringify(data));
				alert(data.error);
				if (textStatus=="success")
					if (data.error=="no") {
						$("#newClassDialogForm p.ulButton").show();
						$("#newClassDialogForm p.ulImage").hide();
					} else {
						alert("<?php print t("error"); ?>");
					}
			}, "json" );
	  	});
	  
	
});
</script>
<div id="newClassDialogForm" title="<?php print t("newClass"); ?>">
<h3><?php print  t("newClass"); ?></h3>
<form id="classEditForm">
<input type="hidden" name="classID" />
<label for="className"><?php print  t("title"); ?></label><input type="text" name="className" /><br />
<label for="classAcronym"><?php print  t("acronym"); ?></label><input type="text" name="classAcronym" /><br />
<label for="classDescription"><?php print  t("description"); ?></label><textarea class="autoResize" name="classDescription"></textarea><br />
<label for="classLink"><?php print  t("link"); ?></label><textarea class="autoResize" name="classLink"></textarea><br />
<label for="classCalories"><?php print  t("calories"); ?></label><input type="text" name="classCalories" /><br />
<p align="center"><?php print  t("file"); ?></p>
<div id="classFileConatiner">
<p align="center" class='ulButton'><input id="classLogo" type="file" name="classLogo" /></p>
<p align="center" class='ulImage'><img src="upload/image_14.jpg" class="classIcon" /> <img class="classLogoDeleteButton" src="images/cancel.gif" /></p>
</div>
</form>
</div>