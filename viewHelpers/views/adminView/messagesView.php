<?php 
$list = $this->c->getList4Admin(1, 0);
?>
<script type="text/javascript">

function makeQTip() {
	$('a.msgsShowRecipients').each(function(){
		   $(this).click(function(){ return false });//JS enabled => link default behavior disabled. Gaceful degradation
		   $(this).qtip({
			   content: {
			      url: 'load.php?page=sifranti&sub=messaging&action=getMsgRecepients',
			      data: { id: ($(this).parent().parent().attr("objid")) },
			      method: 'post'
			   }, style: { 
				      name: 'blue', // Inherit from preset style
				      border: {
				      width: 3,
				      radius: 8,
			          color: '#6699CC'
			      }
							      
			   }
			});
	});
	$("#msgListTable")
		.tablesorter({sortList: [[2,1]], headers: { 
            9: {  
                sorter: false 
            }}, widthFixed: true, widgets: ['zebra']}) 
	    .tablesorterPager({container: $("#pager")});
	$.trigger("appendCache");
}

function removeFromList(list, obj) {
	var objid = obj.href.split("#")[1];
	$(list).find("li[objid=" + objid + "]").fadeOut(function(hEvent) { 
		$(this).remove();	
		var n = $(list).attr("n")-1;
		if (n==0) {
			$(list).find(".niPodatkov").fadeIn();
		} 
		$(list).attr("n", n);	
	});
	
	
	
}

$(document).ready(function(event) {

	$('textarea.autoResize').autoResize({
	    // On resize:
	    onResize : function() {
	        $(this).css({opacity:0.8});
	    },
	    // After resize:
	    animateCallback : function() {
	        $(this).css({opacity:1});
	    },
	    // Quite slow animation:
	    animateDuration : 300,
	    // More extra space:
	    extraSpace : 10
	});



	$("#obvestilDatumOD").datepicker( calConfig );
	$("#obvestilDatumOD").val((new Date()).format("d.m.Y"));
	
	$("#obvestilDatumDO").datepicker( calConfig );



	makeQTip();
	
});
</script>
<script language="javascript">

function feedMessages(data) {
	//alert("lala");
	var brisan = false;
	$(data).each(function(i, obj) {
		if (obj.brisan != 1) {
			brisanje = "<img class='removeItem' src='images/minus.gif'>"; 
		} else {
			brisanje = "&nbsp;";
		}
		//"id":"1","naslov":"Test","tekst":"test","ustvarjen":null,"posodobljen":"2010-12-19 16:56:24","od":"1","za":"uporabnik","prikazan":"da","veljaOD":"2010-12-19 00:00:00","veljaDO":"2010-12-29 00:00:00","brisan":null,"posiljatelj":"Miha Štrumbelj"}
		$("<tr objid='" + obj.id + "' " + (obj.brisan=="1" ? "class='brisan'" : "") + ">" + 
			"<td ftype='naslov'>" + obj.naslov + "</td>" +
			"<td ftype='tekst'>" + obj.tekst + "</td>" +
			"<td ftype='ustvarjen'>" + (obj.ustvarjen==null ? "": obj.ustvarjen) + "</td>" +
			"<td ftype='posodobljen'>" + (obj.posodobljen==null ? "": obj.posodobljen) + "</td>" +
			"<td ftype='posiljatelj'>" + (obj.posiljatelj==null ? "": obj.posiljatelj) + "</td>" +
			"<td ftype='za'>" + 
			(obj.za=="vsi" ? "vsi" : "<a href='#' class='msgsShowRecipients'>prejemniki</a>" )+
			 "</td>" +
			"<td ftype='prikazan'>" + (obj.prikazan=="da" ? "DA - (<a href='#' value='ne' class='messageMakeDraft'>skrij</a>)" : "NE - (<a href='#' value='da' class='messageMakeDraft'>prikazi</a>)") + "</td>" +
			"<td ftype='veljaOD'>" + (obj.veljaOD==null ? "": obj.veljaOD) + "</td>" +
			"<td ftype='veljaDO'>" + (obj.veljaDO==null ? "": obj.veljaDO) + "</td>" +
			"<td>" + brisanje + "</td>" +
		"</tr>").appendTo("#msgListTable tbody");
		//alert(JSON.stringify(obj));
	});
	
	makeQTip();
}

function loadMsgs() {
	
	waitIndicator.show();
	var invisible = ($("#showDrafts").attr("checked") ? 1 : 0);
	var deleted = ($("#showDeleted").attr("checked") ? 1 : 0);
	var q = $("#messageSearchField").val();
	
	jQuery.post( "api.php?params=action=showMessages4Admin", {"invisible": invisible, "deleted": deleted, "q": q}, function(data, textStatus, XMLHttpRequest) {
		//alert("shranjeno");
		//alert("shranjeno " + textStatus);
		//alert("shranjeno " + JSON.stringify(data));
		if (textStatus=="success")
			if (data.error=="no") {
				//$("#noticeField").html("shranjeno");
				//$("#noticeField").show().fadeOut(5000);
				//$("#urnikColTable tbody").prepend(data.message.body.text).hide().fadeIn();
				
				$("#msgListTable tbody tr").remove();

				feedMessages(data.message.body.text);
				
				waitIndicator.hide();
				
			} else {
				if (data.message.body.text=="unauthorized") 
					alert('<?php print t("unauthorized") ?>');
				else
					alert('<?php print t("error") ?>');
			}
		}, "json" );
}

$(document).ready(function (rEv) {
	$("img.removeItem").live("click", function(cEv) {
		if (confirm('<?php print t("areyousure"); ?>')) {
			var msgid = $(this).parent().parent().attr("objid");
			jQuery.post( "api.php?params=action=removeMSG", {"msgid": msgid }, function(data, textStatus, XMLHttpRequest) {
				//alert("shranjeno");
				//alert("shranjeno " + textStatus);
				//alert("shranjeno " + JSON.stringify(data));
				if (textStatus=="success")
					if (data.error=="no") {
						//$("#noticeField").html("shranjeno");
						//$("#noticeField").show().fadeOut(5000);
						//$("#urnikColTable tbody").prepend(data.message.body.text).hide().fadeIn();
						waitIndicator.hide();
						$("tr[objid='" + msgid +"']").fadeOut();
						//alert("all ok");
					} else {
						if (data.message.body.text=="unauthorized") 
							alert('<?php print t("unauthorized") ?>');
						else
							alert('<?php print t("error") ?>');
					}
				}, "json" );
		};
	});
	$("#msgsShowAll").click(function(cEv) {
		cEv.preventDefault();
		$("#showDrafts").attr("checked", true);
		$("#showDeleted").attr("checked", true);
		loadMsgs();
	});

	
	
	$("#showDrafts").change(function(cEv) {
		loadMsgs();
	});
	$("#showDeleted").change(function(cEv) {
		loadMsgs();
	});

	
	$("#messageSearchField").keyup(function(kupEv) {
		var val = $(this).val();
		if (val.length>-1) {
			loadMsgs();
		}
	});

	$("a.messageMakeDraft").live("click", function(cEv) {
		var id = $(this).parent().parent().attr("objid");
		//alert(id);
		var visible = $(this).attr("value");
		//alert(visible);
		 
		jQuery.post( "api.php?params=action=msgChangeVisibility", {"id": id, "visible": visible}, function(data, textStatus, XMLHttpRequest) {
			//alert("shranjeno");
			//alert("shranjeno " + textStatus);
			//alert("shranjeno " + JSON.stringify(data));
			if (textStatus=="success")
				if (data.error=="no") {
					//$("#noticeField").html("shranjeno");
					//$("#noticeField").show().fadeOut(5000);
					//$("#urnikColTable tbody").prepend(data.message.body.text).hide().fadeIn();
					
					loadMsgs();
					waitIndicator.hide();
					
					
				} else {
					if (data.message.body.text=="unauthorized") 
						alert('<?php print t("unauthorized") ?>');
					else
						alert('<?php print t("error") ?>');
				}
			}, "json" );
		cEv.preventDefault();
	});

});
</script>

<h3><?php print t("messagesAdminTitle") ?></h3>
<p><input type="button" id="dodajObvestiloGumb" value="<?php print t("newMessage"); ?>" /></p>
<p><?php print t("options"); ?> - <?php print t("show"); ?>: <a href="#" id="msgsShowAll"><?php print t("all"); ?></a> 
	<input type="checkbox" id="showDrafts" checked="checked" /><?php print t("drafts"); ?> 
	<input type="checkbox" id="showDeleted" /><?php print t("deleted"); ?></p>
<p>
<?php print t("search"); ?>: <input type="text" id="messageSearchField" />
</p>

<?php 
include_once 'messages/newMsgForm.php';
?>
<div style="min-height: 500px;">
<div id="pager" class="pager" style="display: inline;">
					<form>
						<img src="images/first.png" class="first"/>
						<img src="images/prev.png" class="prev"/>
						<input type="text" disabled="disabled" class="pagedisplay"/>
						<img src="images/next.png" class="next"/>
						<img src="images/last.png" class="last"/>
						<select class="pagesize">
							<option selected="selected"  value="10">10</option>
				
							<option value="20">20</option>
							<option value="30">30</option>
							<option  value="40">40</option>
						</select>
					</form>
				</div>
<table cellpadding="5" id="msgListTable" class="styledTable sortableTable">
<thead>
<tr>
	<th><?php print t("title") ?></th>
	<th><?php print t("content") ?></th>
	<th><?php print t("created") ?></th>
	<th><?php print t("updated") ?></th>
	<th><?php print t("sender") ?></th>
	<th><?php print t("receiver") ?></th>
	<th><?php print t("visible") ?></th>
	<th><?php print t("validFrom") ?></th>
	<th><?php print t("validTo") ?></th>
	<th>&nbsp;</th>
</tr>
</thead>
<tbody>
<?php 
foreach ($list as $item) {
	print "\n<tr objid='" . $item["id"] . " " . ($item["brisan"]=="1" ? " class='brisan'" : "") ."'>";
	print "
	<td ftype='naslov'>" . $item["naslov"] . "</td>
	<td ftype='tekst'>" . $item["tekst"] . "</td>
	<td ftype='ustvarjen'>" . datePrintFormated($item["ustvarjen"], PageSettings::dbDateFormat) . "</td>
	<td ftype='posodobljen'>" . datePrintFormated($item["posodobljen"], PageSettings::dbDateFormat) . "</td>
	<td ftype='posiljatelj'>" . $item["posiljatelj"] . "</td>

	<td ftype='za'>" .
	($item["za"]=="vsi" ? $item["za"] : "<a href='#' class='msgsShowRecipients'>" . t("msgsShowRecipients") . "</a>" ) 
	 . "</td>
	<td ftype='prikazan'>" . ($item["prikazan"]=="da" ? "DA - (<a href='#' value='ne' class='messageMakeDraft'>skrij</a>)" : "NE - (<a href='#' value='da' class='messageMakeDraft'>prikazi</a>)" ) . "</td>
	<td ftype='veljaOD'>" . datePrintFormated($item["veljaOD"], PageSettings::dbDateFormat) . "</td>
	<td ftype='veljaDO'>" . datePrintFormated($item["veljaDO"], PageSettings::dbDateFormat) . "</td>
	<td><img class='removeItem' src='images/minus.gif' /></td>
	";
	print "\n</tr>";
}
?>
</tbody>
</table>

</div>