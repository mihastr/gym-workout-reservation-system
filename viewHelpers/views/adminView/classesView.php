<script language="javascript">
var izbranaVadba = null;
$(document).ready(function(rEvent) {
	$("#classCapacityDIV").dialog({
		autoOpen: false,
		height: 600,
		width: 800,
		modal: true,
		buttons: { 
		"<?php print t("save"); ?>": function() {
				var stara = null;
				var nova = null;
				var prostor = null;
				var vadba = null;

				var omejitve = [];
				var kapacitete = [];
				$("#classCapacityDIV table tbody tr").each(function(i, obj) {
					prostor = $(obj).attr("prid");
					vadba = $(obj).attr("vid");
					
					stara = $(obj).find("input[ftype='omejitevUraProstor']").attr("old");
					nova = $(obj).find("input[ftype='omejitevUraProstor']").val();
					if (stara!=nova) {
						omejitve.push({ prostor: prostor, omejitev: nova });
					}
					stara = $(obj).find("input[ftype='kapacitetaProstora']").attr("old");
					nova = $(obj).find("input[ftype='kapacitetaProstora']").val();
					if (stara!=nova) {
						kapacitete.push({ prostor: prostor, kapaciteta: nova });
					}
				});
				var spremembe = {
						prostori: kapacitete,
						vadbe: {
							id: vadba,
							spremembe: omejitve
						}
					};
				shraniSpremembe(spremembe);
			}, 
			"<?php print t("cancel"); ?>": function() { 
				$(this).dialog("close"); 
			}
		}
	});

	function shraniSpremembe(spremembe) {
		jQuery.post( "api.php?params=action=savePlacesCapacityChanges", spremembe, function(data, textStatus, XMLHttpRequest) {
			//alert("shranjeno");
			//alert("shranjeno " + textStatus);
			//alert("shranjeno " + JSON.stringify(data));
			if (textStatus=="success")
				if (data.error=="no") {
					alert("ok");
				} else {
					alert('<?php print t("error"); ?>');
				}
				waitIndicator.hide();
		}, "json" );
	}

	function loadPlacesListToTable(list, vadba) {
		$("#classCapacityDIV table tbody tr").remove();
		$(list).each(function (i, obj) {
			$("#classCapacityDIV table tbody").append("<tr vid='" + 
					vadba + "' prid='" + obj.prid + "'><td>" + obj.fitnescenter + "</td><td>" + obj.prostor + "</td>" +
					"<td><input old='" + (obj.omejitev==null ? "" : obj.omejitev) + "' ftype='omejitevUraProstor' class='smallText' type='text' value='" + (obj.omejitev==null ? "" : obj.omejitev) + "' /></td>" +
					"<td><input old='" + (obj.kapaciteta==null ? "" : obj.kapaciteta) + "' ftype='kapacitetaProstora' class='smallText' type='text' value='" + (obj.kapaciteta==null ? "" : obj.kapaciteta) + "' /></td>" + 
					"</tr>");
		});
	}

	function loadPlacesList(vadba) {
		jQuery.post( "api.php?params=action=showProsoriKapacitete", {"vadba_id": vadba}, function(data, textStatus, XMLHttpRequest) {
			//alert("shranjeno");
			//alert("shranjeno " + textStatus);
			//alert("shranjeno " + JSON.stringify(data));
			if (textStatus=="success")
				if (data.error=="no") {
					loadPlacesListToTable(data.message.body.text, vadba);
				}
				waitIndicator.hide();
		}, "json" );
	}
	
	$("a.showCapacityButton").live("click", function(cEv) {

		$("#adminClassList tbody tr").removeClass("active");
		$(this).parent().parent().addClass("active");
		
		cEv.preventDefault();
		var vadba = $(this).parent().parent().attr("objid");
		loadPlacesList(vadba);
		$("#classCapacityDIV").dialog("open");
	});	

	$("#openClassEditDialogButton").click(function(cEv) {

		$("#newClassDialogForm").dialog("open");
	});


	$("#adminClassList a.removeButton").live("click", function(cEv) {
		waitIndicator.show();

		cEv.preventDefault();

		var id = $(this).parent().parent().attr("objid");

		if (!confirm("Ali ste prepričani?")) return false;
		
		jQuery.post( "api.php?params=action=removeVadba", { "id": id}, function(data, textStatus, XMLHttpRequest) {
			//alert("shranjeno");
			//alert("shranjeno " + textStatus);
			//alert("shranjeno " + JSON.stringify(data));
			if (textStatus=="success")
				if (data.error=="no") {
					//$("#noticeField").html("shranjeno");
					//$("#noticeField").show().fadeOut(5000);
					//$("#urnikColTable tbody").prepend(data.message.body.text).hide().fadeIn();
					
					$("#adminClassList tbody tr[objid='" + id + "']").fadeOut("slow");
					
					waitIndicator.hide();
					
				} else {
					switch (data.message.body.text) {
					case "unauthorized":
						alert('<?php print t("unauthorized") ?>');
						break;
					case "fkFail":
						alert('<?php print t("classExistsInScehdule") ?>');
						break;

					default:
						alert('<?php print t("error") ?>');
						break;
					}	
					waitIndicator.hide();
				}
			}, "json" );
	});
	

	$("#adminClassList a.editButton").live("click", function(cEv) {
		cEv.preventDefault();

		$("#adminClassList tbody tr").removeClass("active");
		$(this).parent().parent().addClass("active");
		
		var row = $(this).parent().parent();
		var objid =  $(row).attr("objid");
		
		
		var naslov = $(row).find("td[ftype='naziv']").text();
		var kratica = $(row).find("td[ftype='kratkoIme']").text();
		var opis = $(row).find("td[ftype='opis']").text();

		var link = $(row).find("td[ftype='link']").text();
		var kalorij = $(row).find("td[ftype='kalorij']").text();


		var logo = $(row).find("td[ftype='logo']").attr("fval");

		if (logo == null || logo == "") {
			$("#classEditForm p.ulButton").show();
			$("#classEditForm p.ulImage").hide();
		} else {
			$("#classEditForm p.ulButton").hide();
			$("#classEditForm p.ulImage").show();
			
			$("#classEditForm img.classIcon").attr("src", "upload/image_" + logo + ".jpg");
		}
		
		$("#classEditForm input[name='classID']").val(objid);
		$("#classEditForm input[name='className']").val(naslov);
		$("#classEditForm input[name='classAcronym']").val(kratica);
		$("#classEditForm textarea[name='classDescription']").val(opis);
		$("#classEditForm input[name='classLink']").val(link);
		$("#classEditForm textarea[name='classCalories']").val(kalorij);

		

		izbranaVadba = objid;
		
		$("#newClassDialogForm").dialog("open");

	});
	$(".sortableTable").tablesorter( {
		sortList: [[0,0]],
		headers: {  
        	4: { sorter: false  },
        	7: { sorter: false  },
        	8: { sorter: false  },
        	9: { sorter: false  }
    	}
    } );
});

</script>

<h3><?php print t("classesAdminTitle"); ?></h3>
<p>
<input type="button" id="openClassEditDialogButton" value="<?php print t("new"); ?>" />
</p>
<?php 
$controller = new VadbaController;
$seznam = $controller->getList();
//print_r($seznam);
?>

<table width="100%" id="adminClassList"  class="styledTable sortableTable">
<thead>
<tr>
<th><?php print t("title"); ?></th>
<th><?php print t("shortText"); ?></th>
<th><?php print t("description"); ?></th>
<th><?php print t("calories"); ?></th>
<th><?php print t("link"); ?></th>
<!-- 
<th><?php print t("visible"); ?></th>
<th><?php print t("validFrom"); ?></th>
<th><?php print t("validTo"); ?></th>
 -->
<th><?php print t("created"); ?></th>
<th><?php print t("updated"); ?></th>
<th>&nbsp;</th><th>&nbsp;</th></th><th>&nbsp;</th></tr>
</thead>
<tbody>
<?php 
foreach ($seznam as $row) {
	print "<tr objid='" . $row["id"] . "'>";
	//<th>naslov</th><th>kratica</th><th>opis</th><th>prikazano</th><th>prikazano od</th><th>prikazano do</th><th>usvtarjeno</th><th>posodobljeno</th>
	print "<td ftype='naziv'>" . $row["naziv"] . "</td><td ftype='kratkoIme'>" . $row["kratkoIme"] . "</td><td ftype='opis'>" . $row["opis"] . "</td><td ftype='kalorij'>" . $row["kalorij"] . "</td><td ftype='link'>" . $row["link"] . "</td>";
	//print "<td>" . $row["veljaven"] . "</td><td>" . $row["veljaOD"] . "</td><td>" . $row["veljaDO"] . "</td>";
	print "<td>" . $row["ustvarjen"] . "</td><td>" . $row["posodobljen"] . "</td>";
	print "<td ftype='logo' fval='". $row["logo"] ."'>" . ($row["logo"]!=null ? "<img src='upload/image_" . $row["logo"] . ".jpg' />" : "") . "</td>";
	print "<td class='{sorter: false}'><a href='#' class='showCapacityButton'>kapacitete</a></td>";
	print "<td class='{sorter: false}'><a href='#' class='editButton'><img src='images/edit-icon.png' /></a> <a href='#' class='removeButton'><img src='images/delete.png' /></a></td></tr>";
}
?>
</tbody>
</table>

<div id="classCapacityDIV" style="display:none" title="<?php print t("capacityEditTitle"); ?>">
<h3><?php print ucfirst(t("fclist")); ?></h3>
<table width="100%">
<thead>
<tr><th><?php print t("fc") ?></th><th><?php print t("place") ?></th><th><?php print t("visitsPerHour") ?></th><th><?php print t("placeCapacity") ?></th></tr>
</thead>
<tbody>
</tbody>
</table>
</div>
<!-- dialogi -->
<div id="newClassDialog" style="display:block">
<?php include_once 'classes/newClassForm.php'; ?>
</div>