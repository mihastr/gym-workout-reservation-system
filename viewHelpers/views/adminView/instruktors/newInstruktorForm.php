<script language="javascript">

function saveFormData() {
	var id = $("#newInstructorForm input[name='instruktorID']").val();
	var dolgoIme = $("#newInstructorForm input[name='dolgoIme']").val();
	var kratkoIme = $("#newInstructorForm input[name='kratkoIme']").val();
	var opis = $("#newInstructorForm textarea[name='opis']").val();

	if (id=="") {
		jQuery.post( "api.php?params=action=addInstruktor", { "dolgoIme": dolgoIme, "kratkoIme": kratkoIme, "opis": opis }, function(data, textStatus, XMLHttpRequest) {
			//alert("shranjeno");
			//alert("shranjeno " + textStatus);
			//alert("shranjeno " + JSON.stringify(data));
			if (textStatus=="success")
				if (data.error=="no") {
					//$("#noticeField").html("shranjeno");
					//$("#noticeField").show().fadeOut(5000);
					//$("#urnikColTable tbody").prepend(data.message.body.text).hide().fadeIn();
					
					
					$("<tr class='newLine' objid='" + data.message.body.text + "'>" +
							
							"<td ftype='dolgoIme'>" + dolgoIme + "</td>" + 
							"<td ftype='kratkoIme'>" + kratkoIme + "</td>" +
							"<td ftype='opis'>" + opis + "</td>" +
							"<td>&nbsp;</td>" +
							"<td>&nbsp;</td>" +
							"<td><a href='#' class='editButton'><img src='images/edit-icon.png' /></a> " +
							"<a href='#' class='removeButton'><img src='images/delete.png' /></a></td>" +
							"</tr>").appendTo("#instruktorListTable tbody");

						
					waitIndicator.hide();
					
				} else {
					switch (data.message.body.text) {
					case "unauthorized":
						alert('<?php print t("unauthorized") ?>');
						break;
					case "fkFail":
						//alert('<?php print t("centerPlaceExists") ?>');
						break;
					default:
						alert('<?php print t("error") ?>');
						break;
					}	
					waitIndicator.hide();
				}
				$("#newInstructorForm").dialog("close");				
			}, "json" );
	} else {
		jQuery.post( "api.php?params=action=editInstruktor", { "id": id, "dolgoIme": dolgoIme, "kratkoIme": kratkoIme, "opis": opis }, function(data, textStatus, XMLHttpRequest) {
			//alert("shranjeno");
			//alert("shranjeno " + textStatus);
			//alert("shranjeno " + JSON.stringify(data));
			if (textStatus=="success")
				if (data.error=="no") {
					//$("#noticeField").html("shranjeno");
					//$("#noticeField").show().fadeOut(5000);
					//$("#urnikColTable tbody").prepend(data.message.body.text).hide().fadeIn();
					
					$("#instruktorListTable tbody tr[objid='" + id + "'] td[ftype='dolgoIme']").text(dolgoIme);
					$("#instruktorListTable tbody tr[objid='" + id + "'] td[ftype='kratkoIme']").text(kratkoIme);
					$("#instruktorListTable tbody tr[objid='" + id + "'] td[ftype='opis']").text(opis);
					

						
					waitIndicator.hide();
					
				} else {
					switch (data.message.body.text) {
					case "unauthorized":
						alert('<?php print t("unauthorized") ?>');
						break;
					case "fkFail":
						//alert('<?php print t("centerPlaceExists") ?>');
						break;
					default:
						alert('<?php print t("error") ?>');
						break;
					}	
					waitIndicator.hide();
				}
				$("#newInstructorForm").dialog("close");				
			}, "json" );
	}
	
} 

$(document).ready(function(rEvt) {
	$("#newInstructorForm").dialog({
		autoOpen: false,
		minHeight: 260,
		minWidth: 350,
		modal: true,
		closeOnEscape: true,
		buttons: {
			"<?php print t("save"); ?>": function() {
				saveFormData();
			},
			"<?php print t("cancel"); ?>": function() {
				$("#newInstructorForm").dialog("close");
			}
		},
		close: function() {
			$("#newInstructorForm input[name='instruktorID']").val("");
			$("#newInstructorForm input[name='dolgoIme']").val("");
			$("#newInstructorForm input[name='kratkoIme']").val("");
			$("#newInstructorForm textarea[name='opis']").val("");
		}
	});
});
</script>

<div title="<?php print t("newInstructorForm"); ?>" id="newInstructorForm" class="dialogForm" style="display: none"> 
<h3><?php print t("newInstructorForm"); ?></h3>
<input type="hidden" name="instruktorID" />
<label for="dolgoIme"><?php print t("name"); ?></label> <input type="text" name="dolgoIme" /> <br />
<label for="kratkoIme"><?php print t("short"); ?></label> <input type="text" name="kratkoIme" /> <br />
<label for="opis"><?php print t("description"); ?></label> <textarea name="opis" ></textarea> <br />
</div>