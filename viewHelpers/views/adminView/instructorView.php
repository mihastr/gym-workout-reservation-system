
<script language="javascript">
$(document).ready(function(rEvent) {
	$(".sortableTable").tablesorter( {sortList: [[0,0]]} );
	$("a.editButton").live("click", function(cEv) {
		cEv.preventDefault();
		$("table.styledTable tbody tr").removeClass("active");
		$(this).parent().parent().addClass("active");

		$("#newInstructorForm input[name='instruktorID']").val( $(this).parent().parent().attr("objid") );
		$("#newInstructorForm input[name='dolgoIme']").val( $(this).parent().parent().find("td[ftype='dolgoIme']").text() );
		$("#newInstructorForm input[name='kratkoIme']").val( $(this).parent().parent().find("td[ftype='kratkoIme']").text() );
		$("#newInstructorForm textarea[name='opis']").val( $(this).parent().parent().find("td[ftype='opis']").text() );

		$("#newInstructorForm").dialog("open");
	
	});
	$("a.removeButton").live("click", function(cEv) {
		cEv.preventDefault();
		$("table.styledTable tbody tr").removeClass("active");
		$(this).parent().parent().addClass("active");
		var id = $(this).parent().parent().attr("objid");
		if(confirm("<?php print t("areYouSure"); ?>")) {
			jQuery.post( "api.php?params=action=removeInstruktor", { "id": id }, function(data, textStatus, XMLHttpRequest) {
				//alert("shranjeno");
				//alert("shranjeno " + textStatus);
				//alert("shranjeno " + JSON.stringify(data));
				if (textStatus=="success")
					if (data.error=="no") {
						//$("#noticeField").html("shranjeno");
						//$("#noticeField").show().fadeOut(5000);
						//$("#urnikColTable tbody").prepend(data.message.body.text).hide().fadeIn();
						
						$("#instruktorListTable tbody tr[objid='" + id + "']").fadeOut("slow");
													
						waitIndicator.hide();
						
					} else {
						switch (data.message.body.text) {
						case "unauthorized":
							alert('<?php print t("unauthorized") ?>');
							break;
						case "fkFail":
							alert('<?php print t("personScheduleExists") ?>');
							break;
						default:
							alert('<?php print t("error") ?>');
							break;
						}	
						waitIndicator.hide();
					}
					$("#newInstructorForm").dialog("close");				
				}, "json" );
		}
	});
	$("#newInstruktorButton").click(function(cEv) {
		$("#newInstructorForm input[name='instruktorID']").val( "" );
		$("#newInstructorForm").dialog("open");
	});
});
</script>

<h3><?php print t("peopleAdminTitle"); ?></h3>
<?php 
$controller = new InstrukorController;
$seznam = $controller->getList();
//print_r($seznam);
?>
<p>
<input type="button" id="newInstruktorButton" value="<?php print t("new"); ?>" />
</p>

<table width="100%" class="styledTable sortableTable" id="instruktorListTable">
<thead>
<tr>
<th><?php print t("name"); ?></th>
<th><?php print t("short"); ?></th>
<th><?php print t("description"); ?></th>
<!-- 
<th><?php print t("visible"); ?></th>
<th><?php print t("validFrom"); ?></th>
<th><?php print t("validTo"); ?></th>
 -->
<th><?php print t("created"); ?></th>
<th><?php print t("updated"); ?></th>
<th>&nbsp;</th>
</tr>
</thead>
<tbody>
<?php 
foreach ($seznam as $row) {
	print "<tr objid='" . $row["id"] . "'>";
	print "<td ftype='dolgoIme'>" . $row["dolgoIme"] . "</td><td ftype='kratkoIme'>" . $row["kratkoIme"] . "</td><td ftype='opis'>" . $row["opis"] . "</td>";
	//print "<td>" . $row["veljaven"] . "</td><td>" . $row["veljaOD"] . "</td><td>" . $row["veljaDO"] . "</td>";
	print "<td>" . $row["ustvarjen"] . "</td><td>" . $row["posodobljen"] . "</td>";
	print "<td><a class='editButton' href='#'><img src='images/edit-icon.png' /></a> <a class='removeButton' href='#'><img src='images/delete.png' /></a></td>";
	print "</tr>";
}
?>
</tbody>
</table>
<?php include_once 'instruktors/newInstruktorForm.php'; ?>
