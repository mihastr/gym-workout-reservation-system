<script language="javascript">

var prostor = 0;
var fc = 0;

function openFcChangeDialog(kapaciteta, fitCenter) {
	$("#cCapacityDialog").dialog("open");	
}
function openPlaceChangeDialog(kapaciteta, fcPlaceName) {
	$("#cCapacityDialog").find("#fcCapacity").val(kapaciteta);
	$("#cCapacityDialog").find("#fcPlaceName").val(fcPlaceName);
	$("#cCapacityDialog").dialog("open");		
	
} 

$(document).ready(function (rEvent) {
	$(".placeRemoveButton").live("click", function(cEv) {
		

		cEv.preventDefault();
		
		var id = $(this).parent().parent().attr("objid");

		if (!confirm("<?php print t("areYouSure"); ?>")) return;

		waitIndicator.show();
		
		// removePlace
		jQuery.post( "api.php?params=action=removePlace", { "id": id }, function(data, textStatus, XMLHttpRequest) {
			//alert("shranjeno");
			//alert("shranjeno " + textStatus);
			//alert("shranjeno " + JSON.stringify(data));
			if (textStatus=="success")
				if (data.error=="no") {
					//$("#noticeField").html("shranjeno");
					//$("#noticeField").show().fadeOut(5000);
					//$("#urnikColTable tbody").prepend(data.message.body.text).hide().fadeIn();
					
					$("#prostoriDIV table tbody tr[objid='" + id + "']").fadeOut("slow");
						
					waitIndicator.hide();
					
				} else {
					switch (data.message.body.text) {
					case "unauthorized":
						alert('<?php print t("unauthorized") ?>');
						break;
					case "fkFail":
						alert('<?php print t("placeScheduleExists") ?>');
						break;

					default:
						alert('<?php print t("error") ?>');
						break;
					}	
					waitIndicator.hide();
				}
			}, "json" );
	});


	$("a.fcRemoveButton").click(function(cEv) {
		waitIndicator.show();

		$("#fcListTable tbody tr").removeClass("active");
		$(this).parent().parent().addClass("active");
		
		cEv.preventDefault();
		
		var id = $(this).parent().parent().attr("objid");

		if (!confirm("<?php print t("areYouSure"); ?>")) return;
		
		// removePlace
		jQuery.post( "api.php?params=action=removeFC", { "id": id }, function(data, textStatus, XMLHttpRequest) {
			//alert("shranjeno");
			//alert("shranjeno " + textStatus);
			//alert("shranjeno " + JSON.stringify(data));
			if (textStatus=="success")
				if (data.error=="no") {
					//$("#noticeField").html("shranjeno");
					//$("#noticeField").show().fadeOut(5000);
					//$("#urnikColTable tbody").prepend(data.message.body.text).hide().fadeIn();
					
					$("#fcListTable tbody tr[objid='" + id + "']").fadeOut("slow");
					$("#prostoriDIV").fadeOut();
						
					waitIndicator.hide();
					
				} else {
					switch (data.message.body.text) {
					case "unauthorized":
						alert('<?php print t("unauthorized") ?>');
						break;
					case "fkFail":
						alert('<?php print t("centerPlaceExists") ?>');
						break;

					default:
						alert('<?php print t("error") ?>');
						break;
					}	
					waitIndicator.hide();
				}
			}, "json" );
	});
	

	
	$("img.fcDetailButton").live("click", function(cEv) {

		$("#fcListTable tbody tr").removeClass("active");
		$(this).parent().parent().addClass("active");
		
		$("img.fcDetailButton").attr("src", "images/arrow_right.png");
		$(this).attr("src", "images/arrow_right2.png");
		$("#prostoriDIV").show();
		fc = $(this).parent().parent().attr("objid");
		//alert(fc);
		jQuery.post( "api.php?params=action=showProstoriZaFC", { "fcID": fc}, function(data, textStatus, XMLHttpRequest) {
			//alert("shranjeno");
			//alert("shranjeno " + textStatus);
			//alert("shranjeno " + JSON.stringify(data));
			if (textStatus=="success")
				if (data.error=="no") {
					//$("#noticeField").html("shranjeno");
					//$("#noticeField").show().fadeOut(5000);
					//$("#urnikColTable tbody").prepend(data.message.body.text).hide().fadeIn();
					
					//feedMessages(data.message.body.text);
					//$("#prostoriDIV").html(JSON.stringify());
					var r = "<table id='fcPlaceDTable' class='styledTable' width='350'>";
					r += '<thead><tr><th>&nbsp;</th><th> <?php print t("title"); ?> </th><th> <?php print t("capacity"); ?> </th></tr></thead><tbody>';
					$(data.message.body.text).each(function(i, obj) {
						r += "<tr objid='" + obj.id + "'><td><a href='#' class='placeEditButton'><img src='images/edit-icon.png' /></a><a href='#' class='placeRemoveButton'><img src='images/delete.png' /></a>" + 
						"</td><td ftype='naziv'>" + obj.naziv + "</td><td ftype='kapaciteta'>" + obj.kapaciteta + "</td></tr>";
					}); 
					r += "</tbody></table>";

					$("#prostoriDIV div").html(r);
					

					//{"id":"8","naziv":"velika dvorana","veljaOD":null,"veljaDO":null,"veljaven":"1","ustvarjen":"2010-11-13 19:53:33","posodobljen":null,"kapaciteta":"30","fitnescenter_id":"6"}
					
					waitIndicator.hide();
					
				} else {
					if (data.message.body.text=="unauthorized") 
						alert('<?php print t("unauthorized") ?>');
					else
						alert('<?php print t("error") ?>');
				}
			}, "json" );
		
		
	});


	$("#cCapacityDialog").dialog({
		autoOpen: false,
		modal: true,
		closeOnEscape: true,
		width: 350,
		buttons: {
			"<?php print t("save"); ?>": function () {
				var value = $("input#fcCapacity").val();

				var nazivProstora = $("#cCapacityDialog").find("#fcPlaceName").val();
	
				//alert(value);
	
				jQuery.post( "api.php?params=action=changePlaceParams", { "placeID": prostor, "value": value, "placename": nazivProstora, "fc": fc}, function(data, textStatus, XMLHttpRequest) {
					//alert("shranjeno");
					//alert("shranjeno " + textStatus);
					//alert("shranjeno " + JSON.stringify(data));
					if (textStatus=="success")
						if (data.error=="no") {
							//$("#noticeField").html("shranjeno");
							//$("#noticeField").show().fadeOut(5000);
							//$("#urnikColTable tbody").prepend(data.message.body.text).hide().fadeIn();
							if (prostor==null) {
								prostor = data.message.body.text;
								$("#prostoriDIV table").prepend("<tr objid='" + prostor + "'><td><a href='#' class='placeEditButton'><img src='images/edit-icon.png' /></a></td><td ftype='naziv'>" + nazivProstora + "</td><td ftype='kapaciteta'>" + value + "</td></tr>");
							} else {
								$("#prostoriDIV table tr[objid='" + prostor + "'] td[ftype='kapaciteta']").text( value );
								$("#prostoriDIV table tr[objid='" + prostor + "'] td[ftype='naziv']").text( nazivProstora );
							}

							$("#cCapacityDialog").dialog("close");
							
							waitIndicator.hide();
							
						} else {
							if (data.message.body.text=="unauthorized") 
								alert('<?php print t("unauthorized") ?>');
							else
								alert('<?php print t("error") ?>');
						}
					}, "json" );
			},
			"<?php print t("cancel"); ?>": function() {
				$(this).dialog("close");
			}
		},
		close: function() {

			}
		});



	$("td[ftype='kapaciteta']").live("click", function(cEv) {
		kapaciteta = $(this).text();
		prostor = $(this).parent().attr("objid");
		$("#cCapacityDialog").find("#fcCapacity").val(kapaciteta);
		var naziv = $(this).parent().parent().find("td[ftype='naziv']").text();  //$(this).parent().parent().attr("objid");
		openPlaceChangeDialog(kapaciteta, naziv);
	});

	$("a.fcEditButton").click(function (cEv) {

		$("#fcListTable tbody tr").removeClass("active");
		$(this).parent().parent().addClass("active");

		$("#newFCForm input[name='id']").val();
		$("#newFCForm input[name='naziv']").val();
		$("#newFCForm input[name='kratkoIme']").val();
		$("#newFCForm input[name='naslov']").val();

		
		cEv.preventDefault();
	});

	
	$("a.placeEditButton").live("click", function (cEv) {

		$("#fcPlaceDTable tbody tr").removeClass("active");
		$(this).parent().parent().addClass("active");
		
		var naziv = $(this).parent().parent().find("td[ftype='naziv']").text();  //$(this).parent().parent().attr("objid");
		var kapaciteta = $(this).parent().parent().find("td[ftype='kapaciteta']").text();

		prostor = $(this).parent().parent().attr("objid"); 

		openPlaceChangeDialog(kapaciteta, naziv);
		cEv.preventDefault();
		/*
		var prostor = $(this).parent().parent().parent().attr("objid");
		alert(prostor);
		$("#cCapacityDialog").find("#fcCapacity").val(kapaciteta);
		openPlaceChangeDialog(kapaciteta, prostor);
		*/
	});

	$("#newPlaceButton").click(function(cEv) {
		prostor = null;
		openPlaceChangeDialog(0, "");
	});

	$("#newFcButton").live("click", function() {
		$("#newFCForm input[name='id']").val("");
		$("#newFCForm").dialog("open");
	});
	$("a.fcEditButton").live("click", function() {
		$("#newFCForm input[name='id']").val($(this).parent().parent().attr("objid"));
		$("#newFCForm input[name='naziv']").val($(this).parent().parent().find("td[ftype='naziv']").text());
		$("#newFCForm input[name='kratkoIme']").val($(this).parent().parent().find("td[ftype='kratkoIme']").text());
		$("#newFCForm input[name='naslov']").val($(this).parent().parent().find("td[ftype='naslov']").text());
		$("#newFCForm").dialog("open");
	});
	
});
</script>
<div id="cCapacityDialog" style="display: none">
	<?php print t("place"); ?> <input type='text' id='fcPlaceName' value='0' />
	<?php print t("capacity"); ?> <input type='text' id='fcCapacity' value='0' />
</div>
<div id="cFCDialog" style="display: none">
	<?php print t("place"); ?> 
	<?php print t("capacity"); ?> 
</div>
<h3><?php print t("placesAdminTitle"); ?></h3>
<p>
<input type="button" id="newFcButton" value="<?php print t("new"); ?>" />
</p>
<?php 
$controller = new FitnesCenterController;
$seznam = $controller->getList();
//print_r($seznam);
?>
<div style="float: left">
<input type="hidden" id="izbranProstorID" />
<table width="100%" id="fcListTable" class="styledTable">
<thead>
<tr><th>&nbsp;</th><th><?php print t("title"); ?></th><th><?php print t("short"); ?></th><th><?php print t("address"); ?></th><th><?php print t("visible"); ?></th><th>&nbsp;</th></th></tr>
</thead>
<tbody>
<?php 
foreach ($seznam as $row) {
	print "<tr objid='" . $row["id"] . "'>";
	print "<td>
	<a href='#' class='fcEditButton'><img src='images/edit-icon.png' /></a>
	<a href='#' class='fcRemoveButton'><img src='images/delete.png' /></a>
		</td>
		<td ftype='naziv'>" . $row["naziv"] . "</td>
		<td ftype='kratkoIme'>" . $row["kratkoIme"] . "</td>
		<td ftype='naslov'>" . $row["naslov"] . "</td>
		<td>" . $row["veljaven"] . "</td>
		<td><img class='fcDetailButton' src='images/arrow_right.png' /></td>";
	print "</tr>";
}
?>
</tbody>
</table>
</div>
<div style="float: left; padding-left: 15px; display:none" id="prostoriDIV">
<div></div>
<p>
<input type="button" id="newPlaceButton" value="<?php print t("new"); ?>" />
</p>
</div>
<?php 
include_once 'places/newCenterForm.php';
?>