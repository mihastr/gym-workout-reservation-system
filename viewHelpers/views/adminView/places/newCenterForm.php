<script language="javascript">

function saveFormData() {
	waitIndicator.show();

	var id = $("#newFCForm input[name='id']").val();
	var naziv = $("#newFCForm input[name='naziv']").val();
	var kratkoIme = $("#newFCForm input[name='kratkoIme']").val();
	var naslov = $("#newFCForm input[name='naslov']").val();

	if (id=="") {
		jQuery.post( "api.php?params=action=addFC", { "naziv": naziv, "kratkoIme": kratkoIme, "naslov": naslov }, function(data, textStatus, XMLHttpRequest) {
			//alert("shranjeno");
			//alert("shranjeno " + textStatus);
			//alert("shranjeno " + JSON.stringify(data));
			if (textStatus=="success")
				if (data.error=="no") {
					//$("#noticeField").html("shranjeno");
					//$("#noticeField").show().fadeOut(5000);
					//$("#urnikColTable tbody").prepend(data.message.body.text).hide().fadeIn();
					
					
					$("<tr objid='" + data.message.body.text + "'>" +
							"<td><a href='#' class='fcEditButton'><img src='images/edit-icon.png' /></a> " +
							"<a href='#' class='fcRemoveButton'><img src='images/delete.png' /></a></td>" +
							"<td ftype='naziv'>" + naziv + "</td>" + 
							"<td ftype='kratkoIme'>" + kratkoIme + "</td>" +
							"<td ftype='naslov'>" + naslov + "</td>" +
							"<td>1</td>" +
							"<td><img class='fcDetailButton' src='images/arrow_right.png' /></td>" +
							"</tr>").appendTo("#fcListTable tbody");

						
					waitIndicator.hide();
					
				} else {
					switch (data.message.body.text) {
					case "unauthorized":
						alert('<?php print t("unauthorized") ?>');
						break;
					case "fkFail":
						//alert('<?php print t("centerPlaceExists") ?>');
						break;
					default:
						alert('<?php print t("error") ?>');
						break;
					}	
					waitIndicator.hide();
				}
				$("#newFCForm").dialog("close");				
			}, "json" );
	} else {
		jQuery.post( "api.php?params=action=editFC", { "id": id, "naziv": naziv, "kratkoIme": kratkoIme, "naslov": naslov}, function(data, textStatus, XMLHttpRequest) {
			//alert("shranjeno");
			//alert("shranjeno " + textStatus);
			//alert("shranjeno " + JSON.stringify(data));
			if (textStatus=="success")
				if (data.error=="no") {
					//$("#noticeField").html("shranjeno");
					//$("#noticeField").show().fadeOut(5000);
					//$("#urnikColTable tbody").prepend(data.message.body.text).hide().fadeIn();
					
					var row = $("#fcListTable tbody tr[objid='" + id + "']");

					row.find("td[ftype='naziv']").text(naziv);
					row.find("td[ftype='naslov']").text(naslov);
					row.find("td[ftype='kratkoIme']").text(kratkoIme);
						
					waitIndicator.hide();
					
				} else {
					switch (data.message.body.text) {
					case "unauthorized":
						alert('<?php print t("unauthorized") ?>');
						break;
					case "fkFail":
						//alert('<?php print t("centerPlaceExists") ?>');
						break;
					default:
						alert('<?php print t("error") ?>');
						break;
					}	
					waitIndicator.hide();
				}
				$("#newFCForm").dialog("close");
			}, "json" );
	}
	


	
}

$(document).ready(function() {
	$("#newFCForm").dialog({
		autoOpen: false,
		modal: true,
		closeOnEscape: true,
		width: 350,
		buttons: {
			"<?php print t("save"); ?>": function () {
				saveFormData();
			},
			"<?php print t("cancel"); ?>": function () {
				$("#newFCForm").dialog("close");
			}
		},
		close: function() {
			$("#newFCForm input[name='id']").val("");
			$("#newFCForm input[name='naziv']").val("");
			$("#newFCForm input[name='kratkoIme']").val("");
			$("#newFCForm input[name='naslov']").val("");
		}
	});
});
</script>


<div id="newFCForm" style="display: block" class="dialogForm" title="<?php print t("fc"); ?>">
<h3><?php print ucfirst(t("fc")); ?></h3>
<form>
<input type="hidden" name="id" />
<label><?php print t("title"); ?>: </label><input name="naziv" /><br />
<label><?php print t("short"); ?>: </label><input name="kratkoIme" /><br />
<label><?php print t("address"); ?>: </label><input name="naslov" /><br />
</form>
</div>