<script language="javascript">
function naloziDovoljenja(userid) {
	$("#profUpDetail div[ctype='dovoljenja']").load("load.php?page=sifranti&sub=people&action=permissions", {"uporabnik": userid}, function() {});
}
function naloziProfil(userid) {
	$("#profUpDetail div[ctype='profil']").load("load.php?page=sifranti&sub=people&action=profile", {"uporabnik": userid}, function() {});
}
function naloziStatistiko(userid) {
	$("#profUpDetail div[ctype='statistika']").load("load.php?page=sifranti&sub=people&action=stats", {"uporabnik": userid}, function() {});
}
function naloziDnevnik(userid, od, stevilo) {
	$("#profUpDetail div[ctype='dnevnik']").load("load.php?page=sifranti&sub=people&action=log", {"uporabnik": userid}, function() {});
}

function naloziPrijave(userid, od, stevilo) {
	$("#profUpDetail div[ctype='prijave']").load("load.php?page=sifranti&sub=people&action=attendance", {"uporabnik": userid}, function() {});
}


$(document).ready(function(rEvent) {
	$( "#userSearchField" ).autocomplete({
		source: function( request, response ) {
			$.ajax({
				url: "api.php?params=action=searchUserByName",
				type: "POST",
				dataType: "json",
				data: {
					q: request.term
				},
				success: function( data ) {
					//alert("lala");
					response( $.map( data.message.body.text, function( item ) {
						//alert(JSON.stringify(item));
						return {
							label: item.ime + ' ' + item.priimek + ' - ' + item.sokol_id,
							value: item.ime + ' ' + item.priimek,
							id: item.id
						}
					}));
				}
			});
		},
		minLength: 2,
		select: function( event, ui ) {
			//alert(ui.item.value);
			//$("#userSearchField").val(ui.item.label);
			$("#profilUporabnika div.nodata").hide();
			$("#profUpDetail").fadeIn();
			naloziDovoljenja(ui.item.id);
			naloziPrijave(ui.item.id);
			naloziProfil(ui.item.id);
			naloziStatistiko(ui.item.id);
			naloziDnevnik(ui.item.id, 0, 20);
			//event.preventDefault();
		},
		open: function() {
			//$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
		},
		close: function() {
			//$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
		}
	});
});
</script>

<div>
	<h3><?php print t("usersAdminTitle"); ?></h3>
	<p><?php print ucfirst(t("search")); ?>: <input id="userSearchField"  /></p>
</div>
<div margin-left: 10px;" id="profilUporabnika">
<div class="nodata"><br /><br /><br /></div>
<div id="profUpDetail" style="display:none">
	<h4>Profil</h4>
	<div ctype="profil"></div>
	<h4>Prijave na ure</h4>
	<div ctype="prijave"></div>
	<h4>Dovoljenja</h4>
	<div ctype="dovoljenja"></div>
	<h4>Statistika</h4>
	<div ctype="statistika"></div>
	<h4>Dnevnik</h4>
	<div ctype="dnevnik"></div>
</div>

</div>