<div style="float: left; margin-left:30px">
</p>
Trenutno so v veljavi naslednji urniki
<p>
<p>Filtriraj rezultate
<a href="index?page=adminurniki">(počisti izbiro)</a>:
<ul id="fcList"><?php 
	// seznam fitnes centrov
	$fc = new FitnesCenterController;
	$fcList = $fc->getList(); 
	$selectedFC = -1;
	if (isset($_GET["fc"])) {
		$selectedFC = (int)$_GET["fc"]; 
	}
	foreach ($fcList as $center) {
		if ($selectedFC==$center["id"])
			print "<li>" . $center["kratkoIme"] . "</li>";
		else
			print "<li><a href='index?page=adminurniki&fc=" . $center["id"] . "'>" . $center["kratkoIme"] . "</a></li>";
	}

?></ul>
<ul id="prostorList">
<?php 
	// prostori
	$prostori = new ProstorController;
	$prList = $prostori->getListForFC($selectedFC);
	//print_r($prList);
	$selectedProstor = -1;
	if (isset($_GET["prostor"])) {
		$selectedProstor = (int)$_GET["prostor"]; 
	}
	//print_r($prList);
	foreach ($prList as $prostor) {
		if ($selectedProstor==$prostor["id"])
			print "<li>" . $prostor["naziv"] . "</li>";
		else
			print "<li><a href='index?page=adminurniki&fc=" . $selectedFC . "&prostor=" . $prostor["id"] . "'>" . $prostor["naziv"] . "</a></li>";
	}
?>
</ul>

<script>
var seznamInstruktorjev = null;
var seznamVadb = null;

function populateUrnikDetail(idUrnik, waitIndicator) {
	var urnikDetail = null;
	var outObject = {"id": idUrnik};
	waitIndicator.show();
	
	jQuery.post( "api.php?params=action=showUrnikDetails", outObject, function(data, textStatus, XMLHttpRequest) {
		//alert("shranjeno");
		//alert("shranjeno " + textStatus);
		//alert("shranjeno " + JSON.stringify(data));
		if (textStatus=="success")
			if (data.error=="no") {
				// nafilamo podatke obrazca
				var urnik = data.message.body.text.urnik[0];
				var ure = data.message.body.text.ure;
				//alert(JSON.stringify(urnik.urnik));
				$("#calNameField").html(urnik.urnik);	
				$("#calDetailMeta input[ftype='urnik_id']").val(urnik.id);
				$("#calDetailMeta input[ftype='urnik']").val(urnik.urnik);
				$("#calDetailMeta input[ftype='veljaOD']").val(urnik.veljaOD);
				$("#calDetailMeta input[ftype='veljaDO']").val(urnik.veljaDO);
				$("#calDetailMeta #calDetailVeljaven input[value='" + urnik.veljaven + "']").attr("checked", "checked");
				$("#calDetailMeta input[ftype='ustvarjen']").val(urnik.ustvarjen);
				$("#calDetailMeta input[ftype='posodobljen']").val(urnik.posodobljen);
				$(ure).each(function (i, ura) {
					//alert(ura.ura);
					var vadba = "<div objtype='V' objid='" + ura.vid + "'><a href='#'>x</a> " + ura.naziv + "</div>";
					var instruktor = "<div objtype='I' objid='" + ura.iid + "'><a href='#'>x</a> " + ura.instruktor + "</div>";
					$("#weekTableSchedukeEdit tr[ura='" + ura.ura + "'] td[dan='" + ura.dan + "']").attr("objid", ura.id);
					if (ura.vid!=null) 
						$("#weekTableSchedukeEdit tr[ura='" + ura.ura + "'] td[dan='" + ura.dan + "'] .forVadba").html(vadba);
					if (ura.iid!=null)
					$("#weekTableSchedukeEdit tr[ura='" + ura.ura + "'] td[dan='" + ura.dan + "'] .forInstruktor").html(instruktor);
				});
			}
		waitIndicator.hide();
		$("#urnikPodrobnostiDIV").dialog("open");	
	}, "json" );
	

};

$(document).ready(function(event) {


	
	var calContainer = "#calEditorContainer";
	
	$("#fval_veljaOD").datepicker( calConfig );
	$("#fval_veljaDO").datepicker( calConfig );
	$("#urnikVeljaven").buttonset();
	$("#urnikColTable tbody tr").live('dblclick', function(ev) {
		$("#urnikColTable tbody tr").removeClass("selectedTR");
		$("#urnikColTable tbody tr").addClass("normal");
		
		$(this).removeClass("normal");
		$(this).addClass("selectedTR");
		var rid = $(this).attr("rowid");
		//alert("spreminjaš urnik: " + rid);

		populateUrnikDetail(rid, waitIndicator);
		
		
	});
	$("#novUrnikGumb").button();
	$("#novUrnikGumb").click(function(ev) {

		waitIndicator.show();
		var napaka = "";
		var naziv = $("#fval_naziv").val();
		var veljaOD = $("#fval_veljaOD").datepicker("getDate");
		var veljaDO = $("#fval_veljaDO").datepicker("getDate");
		var center  = $("#fcID").val();
		var prostor  = $("#prostorID").val();
		
		var urnikVeljaven = $("#urnikColTable input[@name='urnikVeljaven']:checked").val(); 

		if (prostor == null || prostor==undefined || prostor.length==0) {
			napaka += "- izberite fitnes center<br/>";
		}
		if (center == null || center==undefined || center==-1 || center.length==0) {
			napaka += "- izberite prostor<br/>";
		}

		
		if (naziv == null || naziv.length==0) {
			napaka += "- polje 'naziv' ne sme biti prazno<br/>";
		}
		if (veljaOD == null || veljaOD.length==0) {
			napaka += "- polje 'velja OD' ne sme biti prazno<br/>";
		} else {
			var dZ = formatDate(veljaOD, "yyyy-MM-dd");
		}
		if (veljaDO == null || veljaDO.length==0) {
			napaka += "- polje 'velja OD' ne sme biti prazno<br/>";
		} else {
			var dK = formatDate(veljaDO, "yyyy-MM-dd");
		}
		
		if (dZ>dK) {
			napaka += "- urnik se ne more končati preden se je začel<br/>";
		}
		if (napaka.length>0) {
			obvestiloDialog.html(napaka);
			obvestiloDialog.dialog("open");
			waitIndicator.hide();
		} else {
			var outObject = {
				"naziv" : naziv,
				"veljaOD" : dZ,
				"veljaDO": dK,
				"veljaven": urnikVeljaven,
				"prostor_id": prostor
			};
			jQuery.post( "api.php?params=action=createNewSchedule", outObject, function(data, textStatus, XMLHttpRequest) {
				//alert("shranjeno");
				//alert("shranjeno " + textStatus);
				//alert("shranjeno " + JSON.stringify(data));
				if (textStatus=="success")
					if (data.error=="no") {
						$("#noticeField").html("shranjeno");
						$("#noticeField").show().fadeOut(5000);
						$("#urnikColTable tbody").prepend(data.message.body.text).hide().fadeIn();
						$("#urnikColTable input:lt(3)").val("");
					}
				waitIndicator.hide();	
			}, "json" );
			//alert("ok");
			//alert("vse ok " + naziv + " " + dZ + " " + dK + " " + urnikVeljaven);
		}
		//waitIndicator.show();
		
		//waitIndicator.hide();
	});


	$( "#urnikColTable tbody" ).sortable({
		placeholder: "ui-state-highlight",
		update: function(event, ui) {
			$("#shraniGumb").removeAttr("disabled")
			$("#urnikColTable tbody tr").each(function (i, val) {
				$(this).find("[ftype='prioriteta']").html(i);
			});
		}

	});
	$( "#urnikColTable tbody" ).disableSelection();

	$("#shraniGumb").click(function(ev) {
		var spremembe = [];
		$("#urnikColTable tbody tr").each(function (i, val) {
			spremembe[i] = {
					"pogoj": {
						"id": $(this).find("[ftype='id']").html()
					},
					"podatki" : { 
						"prioriteta": $(this).find("[ftype='prioriteta']").html()
					}
				}; 
		});
		var outObject = {
				"data" : spremembe
				};
		jQuery.post( "api.php?params=action=scheduleChangeOrder", outObject, function(data, textStatus, XMLHttpRequest) {
			//alert("shranjeno");
			//alert("shranjeno " + textStatus);
			//alert("shranjeno " + JSON.stringify(data));
			if (textStatus=="success")
				if (data.error=="no") {
					//$("#noticeField").html("shranjeno");
					$("#noticeField").html("shranjeno").show().fadeOut(5000);
					//$("#urnikColTable tbody").prepend(data.message.body.text).hide().fadeIn();
					waitIndicator.hide();
					$("#shraniGumb").attr("disabled","disabled");
				}
			
				
		}, "json" );
		//alert(JSON.stringify(spremembe));
	});

	loadSeznamInstruktorjev($("#seznamInstruktorjev"));
	loadSeznamVadb($("#seznamVadb"));
});
</script>
<div id="noticeFieldDIV"><span id="noticeField">&nbsp;</span></div>
<div class="roundedTableCorner">
<table id="urnikColTable">
<thead>
	<tr>
		<th>&nbsp;</th>
		<th>NAZIV</th>
		<th>VELJA OD</th>
		<th>VELJA DO</th>
		<th>PRIORITETA</th>
		<th>VELJAVEN</th>
		<th>USTVARJEN</th>
		<th>SPREMENJEN</th>
		<th>CENTER</th>
		<th>PROSTOR</th>
		<th>&nbsp;</th>
	</tr>
	<tr>
		<th>&nbsp;</th>
		<th valign="middle"><input class='titleField' type='text' id='fval_naziv' /></th>
		<th valign="middle"><input class='dateField' type='text' id='fval_veljaOD' /></th>
		<th valign="middle"><input class='dateField' type='text' id='fval_veljaDO' /></th>
		<th valign="middle">&nbsp;</th>
		<th valign="middle">
		  <div id="urnikVeljaven">
		    <input type="radio" value="1" name="urnikVeljaven" id="veljavenDA" checked="checked" /><label for="veljavenDA">DA</label>
		    <input type="radio" value="0" name="urnikVeljaven" id="veljavenNE" /><label for="veljavenNE">NE</label>
		  </div>
		</th>
		<th colspan="2">&nbsp;</th>
		<th><?php print getCombobox($fcList, "fcID"); ?></th>
		<th id="novUrnikDvoranaIDDIV"></th>
		<th><input type="button" id="novUrnikGumb" value="+" /></th>
	</tr>	
</thead>
<tbody>	
	<?php 
	// seznam urnikov
	if ($selectedProstor != -1)
		$urnikList = $this->urnik->getUrnikiListByProstor($selectedProstor);
	elseif ($selectedFC != -1) {
		$urnikList = $this->urnik->getUrnikiListByFC($selectedFC);
	} else {
		$urnikList = $this->urnik->getUrnikiList();
	}
		
	
	print convertToTR($urnikList);
	/*
	if (count($urnikList)>0) {	
		foreach($urnikList as $urnik) {
			print "\n\t<tr rowid='" . $urnik["id"] . "'>";
			foreach($urnik as $ftype=>$field) {
				print "<td ftype='$ftype'>";
				if ($ftype=="veljaven") {
					print ($ftype==1) ? "da" : "ne"; 
				} elseif ($ftype=="veljaOD" || $ftype=="veljaDO") {
					print date( 'd.m.Y', strtotime($field) );
				} else {
					print $field;				
				}
				print "</td>";
			}
			print '<td>&uarr; &darr;</td>';
			print '</tr>';
		}	
	} else {
		print 'še ni urnikov';
	}
*/
?>
</tbody> 
</table>
</div>
<p>
	<input id="shraniGumb" type="button" value="shrani spremembe" disabled="disabled" />
</p>

</div>

<script>
$(document).ready(function(rEvent) {
	
	$("#weekTableSchedukeEdit tbody td").droppable({
		drop: function(event, ui) {
			$("#schItemsSave").removeAttr("disabled");
			//alert("drop");
			var text = ui.draggable.text();
			var type = ui.draggable.attr("t");
			var rowid = ui.draggable.attr("rowid");

			addElement = "<div objType='" + type + "' objid='" + rowid + "'><a href='#'>x</a> " + text + "</div>";
			
			if (type=="I")
				$(this).find(".forInstruktor").html("").append(addElement);
			else
			if (type=="V")
				$(this).find(".forVadba").html("").append(addElement);
			/*else {
				$(this).append(text);
			}*/
			
		}
	});

	// namenjeno odstranjevanju 
	$("#weekTableSchedukeEdit tbody td a").live("click", function(ev) {


		$(this).parent().parent().parent().attr("modified", "1");

		// pomeni zbrisano
		$(this).parent().attr("objid", -1);
		
		
		$(this).parent().html("");
		ev.preventDefault();
	});
	
	$("#seznamVadb li").live('mouseover', function() {
		//var isDraggable = $(this).attr("isDraggable");
		//alert(JSON.stringify(isDraggable));
		//if (isDraggable == undefined || isDraggable == "false") {
			$(this).draggable({
				appendTo: "#weekTableSchedukeEdit tbody",
				helper: "clone",
				cursor: 'hand'
			});
			//$(this).attr("isDraggable", true);
		//}
	});
	$("#seznamInstruktorjev li").live('mouseover', function() {
		$(this).draggable({
			appendTo: "#weekTableSchedukeEdit tbody",
			helper: "clone",
			cursor: 'hand'
		});
	});
	
	
});
</script>
<div id="urnikPodrobnostiDIV" title="Urejanje urnika">
	<?php include_once 'adminUrnikView/urnikEdit.php'; ?>
</div>
