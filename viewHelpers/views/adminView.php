<script language="javascript">
var action = window.location.hash.replace("#", "");

if (action.length==0) {
	action = "messaging";
}

function doAction() {
	waitIndicator.show();
	window.location.hash = action;
	$("ul.menuVertical li.selected").removeClass("selected");
	$("ul.menuVertical li a[href='#" + action +"']").parent().addClass("selected");
	$("#adminContent").load("load.php?page=sifranti&sub=" + action, {}, function(a, b, c) {
		//alert("done");
		waitIndicator.hide();
	});
	
}


$(document).ready(function (rEvent) {
	
	doAction();
		
	$("ul.menuVertical li").click(function(cEvent) {
		action = $(this).find("a").attr("href").replace("#", "");
		doAction();
		cEvent.preventDefault();
	});
});
</script>
<p><?php print ucfirst(t("adminBody")); ?></p>
<div class="roundedTableCorner">
<table>
<tr>
<td valign="top">
	<p><?php print ucfirst(t("options")); ?>:</p>
	<ul class="menuVertical">
	<li><a href="#people"><?php print ucfirst(t("users")); ?></a></li>
	<li><a href="#messaging"><?php print ucfirst(t("messaging")); ?></a></li>
	<li><a href="#content"><?php print ucfirst(t("programme")); ?></a></li>
	<li><a href="#instructors"><?php print ucfirst(t("instructors")); ?></a></li>
	<li><a href="#places"><?php print ucfirst(t("fitnesCenters")); ?></a></li>
	<!--<li><a href="#settings"><?php print ucfirst(t("settings")); ?></a></li>-->
	<li><a href="#translations"><?php print ucfirst(t("translations")); ?></a></li>
	<li><a href="#bugreports"><?php print ucfirst(t("bugReports")); ?></a></li>
	</ul>
</td>
<td valign="top">
	<div id="adminContent"></div>
</td>
</tr>
</table>
</div>