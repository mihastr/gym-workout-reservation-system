<script>
$(document).ready(function() {
	waitIndicator.hide();

	function makeQTip() {
		$('a.msgsShowRecipients').each(function(){
			   $(this).click(function(){ return false });//JS enabled => link default behavior disabled. Gaceful degradation
			   $(this).qtip({
				   content: {
				      url: 'load.php?page=sifranti&sub=messaging&action=getMsgRecepients',
				      data: { id: ($(this).parent().parent().attr("objid")) },
				      method: 'post'
				   }, style: { 
					      name: 'blue', // Inherit from preset style
					      border: {
					      width: 3,
					      radius: 8,
				          color: '#6699CC'
				      }
								      
				   }
				});
		});
	}

	makeQTip();
	
});
</script>
<div style="margin-left: 10px;">
<h2><?php print t("messages"); ?></h2>
<?php 
include_once 'inboxView/messagesView.php';
?>
</div>