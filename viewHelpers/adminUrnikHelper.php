<?php
include_once('main.php');
class AdminUrnikHelper extends MainHelper {
	private $urnik = null;
	function __construct() {
		$this->title = "UREJANJE URNIKOV";
		$this->html = "vsebina prve strani";
		$this->urnik = new Urnik;
	}
	function renderHTML() {
		include_once 'views/adminUrnikView.php';
	}
}