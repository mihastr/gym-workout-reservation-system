<?php
include_once('main.php');
class ProfileHelper extends MainHelper {
	public $naziv="Naziv uporabnika";
	
	private $uporabnik = null;
	
	private $fitnesCenter = null;
	
	function __construct() {
		$this->title = "";
		$this->html = "vsebina prve strani";
		$this->uporabnik = new UporabnikController;
		$this->uporabnik->info = $this->uporabnik->findByID($_SESSION["userid"]);
		$this->profil = $this->uporabnik->info;
		
		$this->fitnesCenter = new FitnesCenterController;
		
		//print_r($this->profil);
			
		$this->naziv = $this->profil["ime"] . " " . $this->profil["priimek"] . ((isset($this->profil["naziv"]) &&  strlen($this->profil["naziv"])>0) ? " (" . $this->profil["naziv"] . ")" : ""); 
	}
	
	function getFcListForUser() {
		return $this->fitnesCenter->listPlacesForUser();
	}
	
	function renderHTML() {
		include_once 'views/profileView.php';
	}
	
	function getUserAttendanceHistory() {
		return $this->uporabnik->getUserAttendanceHistory();
	}
}