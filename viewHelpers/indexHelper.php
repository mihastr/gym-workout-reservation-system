<?php
include_once('main.php');
class IndexHelper extends MainHelper {
	public $vreme = null;
	function __construct() {
		$this->title = "URNIK VADB";
		$this->html = "vsebina prve strani";
		$this->vreme = new VremeController;
	}
	function renderHTML() {
		include_once 'views/indexView.php';
	}
}