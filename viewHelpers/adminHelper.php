<?php
include_once('main.php');
include_once('../controlers/mainController.php');
class AdminHelper extends MainHelper {
	
	public $uporabnik;
	
	function getPeopleAdminView() {
		include_once 'views/adminView/peopleView.php';
	}
	
	// messages
	function getMessagingView() { 
		$this->c = new ObvestiloController;
		include_once 'views/adminView/messagesView.php';
	}
	function getMsgRecepientsView() {
		$this->c = new ObvestiloController;
		include_once 'views/adminView/messages/showRecepients.php';
	}	
	
	function getContentView() {
		include_once 'views/adminView/classesView.php';
	}
	function getInstructorsAdminView() {
		include_once 'views/adminView/instructorView.php';
	}
	function getPlacesAdminView() {
		include_once 'views/adminView/placesView.php';
	}
	function getSettingsAdminView() {
		include_once 'views/adminView/settingsView.php';
	}
	
	function getTranslationsView() {
		include_once 'views/adminView/translationsView.php';
	}
	
	function getBugReportView() {
		include_once 'views/adminView/bugReportView.php';
	}
	
	function __construct() {
		$this->title = "ADMINISTRATIVNE STRANI";
		$this->html = "vsebina prve strani";
	}
	function renderHTML() {
		include_once 'views/adminView.php';
	}
}