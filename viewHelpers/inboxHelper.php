<?php
include_once('main.php');
class InboxHelper extends MainHelper {
	public $naziv="Naziv uporabnika";
	
	private $uporabnik = null;
	
	private $fitnesCenter = null;
	
	public $list = null;
	
	function __construct() {
		$this->title = "";
		$this->html = "vsebina prve strani";
		$this->uporabnik = new Uporabnik;
		$this->uporabnik->findByID($_SESSION["userid"]);
		$this->profil = $this->uporabnik->info;
		
		$this->fitnesCenter = new FitnesCenterController;
		
		//print_r($this->profil);
		
		$this->fillList();
			
		$this->naziv = $this->profil["ime"] . " " . $this->profil["priimek"] . ((isset($this->profil["naziv"]) &&  strlen($this->profil["naziv"])>0) ? " (" . $this->profil["naziv"] . ")" : ""); 
	}
	
	function fillList() {
		$c = new ObvestiloController;
		$this->list = $c->getList();
	}
	
	function getFcListForUser() {
		return $this->fitnesCenter->listPlacesForUser();
	}
	
	function renderHTML() {
		include_once 'views/inboxView.php';
	}
}