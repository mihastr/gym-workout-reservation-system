<?php
include_once('main.php');
class StatsHelper extends MainHelper {
	function __construct() {
		$this->title = "STATISTIKE";
		$this->html = "vsebina prve strani";
	}
	
	function getPeopleAdminView() {
		include_once 'views/statsView/peopleView.php';
	}
	
	function getScheduleStatsView() {
		include_once 'views/statsView/scheduleStats.php';
	}
	
	function getAttStatsView() {
		include_once 'views/statsView/attStats.php';
	}
	
	function getMessagingView() {
		include_once 'views/statsView/messagesView.php';
	}
	function getContentView() {
		include_once 'views/statsView/classesView.php';
	}
	function getInstructorsAdminView() {
		include_once 'views/statsView/instructorView.php';
	}
	function getPlacesAdminView() {
		include_once 'views/statsView/placesView.php';
	}
	function getSettingsAdminView() {
		include_once 'views/statsView/settingsView.php';
	}
	
	function renderHTML() {
		include_once 'views/statsView.php';
	}
}