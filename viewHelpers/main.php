<?php
// include model fajlov
include_once '../config/pageSettings.php';
include_once('../models/mainModel.php');

abstract class MainHelper {
	public $title = "Trenutni naslov";
	
	private $html = "to je vsebina strani";
	
	private $path = "";
	
	function __construct() {
		array_push($pathArray, '/');
	}
	function getPath() {
		$path = '';
		foreach ($pathArray as $sub) {
			$path .= ' / ' . $sub; 
		}
	}
	function setPat($path) {
		$this->path = $path;
	}
	function renderHTML() {
		print $this->html;
	}
	function renderTopMenu() {
		include_once 'views/topMenu.php';
	}
}