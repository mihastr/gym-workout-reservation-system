<?php
include_once '../config/dbconfig.php';
include_once '../config/queries/queries.php';

include_once 'Urnik.php';
include_once 'Instruktor.php';
include_once 'Vadba.php';
include_once 'FitnesCenter.php';
include_once 'Prostor.php';
include_once 'Ura.php';
include_once 'Obvestilo.php';

include_once 'Urnik7D.php';

include_once 'Uporabnik.php';

include_once 'Vreme.php';
include_once 'VremeNapoved.php';

abstract class MainModel {
	//private $db=null;
	private  $id = 0;
	
	function __construct() {
		//$this->db = new DbConfig;
	}

	function getID() {
		return $this->id;
	} 
	abstract function getFields();
	
}
