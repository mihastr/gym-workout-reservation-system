<?php
class Ura extends MainModel {

	const  objDBName = "ura";
	
	private  $id = null;
	private  $naziv = null;
	private  $ura = null;
	private  $dan = null;
	private  $omejitev = null;
	private  $veljaOD = null;
	private  $veljaDO = null;
	private  $veljaven  = 1;
	
	//private $db = null;
	
	private $_ureList = null;

	function __construct() {
		//$this->db = new DbConfig;
	}
	// sets object
	function findByID($id) {
		//$this->db = new DbConfig;
	}
	
	function getListByUrnikID($id) {
		if ($this->_ureList==null) {
			$db = new DbConfig;
			$this->_ureList = $db->executeSelect(str_replace("{p1}", $id, Queries::getUreByUrnikID));	
		}
		
		return $this->_ureList;
	}
	
	function updateRecords($records, $urnikID) {
		$db = new DbConfig;
		
		if (count($records)>0) {
			try {
				$db->beginTransaction();
				foreach ($records as $record) {
					if ($record["objid"]=="-1") {
						// nova ura
						$this->createNew($record["dan"], $record["ura"], $record["instruktor"], $record["vadba"], $urnikID);
					} else {
						// posodabljanje ure
						$this->updateForID($record["dan"], $record["ura"], $record["instruktor"], $record["vadba"], $urnikID, $record["objid"]);
					}
				}		
				$db->commitTransaction();
			} catch (Exception $ex) {
				$db->rollbackTransaction();
				throw $ex;
			}
			
		}
	}
	
	
	// update record
	function updateForID($dan, $ura, $instruktor, $vadba, $urnik_id, $id) {
		$db = new DbConfig;
		$conn = $db->getConnection();
		try {
			$record = array();
			$record["dan"] = $dan;
			$record["ura"] = $ura;
			
			$record["vadba_id"] = ($vadba == -1 ? null : $vadba);
			$record["instruktor_id"] = ($instruktor == -1 ? null : $instruktor);

			$condition = array("id"=>$id, "urnik_id" => $urnik_id);
			
			//pisi_log(-21, "debug: " . json_encode($record), "ura.php", 75);
			
			// dobimo vstavljeno vstico nazaj iz baze
			return $db->updateNoCommit(Ura::objDBName, $record, $condition, $conn);
		} catch (Exception $ex) {
			throw new Exception("napaka pri ustvarjanju objekta: " . $ex->getMessage());			
		}
	}
	
	
	// creates new record
	function createNew($dan, $ura, $instruktor, $vadba, $urnik) {
		$db = new DbConfig;
		$conn = $db->getConnection();
		try {
			$record = array();
			$record["dan"] = $dan;
			$record["ura"] = $ura;
			$record["vadba_id"] = $vadba;
			$record["urnik_id"] = $urnik;
			$record["instruktor_id"] = $instruktor;
			
			// dobimo vstavljeno vstico nazaj iz baze
			return $db->insertNoCommit(Ura::objDBName, $record, $conn);
		} catch (Exception $ex) {
			throw new Exception("napaka pri ustvarjanju objekta: " . $ex->getMessage());			
		}
	}
	
	function update($params) {
		$db = new DbConfig;
		
		if (isset($params["bulk"]) && $params["bulk"])
			$db->executeBulkUpdate(Urnik::objDBName, $params["data"]);
		else
			$db->executeUpdate(Urnik::objDBName, $params["data"]);
	}
	

	function getFields() {
		$fields = get_object_vars($this);
		$fRet = array();
		
		foreach ($fields as $ft=>$fv) {
			if ($ft[0]!='_' && $ft!="id") $fRet[$ft] = "'$fv'";
		} 
		return $fRet;
	}
	function getID() {
		return $this->id;
	} 
}
?>