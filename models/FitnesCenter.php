<?php
class FitnesCenter extends MainModel {

	const  objDBName = "fitnescenter";
	
	private  $id = null;
	private  $naziv = null;
	private  $kratkoIme = null;
	private  $naslov = null;
	private  $veljaOD = null;
	private  $veljaDO = null;
	private  $veljaven  = 1;
	
	private $_prostor = null;
	
	private $_db = null;
	
	private $_firnesCenterList = null;

	function __construct() {
		//$this->db = new DbConfig;
		$this->_prostor = new ProstorController;
		$this->_db = new DbConfig;
	}
	// sets object
	function findByID($id) {
		//$this->db = new DbConfig;
	}
	
	function getList() {
		if ($this->_firnesCenterList==null) {
			$db = new DbConfig;
			$this->_firnesCenterList = $db->executeSelect(Queries::getFitnesCentri);	
		}
		
		return $this->_firnesCenterList;
	}
	
	function getListForUser($uid=null) {
		if ($uid==null)
			$uid = $_SESSION["userid"];
			
		return $this->_prostor->getListForUser($uid);
	}
	
	// creates new object
	function create($naziv, $kratkoIme, $naslov) {
		if (!isset($this->_db)) $this->_db = new DbConfig;
		
		return $this->_db->insertNoCommit(FitnesCenter::objDBName, array("naziv" => $naziv, "naslov" => $naslov, "kratkoIme" => $kratkoIme ));
	}
	
	function update($id, $naziv, $kratkoIme, $naslov) {
		if (!isset($this->_db)) $this->_db = new DbConfig;
		
		return $this->_db->updateNoCommit(FitnesCenter::objDBName, array("naziv" => $naziv, "naslov" => $naslov, "kratkoIme" => $kratkoIme ), array("id" => $id));
		
	}
	
	function remove($id) {
		if (!isset($this->_dbConf) || $this->_dbConf==null) $this->_dbConf = new DbConfig;
		
		if (!is_numeric($id)) throw new Exception(DbConfig::paramFail);
		
			if ($this->_dbConf->count("prostor", array("fitnescenter_id"=>$id, "veljaDO"=>"null"))==0) {
				$this->_dbConf->updateNoCommit(FitnesCenter::objDBName, array("veljaDO" => "now()"), array("id"=>$id));		
			} else {
				throw new Exception(DbConfig::fkFail);
			}	
		
		return "ok";
		
	}
	

	function getFields() {
		$fields = get_object_vars($this);
		$fRet = array();
		
		foreach ($fields as $ft=>$fv) {
			if ($ft[0]!='_' && $ft!="id") $fRet[$ft] = "'$fv'";
		} 
		return $fRet;
	}
	function getID() {
		return $this->id;
	} 
}
?>