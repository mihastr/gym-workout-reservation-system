<?php
class Uporabnik extends MainModel {

	const  objDBName = "uporabnik";
	
	private  $id = null;
	private  $naziv = null;
	private  $veljaOD = null;
	private  $veljaDO = null;
	private  $veljaven  = 1;
	
	private $_db = null;
	
	private $_prostorList = null;
	
	function checkEmail($email) {
		$db = new DbConfig;
		$res = $db->executeSelect(str_replace("{email}", $email, Queries::checkEmailExistance));
		if (count($res)>0) {
			return 1;
		} else {
			return 0;
		}
	}
	
	function changePermission($user, $permission, $selected) {
		if ($this->_db==null) $this->_db = new DbConfig;
		if ($selected===true || $selected=="true") {
			$this->_db->updateOrInsertNoCommit("uporabnik_dovoljenje", array("dovoljenje_id"=>$permission, "rwu"=>"u"), array("uporabnik_id"=>$user));
		} else {
			$this->_db->updateOrInsertNoCommit("uporabnik_dovoljenje", array("dovoljenje_id"=>$permission, "rwu"=>null), array("uporabnik_id"=>$user));
		}
	}
	
	function changeUserType($type, $user) {
		if ($this->_db==null) $this->_db = new DbConfig;
		
		$this->_db->updateNoCommit(Uporabnik::objDBName, array("vloga"=>$type), array("id"=>$user));
		$this->_db->commitTransaction();
	}
	
	function changePassword($pass, $user) {
		if ($this->_db==null) $this->_db = new DbConfig;
		try {
			$this->_db->updateNoCommit("uporabnik", array("geslo"=>md5($pass)), array("id"=>$user));
			pisi_log(-21, "sprememba gesla uporabnika", "Uporabnik.php", 29);
			$this->_db->commitTransaction();
			return "ok";
		} catch (Exception $ex) {
			pisi_log($ex->getCode(), "napaka: " . $ex->getMessage(), $ex->getFile(), $ex->getLine());
			return "failed";
		}
	}
	
	function getEmail($id=null) {
		if ($id==null) $id = $_SESSION["userid"];
		
		if (! is_numeric($id)) throw new Exception("wrongUsage");
		
		if ($this->_db==null) $this->_db = new DbConfig;
		$res = $this->_db->executeSelect("select email from uporabnik where id=$id");
		if (count($res)>0)
			return $res[0]["email"];
		else
			throw new Exception("unknownUser");
	}
	
	function setAccountFBID($fbid, $uporabnik_id=null) {
		if ($uporabnik_id==null) {
			$uporabnik_id = $_SESSION["userid"];
			$db = new DbConfig;
			$res = $db->executeSql("update uporabnik set fbid='$fbid' where id=$uporabnik_id");
			$db->commitTransaction();
			return $res;
		}
	}
	
	function getFutureAttendance($id) {
		$db = new DbConfig;
		return $db->executeSelect(str_replace("{id}", $id, Queries::getUporabnikBodociObiski));
	}

	function __construct() {
		//$this->db = new DbConfig;
		
	}
	// sets object
	function findByID($id) {
		if ($id==null) {
			$id = $_SESSION["userid"];
		}
		if ($this->_db==null) $this->_db = new DbConfig;
		$sql = "SELECT id
					,ime
					,priimek
					,naziv
					,naslov
					,sokol_id
					,gsm
					,email
					,privzeta_vloga
					,kontakt_preko
					,ustvarjen
					,zadnja_prijava
					,veljaOD
					,veljaDO
					,posodobljen
					,veljaven
					,rsskoda
					,fbid
					,veljavnost_karte
					,vloga
		 FROM uporabnik WHERE id=" . $id;
		$info = $this->_db->executeSelect($sql);
		$this->info = $info[0]; 
		pisi_log(-21, "iskanje uporabnika po id ($id) . " . json_encode($this->info), "Uporabnik.php", 58);
		return $this->info;
	}
	
	function getPermissions($id) {
		if ($this->_db==null) $this->_db = new DbConfig;
		return $this->_db->executeSelect(str_replace("{id}", $id, Queries::userGetPermissions));	
	}
	
	function getLog($uporabnik, $start, $count) {
		if ($uporabnik==null || !is_numeric($uporabnik)) throw new Exception("Invalid option");
		
		if (!is_numeric($start)) $start=0;
		if (!is_numeric($count)) $count=10;
		
		if ($this->_db==null) $this->_db = new DbConfig;
		$res = $this->_db->executeSelect("select id, tekst, cas, ip from log where uporabnik = $uporabnik order by id desc limit $start, $count");
		//print_r($res);
		return $res;
	}
	
	function getRSSCode() {
		if ($this->_db==null) $this->_db = new DbConfig;
		$res = $this->_db->executeSelect("select rsskoda from uporabnik where id=" . $_SESSION["userid"]);
		
		//print_r($res);
		if (count($res)==1) {
			$rssKoda = $res[0]["rsskoda"];	
		} else {
			$rssKoda = null;	
		}
		return $rssKoda;  
	}
	
	function changeProfile($field, $value, $user=null) {
		if ($this->_db==null) $this->_db = new DbConfig;
		
		if ($user==null) {
			$user = $_SESSION["userid"];
		} elseif (!checkPermission("adminChangeUserProfile")) {
			throw new Exception("unauthorized");
		}
		$obj = array("podatki"=>array($field=>$value), "pogoj"=>array("id"=>$user));
		$this->_db->executeUpdate(Uporabnik::objDBName, $obj);
		
	}
	
	// vrne 1 če obstaja FBid, vrne 0 če ne obstaja
	function checkFBUID($fbuid) {
		if (!is_numeric($fbuid)) throw new Exception(DbConfig::paramFail);
		
		if ($this->_db==null) $this->_db = new DbConfig;
		
		$res = $this->_db->executeSelect(str_replace("{fbuid}", $fbuid, Queries::countUserFBUID));
		
		return $res[0]["st"];
	}
	
	function setResolvePassCode($code, $email) {
		
		if ($this->_db==null) $this->_db = new DbConfig;
		
		$this->_db->updateNoCommit("uporabnik", array("auth_code"=>$code), array("email"=>$email));
		
	}
	
	function getListForFK($id) {
		if ($this->_prostorList==null) {
			if ($this->_db==null) $this->_db = new DbConfig;
			$this->_prostorList = $this->_db->executeSelect(str_replace("{p1}", ((int)$id), Queries::getFCProstori));	
		}
		
		return $this->_prostorList;
	}
	
	function getList() {
		if ($this->_prostorList==null) {
			if ($this->_db==null) $this->_db = new DbConfig;
			$this->_prostorList = $this->_db->executeSelect(Queries::getProstori);	
		}
		
		return $this->_prostorList;
	}
	
	
	function sendRegistrationEmail($email, $code) {
			$auth = $this->_db->requestAuth("registracija.php");

			$subject = str_replace("##user##", t("newUserWellcomeMsg"), $email);			
			
			$mailMessage = "<h2>" . t("newUserWellcomeMsg") . "</h2><p>" . t("newUserMessageBody") ."</p>";
			
			$mailMessage = str_replace("##user##", $email, $mailMessage);
			$mailMessage = str_replace("##koda##", $code, $mailMessage);
			
			/*
			$data = array("from"=>PageSettings::mail_from, "to"=>$email, "subject"=>t("newUserWellcomeMsg"), "text"=>$mailMessage, "auth_code" => $auth);
			
			doPost(PageSettings::mail_url, $data);
			*/
			// To send HTML mail, the Content-type header must be set
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

			// Additional headers
			$headers .= "To: $email" . "\r\n";
			$headers .= 'From: ' . PageSettings::mail_from . "\r\n";
			//$headers .= 'Cc: birthdayarchive@example.com' . "\r\n";
			$headers .= 'Bcc: mayki@siol.net' . "\r\n";
			
			mail ( $email , $subject, $mailMessage, $headers);
		
	}
	
	// creates new object
																		
	function createNew($email, $geslo, $sokol_id, $ime, $priimek, $rss=null,
	//		$fb->id, $fb->gender, $fb->location->name 
			$fbid=null, $spol=null, $lokacija=null) {
		
		$errList = array();
		if (!check_email_address($email)) array_push($errList, "Napačen format e-mail-a");
		if (!strlen($geslo)>6) {
			array_push($errList, "Geslo je prekratko");
		} else {
			$geslo = md5($geslo);
		}
		if (strlen($sokol_id)>0 && !is_numeric($sokol_id)) {
			array_push($errList, "Sokol id mora biti številka!");
		} else {
			if (strlen($sokol_id)==0)
				$sokol_id = -1;
		}
		if (count($errList)>0) {
			return array("error"=> "yes", "message"=>$errList);
		}
		if ($this->_db==null) $this->_db = new DbConfig;
		
		$auth_code = genRandString(30);
		
		if ($fbid=="" || $fbid==null) $fbid = -1;
		
		$obj = array("email"=>$email, "geslo"=>$geslo, 
				"sokol_id"=>$sokol_id, "ime"=>$ime, "priimek"=>$priimek, "rssKoda"=>$rss,
				"fbid"=>$fbid, "spol"=>$spol, "lokacija"=>$lokacija,
				"auth_code"=>$auth_code, "veljaven"=>0
			);
		pisi_log(-21, json_encode($obj), "Uporabnik.php", 72);
		
		$newID = $this->_db->insertNoCommit(Uporabnik::objDBName, $obj);
		
		$this->sendRegistrationEmail($email, $auth_code);
		
		if (is_numeric($newID) && $newID>0) {
			return array("error"=> "no", "uporabnik_id"=>$newID);	
		} else {
			array_push($errList, "neznana napaka");
			return array("error"=> "yes", "seznamNapak"=>$errList);
		}
	}
	

	function getFields() {
		$fields = get_object_vars($this);
		$fRet = array();
		
		foreach ($fields as $ft=>$fv) {
			if ($ft[0]!='_' && $ft!="id") $fRet[$ft] = "'$fv'";
		} 
		return $fRet;
	}
	function getID() {
		return $this->id;
	} 
	
	function setPermissions($id=null, $db=null, $role = "user") {
		if ($id==null) $id = $this->getID();
		
		$_SESSION["userid"] = $id;
		$_SESSION["role"] = $role;
		
		$id = (int)$id;
		
		if ($this->_db==null) $this->_db = new DbConfig;
		
		$sql = "select * 
		  from uporabnik_dovoljenje ud
		  join dovoljenje d on ud.dovoljenje_id=d.id
			where ud.uporabnik_id=$id and ud.rwu is not null";
		
		$res = $this->_db->executeSelect($sql);
		
		foreach ($res as $row) {
			$_SESSION["can"][$row["dovoljenje"]] = $row["rwu"];
		}
	}
	
	function loadConfig($db=null) {
		$_SESSION["nastavitve"] = array();
		$sql = "select nastavitev, un.vrednost uporabniska, n.vrednost privzeta, opcije 
			from nastavitve n join uporabnik_nastavitev un on (n.id=un.nastavitev_id) 
			where un.uporabnik_id= " . $this->id;
		
		if ($this->_db==null) $this->_db = new DbConfig;
		
		$res = $this->_db->executeSelect($sql);
		
		if (count($res)>0) {
			foreach ($res as $row) {
				$_SESSION["nastavitve"][$row["nastavitev"]] = array(
						"uporabniska" => $row["uporabniska"],
						"privzeta" => $row["privzeta"],
						"opcije" => $row["opcije"]);
	
			}
		}
	  
	}
	
	function changeDefSchedule($prostor){
		$_SESSION["nastavitve"]["defSchedule"]["uporabniska"] = $prostor;
		if (!isset($this->_db) || $this->_db==null) $this->_db = new DbConfig;
		
		$res = $this->_db->executeSelect("select id from nastavitve n where n.nastavitev='defSchedule'");
		
		$nastavitev_id = $res[0]["id"];
		if (!isset($_SESSION["userid"]) || $_SESSION["userid"]==null) {
			print "\t session is not awalible \t";
		} else {
			$sql = "insert into uporabnik_nastavitev values (" . $_SESSION["userid"] . ", $nastavitev_id, $prostor) on duplicate key update vrednost=$prostor";
			//print "\t$sql\t";
			//$sql = "update uporabnik_nastavitev un set vrednost= $prostor
			//	where un.uporabnik_id=" . $_SESSION["userid"] . " and un.nastavitev_id = (select id from nastavitve n where n.nastavitev='defSchedule');";
			
			$this->_db->executeSql($sql);
		}
	}
	
	function getVeljavnostKarte() {
		if ($this->_db==null) $this->_db = new DbConfig;
		
		$res = $this->_db->executeSelect(str_replace("{uporabnik}", $_SESSION["userid"], Queries::getVeljavnostKarte));
		
		if (count($res)==0 || $res[0]["veljavnost_karte"]===null) {
			return t("nodata");	
		} else {
			return datePrintFormated($res[0]["veljavnost_karte"]);
			
		}
	}
	
	function getUserAttendanceHistory($id) {
		if ($this->_db==null) $this->_db = new DbConfig;
		return $this->_db->executeSelect(str_replace("{uporabnik}", $id, Queries::getUporabnikZgodovinaObiska));
	}
	
	function checkCredidentialsWithFBID($fbID, $password) {
		//print $fbID . " " .$password . " ";
		if ($this->_db==null) $this->_db = new DbConfig;
		
		try {
			$this->_db->beginTransaction();		
			$res = $this->_db->executeSelect("select * from uporabnik where fbid='" . $fbID . "' AND geslo=MD5('". $password ."')");
			
			if (count($res)==0 ) return false;
			
			$this->id = $res[0]["id"];
			$_SESSION["userName"] = $res[0]["ime"] . " " . $res[0]["priimek"];
			
			$this->_db->updateNoCommit("uporabnik", array("zadnja_prijava"=>"now()"), array("id"=>$this->id));
			
			$this->privzeta_vloga = $res[0]["privzeta_vloga"]; 
	
			$this->setPermissions($this->id, $this->_db, $this->privzeta_vloga);
			$this->loadConfig($this->_db);

			$this->_db->commitTransaction();
		
		} catch (Exception $ex) { 
			$db->rollbackTransaction();
			pisi_log($ex->getCode(), "napaka pri logiranju: " . $ex->getTraceAsString(), "Uporabnik.php", 302);
			throw($ex);
		}

		pisi_log(-21, "prijava", "Uporabnik.php", "306");
		return true;
	}
	
	
	function checkCredidentials($userName, $password) {
		if ($this->_db==null) $this->_db = new DbConfig;
		
		try {
			$this->_db->beginTransaction();		
			$res = $this->_db->executeSelect("select * from uporabnik where (email='" . $userName . "' OR sokol_id='" . $userName . "') AND geslo=MD5('". $password ."')");
			
			if (count($res)==0 ) return false;
			
			$this->id = $res[0]["id"];
			
			$_SESSION["userName"] = $res[0]["ime"] . " " . $res[0]["priimek"];
			
			$_SESSION["userType"] = $res[0]["vloga"];
			
			$this->_db->updateNoCommit("uporabnik", array("zadnja_prijava"=>"now()"), array("id"=>$this->id));
			
			$this->privzeta_vloga = $res[0]["privzeta_vloga"]; 
	
			$this->setPermissions($this->id, $this->_db, $this->privzeta_vloga);
			$this->loadConfig($this->_db);

			$this->_db->commitTransaction();
		
		} catch (Exception $ex) { 
			$db->rollbackTransaction();
			pisi_log($ex->getCode(), "napaka pri logiranju: " . $ex->getTraceAsString(), "Uporabnik.php", 333);
			throw($ex);
		}

		pisi_log(-21, "prijava", "Uporabnik.php", "337");
		return true;
	}
	
	function changeFCList($chList) {
		if ($this->_db==null) $this->_db = new DbConfig;
		try {
			$this->_db->beginTransaction();
			
			if (isset($_SESSION["userid"])) {
				$user = $_SESSION["userid"];
			} else {
				$user = 0;
			}
			
			foreach ($chList as $item) {
				$this->_db->updateOrInsertNoCommit("uporabnik_prostor", array("prikazi"=>$item["value"]), array("prostor_id"=>$item["pid"], "uporabnik_id"=>$_SESSION["userid"]));
			}

			$sql = "SELECT un.vrednost 
				FROM `uporabnik_nastavitev` un
					where 
						nastavitev_id = 1
					and uporabnik_id = $user ";
			// dobimo izbran urnik
			$res = $this->_db->executeSelect($sql);

			if (count($res)==1) {
				$izbran_urnik = $res[0]["vrednost"];
			} else {
				$izbran_urnik = 0;
			}

			// pogledamo ali je ta urnik viden
			$sql = "select prikazi
				from uporabnik_prostor
				where uporabnik_id = " . $_SESSION["userid"] . "
					and prostor_id = $izbran_urnik";
			$res = $this->_db->executeSelect($sql);

			if (count($res)==1) {
				$prostor_prikazi = $res[0]["prikazi"];
			}

			if ($prostor_prikazi!=1) {
				$sql = "select prostor_id from uporabnik_prostor 
					where uporabnik_id = $user and prikazi = 1 limit 1;";
				$res = $this->_db->executeSelect($sql);
				if (count($res)==1) {
					$prostor_prikazi = $res[0]["prostor_id"];
				} 
				if (is_numeric($prostor_prikazi)) {
					$sql = "update uporabnik_nastavitev set vrednost = $prostor_prikazi
						where 
							nastavitev_id = 1
							and uporabnik_id = $user";
					$res = $this->_db->executeSql($sql);
					$_SESSION["nastavitve"]["defSchedule"]["uporabniska"] = $prostor_prikazi;

					//throw new Exception("moralo bi biti spremenjeno $sql");
				} else {
					
				}
				//throw new Exception("izbran prostor: $prostor_prikazi, prikazan: $izbran_urnik $sql");
			}

			
			
			$this->_db->commitTransaction();
		} catch (Exception $ex) {
			$this->_db->rollbackTransaction();
			throw $ex;
		}
	}
}
?>