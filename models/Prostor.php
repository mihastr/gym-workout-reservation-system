<?php
class Prostor extends MainModel {

	const  objDBName = "prostor";
	
	private  $id = null;
	private  $naziv = null;
	private  $veljaOD = null;
	private  $veljaDO = null;
	private  $veljaven  = 1;
	
	//private $db = null;
	
	private $_dbConf = null;
	
	private $_prostorList = null;

	function __construct() {
		//$this->db = new DbConfig;
		$this->_dbConf = new DbConfig;
	}
	// sets object
	function findByID($id) {
		//$this->db = new DbConfig;
	}
	
	function getListVSI() {
		$db = new DbConfig;
		$res = $db->executeSelect(Queries::getProstoriVSI);
		return $res;
	}
	
	function getListForFK($id) {
		if ($this->_prostorList==null) {
			$db = new DbConfig;
			
			$this->_prostorList = $db->executeSelect(str_replace("{p1}", ((int)$id), Queries::getFCProstori));
		}
		
		return $this->_prostorList;
	}
	
	function getListForUser($uid) {
		//print "uporabnik: " . $uid;
		if ($uid==null) throw new Exception("param fault");
		if ($this->_dbConf == null) $this->_dbConf = new DbConfig;
		
		//print str_replace("{uporabnik}", $uid, Queries::getFCsForUser);
		
		return $this->_dbConf->executeSelect(str_replace("{uporabnik}", $uid, Queries::getFCsForUser));
	}
	
	function getList() {
		if ($this->_prostorList==null) {
			$db = new DbConfig;
			$this->_prostorList = $db->executeSelect(Queries::getProstori);	
		}
		
		return $this->_prostorList;
	}
	
	function getListWithClassesCapacity($vadba) {
		$db = new DbConfig;
		$sql = str_replace("{vadba}", (int)$vadba, Queries::getProstoriVadbeOmejitve);
		//print $sql;
		return $db->executeSelect($sql);
	}
	
	// creates new object
	function createNew($naziv, $veljaOD, $veljaDO, $veljaven) {
		/*
		if ($naziv==null || $veljaOD==null || $veljaDO==null || $veljaven==null) throw new Exception("vsa polja so obvezna");
		$db = new DbConfig;
		try {
			$this->naziv = $naziv;
			$this->veljaOD = $veljaOD;
			$this->veljaDO = $veljaDO;
			$this->veljaven = $veljaven;
			
			// dobimo vstavljeno vstico nazaj iz baze
			$dbRow = $db->insertObject(Urnik::objDBName, $this);
			$this->id = $dbRow[0]["id"];
			return $dbRow;
		} catch (Exception $ex) {
			throw new Exception("napaka pri ustvarjanju objekta: " . $ex->getMessage());			
		}
		*/
	}
	
	function update($params) {
		/*
		$db = new DbConfig;
		
		if (isset($params["bulk"]) && $params["bulk"])
			$db->executeBulkUpdate(Urnik::objDBName, $params["data"]);
		else
			$db->executeUpdate(Urnik::objDBName, $params["data"]);
			*/
	}
	
	function remove($id) {
		if ($this->_dbConf==null) $this->_dbConf = new DbConfig;
		
		if (!is_numeric($id)) throw new Exception(DbConfig::paramFail);
		
			if ($this->_dbConf->count("urnik", array("prostor_id"=>$id))==0) {
				$this->_dbConf->updateNoCommit(Prostor::objDBName, array("veljaDO" => "now()"), array("id"=>$id), $this->_dbConf->getConnection());		
			} else {
				throw new Exception(DbConfig::fkFail);
			}	
		
		return "ok";
		
	}
	

	function getFields() {
		$fields = get_object_vars($this);
		$fRet = array();
		
		foreach ($fields as $ft=>$fv) {
			if ($ft[0]!='_' && $ft!="id") $fRet[$ft] = "'$fv'";
		} 
		return $fRet;
	}
	function getID() {
		return $this->id;
	} 
	function updateCapacityData($kapacitete, $db=null) {
		$isTransaction = true;
		if ($db==null) {
			$db = new DbConfig;
			$db->beginTransaction();
			$isTransaction = false;
		}
		
		if (is_array($kapacitete))
		foreach ($kapacitete as $vnos) {
			$db->updateNoCommit("prostor", array("kapaciteta"=>$vnos["kapaciteta"]), array("id"=>$vnos["prostor"]));			
		}
		if (!$isTransaction) $db->commitTransaction();
	}
}
?>