<?php
include_once 'mainModel.php';

class Vreme extends MainModel {

	const  objDBName = "vreme";

	
	private $db = null;
	
	private $_urnikList = null;

	function __construct() {
		$this->db = new DbConfig;
	}
	/*
	// sets object
	function findByID($id) {
	}
	*/
	
	function getVreme($datum, $ura) {
		if ($this->db==null) $db=new DbConfig;
		$sql = str_replace("{ura}", $ura, str_replace("{datum}", $datum, Queries::getVremeForTime));
		//print $sql;
		return $this->db->executeSelect($sql);
	}
	
	function getVremeTrenutno() {
		if ($this->db==null) $db=new DbConfig;
		$sql = Queries::getVreme;
		//print $sql;
		return $this->db->executeSelect($sql);
	}
	
	function getFields() {
		$fields = get_object_vars($this);
		$fRet = array();
		
		foreach ($fields as $ft=>$fv) {
			if ($ft[0]!='_' && $ft!="id") $fRet[$ft] = "'$fv'";
		} 
		return $fRet;
	}
	function getID() {
		return $this->id;
	} 
}
?>