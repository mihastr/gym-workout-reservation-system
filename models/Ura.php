<?php
class Ura extends MainModel {

	const  objDBName = "ura";
	
	private  $id = null;
	private  $naziv = null;
	private  $ura = null;
	private  $dan = null;
	private  $omejitev = null;
	private  $veljaOD = null;
	private  $veljaDO = null;
	private  $veljaven  = 1;
	
	//private $db = null;
	
	private $_ureList = null;

	function __construct() {
		//$this->db = new DbConfig;
	}
	// sets object
	function findByID($id) {
		$db = new DbConfig;
		$res = $db->executeSelect("select * from ura where id = " . (int)$id);
		if (count($res)==1) {
			return $res[0];
		} else 
		return null;
	}
	
	function getListByUrnikID($id) {
		if ($this->_ureList==null) {
			$db = new DbConfig;
			$this->_ureList = $db->executeSelect(str_replace("{p1}", $id, Queries::getUreByUrnikID));	
		}
		
		return $this->_ureList;
	}
	
	function updateRecords($records, $urnikID) {
		$db = new DbConfig;
		
		if (count($records)>0) {
			try {
				$db->beginTransaction();
				foreach ($records as $record) {
					if ($record["objid"]=="-1") {
						// nova ura
						$this->createNew($record["dan"], $record["ura"], $record["instruktor"], $record["vadba"], $urnikID);
					} else {
						// posodabljanje ure
						$this->updateForID($record["dan"], $record["ura"], $record["instruktor"], $record["vadba"], $urnikID, $record["objid"]);
					}
				}		
				$db->commitTransaction();
			} catch (Exception $ex) {
				$db->rollbackTransaction();
				throw $ex;
			}
			
		}
	}
	
	function getDayName($no) {
		return t(PageSettings::$dnevi[$no-1]);
	}
	
	// update record
	function updateForID($dan, $ura, $instruktor, $vadba, $urnik_id, $id) {
		$db = new DbConfig;
		$conn = $db->getConnection();
		$in = 1;
		try {
			// veljavna ura
			$condition = array("id"=>$id, "urnik_id" => $urnik_id);

			$star = $this->findByID($id);

			//pisi_log(-21, json_encode($star), "Ura.php", 76);
			if ($star["VADBA_id"] == $vadba && $star["INSTRUKTOR_id"] == $instruktor) {
				// zapis se ni čiso nič spremenil
				return $id;
			} else {
				// naredimo nov zapis
				
				$newId = $this->createNew($dan, $ura, $instruktor, $vadba, $urnik_id, $id);
	
				$uporabniki = $db->executeSelect("select uporabnik_id from udelezba u where u.ura_id = $id and datum >= now()");

				// preusmeri uporabnike na nove ure
				$db->executeSql("update udelezba set posodobljen=now(), ura_id = $newId where ura_id = $id and datum>=now()");
				
				// obvesti uporabnike
				// TODO: za narest do konca
				// sestavi besedilo:
				
				$naslov = t("notifTitle"); // Obvestilo o spremembi urnika!

				$besedilo = t("notifScheduleChange"); 
				
				$old = array(
					"##old_class##", 
					"##old_instructor##", 
					"##day##", 
					"##time##", 
					"##new_class##",
					"##new_instructor##");
				
				$vadbaObject = new Vadba;
				$instruktorObject = new Instruktor;
				
				//echo "allala"; 
				
				$new = array(
					$vadbaObject->getNameByID($star["VADBA_id"]), 
					$instruktorObject->getNameByID($star["INSTRUKTOR_id"]), 
					$this->getDayName($star["dan"]), 
					$star["ura"], 
					$vadbaObject->getNameByID($vadba), 
					$instruktorObject->getNameByID($instruktor));
				
				//echo "allala"; 
				
				$besedilo = str_replace( $old, $new, $besedilo );
				// Spoštovani obiskovalec! 
				// Vodena vadba ##old_class##, ki jo vodi ##old_instrucotr## za termin ##day## ##time## je bila spremenjena.
				// Sedaj je na urniku v istem terminu ##new_class, vodi pa jo ##new_instructor##.
				// Za razumevanje se vam zahvaljujemo in vas lepo pozdravljamo
				// Sokol Team
				
				
				
				$obvestiloID = $db->insertNoCommit("obvestilo", array(
						"naslov" => $naslov, 
						"tekst" => $besedilo, 
						"ustvarjen" => "now()", 
						"od" => $_SESSION["userid"], 
						"za" => "uporabnik", 
						"prikazan" => "da", 
						"veljaOD" => "now()"));
				
				//echo "pred uporabniki ";
				
				foreach ($uporabniki as $u) {
					$db->insertNoCommit("obvestilo_uporabnik", array("obvestilo_id" => $obvestiloID, "uporabnik_id"=>$u["uporabnik_id"]));
				}
				//echo "za uporabniki ";
				
				// zaključimo starega
				$db->updateNoCommit(Ura::objDBName, array("veljaven_do"=>"now()"), array("id"=>$id));
				
				return $newId;								
			}

		} catch (Exception $ex) {
			throw new Exception("napaka pri ustvarjanju objekta: " . $ex->getMessage());			
		}
	}
	
	
	// creates new record
	function createNew($dan, $ura, $instruktor, $vadba, $urnik, $oce=null) {
		$db = new DbConfig;
		$conn = $db->getConnection();
		try {
			$record = array();
			$record["dan"] = $dan;
			$record["ura"] = $ura;
			$record["vadba_id"] = $vadba;
			$record["urnik_id"] = $urnik;
			$record["instruktor_id"] = $instruktor;
			$record["parent_id"] = ($oce!=null ? $oce : -1);
			
			// dobimo vstavljeno vstico nazaj iz baze
			return $db->insertNoCommit(Ura::objDBName, $record, $conn);
		} catch (Exception $ex) {
			throw new Exception("napaka pri ustvarjanju objekta: " . $ex->getMessage());			
		}
	}
	
	function update($params) {
		$db = new DbConfig;
		
		if (isset($params["bulk"]) && $params["bulk"])
			$db->executeBulkUpdate(Urnik::objDBName, $params["data"]);
		else
			$db->executeUpdate(Urnik::objDBName, $params["data"]);
	}
	

	function getFields() {
		$fields = get_object_vars($this);
		$fRet = array();
		
		foreach ($fields as $ft=>$fv) {
			if ($ft[0]!='_' && $ft!="id") $fRet[$ft] = "'$fv'";
		} 
		return $fRet;
	}
	function getID() {
		return $this->id;
	} 
}
?>