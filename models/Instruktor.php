<?php
class Instruktor extends MainModel {

	const  objDBName = "instruktor";
	
	private  $id = null;
	private  $dolgoIme = null;
	private  $kratkoIme = null;
	private  $veljaOD = null;
	private  $veljaDO = null;
	private  $veljaven  = 1;
	
	//private $db = null;
	
	private $_urnikList = null;

	function __construct() {
		//$this->db = new DbConfig;
	}
	// sets object
	function findByID($id) {
		//$this->db = new DbConfig;
	}
	
	function findByParams($params) {
		$db = new DbConfig;
		foreach ($params as $f=>$v) {
			$db->addAND($f, $v);
		}
		$sql = str_replace("{filter}", $db->getFilterString(), Queries::getInstruktorjiFiltered);
		return $db->executeSelect($sql);
	}
	function findByFilter($params) {
		$db = new DbConfig;
		$sql = str_replace("{filter}", " AND " . $params, Queries::getInstruktorjiFiltered);
		return $db->executeSelect($sql);
	}
	
	
	function getList($params=null) {
		if ($this->_urnikList==null) {
			$db = new DbConfig;
			if ($params!=null && isset($params["veljaven"])) {
				$veljaven = $params["veljaven"];
			} else {
				$veljaven = "1";
			}
			if (isset($params["showAll"]) && $params["showAll"]=="1") {
				$this->_urnikList = $db->executeSelect(Queries::getInstruktorjiAdmin);
			} else {
				$this->_urnikList = $db->executeSelect(str_replace("{veljaven}", $veljaven, Queries::getInstruktorji));
			}	
		}
		
		return $this->_urnikList;
	}
	
	// creates new object
	function createNew($naziv, $veljaOD, $veljaDO, $veljaven, $trWise=false) {
		throw new Exception("unimplemented stuff");
	}
	
	function removeAll($arrayOfObjects) {
		if (!(is_array($arrayOfObjects) && count($arrayOfObjects)>0)) return;
		
		
		$updParams["bulk"] = true;
		
		$updParams["data"] = array();
		foreach ($arrayOfObjects as $obj) {
			array_push($updParams["data"], array("pogoj"=>$obj, "podatki"=> array("veljaDO" => "now()")));
		}
		//pisi_log(-21, "beleženje vhoda " . json_encode($updParams), "Instruktor.php", 48);
		$this->update($updParams);
	}
	
	function createBulk($arrayOfObjects) {
		if (!(is_array($arrayOfObjects) && count($arrayOfObjects)>0)) return;
		$db = new DbConfig;
		try {
			$db->beginTransaction();
			foreach ($arrayOfObjects as $record) {
				$record["ustvarjen"] = "now()"; 
				$db->insertNoCommit(Instruktor::objDBName, $record);
			}
			$db->commitTransaction();
		} catch (Exception $ex) {
			$db->rollbackTransaction();
			throw $ex;
		}
		
		
	}
	
	
	function create($ime, $kratkoIme, $opis) {
		if (!isset($this->_db)) $this->_db = new DbConfig;
		
		return $this->_db->insertNoCommit(Instruktor::objDBName, array("dolgoIme" => $ime, "kratkoIme" => $kratkoIme, "opis" => $opis));
	}
	
	function updateOne($id, $ime, $kratkoIme, $opis) {
		if (!isset($this->_db)) $this->_db = new DbConfig;
		
		return $this->_db->updateNoCommit(Instruktor::objDBName, array("dolgoIme" => $ime, "kratkoIme" => $kratkoIme, "opis" => $opis), array("id" => $id));
	}
	
	function remove($id) {
		if (!isset($this->_db)) $this->_db = new DbConfig;
		
		if ($this->_db->count("ura", array("INSTRUKTOR_id"=>$id))==0) {
			$this->_db->updateNoCommit(Instruktor::objDBName, array("veljaDO" => "now()"), array("id" => $id));		
		} else {
			throw new Exception(DbConfig::fkFail);
		}	
		
		
		
		return "ok";
	}
	
	function update($params) {
		$db = new DbConfig;
		
		if (isset($params["bulk"]) && $params["bulk"])
			$db->executeBulkUpdate(Instruktor::objDBName, $params["data"]);
		else
			$db->executeUpdate(Instruktor::objDBName, $params["data"]);
	}
	
	function getNameByID($id) {
		if (!is_numeric($id)) $id = 0;
		$db = new DbConfig;
		$sql = "select dolgoIme from instruktor where id=$id";
		//echo $sql;
		$res = $db->executeSelect($sql);
		if (count($res)==1) {
			return $res[0]["dolgoIme"];
		} else {
			return "";
		}
	}
	

	function getFields() {
		$fields = get_object_vars($this);
		$fRet = array();
		
		foreach ($fields as $ft=>$fv) {
			if ($ft[0]!='_' && $ft!="id") $fRet[$ft] = "'$fv'";
		} 
		return $fRet;
	}
	function getID() {
		return $this->id;
	} 
}
?>