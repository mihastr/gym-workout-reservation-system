<?php
class Urnik7D extends MainModel {

	const  objDBName = null;
	
	
	private $_ureList = null;
	
	private $_db = null;

	function __construct() {
		$this->_db = new DbConfig;
	}
	// sets object
	function findByID($id) {
		if (is_numeric($id)) {
			$ret = $this->_db->executeSelect("select * from urnik where id=" . $id);
			$res = $ret[0];
		} else { 
			$res = null;
		}
		return $res;		
	}
	
	function getClassByID($id) {
		if (is_numeric($id)) {
			$ret = $this->_db->executeSelect("select * from ura join vadba v on v.id=ura.VADBA_id where ura.id=" . $id);
			$res = $ret[0];
		} else { 
			$res = null;
		}
		return $res;
	}
	
	function getMinMaxDatumUdelezbe() {
		$res = $this->_db->executeSelect(Queries::getMinMaxDatumUdelezbe);
		return $res[0];
	}
	
	function getOsnovnaStatistika($od, $do, $prostor) {
		return $this->_db->executeSelect(str_replace("{prostor}", $prostor, str_replace("{od}", $od, str_replace("{do}", $do, Queries::getOsnovnaStatistikaOdDo))));
	}
	
	function getList($uporabnik, $prostor, $admin=false) {
		//print "...$prostor...";
		if ($this->_ureList==null) {

			if ($uporabnik != null) {
				if ($admin) {
					$sql = str_replace("{prostor}", $prostor , str_replace("{uporabnik}", $uporabnik, Queries::get7DUrnikDetail));
				} else {
					$sql = str_replace("{prostor}", $prostor , str_replace("{uporabnik}", $uporabnik, Queries::get7DUrnik));
				}
			} else {
				$sql = str_replace("{prostor}", $prostor , Queries::get7DUrnikBasic);
			}
			//print "...$sql...";
			//pisi_log(-21, $sql, "Urnik7D.php", 21);
			$res = $this->_db->executeSelect($sql);
			//print $res;
			$this->_ureList = array();
			if (count($res)>0) {
				$prev = null;
				$danDanes = date("w", time());
				
				foreach ($res as $row) {
					// inicializacija
					if ($prev==null) {
						$prev = $row;
						$prev["dan"] = -1; 
					}
					// vnosi urejeni po prioriteti, tako da če obstaja zapis ki se dogaja isti dan ob isti uri ga ne prikažemo
					//print $row["veljaDO"] . " " . $row["dan"] . " ";
					
					if ($row["dan"]<$danDanes) {
						$odmik = 7-$danDanes + $row["dan"];	
					} else {
						$odmik = $row["dan"] - $danDanes;
					}
					$danVadbe = time()+($odmik - 1)*24*60*60;
					
					$urnikVeljaDo = strtotime($row["veljaDO"]);
					/*
					print date("r", $urnikVeljaDo);
					print date("r", $danVadbe);
					print ($danVadbe > $urnikVeljaDo ? "nov" : "star") .  " \n";
					*/
					if (($prev["dan"]!=$row["dan"] || $prev["ura"]!=$row["ura"]) && ($danVadbe <= $urnikVeljaDo))
						array_push($this->_ureList, $row);
					$prev = $row;
				} 					
			}
		}
		
		return $this->_ureList;
	}
	
	// creates new object

	function removeUdelezba($ura_id, $uporabnik_id, $datum) {

		$objekt = array("podatki" => array("`status`"=>"odjavljen"), "pogoj" => array("ura_id"=>$ura_id, "uporabnik_id"=>$uporabnik_id, "datum"=>$datum));

		return $this->_db->executeUpdate("udelezba", $objekt, false, null, true);
	} 
	
	function addUdelezba($ura_id, $uporabnik_id, $datum) {
		
		$sql = "INSERT INTO udelezba (uporabnik_id, ura_id, ustvarjen, `status`, datum) VALUES ('$uporabnik_id', '$ura_id', now(), 'prijavljen', '$datum')
  			ON DUPLICATE KEY UPDATE `status`='prijavljen'";
		
		$res = $this->_db->executeSql($sql);
		if (!$res) throw new Exception("napaka");
		else return "ok";
	} 
	
	function cancelClass($uraid, $datum, $obvesti, $razlog) {
		// odpoved ure
		
		try {
			$this->_db->insertNoCommit("odpovedana_ura", array("ura_id"=>$uraid, "datum"=>$datum, "razlog"=>$razlog, "posiljanje_obvestila"=>$obvesti, "uporabnik_id"=>$_SESSION["userid"]));
		} catch (Exception $ex) {
			throw $ex;
		}
	}
	

	function getFields() {
		$fields = get_object_vars($this);
		$fRet = array();
		
		foreach ($fields as $ft=>$fv) {
			if ($ft[0]!='_' && $ft!="id") $fRet[$ft] = "'$fv'";
		} 
		return $fRet;
	}
	function getID() {
		return $this->id;
	} 
}
?>