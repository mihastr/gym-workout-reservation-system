<?php
include_once 'mainModel.php';

class VremeNapoved extends MainModel {

	const  objDBName = "vreme_napoved";

	const napoved5dni = 5;
	const napoved3dni = 3;
	
	private $db = null;
	
	private $_urnikList = null;

	function __construct() {
		$this->db = new DbConfig;
	}
	/*
	// sets object
	function findByID($id) {
	}
	*/
	
	function getNapovedi($datum, $dni) {
		if ($this->db==null) $db=new DbConfig;
		
		return $this->db->executeSelect(str_replace("{n_dni}", $dni, str_replace("{datum}", $datum, Queries::getNapovediForTime)));
	}
	
	function getFields() {
		$fields = get_object_vars($this);
		$fRet = array();
		
		foreach ($fields as $ft=>$fv) {
			if ($ft[0]!='_' && $ft!="id") $fRet[$ft] = "'$fv'";
		} 
		return $fRet;
	}
	function getID() {
		return $this->id;
	} 
}
?>