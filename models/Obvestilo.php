<?php
class Obvestilo extends MainModel {

	const  objDBName = "obvestilo";
	
	const objNapakaDBName = "napaka";
	
	function reportBug($info, $priority, $text) {
		$db = new DbConfig;
		if (isset($_SESSION["userid"])) {
			$user = $_SESSION["userid"];
		 } else {
			$user = 0;
		 }
		$db->insertNoCommit(Obvestilo::objNapakaDBName, array("uporabnik_id"=> $user, "modul"=>"index", "nivo"=>$info, "prioriteta"=>$priority, "opis_napake"=>$text));
		return "ok";
	}
	
	function listBugs($showResolved=false) {
		if ($showResolved) {
			$sql = "select n.*, email  from napaka n left outer join uporabnik u on (n.uporabnik_id = u.id)";
		} else {
			$sql = "select n.*, email  from napaka n left outer join uporabnik u on (n.uporabnik_id = u.id) where not status = 'resena'";
		}
		$db = new DbConfig;
		return $db->executeSelect($sql);
	}
	
	function alterBugReport($id, $status=null, $response=null) {
		
		if ($status==null && $response==null) throw new Exception("argFail");
		
		$db = new DbConfig;
		$row = array();
		if ($status!=null) {
			$row["status"] = $status;
		}
		if ($response!=null) {
			$row["odgovor"] = $response;
		}
		if (trim($status)=="resena") {
			$row["cas_odprave"] = "now()";
		}
		$db->updateNoCommit(Obvestilo::objNapakaDBName, $row, array("id"=>$id));
	}
	
	private  $id = null;
	
	//private $db = null;
	
	private $_obvestiloList = null;

	function __construct() {
		//$this->db = new DbConfig;
	}
	// sets object
	function findByID($id) {
		//$this->db = new DbConfig;
	}
	
	function getMsgRecepients($id) {
		$db = new DbConfig;
		$sql = str_replace("{id}", $id, Queries::getMsgRecepients);
		//print $sql;
		return $db->executeSelect($sql);
	}
	
	
	function getListForUser() {
		if ($this->_obvestiloList==null) {
			$db = new DbConfig;
			if (isset($_SESSION["userid"])) {
				$user = $_SESSION["userid"];
			} else {
				$user = "NULL";
			}
			if ($user)
			$this->_obvestiloList = $db->executeSelect(str_replace("{uporabnik}", $user, Queries::getObvestila));	
		}
		
		return $this->_obvestiloList;
	}
	
	function delete($id) {
		$db = new DbConfig;
		$db->updateNoCommit(Obvestilo::objDBName, array("brisan"=>"1"), array("id"=>$id));
	}
	
	// creates new object
	function createNew($naslov, $tekst, $od, $prikazan, $veljaod, $veljado, $prejemniki=null) {
		$db = new DbConfig;
		try {
			$this->naslov = $naslov;
			$this->tekst = $tekst;
			$this->od = $od;
			$this->za = ($prejemniki==null ? "vsi" : "uporabnik");
			$this->prikazan = $prikazan;
			$this->veljaod = $veljaod;
			$this->veljado = $veljado;
			
			$this->ustvarjen = "now()";
			
			// dobimo vstavljeno vstico nazaj iz baze
			$db->beginTransaction();
			$dbRow = $db->insertObject(Obvestilo::objDBName, $this, false);
			$this->id = (int)$dbRow[0]["id"];
			
			if ($prejemniki!=null && is_array($prejemniki))
			foreach ($prejemniki as $prejemnik) {
				$db->insertNoCommit("obvestilo_uporabnik", array("obvestilo_id" => $this->id, "uporabnik_id" => (int)$prejemnik));
			}
			
			$db->commitTransaction();
			
			return $dbRow;
		} catch (Exception $ex) {
			$db->rollbackTransaction();
			throw new Exception("napaka pri ustvarjanju objekta: " . $ex->getMessage());			
		}
	}
	
	function changeVisibility($id, $visible) {
		$db = new DbConfig;
		try {
			$db->beginTransaction();
			$db->updateNoCommit(Obvestilo::objDBName, array("prikazan"=>$visible), array("id"=>$id));
			$db->commitTransaction();
			return "ok";
		} catch (Exception $e) {
			return "napaka: " . $e->getMessage();
		}
		
	}
	
	function update($params) {
		/*
		$db = new DbConfig;
		
		if (isset($params["bulk"]) && $params["bulk"])
			$db->executeBulkUpdate(Urnik::objDBName, $params["data"]);
		else
			$db->executeUpdate(Urnik::objDBName, $params["data"]);
			*/
	}
	
	function getMsgs4Admin($drafts, $deleted, $q) {
		
		$filter = "";
		if ($q!=null) {
			$filter = " AND (o.naslov like '%$q%' OR o.tekst like '%$q%')";
		}

		/*
		SELECT o.*, concat(u.ime, ' ', u.priimek) posiljatelj FROM obvestilo o
	  join uporabnik u on (o.od=u.id)
		WHERE (brisan is null or not brisan='1') and  now() BETWEEN o.veljaOD AND o.veljaDO AND prikazan=\"da\" AND (o.od={uporabnik} or o.za=\"vsi\" or o.id IN 
		(SELECT obvestilo_id 
		 FROM obvestilo_uporabnik ou 
		 WHERE ou.uporabnik_id={uporabnik}))*/
		//print " $drafts $deleted ";
		if ($drafts==0 && $deleted==0) {
			$sql = "SELECT o.id, o.naslov, o.tekst, 
			date_format(o.ustvarjen, \"" . PageSettings::mysqlDateFormat . "\") ustvarjen, 
			date_format(o.posodobljen, \"" . PageSettings::mysqlDateFormat . "\") posodobljen, 
			date_format(o.veljaOD, '" . PageSettings::mysqlDateFormat . "') veljaOD,  
			date_format(o.veljaDO, '" . PageSettings::mysqlDateFormat . "') veljaDO, 
			o.od, o.za, o.prikazan, concat(u.ime, ' ', u.priimek) posiljatelj, brisan
			FROM obvestilo o join uporabnik u on (o.od=u.id)
			WHERE (o.brisan='0' or o.brisan is null) AND (o.prikazan is null or o.prikazan='da') $filter";	
		} elseif ($drafts==0 && $deleted==1) {
			$sql = "SELECT o.id, o.naslov, o.tekst, 
			date_format(o.ustvarjen, \"" . PageSettings::mysqlDateFormat . "\") ustvarjen, 
			date_format(o.posodobljen, \"" . PageSettings::mysqlDateFormat . "\") posodobljen, 
			date_format(o.veljaOD, '" . PageSettings::mysqlDateFormat . "') veljaOD,  
			date_format(o.veljaDO, '" . PageSettings::mysqlDateFormat . "') veljaDO, 
			o.od, o.za, o.prikazan, concat(u.ime, ' ', u.priimek) posiljatelj, brisan			 
			FROM obvestilo o join uporabnik u on (o.od=u.id)
			WHERE (o.prikazan is null or o.prikazan='da') $filter";			
		} elseif ($drafts==1 && $deleted==0) {
			$sql = "SELECT o.id, o.naslov, o.tekst, 
			date_format(o.ustvarjen, \"" . PageSettings::mysqlDateFormat . "\") ustvarjen, 
			date_format(o.posodobljen, \"" . PageSettings::mysqlDateFormat . "\") posodobljen, 
			date_format(o.veljaOD, '" . PageSettings::mysqlDateFormat . "') veljaOD,  
			date_format(o.veljaDO, '" . PageSettings::mysqlDateFormat . "') veljaDO, 
			o.od, o.za, o.prikazan, concat(u.ime, ' ', u.priimek) posiljatelj, brisan
			FROM obvestilo o join uporabnik u on (o.od=u.id)
			WHERE (o.brisan='0' or o.brisan is null) $filter";	
		} else {
			$sql = "SELECT o.id, o.naslov, o.tekst, 
			date_format(o.ustvarjen, \"" . PageSettings::mysqlDateFormat . "\") ustvarjen, 
			date_format(o.posodobljen, \"" . PageSettings::mysqlDateFormat . "\") posodobljen, 
			date_format(o.veljaOD, '" . PageSettings::mysqlDateFormat . "') veljaOD,  
			date_format(o.veljaDO, '" . PageSettings::mysqlDateFormat . "') veljaDO, 
			o.od, o.za, o.prikazan, concat(u.ime, ' ', u.priimek) posiljatelj, brisan 
			FROM obvestilo o join uporabnik u on (o.od=u.id)" . (strlen($filter)>0 ? " 
			WHERE 1=1 $filter" : "");
		}
		$sql .= " ORDER BY id DESC";
		$db = new DbConfig;
		//print " " . $sql . " ";
		return $db->executeSelect($sql);
	}
	

	function getFields() {
		$fields = get_object_vars($this);
		$fRet = array();
		
		foreach ($fields as $ft=>$fv) {
			if ($ft[0]!='_' && $ft!="id") $fRet[$ft] = "'$fv'";
		} 
		return $fRet;
	}
	function getID() {
		return $this->id;
	} 
}
?>