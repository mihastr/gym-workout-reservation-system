<?php
class Vadba extends MainModel {

	const  objDBName = "vadba";
	
	private  $id = null;
	private  $naziv = null;
	private  $kratkoIme = null;
	private  $veljaOD = null;
	private  $veljaDO = null;
	private  $veljaven  = 1;
	
	//private $db = null;
	
	private $_vadbaList = null;
	
	private $_db = null;

	function __construct() {
		$this->_db = new DbConfig;
	}
	// sets object
	function findByID($id) {
		//$this->db = new DbConfig;
	}
	
	function getNameByID($id) {
		if (!is_numeric($id)) $id = 0;
		$db = new DbConfig;
		$sql = "select naziv from vadba where id=$id";
		$res = $db->executeSelect($sql);
		//echo $sql;
		if (count($res)==1) {
			return $res[0]["naziv"];
		} else {
			return "";
		}
	}
	
	function create($ime, $kratkoIme, $opis, $link, $kalorij, $logo) {
		if (!isset($this->_db)) $this->_db = new DbConfig;
		
		return $this->_db->insertNoCommit(Vadba::objDBName, array("naziv" => $ime, "kratkoIme" => $kratkoIme, "opis" => $opis, "link"=>$link, "kalorij"=>$kalorij, "logo" => $logo));
	}
	
	function updateOne($id, $ime, $kratkoIme, $opis, $link, $kalorij, $logo=null) {
		if (!isset($this->_db)) $this->_db = new DbConfig;
		if ($logo==null) {
			return $this->_db->updateNoCommit(Vadba::objDBName, array("naziv" => $ime, "kratkoIme" => $kratkoIme, "opis" => $opis, "link"=>$link, "kalorij"=>$kalorij), array("id" => $id));	
		} else {
			return $this->_db->updateNoCommit(Vadba::objDBName, array("naziv" => $ime, "kratkoIme" => $kratkoIme, "opis" => $opis, "link"=>$link, "kalorij"=>$kalorij, "logo" => $logo), array("id" => $id));
		}
		
	}
	
	function remove($id) {
		if (!isset($this->_db)) $this->_db = new DbConfig;
		
		if ($this->_db->count("ura", array("VADBA_id"=>$id))==0) {
			$this->_db->updateNoCommit(Vadba::objDBName, array("veljaDO" => "now()"), array("id" => $id));		
		} else {
			throw new Exception(DbConfig::fkFail);
		}	
		
		
		
		return "ok";
	}
	
	function getList($filter) {
		if ($this->_vadbaList==null) {
			$db = new DbConfig;
			
			if (isset($filter["veljaven"])) {
				$db->addAnd("veljaven", $filter["veljaven"]);
			}

			$this->_vadbaList = $db->executeSelect(str_replace("{filter}", $db->getFilterString(), Queries::getVadbe));	
		}
		
		return $this->_vadbaList;
	}
	
	// creates new object
	function createNew($naziv, $veljaOD, $veljaDO, $veljaven) {
		/*
		if ($naziv==null || $veljaOD==null || $veljaDO==null || $veljaven==null) throw new Exception("vsa polja so obvezna");
		$db = new DbConfig;
		try {
			$this->naziv = $naziv;
			$this->veljaOD = $veljaOD;
			$this->veljaDO = $veljaDO;
			$this->veljaven = $veljaven;
			
			// dobimo vstavljeno vstico nazaj iz baze
			$dbRow = $db->insertObject(Urnik::objDBName, $this);
			$this->id = $dbRow[0]["id"];
			return $dbRow;
		} catch (Exception $ex) {
			throw new Exception("napaka pri ustvarjanju objekta: " . $ex->getMessage());			
		}
		*/
	}
	
	
	function createBulk($arrayOfObjects) {
		if (!(is_array($arrayOfObjects) && count($arrayOfObjects)>0)) return;
		$db = new DbConfig;
		try {
			$db->beginTransaction();
			foreach ($arrayOfObjects as $record) {
				$record["ustvarjen"] = "now()"; 
				$db->insertNoCommit(Vadba::objDBName, $record);
			}
			$db->commitTransaction();
		} catch (Exception $ex) {
			$db->rollbackTransaction();
			throw $ex;
		}
		
		
	}
	
	
	function removeAll($arrayOfObjects) {
		if (!(is_array($arrayOfObjects) && count($arrayOfObjects)>0)) return;
		
		
		$updParams["bulk"] = true;
		
		$updParams["data"] = array();
		if (count($arrayOfObjects)>0) {
			foreach ($arrayOfObjects as $obj) {
				array_push($updParams["data"], array("pogoj"=>$obj, "podatki"=> array("veljaDO" => "now()")));
			}
			//pisi_log(-21, "beleženje vhoda " . json_encode($updParams), "Instruktor.php", 48);
			$this->update($updParams);
		}
	}
	
	function update($params) {
		$db = new DbConfig;
		
		if (isset($params["bulk"]) && $params["bulk"])
			$db->executeBulkUpdate(Vadba::objDBName, $params["data"]);
		else
			$db->executeUpdate(Vadba::objDBName, $params["data"]);
	}
	

	function getFields() {
		$fields = get_object_vars($this);
		$fRet = array();
		
		foreach ($fields as $ft=>$fv) {
			if ($ft[0]!='_' && $ft!="id") $fRet[$ft] = "'$fv'";
		} 
		return $fRet;
	}
	function getID() {
		return $this->id;
	} 
	
	function removeClassLogo($id) {
		$db = new DbConfig;
		$db->updateNoCommit("vadba", array("logo"=>null), array("id"=>$id));
		return "ok";
	}
	
	function updateUraOmejitve($id, $omejitve, $db=null) {
		$isTransaction = true;
		if ($db==null) {
			$db = new DbConfig;
			$db->beginTransaction();
			$isTransaction = false;
		}

		if (is_array($omejitve))
		foreach ($omejitve as $vnos) {
			$db->updateOrInsertNoCommit("ura_omejitev", array("omejitev"=>$vnos["omejitev"]), array("vadba_id"=>$id, "prostor_id"=>$vnos["prostor"]));			
		}
		if (!$isTransaction) $db->commitTransaction();
	}
}
?>